//
//  UITextFieldStaticText.h
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 15/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextFieldStaticText : UITextField

@end
