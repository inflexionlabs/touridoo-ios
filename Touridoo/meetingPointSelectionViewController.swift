//
//  meetingPointSelectionViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 20/10/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit
import MapKit

class meetingPointSelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, CLLocationManagerDelegate
{
    @IBOutlet weak var titleText : UILabel?
    @IBOutlet weak var NextButton : UIButton?
    @IBOutlet weak var tableMeetings : UITableView?
    @IBOutlet weak var containerView : UIView?
    @IBOutlet weak var weAreWeGoing : UILabel?

    var currentTour : TourDescriptionModel?
    var UserDataSet : Bool = false
    var newAccountData : NewAccountModel?
    var selectedID : Int = -1;
    
    @IBOutlet weak var mapView : MKMapView?
    var locationManager = CLLocationManager()
    var onRequest : Bool = false
    var MapInitialConfiguration : Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        self.tableMeetings?.delegate = self
        self.tableMeetings?.dataSource = self
        
        containerView?.backgroundColor = UIColor.clear
        containerView?.layer.shadowColor = UIColor.gray.cgColor
        containerView?.layer.shadowOffset = CGSize(width: 0, height: -2)
        containerView?.layer.shadowOpacity = 0.10
        containerView?.layer.shadowRadius = 1
        
        tableMeetings?.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
        titleText?.text = NSLocalizedString("Meeting point", comment: "Meeting point")
        NextButton?.setTitle(NSLocalizedString("Choose a payment method", comment: "Choose a payment method"), for: UIControlState.normal)
        weAreWeGoing?.text = NSLocalizedString("Where are we going for you?", comment: "Where are we going for you?")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if onRequest == true {
            return
        }
        
        onRequest = true
        
        //Check for Location Services
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            
        }
   }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell : meetingPointCellTableViewCell = tableView.cellForRow(at: indexPath) as! meetingPointCellTableViewCell
        Tools.doPushEffect(viewEffect: cell.imageM!, duration: 0.30, scale: 0.90)
        Tools.doPushEffect(viewEffect: cell.title!, duration: 0.30, scale: 0.98)
        Tools.doPushEffect(viewEffect: cell.subtitle!, duration: 0.30, scale: 0.98)
        
        
        //Clean Array
        for integer in 0...((currentTour?.meetingPoints?.count)! - 1)
        {
            let meeting : MeetingPointsModel = (currentTour?.meetingPoints![integer])!
            meeting.isSelected = false
        }
        
        //Clean map
        let allAnnotations = self.mapView?.annotations
        self.mapView?.removeAnnotations(allAnnotations!)
        
        if (indexPath.row  != ((currentTour?.meetingPoints?.count)!))
        {
            let meetingPoint : MeetingPointsModel = (self.currentTour?.meetingPoints![indexPath.row])!;
            meetingPoint.isSelected = true
            self.newAccountData?.bookingData?.meetingPointId = meetingPoint.id
            self.selectedID = meetingPoint.id
            
            let dispatchTime = DispatchTime.now() + 0.10;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.updateMap()
                self.tableMeetings?.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (currentTour?.meetingPoints?.count)! + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : meetingPointCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "meeting_point_cell") as! meetingPointCellTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if (indexPath.row  == ((currentTour?.meetingPoints?.count)!))
        {
            cell.title?.text = ""
            cell.subtitle?.text = ""
            cell.imageM?.isHidden = true
        }
        else
        {
            let meetingPoint : MeetingPointsModel = (currentTour?.meetingPoints![indexPath.row])!;
            let name : String = meetingPoint.name!;
            let descrip : String = meetingPoint.descriptionText!;
            
            cell.title?.text = name
            cell.subtitle?.text = descrip
            cell.imageM?.isHidden = false
            
            if meetingPoint.isSelected == false
            {
                cell.imageM?.image = UIImage.init(named: "marker_green.png")
            }
            else
            {
                cell.imageM?.image = UIImage.init(named: "marker_red_map.png")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    @IBAction func NextAction()
    {
        if selectedID == -1
        {
            Tools.showAlertView(withText: NSLocalizedString("Please select your meeting point", comment: "Please select your meeting point"))
            return
        }
        
        let dispatchTime = DispatchTime.now() + 0.50;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let ReserveController : PaymentMethodViewController  = self.storyboard!.instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
            ReserveController.currentTour = self.currentTour
            ReserveController.newAccountData = self.newAccountData
            self.navigationController?.pushViewController(ReserveController, animated: true)
        }
    }
    
    @IBAction func closeView()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        print("changue status")
        
        if status == CLAuthorizationStatus.authorizedWhenInUse
        {
            if mapView != nil
            {
                mapView?.delegate = self
                mapView?.isUserInteractionEnabled = false
                
                if MapInitialConfiguration == false
                {
                    MapInitialConfiguration = true
                    
                    let dispatchTime = DispatchTime.now() + 0.30;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                    {
                        self.SetMeetingPointLocations()
                    }
                }
            }
        }
        
    }
    
    func  SetMeetingPointLocations()
    {
        if mapView == nil
        {
            return
        }
        updateMap()
        
    }
    
    func updateMap()
    {
        if currentTour?.meetingPoints != nil
        {
            if currentTour?.meetingPoints?.count != 0
            {
                var locationDriver : CLLocationCoordinate2D = CLLocationCoordinate2DMake(0, 0)
                
                for integer in 0...((currentTour?.meetingPoints?.count)! - 1)
                {
                    let meeting : MeetingPointsModel = (currentTour?.meetingPoints![integer])!
                    let latitude : CGFloat = CGFloat(meeting.latitude!)
                    let longitude : CGFloat = CGFloat(meeting.longitude!)
                    
                    locationDriver = CLLocationCoordinate2DMake(CLLocationDegrees(latitude),
                                                                CLLocationDegrees(longitude))
                    
                    
                    var imageMarker = UIImage.init(named: "marker_green.png")
                    
                    if meeting.isSelected == true {
                        imageMarker = UIImage.init(named: "marker_red_map.png")
                    }
                    
                    let driverIndicator = AnnotationMark(title: meeting.name!, coordinate: locationDriver, subtitle: "", image: imageMarker)
                    
                    
                    self.mapView?.addAnnotation(driverIndicator);
                }
                
                let viewRegion : MKCoordinateRegion = MKCoordinateRegion.init(center: locationDriver,
                                                                              span: MKCoordinateSpan.init(latitudeDelta: 0.035,
                                                                                                          longitudeDelta: 0.035))
                let adjustedRegion : MKCoordinateRegion = (self.mapView?.regionThatFits(viewRegion))!
                
                if adjustedRegion.span.longitudeDelta != 0 && adjustedRegion.span.latitudeDelta != 0
                {
                    self.mapView?.setRegion(adjustedRegion, animated: true)
                    self.mapView?.isUserInteractionEnabled = true
                }
               
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is AnnotationMark) {
            return nil
        }
        
        let reuseId = "test"
        
        var annotationView : MKAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        
        if annotationView == nil
        {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            annotationView?.canShowCallout = true
        }
        else {
            annotationView?.annotation = annotation
        }
        
        let currentAnno = annotation as! AnnotationMark
        annotationView?.image = currentAnno.image
        
        return annotationView
    }
    

}
