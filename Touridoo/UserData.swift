//
//  UserData.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 13/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class UserData: NSObject
{
    var username : String?
    var name : String?
    var lastname : String?
    var lastname_1 : String?
    var lastname_2 : String?
    var email : String?
    var pic_url : String?
    var phone_number : String?
    var phone_prefix : String?
}
