//
//  onTourStepsViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 16/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class onTourStepsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ResponseServicesProtocol, UIAlertViewDelegate
{
    
    @IBOutlet var tablePoint : UITableView!
    var pointsOfInteresArray: NSMutableArray = NSMutableArray.init()
    var indexToScroll : Int = 0
    var selectedIndexPath : IndexPath = IndexPath.init(row: 0, section: 0)
    var oneScrollToCurrenPoint = false
    
    @IBOutlet weak var duration : UILabel?
    @IBOutlet weak var titleLable : UILabel?
    @IBOutlet weak var descriptionlabel: UILabel?
    @IBOutlet weak var datelabel : UILabel?
    
    var clockFirstTime : Bool = false
    var timerOnTour : Timer?
    var currentIndex : Int?
    var currentPointText : String = ""
    var currentPointDuration : String = ""
    var indexStepOnWay : Int = -1
    
    var downloadImage : Bool = false
    
    @IBOutlet weak var driverName : UILabel?
    @IBOutlet weak var vehiculeName : UILabel?
    @IBOutlet weak var imageDriver : UIImageView?
    @IBOutlet weak var bookingNumber : UILabel?
    @IBOutlet weak var imageVehicle : UIImageView?
    @IBOutlet weak var carNumber : UILabel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tablePoint?.delegate = self
        tablePoint?.dataSource = self
        
        imageDriver?.layer.cornerRadius = (imageDriver?.layer.frame.size.height)! / 2;
        imageDriver?.layer.masksToBounds = true
        
        imageVehicle?.layer.cornerRadius = (imageVehicle?.layer.frame.size.height)! / 2;
        imageVehicle?.layer.masksToBounds = true
        
        let session = sessionData.shared
        
        carNumber?.text = ""
        
        if session.currentTour != nil
        {
            driverName?.text = ""
            
            if session.currentTour?.tourRun?.guide?.name != nil
            {
                driverName?.text = (session.currentTour?.tourRun?.guide?.name)! + " " + (session.currentTour?.tourRun?.guide?.last_name)!
                driverName?.adjustsFontSizeToFitWidth = true
            }
            
            
            if session.currentTour?.booking?.booking_code != nil
            {
                let upperCase = session.currentTour?.booking?.booking_code?.uppercased()
                bookingNumber?.text = upperCase
            }
            
            vehiculeName?.text = ""
            
            if session.currentTour?.tourRun?.car?.brand != nil
            {
                vehiculeName?.text = (session.currentTour?.tourRun?.car?.brand)! + "-" + (session.currentTour?.tourRun?.car?.subbrand)!
            }
            
            if session.currentTour?.booking?.tour_date != nil
            {
                let stingDate : String = (session.currentTour?.booking?.tour_date!)!
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                
                let dateObj = dateFormatter.date(from: stingDate)
                
                var hourString : String?
                dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm"
                hourString = dateFormatter.string(from: dateObj!)
                
                datelabel?.text = hourString
                
            }
            
            if session.currentTour?.name != nil
            {
                titleLable?.text = session.currentTour?.name
                titleLable?.adjustsFontSizeToFitWidth = true
                descriptionlabel?.text = session.currentTour?.name
            }
            
            if session.currentTour?.duration_in_hours != nil {
                
                let num : Int = (session.currentTour?.duration_in_hours)!
                let hours : String = String(num)
                duration?.text = hours + " " + NSLocalizedString("hours", comment: "hours")
            }
           
            updateTable()

        }

        tablePoint.reloadData()
       print(pointsOfInteresArray)
    }
    
    func updateTable()
    {
        let session = sessionData.shared
        
        if session.currentTour != nil
        {
            if session.currentTour?.booking?.tour_date != nil
            {
                pointsOfInteresArray.removeAllObjects()
                
                let array : NSArray = (session.currentTour?.tourRun?.tour_steps)!
                
                var superIndex = -1
                
                for index in  0...(array.count - 1)
                {
                    let step : TourStepModel = array.object(at: index) as! TourStepModel
                    
                    if step.is_current_step == true
                    {
                        superIndex = index
                    }
                }
                
                
                let arrayvisited = NSMutableArray.init()
                for index in  0...(array.count - 1)
                {
                    let step : TourStepModel = array.object(at: index) as! TourStepModel
                    
                    if step.is_current_step == true
                    {
                        currentPointText = step.step_name!
                        let time : Int = step.duration_in_minutes!
                        currentPointDuration = String(time)
                        
                        if step.step_type == 3
                        {
                            indexStepOnWay = step.step_index!
                        }
                        else
                        {
                            indexStepOnWay = -1
                        }
                    }
                    
                    if step.step_type == 4 //|| step.step_type == 3
                    {
                        var backTotheFutureStep = false
                        
                        if superIndex != -1 {
                            
                            if index < superIndex
                            {
                                backTotheFutureStep = true
                            }
                        }
                        
                        print("current " + step.step_name!)
                        let minutes: Int = step.duration_in_minutes!
                        let stringMinutes = NSLocalizedString("Duration:", comment: "Duration:") + " " + String(minutes) + " " + NSLocalizedString("minutes", comment: "minutes")
                        
                        let dicData = PointOfInteresModel.init(title: step.step_name!,
                                                               stay: stringMinutes ,
                                                               inLocation: step.is_current_step!,
                                                               step_description: step.step_description,
                                                               step_essentials: step.step_essentials,
                                                               step_suggestions: step.step_suggestions,
                                                               step_security: step.step_security,
                                                               step_food : step.step_food,
                                                               step_type: step.step_type,
                                                               step_ended: backTotheFutureStep,
                                                               realIndex: step.step_index,
                                                               duration_in_minutes: step.duration_in_minutes,
                                                               HasWiFi: step.HasWiFi,
                                                               HasBathrooms: step.HasBathrooms,
                                                               HasCoffeeShop: step.HasCoffeeShop,
                                                               HasFood: step.HasFood,
                                                               HasGiftShop: step.HasGiftShop,
                                                               HasWardrobe: step.HasWardrobe,
                                                               HasAccessibility: step.HasAccessibility,
                                                               HasCashMachine: step.HasAccessibility,
                                                               ImageURL: step.ImageURL)
                        
                        if backTotheFutureStep == false
                        {
                            pointsOfInteresArray.add(dicData)
                        }
                        else
                        {
                            arrayvisited.add(dicData)
                        }
                    }
                }
                
                if arrayvisited.count != 0
                {
                    for index in  0...(arrayvisited.count - 1)
                    {
                        pointsOfInteresArray.add(arrayvisited.object(at: index))
                    }
                }
         
            }
            
            let dispatchTime = DispatchTime.now() + 0.05;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.tablePoint.reloadData()
            }
        }
 
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        print("will appear")
        
        let session = sessionData.shared
        
        if session.currentTour != nil
        {
            if session.driverImage != nil
            {
                imageDriver?.image = session.driverImage
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
     
        Tools.hiddenBurgerButton(hidden: false)
        
        timerOnTour = Timer.scheduledTimer(timeInterval: TimeInterval(3.0), target: self, selector: #selector(updateData), userInfo: nil, repeats: true)
        timerOnTour?.fire()
        
        if oneScrollToCurrenPoint == true {
            return
        }
        
        oneScrollToCurrenPoint = true
        
        self.setMarketInView()
        
        currentIndex = UserDefaults.standard.integer(forKey: "tourState")
    
        let session = sessionData.shared
        
        if session.currentTour != nil
        {
            if session.driverImage != nil
            {
                imageDriver?.image = session.driverImage
            }
            else
            {
                if session.currentTour?.tourRun?.guide?.PicProfile != nil
                {
                    let urlPic : String = (session.currentTour?.tourRun?.guide?.PicProfile)!
                    
                    if downloadImage == false
                    {
                        downloadImage = true
                        
                        let urlToDownload : URL = URL.init(string: urlPic)!
                        
                        do
                        {
                            let RealImage = try  Data.init(contentsOf: urlToDownload)
                            
                            if RealImage.count != 0
                            {
                                imageDriver?.image = UIImage.init(data: RealImage)
                                session.driverImage = UIImage.init(data: RealImage)
                            }
                        }
                        catch
                        {
                            print(error)
                        }
                    }
                }
            }
            
            if session.currentTour?.tourRun?.guide?.plates != nil
            {
                if carNumber?.text == ""
                {
                    carNumber?.text = session.currentTour?.tourRun?.guide?.plates
                }
            }
            
            let image_url : String? = session.currentTour?.image_url
            
            var RealImage : Data?
            
            if image_url != nil
            {
                let urlToDownload : URL = URL.init(string: image_url!)!
                do {
                    RealImage = try  Data.init(contentsOf: urlToDownload)
                } catch {
                    print(error)
                }
                
                if RealImage != nil
                {
                    if RealImage?.count != 0
                    {
                        session.currentTourImage = RealImage
                    }
                }
            }
        }
        
    }
    
    func updateData()
    {
        print("Start update data")
        
        if clockFirstTime == true
        {
            let code : String? = UserDefaults.standard.object(forKey: "bookingcode") as? String
            
            if (UserDefaults.standard.bool(forKey: "bookingcode_mode") == true) && (code != nil)
            {
                let Service = TouridooServices.init(delegate: self)
                Service.bookingCode(code: code!, deviceID: "")
            }
            else
            {
                let Service = TouridooServices.init(delegate: self)
                Service.GetMyTours()
            }
            
        }
        
        if clockFirstTime == false
        {
            clockFirstTime = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        print("kill clock!")
        timerOnTour?.invalidate()
        timerOnTour = nil
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.GET_MY_TOURS || name == ServiceName.BOOKING_CODE
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                    loadTourData.updateData(content: content)
                    loadTourData.updateFlags()
                    
                    self.updateTable()
                    
                    let newIndex = UserDefaults.standard.integer(forKey: "tourState")
                    
                    if newIndex != currentIndex
                    {
                        let dispatchTime = DispatchTime.now() + 0.30;
                        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                        {
                            loadTourData.updateUI()
                        }
                    }
                    
                }
            }
            
        }
    }
    
    func onError(Error : String, name : ServiceName)
    {
        
    }
    
    func setMarketInView()
    {
        for integer in 0...(pointsOfInteresArray.count - 1)
        {
            let dicData : PointOfInteresModel = pointsOfInteresArray.object(at:integer) as! PointOfInteresModel
            if dicData.inLocation == true
            {
                indexToScroll = integer
            }
        }
        
        if indexToScroll != 0
        {
            let index = IndexPath(row: indexToScroll + 1, section: 0)
            tablePoint.scrollToRow(at: index, at: UITableViewScrollPosition.middle, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row != 0
        {
            let dicData : PointOfInteresModel = pointsOfInteresArray.object(at: (indexPath.row - 1)) as! PointOfInteresModel
            if dicData.step_type != 3
            {
                selectedIndexPath = IndexPath(row: (indexPath.row - 1), section: 0)
                selectDescription()
            }
        }
    }
    
    func selectDescription()
    {
        let scrollcomponent : PointOfInteresScrollViewController  = self.storyboard!.instantiateViewController(withIdentifier: "PointOfInteresScrollViewController") as! PointOfInteresScrollViewController
        scrollcomponent.currentPointOfInterest = pointsOfInteresArray.object(at: selectedIndexPath.row) as? PointOfInteresModel
        scrollcomponent.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        let dicData : PointOfInteresModel = pointsOfInteresArray.object(at: (selectedIndexPath.row)) as! PointOfInteresModel
        let arrayServices : NSMutableArray = NSMutableArray.init()
        
        if dicData.HasWiFi! {arrayServices.add("wifi")}
        if dicData.HasBathrooms! {arrayServices.add("bathroom")}
        if dicData.HasCoffeeShop! {arrayServices.add("coffee")}
        if dicData.HasGiftShop! {arrayServices.add("gifts")}
        if dicData.HasFood! {arrayServices.add("food")}
        if dicData.HasWardrobe! {arrayServices.add("wardrobe")}
        if dicData.HasAccessibility! {arrayServices.add("Accessibility")}
        if dicData.HasCashMachine! {arrayServices.add("ATM")}
        
        scrollcomponent.servicesArray = arrayServices
        
        self.present(scrollcomponent, animated: true, completion: nil)
    }
    
    @IBAction func contactGuide()
    {
        print("Help me please! I cant find my mexican sombrero!")
        
        let session = sessionData.shared
        var phoneNumber : String = (session.currentTour?.tourRun?.guide?.phone_number)!
        phoneNumber = phoneNumber.replacingOccurrences(of: "+52 ", with: "")
        let number = URL(string: "tel://" + phoneNumber)
        UIApplication.shared.open(number!)
        /*
        let AlertView = UIAlertController(title: NSLocalizedString("Call your host", comment: "Call your host"),
                                          message: NSLocalizedString("Do you want to make a phone call to your host?", comment: "Do you want to make a phone call to your host?"),
                                          preferredStyle: UIAlertControllerStyle.alert)
        
        AlertView.addAction(UIAlertAction(title: NSLocalizedString("Call", comment: "Call"),
                                          style: UIAlertActionStyle.default,
                                          handler:callAction))
            
        AlertView.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"),
                                          style: UIAlertActionStyle.cancel,
                                          handler: cancelAction))
            
        self.present(AlertView, animated: true, completion: nil)
 */
    }
    
    func callAction(alert: UIAlertAction!)
    {
        
        
    }
    
    func cancelAction(alert: UIAlertAction!) {
        print("Cancel")
    }
    
   
    
    //TableView methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            //clear cell
            let cell : OnWayCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "clear_cell") as! OnWayCellTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.contentView.backgroundColor = Tools.colorAppSecondary()
            
            let superText = NSMutableAttributedString()
            var currentWay : NSMutableAttributedString?
            
            if indexStepOnWay == -1
            {
                currentWay = NSMutableAttributedString(string: NSLocalizedString("You are at: ", comment: "You are at: "),
                                          attributes: [NSFontAttributeName:Tools.fontAppRegular(withSize: 13)])
            }
            else
            {
                currentWay = NSMutableAttributedString(string: NSLocalizedString("On way to: ", comment: "On way to: "),
                                                       attributes: [NSFontAttributeName:Tools.fontAppRegular(withSize: 13)])
            }
            
            superText.append(currentWay!)
            
            let pointText = NSMutableAttributedString(string: currentPointText, attributes: [NSFontAttributeName:Tools.fontAppBold(withSize: 16)])
            
            superText.append(pointText)

            cell.titleLabel?.attributedText = superText
            
            if (currentPointText == "Iniciando" || currentPointText == "Starting" || currentPointText == "Regreso" || currentPointText == "Returning")
            {
                cell.titleLabel?.text = currentPointText
            }
            
            cell.titleLabel?.adjustsFontSizeToFitWidth = true
            
            return cell
        }
        else
        {
            let dicData : PointOfInteresModel = pointsOfInteresArray.object(at: (indexPath.row - 1)) as! PointOfInteresModel
            let title : String = dicData.title
            let stay : String = dicData.stay
            let inLocation : Bool = dicData.inLocation
            
            //Point Visited
            if dicData.step_ended == true
            {
                let cell : PointOfInterestVisitedTableViewCell = tableView.dequeueReusableCell(withIdentifier: "pointsOfInterestVisited_cell") as! PointOfInterestVisitedTableViewCell
                
                cell.titleLabel?.text = title
                cell.titleLabel?.adjustsFontSizeToFitWidth = true
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            
            
            if dicData.step_type == 3
            {
                let cell : PointsOfInterestOnWayTableViewCell = tableView.dequeueReusableCell(withIdentifier: "pointsOfInterestOnWay_cell") as! PointsOfInterestOnWayTableViewCell
                
                cell.titleLabel?.text = title
                cell.titleLabel?.adjustsFontSizeToFitWidth = true
                cell.statyLabel?.text = stay
                cell.iconDetail?.isHidden = true
                cell.iconOnWay?.isHidden = !inLocation
                cell.detailLabel?.isHidden = !inLocation
                cell.timeLabel?.isHidden = !inLocation
               
                
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            else
            {
                if indexStepOnWay != -1
                {
                    let currentIndex = dicData.realIndex
                    let nextPathOfStepOnWay = (indexStepOnWay + 1)
                    
                    if currentIndex == nextPathOfStepOnWay
                    {
                        let cell : PointsOfInterestOnWayTableViewCell = tableView.dequeueReusableCell(withIdentifier: "pointsOfInterestOnWay_cell") as! PointsOfInterestOnWayTableViewCell
                        
                        cell.titleLabel?.text = title
                        cell.titleLabel?.adjustsFontSizeToFitWidth = true
                        cell.statyLabel?.text = stay
                        cell.iconDetail?.isHidden = false
                        cell.selectionStyle = UITableViewCellSelectionStyle.none
                        
                        let durationText = String(currentPointDuration) + ":00"
                        cell.timeLabel?.text = durationText
                        
                        if currentPointDuration == ""
                        {
                            cell.timeLabel?.text = ""
                        }
                        
                        let arrayServices : NSMutableArray = NSMutableArray.init()
                        
                        if dicData.HasWiFi! {arrayServices.add("wifi")}
                        if dicData.HasBathrooms! {arrayServices.add("bathroom")}
                        if dicData.HasCoffeeShop! {arrayServices.add("coffee")}
                        if dicData.HasGiftShop! {arrayServices.add("gifts")}
                        if dicData.HasFood! {arrayServices.add("food")}
                        if dicData.HasWardrobe! {arrayServices.add("wardrobe")}
                        if dicData.HasAccessibility! {arrayServices.add("Accessibility")}
                        if dicData.HasCashMachine! {arrayServices.add("ATM")}
                        
                        cell.setServicesIcon(services: arrayServices)
                        
                        return cell
                    }
                }
                
                
                if inLocation == false
                {
                    let cell : PointsOfInterestOnWayTableViewCell = tableView.dequeueReusableCell(withIdentifier: "pointsOfInterestOnWay_cell") as! PointsOfInterestOnWayTableViewCell
                    
                    cell.titleLabel?.text = title
                    cell.titleLabel?.adjustsFontSizeToFitWidth = true
                    cell.statyLabel?.text = stay
                    cell.iconDetail?.isHidden = false
                    
                    cell.iconOnWay?.isHidden = !inLocation
                    cell.detailLabel?.isHidden = !inLocation
                    cell.timeLabel?.isHidden = !inLocation
                    
                    let arrayServices : NSMutableArray = NSMutableArray.init()
                    if dicData.HasWiFi! {arrayServices.add("wifi")}
                    if dicData.HasBathrooms! {arrayServices.add("bathroom")}
                    if dicData.HasCoffeeShop! {arrayServices.add("coffee")}
                    if dicData.HasGiftShop! {arrayServices.add("gifts")}
                    if dicData.HasFood! {arrayServices.add("food")}
                    if dicData.HasWardrobe! {arrayServices.add("wardrobe")}
                    if dicData.HasAccessibility! {arrayServices.add("Accessibility")}
                    if dicData.HasCashMachine! {arrayServices.add("ATM")}
                    
                    cell.setServicesIcon(services: arrayServices)
            
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    return cell
                }
                else
                {
                    let cell : PointsOfInterestTableViewCell = tableView.dequeueReusableCell(withIdentifier: "pointsOfInterest_cell") as! PointsOfInterestTableViewCell
                    
                    cell.titleLabel?.text = title
                    cell.titleLabel?.adjustsFontSizeToFitWidth = true
                    cell.stepTime = dicData.duration_in_minutes!
                    //cell.iconDetail?.isHidden = false
                    
                    let arrayServices : NSMutableArray = NSMutableArray.init()
                    if dicData.HasWiFi! {arrayServices.add("wifi")}
                    if dicData.HasBathrooms! {arrayServices.add("bathroom")}
                    if dicData.HasCoffeeShop! {arrayServices.add("coffee")}
                    if dicData.HasGiftShop! {arrayServices.add("gifts")}
                    if dicData.HasFood! {arrayServices.add("food")}
                    if dicData.HasWardrobe! {arrayServices.add("wardrobe")}
                    if dicData.HasAccessibility! {arrayServices.add("Accessibility")}
                    if dicData.HasCashMachine! {arrayServices.add("ATM")}
                    
                    cell.setServicesIcon(services: arrayServices)
                    
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    return cell
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tablePoint.frame.size.width, height: 150))
        footer.backgroundColor = UIColor.clear
        return footer
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let header = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tablePoint.frame.size.width, height: 135))
        header.backgroundColor = UIColor.clear
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 135
    }
    
    @IBAction func openDetailsAndRecommendations()
    {
        let DetailsController : DetailsAndRecommendationsViewController  = self.storyboard!.instantiateViewController(withIdentifier: "DetailsAndRecommendationsViewController") as! DetailsAndRecommendationsViewController
        DetailsController.view.frame = (self.view.frame)
        //DetailsController.view.alpha = 0;
        Tools.PushViewCenter(View: DetailsController.view, Points: self.view.frame.size.width)
        
        self.view.addSubview(DetailsController.view)
        self.addChildViewController(DetailsController)
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                Tools.PullViewCenter(View: DetailsController.view, Points: self.view.frame.size.width)
                
        }, completion:
            { _ in
        })
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.row == 0
        {
            return 50
        }
        
        let dicData : PointOfInteresModel = pointsOfInteresArray.object(at: (indexPath.row - 1)) as! PointOfInteresModel
        
        if dicData.step_ended == true
        {
            return 50
        }
        else
        {
            return 100
        }
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointsOfInteresArray.count + 1
    }

}
