//
//  LoaderController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 30/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class LoaderController: UIViewController
{
    @IBOutlet weak var animateImage1 : UIView?
    @IBOutlet weak var animateImage2 : UIView?
    @IBOutlet weak var backgroundView : UIView?
    @IBOutlet weak var roundCentralView : UIView?
    @IBOutlet weak var textDescription : UILabel?
    
    let animationDuration : Double = 2.0;
    let maxAlpha : CGFloat = 0.70;

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    func setText(text : String){
        textDescription?.text = text;
    }

    override func viewWillAppear(_ animated: Bool) {
        
        roundCentralView?.layer.cornerRadius = 15;
        
        animateImage1?.layer.cornerRadius = (animateImage1?.frame.size.height)! / 2;
        animateImage2?.layer.cornerRadius = (animateImage2?.frame.size.height)! / 2;
        
        animateImage1?.alpha = 1;
        animateImage2?.alpha = 1;
        
        animateImage1?.transform = CGAffineTransform(scaleX: 0, y: 0);
        animateImage2?.transform = CGAffineTransform(scaleX: 0, y: 0);
        
        animateImage1?.backgroundColor = Tools.colorApp()
        animateImage2?.backgroundColor = Tools.colorApp()
    
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        firstAnimation()
        
        let dispatchTime = DispatchTime.now() + self.animationDuration / 5;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.secondAnimation()
        }

    }
    
    func firstAnimation()
    {
        UIView.animate(withDuration: self.animationDuration, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.animateImage1?.alpha = 0
                self.animateImage1?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                
        }, completion:{ _ in
            
            self.animateImage1?.alpha = 1
            self.animateImage1?.transform = CGAffineTransform(scaleX: 0, y: 0)
            
            self.firstAnimation()
        })
    }
    
    func secondAnimation()
    {
        UIView.animate(withDuration: self.animationDuration, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.animateImage2?.alpha = 0
                self.animateImage2?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                
        }, completion:{ _ in
            
            self.animateImage2?.alpha = 1
            self.animateImage2?.transform = CGAffineTransform(scaleX: 0, y: 0)
            
            self.secondAnimation()
            
        })
    }

}
