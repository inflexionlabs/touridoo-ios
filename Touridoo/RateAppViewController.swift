//
//  RateAppViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 06/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class RateAppViewController: UIViewController, UITextViewDelegate, ResponseServicesProtocol
{

    @IBOutlet weak var sendButton : UIButton?
    @IBOutlet weak var skipButton : UIButton?
    
    var offStar : UIImage = UIImage.init(named: "star_rate_off.png")!
    var onStar : UIImage = UIImage.init(named: "star_rate_on.png")!
    
    @IBOutlet weak var star1 : UIButton?
    @IBOutlet weak var star2 : UIButton?
    @IBOutlet weak var star3 : UIButton?
    @IBOutlet weak var star4 : UIButton?
    @IBOutlet weak var star5 : UIButton?
    @IBOutlet weak var commentsTextField : UITextView?
    @IBOutlet weak var scrollData  : UIScrollView?
    @IBOutlet weak var labelComments : UILabel?
    
    @IBOutlet weak var whatThinkApp : UILabel?
    @IBOutlet weak var howNowApp : UILabel?
    @IBOutlet weak var someone : UILabel?
    @IBOutlet weak var socialMedia : UILabel?
    @IBOutlet weak var internet : UILabel?
    @IBOutlet weak var hotel : UILabel?
    @IBOutlet weak var other : UILabel?
    @IBOutlet weak var whatLikeMost : UILabel?
    
    @IBOutlet weak var someoneButton : UIButton?
    @IBOutlet weak var socialMediaButton : UIButton?
    @IBOutlet weak var internetButton : UIButton?
    @IBOutlet weak var hotelButton : UIButton?
    @IBOutlet weak var otherButon : UIButton?
    
    var currentTour : TourDescriptionModel?
    
    var rate : Int = 0
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        
        commentsTextField?.delegate = self;
        
        commentsTextField?.layer.borderWidth = 0.25;
        commentsTextField?.layer.borderColor = UIColor.lightGray.cgColor
        commentsTextField?.layer.cornerRadius = 3;
        
        labelComments?.text = NSLocalizedString("Coments (optional) - 0/500", comment: "Coments (optional) - 0/500")
        whatThinkApp?.text = NSLocalizedString("What do you think of our app?", comment: "What do you think of our app?")
        howNowApp?.text = NSLocalizedString("How did you know about Touridoo?", comment: "How did you know about Touridoo?")
        someone?.text = NSLocalizedString("Someone recommended Touridoo to me", comment: "Someone recommended Touridoo to me")
        socialMedia?.text = NSLocalizedString("Social Media", comment: "Social Media")
        internet?.text = NSLocalizedString("Internet", comment: "Internet")
        hotel?.text = NSLocalizedString("Hotel or tourism magazines", comment: "Hotel or tourism magazines")
        other?.text = NSLocalizedString("Other...", comment: "Other...")
        whatLikeMost?.text = NSLocalizedString("What do you like most about Touridoo?", comment: "What do you like most about Touridoo?")
        
        var tap = UITapGestureRecognizer.init(target: self, action: #selector(selectSomeOne))
        someoneButton?.addGestureRecognizer(tap)
        someone?.addGestureRecognizer(tap)
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(selectSocialMedia))
        socialMediaButton?.addGestureRecognizer(tap)
        socialMedia?.addGestureRecognizer(tap)
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(selectInternet))
        internetButton?.addGestureRecognizer(tap)
        internet?.addGestureRecognizer(tap)
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(selectHotel))
        hotelButton?.addGestureRecognizer(tap)
        hotel?.addGestureRecognizer(tap)
        
        tap = UITapGestureRecognizer.init(target: self, action: #selector(selectOther))
        otherButon?.addGestureRecognizer(tap)
        other?.addGestureRecognizer(tap)
    }
    
    func selectSomeOne()
    {
        offAllButtons()
        someoneButton?.isSelected = true
    }
    
    func selectSocialMedia()
    {
        offAllButtons()
        socialMediaButton?.isSelected = true
    }
    
    func selectInternet()
    {
        offAllButtons()
        internetButton?.isSelected = true
    }
    
    func selectHotel()
    {
        offAllButtons()
        hotelButton?.isSelected = true
    }
    
    func selectOther()
    {
        offAllButtons()
        otherButon?.isSelected = true
    }
    
    func offAllButtons()
    {
        Tools.feedback()
        
        someoneButton?.isSelected = false
        socialMediaButton?.isSelected = false
        internetButton?.isSelected = false
        hotelButton?.isSelected = false
        otherButon?.isSelected = false
    }
    
    @IBAction func radioAction(radioButton : UIButton)
    {
        offAllButtons()
        radioButton.isSelected = true
    }

    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
        sendButton?.setTitle(NSLocalizedString("Send", comment:"Send"), for: UIControlState.normal)
        skipButton?.setTitle(NSLocalizedString("Skip", comment:"Skip"), for: UIControlState.normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let content : CGSize = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(260)))
        
        let newHeight : CGFloat = self.view.frame.size.height - CGFloat(66)
        
        let scrollFrameInStoryboar : CGRect = CGRect(x: (scrollData?.frame.origin.x)!,
                                                     y: (scrollData?.frame.origin.y)!,
                                                     width: (scrollData?.frame.size.width)!,
                                                     height: newHeight)
       
        scrollData?.frame = scrollFrameInStoryboar
        scrollData?.contentSize = content
        scrollData?.showsVerticalScrollIndicator = false
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hiddenKeyboard))
        self.scrollData?.addGestureRecognizer(tap)
        
        requestForNotifications()
    }
    
    func hiddenKeyboard()
    {
        commentsTextField?.resignFirstResponder()
    }
    
    func requestForNotifications()
    {
        let Service = TouridooServices.init(delegate: self)
        let deviceID : String? = UserDefaults.standard.object(forKey: "device_token") as? String
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        
        if (token != nil && deviceID != nil)
        {
            Service.RegisterForPushNotifications(deciveID: deviceID!)
        }
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.REGISTER_FOR_NOTIFICATIONS {
            
            UserDefaults.standard.set("true", forKey: "device_registered")
            return
        }
        
        if name == ServiceName.RATE_APP {
            skipAction()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let maxlenght : Int = 499;
        let textlenght = textView.text?.characters.count
        
        let newlenght = textlenght! + text.characters.count - range.length
        
        let chars : Int = newlenght
        let countLabel : String = NSLocalizedString("Coments (optional) - ", comment: "Coments (optional) - ") + String(chars) + "/500"
        labelComments?.text = countLabel
        
        return newlenght <= maxlenght
    }
    
    func onError(Error : String, name : ServiceName)
    {
        if name == ServiceName.RATE_APP {
            skipAction()
        }
    }
    
    @IBAction func skipAction()
    {
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            //Open MenuBarApp
            let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let mainMenu : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
            mainMenu.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(mainMenu, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func sendData()
    {
        print("Send data")
        
        if rate == 0
        {
            Tools.showAlertViewinModalViewController(withText: "Please select a rating", ModalViewController: self)
            return
        }
        
        let Service = TouridooServices.init(delegate: self)
        Service.rateApp(stars: rate, comment: (self.commentsTextField?.text)!, IdTour: (currentTour?.id)!)
        
    }
    
    @IBAction func starAction(starView : UIButton)
    {
        hiddenKeyboard()
        Tools.feedback()
    
        if starView == star1 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star2?.setBackgroundImage(offStar, for: UIControlState.normal)
            star3?.setBackgroundImage(offStar, for: UIControlState.normal)
            star4?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            rate = 1

        }
        
        if starView == star2 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star3?.setBackgroundImage(offStar, for: UIControlState.normal)
            star4?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            doRebound(viewEffect: star2!)
            
            rate = 2
        
        }
        
        if starView == star3 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star4?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            doRebound(viewEffect: star2!)
            doRebound(viewEffect: star3!)
            
            rate = 3

        }
        
        if starView == star4 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3?.setBackgroundImage(onStar, for: UIControlState.normal)
            star4?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star5?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            doRebound(viewEffect: star2!)
            doRebound(viewEffect: star3!)
            doRebound(viewEffect: star4!)
            
            rate = 4
          
        }
        
        if starView == star5 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3?.setBackgroundImage(onStar, for: UIControlState.normal)
            star4?.setBackgroundImage(onStar, for: UIControlState.normal)
            star5?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            doRebound(viewEffect: star2!)
            doRebound(viewEffect: star3!)
            doRebound(viewEffect: star4!)
            doRebound(viewEffect: star5!)
            
            rate = 5
        }
    }
    
    func doRebound(viewEffect : UIView)
    {
    
        UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                viewEffect.transform = CGAffineTransform(scaleX: 1.10, y: 1.10)
                
        }, completion:
            { _ in
                UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                    {
                        viewEffect.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion:nil)
                
        })
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }

}
