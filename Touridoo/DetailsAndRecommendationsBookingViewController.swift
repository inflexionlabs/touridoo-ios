//
//  DetailsAndRecommendationsBookingViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 18/08/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class DetailsAndRecommendationsBookingViewController: UIViewController {

    @IBOutlet weak var titleText : UILabel?
    @IBOutlet weak var scrollData  : UIScrollView?
    
    @IBOutlet weak var initWeatherView : UIView?
    @IBOutlet weak var finishWeatherView : UIView?
    
    @IBOutlet weak var textRecomendations : UITextView?
    @IBOutlet weak var textPick : UITextView?
    
    @IBOutlet weak var atBeLabel : UILabel?
    @IBOutlet weak var atEndLabel : UILabel?
    @IBOutlet weak var weatherLabel : UILabel?
    @IBOutlet weak var recommendationslabel : UILabel?
    
    @IBOutlet weak var initialWeather : UILabel?
    @IBOutlet weak var initialImage : UIImageView?
    
    @IBOutlet weak var finalWeather : UILabel?
    @IBOutlet weak var finalImage : UIImageView?
    
    var currentTour : Tour?
    
    var latitude = ""
    var longitude = ""
    var fahren : Bool = true
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        initWeatherView?.layer.cornerRadius = 5
        initWeatherView?.layer.borderColor = Tools.colorAppSecondary().cgColor
        initWeatherView?.layer.borderWidth = 1
        
        finishWeatherView?.layer.cornerRadius = 5
        finishWeatherView?.layer.borderColor = Tools.colorAppSecondary().cgColor
        finishWeatherView?.layer.borderWidth = 1
        
        let tapInitila = UITapGestureRecognizer.init(target: self, action: #selector(self.changeDegrees(sender:)))
        initWeatherView?.addGestureRecognizer(tapInitila)
        
        let tapFinal = UITapGestureRecognizer.init(target: self, action: #selector(self.changeDegrees(sender:)))
        finishWeatherView?.addGestureRecognizer(tapFinal)
        if Tools.iPhone4(){
            scrollData?.contentSize = CGSize(width: (scrollData?.frame.size.width)!, height: (scrollData?.frame.size.height)! + CGFloat(10))
            Tools.AddHeight(View: textRecomendations!, Points: 20)
            Tools.AddHeight(View: textPick!, Points: 20)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        textRecomendations?.text = NSLocalizedString("For daytime activities, we recommend that participants wear sports clothing, comfortable walking shoes, sun screen lotion, a hat or cap, and bring a photo or video camera.", comment: "For daytime activities, we recommend that participants wear sports clothing, comfortable walking shoes, sun screen lotion, a hat or cap, and bring a photo or video camera.")

        textPick?.text = NSLocalizedString("We will pick you up at your hotel lobby. You must be 15 minutes before the indicated time", comment: "We will pick you up at your hotel lobby. You must be 15 minutes before the indicated time")

        titleText?.text = NSLocalizedString("Details and recommendations", comment: "Details and recommendations")
        
        atBeLabel?.text = NSLocalizedString("At the beginning", comment: "At the beginning")
        atEndLabel?.text = NSLocalizedString("At the end", comment: "At the end")
        weatherLabel?.text = NSLocalizedString("The weather expected to be", comment: "The weather expected to be")
        
        recommendationslabel?.text = NSLocalizedString("Recommendations", comment: "Recommendations")
        
        Tools.hiddenBurgerButton(hidden: true)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        var latitude = ""
        var longitude = ""
        
        if currentTour != nil
        {
            if currentTour?.tourRun != nil
            {
                if currentTour?.tourRun?.tour_steps != nil
                {
                    let steps : NSArray = (currentTour?.tourRun?.tour_steps)!
                    
                    for index in 0...(steps.count - 1)
                    {
                        if latitude == ""
                        {
                            let data : TourStepModel = steps[index] as! TourStepModel;
                            if data.latitude != nil
                            {
                                if data.latitude != ""
                                {
                                    latitude = data.latitude!
                                }
                            }
                        }
                        
                        if longitude == ""
                        {
                            let data : TourStepModel = steps[index] as! TourStepModel;
                            if data.longitude != nil
                            {
                                if data.longitude != ""
                                {
                                    longitude = data.longitude!
                                }
                            }
                        }
                    }
                }
            }
        }
        
        let yahooW = yahooWeather.init()
        let weather : weatherModel = yahooW.getFromWoeid(self.currentTour?.woeid)
        print(weather.initialTemperature);
        
        initialImage?.image = weather.initialWeatherType
        finalImage?.image = weather.finalWeatherType
        
        initialWeather?.text = weather.initialTemperature + "° F"
        finalWeather?.text = weather.finalTemperature + "° F"
        
        var languageID : String = Locale.current.languageCode!
        languageID = languageID.uppercased()
        if languageID == "ES"
        {
            let initial : String = yahooW.toCelsius(weather.initialTemperature)
            let final : String = yahooW.toCelsius(weather.finalTemperature)
            initialWeather?.text = initial + "° C"
            finalWeather?.text = final + "° C"
        }

    }
    
    func changeDegrees(sender: UITapGestureRecognizer)
    {
        let yahooW = yahooWeather.init()
        let weather : weatherModel = yahooW.getFromWoeid(self.currentTour?.woeid)
        
        Tools.doRebound(viewEffect: sender.view!)
        
        if fahren == true
        {
            let initial : String = yahooW.toCelsius(weather.initialTemperature)
            let final : String = yahooW.toCelsius(weather.finalTemperature)
            initialWeather?.text = initial + "° C"
            finalWeather?.text = final + "° C"
        }
        else
        {
            initialImage?.image = weather.initialWeatherType
            finalImage?.image = weather.finalWeatherType
            
            initialWeather?.text = weather.initialTemperature + "° F"
            finalWeather?.text = weather.finalTemperature + "° F"
        }
        
        fahren = !fahren
    }
    
    @IBAction func closeView()
    {
        
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }

}
