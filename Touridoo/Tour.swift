//
//  Tour.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class Tour: NSObject
{
    var name : String?
    var descriptionTour : String?
    var image_url : String?
    var duration_in_hours : Int?
    var id : Int?
    var stars_average : Int?
    var image_footer_label : String?
    var details : String?
    var rendezvous_description : String?
    var cancelation_description : String?
    var rates_count : Int?
    var base10_stars_average : Int?
    var rates : String?
    var max_occupants : Int?
    var tourRun : TourRunModel?
    var booking : BookingModel?
    var woeid : String?
    var singlePrice : Float?
    var prices : [PriceModel]?
    
    init(name : String?,
         descriptionTour : String?,
         image_url : String?,
         duration_in_hours : Int?,
         id : Int?,
         stars_average : Int?,
         image_footer_label : String?,
         details : String?,
         rendezvous_description : String?,
         cancelation_description : String?,
         rates_count : Int?,
         base10_stars_average : Int?,
         rates : String?,
         max_occupants : Int?,
         TourRun : TourRunModel?,
         booking : BookingModel?,
         woeid : String?,
         singlePrice : Float?,
         prices : [PriceModel]?)
    {
        self.name = name
        self.descriptionTour = descriptionTour
        self.image_url = image_url
        self.singlePrice = singlePrice
        self.duration_in_hours = duration_in_hours
        self.id = id
        self.stars_average = stars_average
        self.image_footer_label = image_footer_label
        self.details = details
        self.rendezvous_description = rendezvous_description
        self.cancelation_description = cancelation_description
        self.rates_count = rates_count
        self.base10_stars_average = base10_stars_average
        self.rates = rates
        self.max_occupants = max_occupants
        self.tourRun = TourRun
        self.booking = booking
        self.woeid = woeid
        self.prices = prices
    }

}
