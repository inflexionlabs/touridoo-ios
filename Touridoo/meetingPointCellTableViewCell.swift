//
//  meetingPointCellTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 20/10/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class meetingPointCellTableViewCell: UITableViewCell {

    @IBOutlet weak var title : UILabel?
    @IBOutlet weak var subtitle : UILabel?
    @IBOutlet weak var imageM : UIImageView?
    @IBOutlet weak var imageDetail : UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
