//
//  MeetingPointTourViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 02/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit
import MapKit

class MeetingPointTourViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate
{
    @IBOutlet weak var mapView : MKMapView?
    var locationManager = CLLocationManager()
    var onRequest : Bool = false
    var tourDescription : TourDescriptionModel?
    var MapInitialConfiguration : Bool = false
    
    @IBOutlet weak var textviewDescription : UITextView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
        textviewDescription?.text = NSLocalizedString("We suggest you arrive 15 minutes earlier. Our team will wait for you only 10 minutes after the stablished departure time. Once the trip is on route you will be able to return once it ends", comment: "We suggest you arrive 15 minutes earlier. Our team will wait for you only 10 minutes after the stablished departure time. Once the trip is on route you will be able to return once it ends")
    }
    
    override func viewDidAppear(_ animated: Bool)
    {

    }
    
    func updateData()
    {
         //requestForMap()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        print("changue status")
        
        if status == CLAuthorizationStatus.authorizedWhenInUse
        {
            if mapView != nil
            {
                mapView?.delegate = self
                mapView?.isUserInteractionEnabled = false
                //mapView?.isScrollEnabled = false
      
                if MapInitialConfiguration == false
                {
                    MapInitialConfiguration = true
                    
                    let dispatchTime = DispatchTime.now() + 0.30;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                    {
                        self.SetMeetingPointLocations()
                    }
                }
            }
        }
        
    }
    
    func  SetMeetingPointLocations()
    {
        if mapView == nil
        {
            return
        }
        
        if tourDescription?.meetingPoints != nil
        {
            if tourDescription?.meetingPoints?.count != 0
            {
                var locationDriver : CLLocationCoordinate2D = CLLocationCoordinate2DMake(0, 0)
                
                for integer in 0...((tourDescription?.meetingPoints?.count)! - 1)
                {
                    let meeting : MeetingPointsModel = (tourDescription?.meetingPoints![integer])!
                    
                   if meeting.latitude != nil && meeting.longitude != nil
                   {
                    
                    let latitude : CGFloat = CGFloat(meeting.latitude!)
                    let longitude : CGFloat = CGFloat(meeting.longitude!)
                    
                    if CLLocationCoordinate2DIsValid(locationDriver)
                    {
                        locationDriver = CLLocationCoordinate2DMake(CLLocationDegrees(latitude),
                                                                    CLLocationDegrees(longitude))
                        let driverIndicator = AnnotationMark(title: meeting.name!, coordinate: locationDriver, subtitle: "",image: UIImage.init(named: "marker_green.png"))
                        
                        self.mapView?.addAnnotation(driverIndicator);
                    }
                    
                    }
                    
                }
                
                
                if CLLocationCoordinate2DIsValid(locationDriver)
                {
                    let viewRegion : MKCoordinateRegion = MKCoordinateRegion.init(center: locationDriver,
                                                                                  span: MKCoordinateSpan.init(latitudeDelta: 0.035,
                                                                                                              longitudeDelta: 0.035))
                    let adjustedRegion : MKCoordinateRegion = (self.mapView?.regionThatFits(viewRegion))!
            
                    if adjustedRegion.span.longitudeDelta != 0 && adjustedRegion.span.latitudeDelta != 0
                    {
                        self.mapView?.setRegion(adjustedRegion, animated: true)
                        self.mapView?.isUserInteractionEnabled = true
                    }
                   
                }
                
            }
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is AnnotationMark) {
            return nil
        }
        
        let reuseId = "test"
        
        var annotationView : MKAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        
        if annotationView == nil
        {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            annotationView?.canShowCallout = true
        }
        else {
            annotationView?.annotation = annotation
        }
        
        let currentAnno = annotation as! AnnotationMark
        annotationView?.image = currentAnno.image
        
        return annotationView
    }
    
    func requestForMap()
    {
        if onRequest == true {
            return
        }
        
        onRequest = true
        
        //Check for Location Services
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            
        }
        
    }
    
    
    func mapViewWillStartLocatingUser(_ mapView: MKMapView)
    {
        /*
        print("star search user location")
        
        mapView.alpha = 1
        mapView.isUserInteractionEnabled = true
        mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true) */
    }


}
