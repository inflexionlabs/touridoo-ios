//
//  setupReminderViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 07/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit
import EventKit

class setupReminderViewController: UIViewController
{
    
    @IBOutlet weak var option1 : UILabel?
    @IBOutlet weak var option2 : UILabel?
    @IBOutlet weak var option3 : UILabel?
    @IBOutlet weak var option4 : UILabel?
    @IBOutlet weak var option5 : UILabel?
    @IBOutlet weak var option6 : UILabel?
    @IBOutlet weak var option7 : UILabel?
    //@IBOutlet weak var option8 : UILabel?
    @IBOutlet weak var viewOptions : UIView?
    
    @IBOutlet weak var skipButton : UIButton?
    @IBOutlet weak var headerView : UIView?
    
    var currentTour : Tour?
    
    var minutes : Int = 0
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        let pathTop = UIBezierPath(roundedRect:(headerView?.bounds)!,
                                   byRoundingCorners:[.topRight, .topLeft],
                                   cornerRadii: CGSize(width: 5.0, height:  5.0))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = pathTop.cgPath
        headerView?.layer.mask = maskLayer
        
        let pathBottom = UIBezierPath(roundedRect:(skipButton?.bounds)!,
                                      byRoundingCorners:[.bottomLeft, .bottomRight],
                                      cornerRadii: CGSize(width: 5.0, height:  5.0))
        
        let maskLayerB = CAShapeLayer()
        
        maskLayerB.path = pathBottom.cgPath
        skipButton?.layer.mask = maskLayerB
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption1))
        option1?.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption2))
        option2?.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption3))
        option3?.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption4))
        option4?.addGestureRecognizer(tap4)
        
        let tap5 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption5))
        option5?.addGestureRecognizer(tap5)
        
        let tap6 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption6))
        option6?.addGestureRecognizer(tap6)
        
        let tap7 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption7))
        option7?.addGestureRecognizer(tap7)
        
        let tap8 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption8))
        //option8?.addGestureRecognizer(tap8)
        
        option1?.text = NSLocalizedString("10 minutes before", comment: "10 minutes before")
        option2?.text = NSLocalizedString("20 minutes before", comment: "20 minutes before")
        option3?.text = NSLocalizedString("30 minutes before", comment: "30 minutes before")
        option4?.text = NSLocalizedString("1 hour before", comment: "1 hour before")
        option5?.text = NSLocalizedString("3 hour before", comment: "3 hour before")
        option6?.text = NSLocalizedString("12 hour before", comment: "12 hour before")
        option7?.text = NSLocalizedString("24 hour before", comment: "24 hour before")
     
        
        option1?.isUserInteractionEnabled = true
        option2?.isUserInteractionEnabled = true
        option3?.isUserInteractionEnabled = true
        option4?.isUserInteractionEnabled = true
        option5?.isUserInteractionEnabled = true
        option6?.isUserInteractionEnabled = true
        option7?.isUserInteractionEnabled = true
        //option8?.isUserInteractionEnabled = true
        
        option1?.tag = 1
        option2?.tag = 2
        option3?.tag = 3
        option4?.tag = 4
        option5?.tag = 5
        option6?.tag = 6
        option7?.tag = 7
        //option8?.tag = 8
        
    }
    
    @IBAction func cancelAction()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
                self.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                
        }, completion:
            { _ in
                
                self.view.removeFromSuperview()
        })
    }
    
    func selectAction(labelAction : UILabel)
    {
        UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                labelAction.alpha = 0.25
        }, completion:
            { _ in
                UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                    {
                        labelAction.alpha = 1
                }, completion:
                    { _ in
                        
                        self.showAlertWithTag(index: labelAction.tag, textReminder: (labelAction.text)!)
                })
        })
    }
    
    func tapOption1()
    {
        selectAction(labelAction: option1!)
    }
    func tapOption2()
    {
        selectAction(labelAction: option2!)
    }
    func tapOption3()
    {
        selectAction(labelAction: option3!)
    }
    func tapOption4()
    {
        selectAction(labelAction: option4!)
    }
    func tapOption5()
    {
       selectAction(labelAction: option5!)
    }
    func tapOption6()
    {
        selectAction(labelAction: option6!)
    }
    func tapOption7()
    {
        selectAction(labelAction: option7!)
    }
    func tapOption8()
    {
        //selectAction(labelAction: option8!)
    }
    
    func showAlertWithTag(index : Int, textReminder : String)
    {
        if index == 1 {
            minutes = 10
        }
        if index == 2 {
            minutes = 20
        }
        if index == 3 {
            minutes = 30
        }
        if index == 4 {
            minutes = 60
        }
        if index == 5 {
            minutes = 180
        }
        if index == 6 {
            minutes = 720
        }
        if index == 7 {
            minutes = 1440
        }
        if index == 8 {
            minutes = 1
        }
        
        print("minutes before: " + String(minutes))
        var textAlert = textReminder
        textAlert = textReminder.replacingOccurrences(of: " antes", with: "")
        var reminderText = NSLocalizedString("Want a reminder", comment: "Want a reminder") + " " + textAlert + NSLocalizedString(" earlier?", comment: " earlier?")
        reminderText = reminderText.replacingOccurrences(of: " before", with: "")
     
        let AlertView = UIAlertController(title: reminderText,//NSLocalizedString("Reminder", comment: "Reminder"),
                                          message: "",
                                          preferredStyle: UIAlertControllerStyle.alert)
        
        AlertView.addAction(UIAlertAction(title: NSLocalizedString("Set reminder", comment: "Set reminder"),
                                          style: UIAlertActionStyle.default,
                                          handler:callAction))
        
        AlertView.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"),
                                          style: UIAlertActionStyle.cancel,
                                          handler: cancelAction))
        
        self.present(AlertView, animated: true, completion: nil)
    }
    
    
    
    func callAction(alert: UIAlertAction!)
    {
        print("reminder")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let stringDate = currentTour?.booking?.tour_date
        let tourdate = dateFormatter.date(from: stringDate!)
        
        let seconds = minutes * 60
        let doubleseconds = Double(seconds)
        let reminderDate : Date = (tourdate?.addingTimeInterval(-doubleseconds))!
        
        let durationTour : Int = (currentTour?.duration_in_hours)!
        let minutesDurationTour = durationTour * 60
        let secondsDurationTour = minutesDurationTour * 60
        let doubleSecondsTour = Double(secondsDurationTour)
        
        //let finaldateTour : Date = (tourdate?.addingTimeInterval(doubleSecondsTour))!
        let finaldateTour : Date = reminderDate.addingTimeInterval(300)
        
        let StringTilte : String = (currentTour?.name)!
        
        Tools.showActivityView(inView: self)
        UIView.animate(withDuration: 0.30, animations: {self.viewOptions?.alpha = 0.75})
        
        let dispatchTime = DispatchTime.now() + 0.35;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.addEventToCalendar(title: StringTilte, description: " ", startDate: reminderDate, endDate: finaldateTour)
        }
        
    }
    
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil)
    {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.calendar = eventStore.defaultCalendarForNewEvents
                let alarm = EKAlarm(absoluteDate: startDate)
                event.addAlarm(alarm)
                
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    self.calendarPermission()
                    return
                }
                completion?(true, nil)
                self.done()
            } else {
                completion?(false, error as NSError?)
                self.calendarPermission()
            }
        })
    }
    
    func cancelAction(alert: UIAlertAction!) {
        print("Cancel")
    }
    
    func done()
    {
        let dispatchTime = DispatchTime.now() + 1.0;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            UIView.animate(withDuration: 0.30, animations: {self.viewOptions?.alpha = 1})
            Tools.hiddenActivityView(inView: self)
            
            let AlertView = UIAlertController(title: NSLocalizedString("Reminder added to calendar", comment: "Reminder added to calendar"),
                message: "",
                preferredStyle: UIAlertControllerStyle.alert)
            
            AlertView.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "OK"),
                                              style: UIAlertActionStyle.default,
                                              handler:self.doneCalendar))
            self.present(AlertView, animated: true, completion: nil)
        }
 
    }
    
    func doneCalendar(alert: UIAlertAction!)
    {
        self.cancelAction()
    }
    
    func errorCalendar(alert: UIAlertAction!)
    {
        
    }
    
    func calendarPermission()
    {
        UIView.animate(withDuration: 0.30, animations: {self.viewOptions?.alpha = 1})
        
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 1.0;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let AlertView = UIAlertController(title: NSLocalizedString("We need calendar permission to add a reminder", comment: "We need calendar permission to add a reminder"),
                                              message: NSLocalizedString("Settings > Touridoo > Calendars > On", comment: "Settings > Touridoo > Calendars > On"),
                                              preferredStyle: UIAlertControllerStyle.alert)
            
            AlertView.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "OK"),
                                              style: UIAlertActionStyle.default,
                                              handler:self.errorCalendar))
            self.present(AlertView, animated: true, completion: nil)
        }
    }
    
    
}
