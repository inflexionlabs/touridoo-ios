//
//  SelectPrefixView.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/08/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class SelectPrefixView: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var viewOptions : UIView?
    @IBOutlet weak var skipButton : UIButton?
    @IBOutlet weak var headerView : UIView?
    @IBOutlet weak var titleText : UILabel?
    @IBOutlet weak var tablePrefix : UITableView?
    @IBOutlet weak var darkView : UIView?
    
    var textFieldDelegate : UITextField?
    var selectedIndex : IndexPath = IndexPath.init(row: 0, section: 0)
    
    var prefixArray : Array = ["México (+52)","United States (+1)"]
    var singlePrefix : Array = ["+52","+1"]
    
    override func viewWillAppear(_ animated: Bool)
    {
        titleText?.text = NSLocalizedString("Select a phone prefix", comment: "Please select a phone prefix")
        skipButton?.setTitle(NSLocalizedString("Cancel", comment: "Cancel"), for: .normal)
        titleText?.adjustsFontSizeToFitWidth = true
        
        let pathTop = UIBezierPath(roundedRect:(headerView?.bounds)!,
                                   byRoundingCorners:[.topRight, .topLeft],
                                   cornerRadii: CGSize(width: 5.0, height:  5.0))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = pathTop.cgPath
        headerView?.layer.mask = maskLayer
        
        let pathBottom = UIBezierPath(roundedRect:(skipButton?.bounds)!,
                                      byRoundingCorners:[.bottomLeft, .bottomRight],
                                      cornerRadii: CGSize(width: 5.0, height:  5.0))
        
        let maskLayerB = CAShapeLayer()
        
        maskLayerB.path = pathBottom.cgPath
        skipButton?.layer.mask = maskLayerB
        
        self.tablePrefix?.delegate = self
        self.tablePrefix?.dataSource = self
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return prefixArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let cell : PrefixTableViewCell = tableView.cellForRow(at: indexPath) as! PrefixTableViewCell
        cell.setSelected(false, animated: true)
        
    }
    
    func taptapDelegateDontWorks(tap : UITapGestureRecognizer)
    {
        let index : IndexPath = IndexPath.init(row: (tap.view?.tag)!, section: 0)
        //let cell : PrefixTableViewCell = tablePrefix!.cellForRow(at: index) as! PrefixTableViewCell
        
        let dispatchTime = DispatchTime.now() + 0.05;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let selection : String = self.singlePrefix[index.row]
            
            if selection != ""
            {
                if self.textFieldDelegate != nil
                {
                    self.textFieldDelegate?.text = selection
                }
            }
            
            self.cancelAction()
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : PrefixTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell_prefix") as! PrefixTableViewCell
        cell.title?.text = prefixArray[indexPath.row]
        cell.title?.font = Tools.fontAppBold(withSize: 13)
        cell.title?.textColor = UIColor.darkGray
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.contentView.tag = indexPath.row
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.taptapDelegateDontWorks(tap:)))
        
        cell.contentView.addGestureRecognizer(tap)
        
        return cell
    }
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let tapToclose = UITapGestureRecognizer.init(target: self, action: #selector(cancelAction))
        viewOptions?.addGestureRecognizer(tapToclose)
        viewOptions?.isUserInteractionEnabled = true
    }
    
    @IBAction func cancelAction()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
                self.viewOptions?.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                
        }, completion:
            { _ in
                
                self.view.removeFromSuperview()
        })
    }

    

}
