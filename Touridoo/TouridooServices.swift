//
//  TouridooServices.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 29/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit


class TouridooServices: NSObject
{
    override init()
    {
        super.init()
    }

    //URL Prod
    static let URLService : String = "http://api.touridoo.com"
    
    //URL Development
    //static let URLService : String = "http://devapi.touridoo.com"
    
    //delegate is requiered
    weak var delegate : ResponseServicesProtocol?
    
    init(delegate: ResponseServicesProtocol)
    {
        self.delegate = delegate;
        super.init()
    }
    
    //delegate Methods (should be in the delegate ViewController)
    func onSucces(Result : String, name : ServiceName)
    {
        delegate?.onSucces(Result: Result, name: name);
    }
    
    func onError(Error : String, name : ServiceName)
    {
        delegate?.onError(Error: Error, name: name);
    }
    
    //Touridoo Methods
    
    func GetRegions()
    {
        let languageID = Locale.current.languageCode
        let URLRequest  = TouridooServices.URLService + "/api/v1/Regions?languageCode=" + languageID!
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.GET_REGIONS);
        Client.RequestGET(URLString: URLRequest)
    }
    
    func GetTours(regionID : String)
    {
        let URLRequest  = TouridooServices.URLService + "/api/v1/Tours?idRegion=" + regionID
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.GET_TOURS);
        Client.RequestGET(URLString: URLRequest)
    }
    
    func GetTourById(id : String)
    {
        let languageID = Locale.current.languageCode
        let URLRequest  = TouridooServices.URLService + "/api/v1/Tours/" + id + "?languageCode=" + languageID!
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.GET_TOUR_BY_ID);
        Client.RequestGET(URLString: URLRequest)
    }
    
    func GetToursDatesBy(id : String)
    {
        //let languageID = Locale.current.languageCode
        let URLRequest  = TouridooServices.URLService + "/api/v1/Tours/Dates/" + id
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.GET_TOUR_DATES);
        Client.RequestGET(URLString: URLRequest)
    }
    
    func Login(username : String, password : String)
    {
        let URLRequest  = TouridooServices.URLService + "/api/v1/Token"
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.LOGIN);
        Client.RequestForLogin(URLString: URLRequest, username: username, password: password)
    }
    
    func GetMyTours()
    {
        let languageID = Locale.current.languageCode
        //let URLRequest  = TouridooServices.URLService + "/api/v1/Tours/Mine?languageCode=" + languageID!
        let URLRequest  = TouridooServices.URLService + "/api/v1/Tours/Mine"
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.GET_MY_TOURS);
        Client.RequestGETWithAutorization(URLString: URLRequest)
    }
    
    func createNewUser(name: String,lastName: String,email: String,phonePrefix: String,PhoneNumber: String,rendezvous_point: String,rendezvous_address: String,hotelReservationName: String,comments: String,policy_accepted: Bool,recived_info: Bool,password: String)
    {
        let data = NSMutableDictionary()
        data.setValue(email, forKey: "username")
        data.setValue(name, forKey: "name")
        data.setValue(lastName, forKey: "last_name")
        data.setValue(email, forKey: "email")
        data.setValue(phonePrefix, forKey: "phone_prefix")
        data.setValue(PhoneNumber, forKey: "phone_number")
        data.setValue(rendezvous_point, forKey: "rendezvous_point_name")
        data.setValue(rendezvous_address, forKey: "rendezvous_point_address")
        data.setValue(hotelReservationName, forKey: "hotel_reservation_owner_name")
        data.setValue(policy_accepted, forKey: "policy_accepted")
        data.setValue(recived_info, forKey: "receive_info")
        data.setValue(password, forKey: "password")
        data.setValue(comments, forKey: "comments")
        data.setValue(1, forKey: "IdLanguage")
        
        let URLRequest  = TouridooServices.URLService + "/api/v1/Users"
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.CREATE_USER);
        Client.RequestPOST(Parameters:data, URLString: URLRequest)
    }
    
    func bookingTour(tourDate : String, idTour : Int, idPayment : Int, payment : String, total : Float, playerID : String, paymentToken : String, idMeetingPoint : Int, tickets : NSArray)
    {
        let data = NSMutableDictionary()
        data.setValue(tourDate, forKey: "tour_date")
        data.setValue(idTour, forKey: "id_tour")
        data.setValue(payment, forKey: "paymentID")
        data.setValue(total, forKey: "TotalPrice")
        data.setValue(playerID, forKey: "payerID")
        data.setValue(paymentToken, forKey: "paymentToken")
        data.setValue(idMeetingPoint, forKey: "id_meetingPoint")
        //data.setValue(ni, forKey: "customMeetingPoint")
        data.setValue(false, forKey: "isLanding")
        data.setValue(tickets, forKey: "tickets")
       
        let URLRequest  = TouridooServices.URLService + "/api/v1/Bookings"
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.BOOKING_TOUR);
        Client.RequestPOSTWithAutorization(Parameters:data, URLString: URLRequest)
    }
    
    func rateTour(comment : String, stars : Int, id_tour_runTour : Int, id_tour_rate_type : Int, idTour: Int)
    {
        let data = NSMutableDictionary()
        data.setValue(comment, forKey: "comment")
        data.setValue(stars, forKey: "stars")
        data.setValue(idTour, forKey: "id_tour")
        data.setValue(id_tour_rate_type, forKey: "id_tour_rate_type")
        data.setValue(id_tour_runTour, forKey: "id_tour_run")
        
        let URLRequest  = TouridooServices.URLService + "/api/v1/Ratings/Tour"
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.RATE_TOUR);
        Client.RequestPOSTWithAutorization(Parameters:data, URLString: URLRequest)
        
    }
    
    func GetTourRateCatalog()
    {
        let languageID = Locale.current.languageCode
        let URLRequest  = TouridooServices.URLService + "/api/v1/Ratings/TourRateTypesCatalog?languageCode=" + languageID!
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.RATE_CATALOG);
        Client.RequestGETWithAutorization(URLString: URLRequest)
    }
    
    func RegisterForPushNotifications(deciveID : String)
    {
        //let languageID = Locale.current.languageCode
        let URLRequest  = TouridooServices.URLService + "/api/v1/Devices"
        
        let data = NSMutableDictionary()
        data.setValue("ios", forKey: "type")
        data.setValue(deciveID, forKey: "identifier")
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.REGISTER_FOR_NOTIFICATIONS);
        Client.RequestPOSTWithAutorization(Parameters: data, URLString: URLRequest)
    }
    
    func checkTourAvailability(idtour : String, tourdate : String, ocupantsNumber : String)
    {
        //let languageID = Locale.current.languageCode
        let URLRequest  = TouridooServices.URLService + "/api/v1/Tours/DateAvailability/" + idtour + "?tourDate=" + tourdate + "&occupants=" + ocupantsNumber
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.CHECK_TOUR_AVAILABILITY);
        Client.RequestGET(URLString: URLRequest)
    }
    
    func changePassword(username : String, old_password : String, new_password : String)
    {
        //let languageID = Locale.current.languageCode
        let URLRequest  = TouridooServices.URLService + "/api/v1/Users/ChangePassword"
        
        let data = NSMutableDictionary()
        data.setValue(username, forKey: "username")
        data.setValue(old_password, forKey: "old_password")
        data.setValue(new_password, forKey: "new_password")
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.CHANGE_PASSWORD);
        Client.RequestPUT(Parameters: data, URLString: URLRequest)
    }
    
    func rateApp(stars : Int, comment : String, IdTour : Int)
    {
        let languageID = Locale.current.languageCode
        let URLRequest  = TouridooServices.URLService + "/api/v1/Ratings/AttentionReview"
        
        let data = NSMutableDictionary()
        data.setValue(stars, forKey: "stars")
        data.setValue(comment, forKey: "comment")
        data.setValue(IdTour, forKey: "id_tour")
        data.setValue(languageID, forKey: "language_code")
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.RATE_APP);
        Client.RequestPOSTWithAutorization(Parameters: data, URLString: URLRequest)
    }
    
    func updateTour(OldTourDate : String, OldIdTourRun : Int, NewTourDate : String, idTour : Int)
    {
        let languageID : String = Locale.current.languageCode!
        
        let data = NSMutableDictionary()
        data.setValue(OldTourDate, forKey: "OldTourDate")
        data.setValue(OldIdTourRun, forKey: "OldIdTourRun")
        data.setValue(NewTourDate, forKey: "NewTourDate")
        data.setValue(idTour, forKey: "idTour")
        data.setValue(languageID, forKey: "languagueCode")
        
        let URLRequest  = TouridooServices.URLService + "/api/v1/Tours/update"
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.UPDATE_TOUR);
        Client.RequestPOSTWithAutorization(Parameters: data, URLString: URLRequest)
    }
    
    func enableUser(mail : String)
    {
        let data = NSMutableDictionary()
        data.setValue(mail, forKey: "username")
        
        let URLRequest  = TouridooServices.URLService + "/api/v1/Users/emptyUser"
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.EMPTY_USER);
        Client.RequestPOST(Parameters: data, URLString: URLRequest)
    }
    
    func updateUserData(username : String, prefix : String, phone : String)
    {
        let data = NSMutableDictionary()
        data.setValue(username, forKey: "username")
        data.setValue(prefix, forKey: "phone_prefix")
        data.setValue(phone, forKey: "phone_number")
      
        let URLRequest  = TouridooServices.URLService + "/api/v1/Users/updateData"
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.UPDATE_USER_DATA);
        Client.RequestPOSTWithAutorization(Parameters:data, URLString: URLRequest)
        
    }
    
    func bookingCode(code : String, deviceID : String)
    {
        let data = NSMutableDictionary()
        data.setValue(code, forKey: "bookingCode")
        data.setValue(deviceID, forKey: "deviceId")
    
        let URLRequest  = TouridooServices.URLService + "/api/v1/Tours/bookingCode"
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.BOOKING_CODE);
        Client.RequestPOST(Parameters:data, URLString: URLRequest)
    }
    
    func search(searchText : String)
    {
        let languageID = Locale.current.languageCode
        
        let data = NSMutableDictionary()
        data.setValue(searchText, forKey: "searchText")
        //data.setValue(languageID, forKey: "languageCode")
        
        let URLRequest  = TouridooServices.URLService + "/api/v1/Search"
        
        let Client = ServiceRequest.init(delegate: (delegate)!, service :ServiceName.SEARCH);
        Client.RequestPOST(Parameters: data, URLString: URLRequest)
    }
    
}
