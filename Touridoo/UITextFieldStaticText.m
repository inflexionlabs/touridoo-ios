//
//  UITextFieldStaticText.m
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 15/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

#import "UITextFieldStaticText.h"

@implementation UITextFieldStaticText
{
    UILabel *DetailLabel;
    NSString *PlaceHolderBackup;
}

- (void)drawRect:(CGRect)rect
{
    //    if (self.keyboardType != UIKeyboardTypePhonePad
    //        && self.keyboardType != UIKeyboardTypeNumberPad
    //        && self.keyboardType != UIKeyboardTypeDecimalPad)
    //    {
    //        self.keyboardType = UIKeyboardTypeASCIICapable;
    //    }
    self.tintColor = [UIColor colorWithRed: 227.0 / 255.0 green: 34.0 / 255.0 blue: 48.0 / 255.0 alpha: 1.0];

}

- (BOOL)becomeFirstResponder
{
    BOOL outcome = [super becomeFirstResponder];
    
    if (outcome)
    {
        CGRect LabelFrame = self.frame;
        LabelFrame.size.height = self.frame.size.height / 2;
        LabelFrame.origin.y = self.frame.origin.y - (self.frame.size.height / 2) + 5;
        
        if (!DetailLabel)
        {
            
            DetailLabel = [[UILabel alloc] initWithFrame:self.frame];
            
            DetailLabel.text = self.placeholder;
            DetailLabel.adjustsFontSizeToFitWidth = YES;
            DetailLabel.textColor = [UIColor lightGrayColor];
            DetailLabel.alpha = 0;
            [DetailLabel setFont:[UIFont fontWithName:@"Montserrat-Regular" size:11]];
            
            // [DetailLabel setBackgroundColor:[UIColor redColor]];
            
            PlaceHolderBackup = self.placeholder;
            
            [self.superview addSubview:DetailLabel];
            
        }
        
        
        [UIView animateWithDuration:0.20 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^
         {
             DetailLabel.alpha = 1;
             DetailLabel.frame = LabelFrame;
             
             self.placeholder = @"";
             
         } completion:nil];
        
    }
    return outcome;
}

- (BOOL)resignFirstResponder
{
    BOOL outcome = [super resignFirstResponder];
    if (outcome)
    {
        self.backgroundColor = [UIColor clearColor];
        
        if ([self.text isEqualToString:@""])
        {
            if (DetailLabel)
            {
                [UIView animateWithDuration:0.20 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^
                 {
                     self.placeholder = PlaceHolderBackup;
                     DetailLabel.alpha = 0;
                     DetailLabel.frame =  self.frame;
                     
                 } completion:nil];
            }
        }
    }
    return outcome;
}

-(void)setText:(NSString *)text
{
    [super setText:text];
    
    CGRect LabelFrame = self.frame;
    LabelFrame.size.height = self.frame.size.height / 2;
    LabelFrame.origin.y = self.frame.origin.y - (self.frame.size.height / 2) + 5;
    
    if (!DetailLabel)
    {
        
        DetailLabel = [[UILabel alloc] initWithFrame:self.frame];
        
        DetailLabel.text = self.placeholder;
        DetailLabel.adjustsFontSizeToFitWidth = YES;
        DetailLabel.textColor = [UIColor lightGrayColor];
        DetailLabel.alpha = 0;
        [DetailLabel setFont:[UIFont fontWithName:@"Montserrat-Regular" size:11]];
    
        PlaceHolderBackup = self.placeholder;
        
        [self.superview addSubview:DetailLabel];
    }
    
    
    [UIView animateWithDuration:0.20 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^
     {
         DetailLabel.alpha = 1;
         DetailLabel.frame = LabelFrame;
         
         self.placeholder = @"";
         
     } completion:nil];
    
}

@end
