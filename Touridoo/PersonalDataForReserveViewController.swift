//
//  PersonalDataForReserveViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 06/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PersonalDataForReserveViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, ResponseServicesProtocol
{

    @IBOutlet weak var titleText : UILabel?
    @IBOutlet weak var NextButton : UIButton?
    @IBOutlet weak var scrollData  : UIScrollView?
    @IBOutlet weak var doYouHaveAccountButton : UIButton?
    @IBOutlet weak var nameTextField : UITextField?
    @IBOutlet weak var lastNameTextField : UITextField?
    @IBOutlet weak var mailTextField : UITextField?
    @IBOutlet weak var cellPhoneTextField : UITextField?
    @IBOutlet weak var hotelOrMeetingTextField : UITextField?
    @IBOutlet weak var addreesTextField : UITextField?
    @IBOutlet weak var hotelHolderName : UITextField?
    @IBOutlet weak var prefixTextField : UITextField?
    @IBOutlet weak var commentsTextField : UITextView?
    
    @IBOutlet weak var radioButtonTerms : UIButton?
    @IBOutlet weak var radioButtonOfferts : UIButton?
    
    @IBOutlet weak var labelTerms : UILabel?
    @IBOutlet weak var labelOfferts : UILabel?
    @IBOutlet weak var labelComments : UILabel?
    
    var currentTour : TourDescriptionModel?
    var setupScrollSize : Bool = false
    var bookingData : BookingDataReserve?
    var UserDataSet : Bool = false

    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        scrollData?.backgroundColor = UIColor.clear
        
        nameTextField?.delegate = self;
        lastNameTextField?.delegate = self;
        mailTextField?.delegate = self;
        cellPhoneTextField?.delegate = self;
        //hotelOrMeetingTextField?.delegate = self;
        //addreesTextField?.delegate = self;
        commentsTextField?.delegate = self;
        //hotelHolderName?.delegate = self;
        
        nameTextField?.placeholder = NSLocalizedString("Name", comment: "Name")
        lastNameTextField?.placeholder = NSLocalizedString("Last name", comment: "Last name")
        mailTextField?.placeholder = NSLocalizedString("Email", comment: "Email")
        cellPhoneTextField?.placeholder = NSLocalizedString("Cell phone number", comment: "Cell phone number")
     //   hotelOrMeetingTextField?.placeholder = NSLocalizedString("Hotel or meeting point", comment: "Hotel or meeting point")
     //   addreesTextField?.placeholder = NSLocalizedString("Meeting point address", comment: "Meeting point address")
     //   hotelHolderName?.placeholder = NSLocalizedString("Hotel holder’s name", comment: "Hotel holder’s name")
        prefixTextField?.placeholder = NSLocalizedString("Prefix", comment: "Prefix")
        
        addDoneButtonOnKeyboard()

        commentsTextField?.layer.borderWidth = 0.25;
        commentsTextField?.layer.borderColor = UIColor.lightGray.cgColor
        commentsTextField?.layer.cornerRadius = 3;
        commentsTextField?.tintColor = Tools.colorApp()

        labelTerms?.text = NSLocalizedString("Accept Privacy Policy", comment: "Accept Privacy Policy")
        labelOfferts?.text = NSLocalizedString("Get notifications and alerts", comment: "Get notifications and alerts")
        labelComments?.text = NSLocalizedString("Coments (optional) - 0/500", comment: "Coments (optional) - 0/500")
        labelTerms?.adjustsFontSizeToFitWidth = true
        
        radioButtonTerms?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButtonOfferts?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        
        let tapTerms = UITapGestureRecognizer.init(target: self, action: #selector(tapTermsAction))
        labelTerms?.addGestureRecognizer(tapTerms)
        let tapOfferts = UITapGestureRecognizer.init(target: self, action: #selector(tapOffertsAction))
        labelOfferts?.addGestureRecognizer(tapOfferts)
    }
    
    @IBAction func openPrefixSelector()
    {
        Tools.openPrefixSelector(inView: self, withTextField: prefixTextField!)
    }
    
    func tapTermsAction()
    {
        if radioButtonTerms?.isSelected == false
        {
            radioAction(radioButton: radioButtonTerms!)
        }
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let tycosView : PrivacyPolicyViewController  = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        tycosView.typeModaController = true
        self.present(tycosView, animated: true, completion: nil)
        
    }
    
    func tapOffertsAction()
    {
        radioAction(radioButton: radioButtonOfferts!)
    }
    
    @IBAction func radioAction(radioButton : UIButton)
    {
        Tools.feedback()
        
        if radioButton.isSelected {
            radioButton.isSelected = false
        }
        else
        {
            radioButton.isSelected = true
        }
    }
    
    @IBAction func openLogin()
    {
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let LoginModal : LoginModalViewController = storyboard.instantiateViewController(withIdentifier: "LoginModalViewController") as! LoginModalViewController
        LoginModal.view.frame = UIScreen.main.bounds
        LoginModal.view.alpha = 0
        LoginModal.personalDataParentController = self
    
        self.view.addSubview(LoginModal.view)
        self.addChildViewController(LoginModal)
        
        UIView.animate(withDuration: 0.30, animations:
            {
                LoginModal.view.alpha = 1
        })
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
        titleText?.text = NSLocalizedString("Personal information", comment: "Personal information")
        NextButton?.setTitle(NSLocalizedString("Meeting point", comment: "Meeting point"), for: UIControlState.normal)
        updaUserData()
        doYouHaveAccountButton?.setTitle(NSLocalizedString("Do you have an account? Sign in here", comment: "Do you have an account? Sign in here"), for: UIControlState.normal)
    }
    
    func updaUserData()
    {
        if UserDataSet == false
        {
            let token : String? = UserDefaults.standard.object(forKey: "token") as? String
            if token != nil
            {
                let session = sessionData.shared
                
                if session.User != nil
                {
                    UserDataSet = true
                    
                    radioButtonTerms?.isSelected = true
                    
                    if session.User?.name != nil {
                        nameTextField?.text = session.User?.name
                        nameTextField?.isEnabled = false
                        doYouHaveAccountButton?.isEnabled = false
                        doYouHaveAccountButton?.alpha = 0
                    }
                    
                    if session.User?.lastname != nil {
                        lastNameTextField?.text = session.User?.lastname
                        lastNameTextField?.isEnabled = false
                    }
                    
                    if session.User?.email != nil {
                        mailTextField?.text = session.User?.email
                        mailTextField?.isEnabled = false
                    }
                    
                    if session.User?.phone_number != nil
                    {
                        cellPhoneTextField?.text = session.User?.phone_number
                        
                        var selectedPrefix = (session.User?.phone_number)!
                        
                        selectedPrefix = selectedPrefix.replacingOccurrences(of: "México +52 ", with: "")
                        selectedPrefix = selectedPrefix.replacingOccurrences(of: ")", with: "")
                        
                        selectedPrefix = selectedPrefix.replacingOccurrences(of: "United States (", with: "")
                        selectedPrefix = selectedPrefix.replacingOccurrences(of: ")", with: "")
                        
                        cellPhoneTextField?.text = selectedPrefix
                    }
                    
                    if session.User?.phone_prefix != nil
                    {
                        prefixTextField?.text = session.User?.phone_prefix
                        var selectedPrefix = (session.User?.phone_prefix)!
                        
                        selectedPrefix = selectedPrefix.replacingOccurrences(of: "México (", with: "")
                        selectedPrefix = selectedPrefix.replacingOccurrences(of: ")", with: "")
                        
                        selectedPrefix = selectedPrefix.replacingOccurrences(of: "United States (", with: "")
                        selectedPrefix = selectedPrefix.replacingOccurrences(of: ")", with: "")
                        
                        prefixTextField?.text = selectedPrefix
                    }
                
                }
            }
        }
    }
    
    func hasSession()
    {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if setupScrollSize == false
        {
            setupScrollSize = true
            
            var content : CGSize = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(1)))
            
            let newHeight : CGFloat = self.view.frame.size.height - CGFloat(66)
            
            let scrollFrameInStoryboar : CGRect = CGRect(x: (scrollData?.frame.origin.x)!,
                                                         y: (scrollData?.frame.origin.y)!,
                                                         width: (scrollData?.frame.size.width)!,
                                                         height: newHeight)
            
            if content.height <= scrollFrameInStoryboar.size.height {
                content.height = scrollFrameInStoryboar.size.height + 1
            }
            scrollData?.frame = scrollFrameInStoryboar
            scrollData?.contentSize = content
        
        }
       
        //Add notifications for crazy keyboard fix!
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification:NSNotification)
    {
        //Keyboard is in da house!
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollData!.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollData?.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification)
    {
        //Hidden Keyboard
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollData?.contentInset = contentInset
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        //Remove notifications
        NotificationCenter.default.removeObserver(self)
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func NextAction()
    {
        if nameTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your name", comment: "Please enter your name"))
            return
        }
        
        if lastNameTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your last name", comment: "Please enter your last name"))
            return
        }
        
        if mailTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your email", comment: "Please enter your email"))
            return
        }
        
        if isValidEmail(testStr: (mailTextField?.text)!) == false
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter a valid email", comment: "Please enter a valid email"))
            return
        }
        
        if prefixTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please select a phone prefix", comment: "Please select a phone prefix"))
            return
        }
    
        if cellPhoneTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your cell phone", comment: "Please enter your cell phone"))
            return
        }
        
        /*
        if cellPhoneTextField?.text?.characters.count != 10
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter a valid cell number (10 digits)", comment: "Please enter a valid cell number (10 digits)"))
            return
        }*/
        

        if radioButtonTerms?.isSelected == false
        {
            Tools.showAlertView(withText: NSLocalizedString("You need to accept the terms and conditions to continue", comment: "You need to accept the terms and conditions to continue"))
            return
        }
        
        self.view.endEditing(true)
        
        Tools.showActivityView(inView: self)
        self.view.isUserInteractionEnabled = false
        
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        if token == nil
        {
            let dispatchTime = DispatchTime.now() + 0.50;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                let services = TouridooServices.init(delegate: self)
                services.enableUser(mail: (self.mailTextField?.text)!)
            }
        }
        else
        {
            booking()
        }
        
    }
    
    func booking()
    {
        let userdata = NewAccountModel.init()
        userdata.name = nameTextField?.text
        userdata.last_name = lastNameTextField?.text
        userdata.email = mailTextField?.text
        userdata.phone_prefix = prefixTextField?.text
        userdata.phone_number = cellPhoneTextField?.text
        userdata.rendezvous_point_name = ""//hotelOrMeetingTextField?.text
        userdata.rendezvous_point_address = ""//addreesTextField?.text
        userdata.hotel_reservation_owner_name = ""//hotelHolderName?.text
        userdata.comments = commentsTextField?.text
        userdata.policy_accepted = true
        userdata.receive_info = radioButtonOfferts?.isSelected
        userdata.password = ""
        userdata.bookingData = bookingData
        
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.50;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let ReserveController : meetingPointSelectionViewController  = self.storyboard!.instantiateViewController(withIdentifier: "meetingPointSelectionViewController") as! meetingPointSelectionViewController
            ReserveController.currentTour = self.currentTour
            ReserveController.newAccountData = userdata
            self.navigationController?.pushViewController(ReserveController, animated: true)
        }
    }
    
    func onSucces(Result: String, name: ServiceName)
    {
        let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
        let success : Bool = (dataResult.object(forKey: "success") != nil)
        
        if success
        {
            let content : String? = dataResult.object(forKey: "content") as? String
            
            if content == "empty"
            {
                self.booking()
            }
            else
            {
                Tools.hiddenActivityView(inView: self)
                
                let dispatchTime = DispatchTime.now() + 0.50;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    self.NextButton?.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    Tools.showAlertForBackendError(ModalViewController: self, errorDescription: NSLocalizedString("Email is already registered", comment: "Email is already registered"))
                }
            }
            
        }
        
    }
    
    func onError(Error: String, name: ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.NextButton?.alpha = 1
            self.view.isUserInteractionEnabled = true
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }
    
    @IBAction func closeView()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {

        if textField == nameTextField {
            lastNameTextField?.becomeFirstResponder()
        }
        
        if textField == lastNameTextField {
            mailTextField?.becomeFirstResponder()
        }
        
        if textField == mailTextField {
            openPrefixSelector()
        }
        
        if textField == cellPhoneTextField {
            textField.resignFirstResponder()
        }
        
        /*
        if textField == addreesTextField {
            hotelHolderName?.becomeFirstResponder()
        }
        
        if textField == hotelHolderName {
            textField.resignFirstResponder()
        }*/
        
        
        return true
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Next", comment: "Next"), style: UIBarButtonItemStyle.done, target: self, action:#selector(doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        cellPhoneTextField?.inputAccessoryView = doneToolbar
        
    }
    
    func addDoneButtonOnPrefixKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Next", comment: "Next"), style: UIBarButtonItemStyle.done, target: self, action:#selector(donePrefixButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        prefixTextField?.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        //hotelOrMeetingTextField?.becomeFirstResponder()
        self.view.endEditing(true)
    }
    
    func donePrefixButtonAction()
    {
        cellPhoneTextField?.becomeFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let maxlenght : Int = 499;
        let textlenght = textView.text?.characters.count
        
        let newlenght = textlenght! + text.characters.count - range.length
        
        let chars : Int = newlenght
        let countLabel : String = NSLocalizedString("Coments (optional) - ", comment: "Coments (optional) - ") + String(chars) + "/500"
        labelComments?.text = countLabel
        
        return newlenght <= maxlenght
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var maxlenght : Int = 50;
        
        if textField == cellPhoneTextField
        {
            maxlenght = 10
        }
        
        let textlenght = textField.text?.characters.count
        
        let newlenght = textlenght! + string.characters.count - range.length
        return newlenght <= maxlenght
        
    }
    
}
