//
//  BookingModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class BookingModel: NSObject
{
    var id : Int?
    var booking_code : String?
    var tour_date : String?
    var id_user : Int?
    var id_payment : Int?
    var id_tour : Int?
    var HasRatedTour : Bool?
    var TotalPrice : Float?
    var tickets : [PriceModel]?
    var LongitudeMeetingPoint : Float?
    var LatitudeMeetingPoint : Float?
    var meetingPoint : String?
    var meetingPointDescription : String?

    init(id : Int?,
         booking_code : String?,
         tour_date : String?,
         id_user : Int?,
         id_payment : Int?,
         id_tour : Int?,
         HasRatedTour : Bool?,
         TotalPrice : Float?,
         tickets : [PriceModel]?,
         LongitudeMeetingPoint : Float?,
         LatitudeMeetingPoint : Float?,
         meetingPoint : String?,
         meetingPointDescription : String?)
    {
        self.id = id
        self.booking_code = booking_code
        self.tour_date = tour_date
        self.id_user = id_user
        self.id_payment = id_payment
        self.id_tour = id_tour
        self.HasRatedTour = HasRatedTour
        self.TotalPrice = TotalPrice
        self.tickets = tickets
        self.LongitudeMeetingPoint = LongitudeMeetingPoint
        self.LatitudeMeetingPoint  = LatitudeMeetingPoint
        self.meetingPoint = meetingPoint
        self.meetingPointDescription = meetingPointDescription
    }

}
