//
//  tutorialSubview.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 26/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class tutorialSubview: UIViewController
{
    @IBOutlet weak var textView : UITextView?
    var text : String?
    @IBOutlet weak var imageInfomation : UIImageView?
    
    override func viewWillAppear(_ animated: Bool)
    {
        if text != nil
        {
            textView?.text = text
            textView?.font = Tools.fontAppBold(withSize: 16)
        }
        
        if imageInfomation != nil
        {
            imageInfomation?.layer.cornerRadius = 15 //(imageInfomation?.frame.size.height)! / 2
            imageInfomation?.layer.masksToBounds = true
            imageInfomation?.layer.borderColor = UIColor.white.cgColor
            imageInfomation?.layer.borderWidth = 5.0
            imageInfomation?.alpha = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if text != nil
        {
            textView?.text = text
        }
    }
    
}
