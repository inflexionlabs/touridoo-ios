//
//  TourDescriptionModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 20/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TourDescriptionModel: NSObject
{
    var id : Int
    var name : String
    var descriptionTour : String
    var image_url : String
    var image : Data?
    var duration_in_hours : Int
    var stars_average : Int
    var image_footer_label : String?
    var details : String?
    var rendezvous_description : String?
    var cancelation_description : String?
    var rates_count : Int
    var base10_stars_average : Int
    var rates : NSArray
    var languages : String
    var max_occupants : Int
    var tourSteps : NSArray?
    var woeid : String?
    var prices : [PriceModel]?
    var meetingPoints : [MeetingPointsModel]?
    
    init(id : Int, name : String,
         descriptionTour : String,
         image_url : String,
         duration_in_hours : Int,
         stars_average : Int,
         image_footer_label : String?,
         details : String?,
         rendezvous_description : String?,
         cancelation_description : String?,
         rates_count : Int,
         base10_stars_average : Int,
         rates : NSArray,
         languages : String,
         max_occupants : Int,
         image : Data?,
         tourSteps : NSArray?,
         woeid : String?,
         prices: [PriceModel]?,
         meetingPoints : [MeetingPointsModel]?)
    {
        self.id = id
        self.name = name
        self.descriptionTour = descriptionTour
        self.image_url = image_url
        self.duration_in_hours = duration_in_hours
        self.stars_average = stars_average
        self.image_footer_label = image_footer_label
        self.details = details
        self.rendezvous_description = rendezvous_description
        self.cancelation_description = cancelation_description
        self.rates_count = rates_count
        self.base10_stars_average = base10_stars_average
        self.rates = rates
        self.languages = languages
        self.max_occupants = max_occupants
        self.image = image
        self.tourSteps = tourSteps
        self.woeid = woeid
        self.prices = prices
        self.meetingPoints = meetingPoints
    }
    
    func toString() -> String
    {
        return "TourDescription:\nid:" + String(self.id) + "\n" + "name:" + String(self.name) + "\n"
    }
}

