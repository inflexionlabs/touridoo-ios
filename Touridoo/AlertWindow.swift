//
//  AlertWindow.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 14/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class AlertWindow: UIViewController
{
    @IBOutlet weak var background : UIView?
    @IBOutlet weak var text : UILabel?
    
     @IBOutlet weak var leftView : UIView?
     @IBOutlet weak var rightView : UIView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.text?.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Tools.PullViewCenter(View: leftView!, Points: (leftView?.frame.size.width)!)
        Tools.PushViewCenter(View: rightView!, Points: (rightView?.frame.size.width)!)
        
        background?.layer.shadowColor = UIColor.black.cgColor
        background?.layer.shadowOpacity = 0.25
        background?.layer.shadowOffset = CGSize.zero
        background?.layer.shadowRadius = 5
        
        rightView?.backgroundColor = Tools.colorApp()
        leftView?.backgroundColor = Tools.colorApp()
        self.text?.textColor = UIColor.white
        
    }

    func setText(withText : String)
    {
        text?.text = withText
    }
    
    func doYourMagic()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:
            {
                Tools.PullViewCenter(View: self.rightView!, Points: (self.leftView?.frame.size.width)!)
                Tools.PushViewCenter(View: self.leftView!, Points: (self.rightView?.frame.size.width)!)
         
        }, completion: { _ in
            UIView.animate(withDuration: 0.20, animations: {self.text?.alpha = 1})
        })
    }
    
    func kill()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if MainMenuApp.alertView == nil
        {
            return
        }
        
        if MainMenuApp.alertView != nil
        {
            MainMenuApp.alertView.view.removeFromSuperview()
            MainMenuApp.alertView = nil
        }
        
    }
    
    func hidden()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if MainMenuApp.alertView == nil
        {
            return
        }
        
        UIView.animate(withDuration: 0.15, animations: {self.text?.alpha = 0})
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations:
            {
                Tools.PullViewCenter(View: self.leftView!, Points: (self.leftView?.frame.size.width)!)
                Tools.PushViewCenter(View: self.rightView!, Points: (self.rightView?.frame.size.width)!)
                
        }, completion: { _ in
            
            self.leftView?.removeFromSuperview()
            self.rightView?.removeFromSuperview()
            self.background?.removeFromSuperview()
            self.text?.removeFromSuperview()
            
            if MainMenuApp.alertView != nil
            {
                MainMenuApp.alertView.view.removeFromSuperview()
                MainMenuApp.alertView = nil
            }
        
          
        })
    }
    
    func hiddenInModalViewController()
    {
        UIView.animate(withDuration: 0.15, animations: {self.text?.alpha = 0})
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations:
            {
                Tools.PullViewCenter(View: self.leftView!, Points: (self.leftView?.frame.size.width)!)
                Tools.PushViewCenter(View: self.rightView!, Points: (self.rightView?.frame.size.width)!)
                
        }, completion: { _ in
            self.view.removeFromSuperview() //Mario: "Creo es mejor remover toda la vista, no estoy seguro porque pero cuando la vista de notificación se sobreponia sobre botones, la vista dejaba sin interacción al boton"
            
            //self.leftView?.removeFromSuperview()
            //self.rightView?.removeFromSuperview()
            //self.background?.removeFromSuperview()
            //self.text?.removeFromSuperview()
            
        })
    }

}
