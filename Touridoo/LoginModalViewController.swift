//
//  LoginModalViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 05/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class LoginModalViewController: UIViewController, UITextFieldDelegate, ResponseServicesProtocol
{
    @IBOutlet weak var loginButton : UIButton?
    @IBOutlet weak var mailTextField : UITextField?
    @IBOutlet weak var passwordTextField : UITextField?
    @IBOutlet weak var forgotPassword : UIButton?
    
    var parentController : ProfileViewController?
    var personalDataParentController : PersonalDataForReserveViewController?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        mailTextField?.delegate = self
        passwordTextField?.delegate = self
        //Se oculta el boton de olvidé mi contraseña por ahora
        forgotPassword?.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        loginButton?.setTitle(NSLocalizedString("Login", comment:"Login"), for: UIControlState.normal)
        Tools.hiddenBurgerButton(hidden: true)
        mailTextField?.becomeFirstResponder()
        forgotPassword?.setTitle(NSLocalizedString("Forgot my password", comment: "Forgot my password"), for: UIControlState.normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hiddenKeyboard))
        self.view.addGestureRecognizer(tap)
        //test
        //mailTextField?.text = "eduardo@mail.com"
        //passwordTextField?.text = "12345678"
    }
    
    func hiddenKeyboard()
    {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    
   @IBAction func forgotMyPass()
   {
        let pass : ForgotMyPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotMyPasswordViewController") as! ForgotMyPasswordViewController
        if mailTextField?.text != ""
        {
            pass.mailBackup = (mailTextField?.text)!
        }
        pass.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(pass, animated: true, completion: nil)
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.LOGIN
        {
            UserDefaults.standard.set(mailTextField?.text, forKey: "email")
            UserDefaults.standard.set(passwordTextField?.text, forKey: "password")
            
            let Service = TouridooServices.init(delegate: self)
            let deviceID : String? = UserDefaults.standard.object(forKey: "device_token") as? String
            let token : String? = UserDefaults.standard.object(forKey: "token") as? String
            
            if (token != nil && deviceID != nil)
            {
                Service.RegisterForPushNotifications(deciveID: deviceID!)
            }
            else
            {
                let dispatchTime = DispatchTime.now() + 0.30;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    let session = sessionData.shared
                    let welcomeT = NSLocalizedString("Welcome ", comment: "Welcome ") + (session.User?.name)! + ""
                    Tools.showDoneView(withText: welcomeT)
                    self.closeView()
                }
            }

        }
        
        if name == ServiceName.REGISTER_FOR_NOTIFICATIONS
        {
            UserDefaults.standard.set("true", forKey: "device_registered")
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                let session = sessionData.shared
                let welcomeT = NSLocalizedString("Welcome ", comment: "Welcome ") + (session.User?.name)! + ""
                Tools.showDoneView(withText: welcomeT)
                self.closeView()
            }
        }
        
    }
    
    func onError(Error : String, name : ServiceName)
    {
        if name == ServiceName.LOGIN
        {
            Tools.hiddenActivityView(inView: self)
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
            }
        }
        
        if name == ServiceName.REGISTER_FOR_NOTIFICATIONS
        {
            UserDefaults.standard.set("true", forKey: "device_registered")
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.closeView()
            }
        }
    }
    
    @IBAction func loginAction()
    {
        print("Login")
        if mailTextField?.text == "" {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Please enter your email", comment: "Please enter your email"), ModalViewController: self)
            return
        }
        
        if passwordTextField?.text == "" {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Please enter your password", comment: "Please enter your password"), ModalViewController: self)
            return
        }
        
        mailTextField?.resignFirstResponder()
        passwordTextField?.resignFirstResponder()

        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showActivityView(inView: self)
            let Service = TouridooServices.init(delegate: self)
            Service.Login(username: (self.mailTextField?.text)!, password: (self.passwordTextField?.text)!)
            
        }
    
    }
    
    @IBAction func backAction()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
                
        }, completion:
            { _ in
                
                self.view.removeFromSuperview()
                if self.parentController != nil
                {
                    Tools.hiddenBurgerButton(hidden: false)
                }
                
        })
    }
    
    func closeView()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
                
        }, completion:
            { _ in
                
                self.view.removeFromSuperview()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.MainMenuApp.updateDataAfterLogin()
                appDelegate.MainMenuApp.updateButtonsUI()
                
                if self.parentController != nil
                {
                    self.parentController?.updateUI()
                }
                
                if self.personalDataParentController != nil
                {
                    self.personalDataParentController?.updaUserData()
                }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if textField == mailTextField {
            passwordTextField?.becomeFirstResponder()
        }
        
        if textField == passwordTextField
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}
