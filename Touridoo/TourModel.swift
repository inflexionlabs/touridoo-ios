//
//  TourModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 01/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TourModel: NSObject
{
    var id : Int
    var image : Data?
    var image_url : String?
    var from_price : Float?
    var duration_in_hours : Int
    var name : String
    var stars_average : Int
    var rates_count : Int
    var base10_stars_average : Int
    var date : String?
    var type : String?
    var TopTenNumber : Int?
    var singlePrice : Float?
    var prices : [PriceModel]?
    
    init(id : Int,
         image: Data?,
         image_url : String?,
         duration_in_hours : Int,
         short_name : String,
         stars_average : Int,
         rates_count : Int,
         base10_stars_average : Int,
         from_price : Float?,
         date : String?,
         type : String?,
         TopTenNumber : Int?,
         singlePrice : Float?,
         prices : [PriceModel]?)
    {
        self.id = id
        self.image = image
        self.image_url = image_url
        self.duration_in_hours = duration_in_hours
        self.name = short_name
        self.stars_average = stars_average
        self.rates_count = rates_count
        self.base10_stars_average = base10_stars_average
        self.from_price = from_price
        self.date = date
        self.type = type
        self.TopTenNumber = TopTenNumber
        self.singlePrice = singlePrice
        self.prices = prices
    }
    
}
