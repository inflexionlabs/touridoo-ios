//
//  withoutConnection.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 26/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class withoutConnection: UIViewController {

    @IBOutlet weak var viewConnection : UIView?
    @IBOutlet weak var textDisplay : UILabel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        textDisplay?.text = NSLocalizedString("No internet connection, please check the connection of your device", comment: "No internet connection, please check the connection of your device")
        textDisplay?.adjustsFontSizeToFitWidth = true
        
        if viewConnection != nil
        {
            viewConnection?.alpha = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        Tools.DownView(View: self.viewConnection!, Points: 100)
    }
    
    func show()
    {
        viewConnection?.alpha = 1
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 1
                
                Tools.UpView(View: self.viewConnection!, Points: 100)
                
        }, completion:
            { _ in
        })
    }
    
    func hidden()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
                Tools.DownView(View: self.viewConnection!, Points: 100)
                
        }, completion:
            { _ in
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
                
                if MainMenuApp.withoutConnectionView != nil
                {
                    MainMenuApp.withoutConnectionView.view.removeFromSuperview()
                }
                
                if MainMenuApp.withoutConnectionView != nil
                {
                    MainMenuApp.withoutConnectionView = nil
                }
                
        })
    }

}
