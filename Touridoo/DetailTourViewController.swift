//
//  DetailTourViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 02/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class DetailTourViewController: UIViewController {

    @IBOutlet weak var scrollData  : UIScrollView?
    var tourDescription : TourDescriptionModel?
    
    //@IBOutlet weak var servicesProviderTitle : UILabel?
    @IBOutlet weak var durationTitle : UILabel?
    @IBOutlet weak var languageTitle : UILabel?
    @IBOutlet weak var includedTitle : UILabel?
    @IBOutlet weak var reservationTitle : UILabel?
    @IBOutlet weak var TicketTitle : UILabel?
    @IBOutlet weak var accessibilityTitle : UILabel?
    @IBOutlet weak var questionsTitle : UILabel?
    
   // @IBOutlet weak var servicesProviderDescription : UITextView?
    @IBOutlet weak var durationDescription : UITextView?
    @IBOutlet weak var languageDescription : UITextView?
    @IBOutlet weak var incluedDescription : UITextView?
    @IBOutlet weak var reservationDescription : UITextView?
    @IBOutlet weak var TicketDescription: UITextView?
    @IBOutlet weak var accessibilityDescription : UITextView?
    @IBOutlet weak var questionsDescription : UITextView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        durationTitle?.text = NSLocalizedString("Duration", comment: "Duration")
        languageTitle?.text = NSLocalizedString("Language", comment: "Language")
        includedTitle?.text = NSLocalizedString("Included", comment: "Included")
        reservationTitle?.text = NSLocalizedString("When to book in?", comment: "When to book in?")
        TicketTitle?.text = NSLocalizedString("Ticket", comment: "Ticket")
        accessibilityTitle?.text = NSLocalizedString("Accesibility", comment: "Accesibility")
        questionsTitle?.text = NSLocalizedString("Questions?", comment: "Questions")
        
        languageDescription?.text = NSLocalizedString("Bilingual guide", comment: "Bilingual guide")
        
        incluedDescription?.text = NSLocalizedString("Transportation in Minibus or Bus depending on\nthe number of participants\ndiscounts of 20% on the official price.", comment: "Transportation in Minibus or Bus depending on\nthe number of participants\ndiscounts of 20% on the official price.")
        
        reservationDescription?.text = NSLocalizedString("We recommend booking as soon as possible to ensure availability, especially on\nbridges and public holidays.\nYou can book up to 2 days before (Mexico City time) whenever there are places left.",
                                                     comment: "We recommend booking as soon as possible to ensure availability, especially on\nbridges and public holidays.\nYou can book up to 2 days before (Mexico City time) whenever there are places left.")
        
        TicketDescription?.text = NSLocalizedString("Once booked you will receive an email with a bonus that you can print or take on your mobile or tablet on the day of the activity.",
                                                     comment: "Once booked you will receive an email with a bonus that you can print or take on your mobile or tablet on the day of the activity.")
        
        accessibilityDescription?.text = NSLocalizedString("Our transport units have spaces for people with disabilities, if you require a guide who knows sign language you can contact us.",
                                                     comment: "Our transport units have spaces for people with disabilities, if you require a guide who knows sign language you can contact us.")
        
        questionsDescription?.text = NSLocalizedString("You will find the answers to the most common questions in our FAQ section. If you have any problems to make the reservation or need a different service, contact us",
                                                           comment: "You will find the answers to the most common questions in our FAQ section. If you have any problems to make the reservation or need a different service, contact us")

        
    }
    func updateData()
    {
        
        let hours : Int = (tourDescription?.duration_in_hours)!
        let hoursString : String = String(hours)
        durationDescription?.text = hoursString + " " + NSLocalizedString("Hours", comment: "Hours")
        
        //languageDescription?.text = "Spanish";
        //incluedDescription?.text = "Bilingual guide throughout the visit. Transport by minibus or bus according to the number of participants. Discount up to 20% off the official price";
        //reservationDescription?.text = "";
        //TicketDescription?.text = "";
        //accessibilityDescription?.text = "";
        //questionsDescription?.text = "";
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let content: CGSize
        if Tools.iPhone4()
        {
             content = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(80)))
        }
        else
        {
             content = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(50)))
        }
        let newHeight : CGFloat = self.view.frame.size.height
        
        let scrollFrameInStoryboar : CGRect = CGRect(x: (scrollData?.frame.origin.x)!,
                                                     y: (scrollData?.frame.origin.y)!,
                                                     width: (scrollData?.frame.size.width)!,
                                                     height: newHeight)
        
        
        scrollData?.frame = scrollFrameInStoryboar
        scrollData?.contentSize = content
    }

}
