//
//  ServiceRequest.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 29/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration

//Requiered Protocl methods
protocol ResponseServicesProtocol: class
{
    func onSucces(Result : String, name : ServiceName)
    
    func onError(Error : String, name : ServiceName)
}

class ServiceRequest: NSObject
{
    weak var delegate : ResponseServicesProtocol?
    var currentService : ServiceName?
    var timer : Timer?
    var seconds : Int?
    var requestDone = false
    
    override init()
    {
        super.init()
    }
    
    init(delegate: ResponseServicesProtocol, service : ServiceName)
    {
        self.delegate = delegate;
        self.currentService = service
        
        super.init()
    }
    
    //GET Method
    func RequestGET(URLString : String)
    {
        print("\n")
        print("Request(GET) " + URLString);
        var Request = URLRequest(url: URL(string: URLString)!)
        Request.httpMethod = "GET"
        
        if hasInternet()
        {
            requestTimer()
            ExecuteTask(Request: Request)
        }
        else {
            notInternetAlert()
        }
    }
    
    //POST Method
    func RequestPOST(Parameters : NSDictionary, URLString : String)
    {
        print("\n")
        print("Request(POST) " + URLString);
        var Request = URLRequest(url: URL(string: URLString)!)
        Request.httpMethod = "POST"
        Request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        var postString : String? = "";
        
        if Parameters.count != 0
        {
            postString! = try! DictionaryToJSONData(jsonObject: Parameters)!
            
            print("with Body:\n"+postString!)
            
        }
        
        Request.httpBody = postString?.data(using: .utf8)
        
        if hasInternet()
        {
            requestTimer()
            ExecuteTask(Request: Request)
        }
        else {
            notInternetAlert()
        }
        
    }
    
    //POST Method with Token
    func RequestPOSTWithAutorization(Parameters : NSDictionary, URLString : String)
    {
        print("\n")
        print("Request(POST) " + URLString);
        var Request = URLRequest(url: URL(string: URLString)!)
        Request.httpMethod = "POST"
        
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String

        Request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        Request.setValue("Bearer " + token!, forHTTPHeaderField: "Authorization")
        
        var postString : String? = "";
        
        if Parameters.count != 0
        {
            postString! = try! DictionaryToJSONData(jsonObject: Parameters)!
            
            print("with Body:\n"+postString!)
            
        }
        
        Request.httpBody = postString?.data(using: .utf8)
        
        if hasInternet()
        {
            requestTimer()
            ExecuteTask(Request: Request)
        }
        else {
            notInternetAlert()
        }
        
    }
    
    //POST GET with Token
    func RequestGETWithAutorization(URLString : String)
    {
        print("\n")
        print("Request(GET) " + URLString);
        
        var Request = URLRequest(url: URL(string: URLString)!)
        Request.httpMethod = "GET"
        
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        if token == nil
        {
            return
        }
        
        Request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        Request.setValue("Bearer " + token!, forHTTPHeaderField: "Authorization")
        
        let postString : String? = "";
        
        Request.httpBody = postString?.data(using: .utf8)
        
        if hasInternet()
        {
            requestTimer()
            ExecuteTask(Request: Request)
        }
        else {
            notInternetAlert()
        }
        
    }
    
    
    //POST Method with URL encode
    func RequestPOSTWithURLEncode(URLString : String)
    {
        print("\n")
        print("Request(POST) " + URLString);
        
        var Request = URLRequest(url: URL(string: URLString)!)
        
        Request.httpMethod = "POST"
        let postString = "grant_type=password&username=Em&password=beberaton&platform=web"
        
        Request.httpBody = postString.data(using: .utf8)
        
        if hasInternet()
        {
            requestTimer()
            ExecuteTask(Request: Request)
        }
        else {
            notInternetAlert()
        }
    }
    
    
    //POST Method
    func RequestForLogin(URLString : String, username : String, password : String)
    {
        print("\n")
        print("Request(POST) " + URLString);
        
        var Request = URLRequest(url: URL(string: URLString)!)
        
        Request.httpMethod = "POST"
        let postString = "grant_type=password&username=" + username + "&password=" + password + "&platform=ios"
        
        Request.httpBody = postString.data(using: .utf8)
        
        if hasInternet()
        {
            requestTimer()
            ExecuteTask(Request: Request)
        }
        else {
            notInternetAlert()
        }
    }
    
    //PUT Method
    func RequestPUT(Parameters : NSDictionary, URLString : String)
    {
        print("\n")
        print("Request(PUT) " + URLString);
        var Request = URLRequest(url: URL(string: URLString)!)
        Request.httpMethod = "PUT"
        Request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var postString : String? = "";
        
        if Parameters.count != 0
        {
            postString! = try! DictionaryToJSONData(jsonObject: Parameters)!
            
            print("with Body:\n"+postString!)
        }
        
        Request.httpBody = postString?.data(using: .utf8)
        
        if hasInternet()
        {
            requestTimer()
            ExecuteTask(Request: Request)
        }
        else {
            notInternetAlert()
        }
        
    }
    
    func notInternetAlert()
    {
        print("No connection available")
        self.delegate?.onError(Error:"", name : self.currentService!)
        Tools.showNotConnectionAvailable()
        //Tools.showSlowConnectionView()
    }
    
    
    func hasInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func requestTimer()
    {
        requestDone = false
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(1.0), target: self, selector: #selector(countDown), userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    func countDown()
    {
        if timer == nil
        {
            return
        }
        
        if seconds == nil
        {
            seconds = -1
        }
        
        seconds = seconds! + 1
        print("Conection time: " + String(describing: seconds))
        
        if seconds == 10 && requestDone == false
        {
            Tools.showSlowConnectionView()
        }
        
        if requestDone == true
        {
            if self.currentService != ServiceName.LOGIN
            {
                Tools.hiddeSlowConnectionView()
            }
            
            timer?.invalidate()
            timer = nil
            seconds = nil
        }
        
    }
    
    //Request Task
    func ExecuteTask(Request : URLRequest)
    {
        let task = URLSession.shared.dataTask(with: Request) { data, response, error in
            guard let data = data, error == nil else
            {
                self.requestDone = true
                print("Error (😔😢😭)")
                print("FATAL ERROR:\n\(String(describing: error!))")
                self.delegate?.onError(Error: NSLocalizedString("Sorry! an error occurred in the system, please try again later", comment: "Sorry! an error occurred in the system, please try again later"), name : self.currentService!)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 404
            {
                //OnSuccess with error
                self.requestDone = true
                print("OK (😯😄😃)")
                let responseString = String(data: data, encoding: .utf8)
                print("Result:\n \(String(describing: responseString!))")
                let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: responseString!)! as NSDictionary
                let content : NSDictionary? = dataResult.object(forKey: "content") as? NSDictionary
                var message : String?
                if content == nil {
                    message = NSLocalizedString("Sorry! an error occurred in the system, please try again later", comment: "Sorry! an error occurred in the system, please try again later")
                }
                message = content?.object(forKey: "error_message") as? String
                if message == nil {
                    message = NSLocalizedString("Sorry! an error occurred in the system, please try again later", comment: "Sorry! an error occurred in the system, please try again later")
                }
                self.delegate?.onError(Error: message!, name : self.currentService!)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 400
            {
                //OnSuccess with error
                self.requestDone = true
                print("OK (😯😄😃)")
                let responseString = String(data: data, encoding: .utf8)
                print("Result:\n \(String(describing: responseString!))")
                
                let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: responseString!)! as NSDictionary
                
                let error_description : String? = dataResult.object(forKey: "error_description") as? String
                if error_description != nil
                {
                    let errorString = NSLocalizedString("The user name or password is incorrect.", comment: "The user name or password is let.")
                    self.delegate?.onError(Error: errorString, name: self.currentService!)
                    return
                }
                //let success : Bool = dataResult.object(forKey: "success") as! Bool
                let content : NSDictionary? = dataResult.object(forKey: "content") as? NSDictionary
                var errorCode = 0
                
                if content != nil
                {
                    let ValidationErrorCode : Int? = content?.object(forKey: "ValidationErrorCode") as? Int
                    if ValidationErrorCode != nil
                    {
                        errorCode = ValidationErrorCode!
                    }
                }
            
                var errorString : String = errorCodeCatalog.messageByCode(code: errorCode)
                
                if errorString == ""
                {
                    var ValidationErrorMessage : String? = content?.object(forKey: "ValidationErrorMessage") as? String
                    
                    if ValidationErrorMessage == nil
                    {
                        ValidationErrorMessage = NSLocalizedString("Sorry! an error occurred in the system, please try again later",
                                                                   comment: "Sorry! an error occurred in the system, please try again later")
                    }
                    
                    errorString = ValidationErrorMessage!
                }
                
                self.delegate?.onError(Error: errorString, name: self.currentService!)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {
                //OnError
                self.requestDone = true
                print("Error (😣😖😵)")
                print("Status Code: \(httpStatus.statusCode)")
                //print("\nDescription error:\(String(describing: error!))")
                print("response = \(String(describing: response!))")
                self.delegate?.onError(Error: NSLocalizedString("Sorry! an error occurred in the system, please try again later",
                                                                comment: "Sorry! an error occurred in the system, please try again later"), name : self.currentService!)
            }
            else
            {
                //OnSuccess
                self.requestDone = true
                print("OK (😯😄😃)")
                let responseString = String(data: data, encoding: .utf8)
                
                if self.currentService == ServiceName.LOGIN
                {
                    let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: responseString!)! as NSDictionary
                    let AccessToken : String = dataResult.object(forKey: "access_token") as! String
                    UserDefaults.standard.set(AccessToken, forKey: "token")
                    
                    UserDefaults.standard.set(false, forKey: "bookingcode_mode")
                    
                    let username : String? = dataResult.object(forKey: "username") as? String
                    let name : String? = dataResult.object(forKey: "name") as? String
                    let lastname : String? = dataResult.object(forKey: "lastname") as? String
                    let lastname_1 : String? = dataResult.object(forKey: "lastname_1") as? String
                    let lastname_2 : String? = dataResult.object(forKey: "lastname_2") as? String
                    let email : String? = dataResult.object(forKey: "email") as? String
                    let pic_url : String? = dataResult.object(forKey: "pic_url") as? String
                    let phone_number : String? = dataResult.object(forKey: "phone_number") as? String
                    let phone_prefix : String? = dataResult.object(forKey: "phone_prefix") as? String
                    
                    let data : UserData = UserData.init()
                    data.username = username
                    data.name = name
                    data.lastname = lastname
                    data.lastname_1 = lastname_1
                    data.lastname_2 = lastname_2
                    data.email = email
                    data.pic_url = pic_url
                    data.phone_number = phone_number
                    data.phone_prefix = phone_prefix
                    
                    let session = sessionData.shared
                    session.User = data
                
                }
                
                print("Result:\n \(String(describing: responseString!))")
                self.delegate?.onSucces(Result: String(describing: responseString!), name : self.currentService!)
                
                
            }
        }
        
        task.resume()
    }
    
    func DictionaryToJSONData(jsonObject: AnyObject) throws -> String?
    {
        let data: NSData? = try? JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
        
        var jsonStr: String?
        if data != nil {
            jsonStr = String(data: data! as Data, encoding: String.Encoding.utf8)
        }
        
        return jsonStr
    }
    
    func JSONDataToDiccionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
}
