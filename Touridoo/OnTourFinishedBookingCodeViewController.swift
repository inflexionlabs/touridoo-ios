//
//  OnTourFinishedBookingCodeViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 13/09/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class OnTourFinishedBookingCodeViewController: UIViewController {

    @IBOutlet weak var sendButton : UIButton?
    
    var offStar : UIImage = UIImage.init(named: "star_rate_off.png")!
    var onStar : UIImage = UIImage.init(named: "star_rate_on.png")!
    
    @IBOutlet weak var star1 : UIButton?
    @IBOutlet weak var star2 : UIButton?
    @IBOutlet weak var star3 : UIButton?
    @IBOutlet weak var star4 : UIButton?
    @IBOutlet weak var star5 : UIButton?
    
    var rate : Int = 0
    
    @IBOutlet weak var star1Guide : UIButton?
    @IBOutlet weak var star2Guide : UIButton?
    @IBOutlet weak var star3Guide : UIButton?
    @IBOutlet weak var star4Guide : UIButton?
    @IBOutlet weak var star5Guide : UIButton?
    
    var rateGuide : Int = 0
    var id_tour_rate_type : Int = 0
    
    
    @IBOutlet weak var tourIsOver: UILabel!
    @IBOutlet weak var duration : UILabel?
    @IBOutlet weak var titleLable : UILabel?
    @IBOutlet weak var descriptionlabel: UILabel?
    @IBOutlet weak var datelabel : UILabel?
    @IBOutlet weak var imageTour : UIImageView?
    
    @IBOutlet weak var textQuestions : UITextView?
    
    var comment = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sendButton?.setTitle(NSLocalizedString("Finish", comment:"Finish"), for: UIControlState.normal)
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        textQuestions?.text = NSLocalizedString("Thank you for your booking, enjoy! Please feel free to reach us for any questions", comment: "Thank you for your booking, enjoy! Please feel free to reach us for any questions")
        tourIsOver.text = NSLocalizedString("The tour is over", comment: "The tour is over")
        Tools.hiddenBurgerButton(hidden: true)
        
        let session = sessionData.shared
        
        if session.currentTour != nil
        {
            if session.currentTour?.booking?.tour_date != nil
            {
                let stingDate : String = (session.currentTour?.booking?.tour_date!)!
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                
                let dateObj = dateFormatter.date(from: stingDate)
                
                var hourString : String?
                dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm"
                hourString = dateFormatter.string(from: dateObj!)
                
                datelabel?.text = hourString
            }
            
            if session.currentTour?.name != nil
            {
                titleLable?.text = session.currentTour?.name
                descriptionlabel?.text = session.currentTour?.name
                descriptionlabel?.adjustsFontSizeToFitWidth = true
            }
            
            if session.currentTour?.duration_in_hours != nil {
                
                let num : Int = (session.currentTour?.duration_in_hours)!
                let hours : String = String(num)
                duration?.text = hours + " " + NSLocalizedString("hours", comment: "hours")
            }
            
        }
        
        if session.currentTourImage != nil
        {
            if session.currentTourImage?.count != 0
            {
                imageTour?.image = UIImage.init(data: session.currentTourImage!)
            }
        }
        else
        {
            if session.currentTour != nil
            {
                let image_url : String? = session.currentTour?.image_url
                
                var RealImage : Data?
                
                if image_url != nil
                {
                    let urlToDownload : URL = URL.init(string: image_url!)!
                    do {
                        RealImage = try  Data.init(contentsOf: urlToDownload)
                    } catch {
                        print(error)
                    }
                    
                    if RealImage != nil
                    {
                        if RealImage?.count != 0
                        {
                            session.currentTourImage = RealImage
                            imageTour?.image = UIImage.init(data: RealImage!)
                        }
                    }
                }
            }
        }
        
        
    }
    
    @IBAction func addCommen()
    {
       
    }
    
    @IBAction func starAction(starView : UIButton)
    {
        
    }
    
    @IBAction func starActionGuide(starView : UIButton)
    {
        
    }
    
    func doRebound(viewEffect : UIView)
    {
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }
    
    func nextView()
    {
        let session = sessionData.shared
        
        session.ToursArray = nil
        session.currentTour = nil
        session.currentTourImage = nil
        session.User = nil
        
        UserDefaults.standard.set(false, forKey: "hasTour")
        UserDefaults.standard.set(nil, forKey: "token")
        UserDefaults.standard.set(false, forKey: "hasData")
        UserDefaults.standard.set(-1, forKey: "tourState")
        UserDefaults.standard.set(false, forKey: "showReservationView");
        UserDefaults.standard.set(false, forKey: "device_registered")
        UserDefaults.standard.set(nil, forKey: "email")
        UserDefaults.standard.set(nil, forKey: "password")
        UserDefaults.standard.set(false, forKey: "bookingcode_mode")
        UserDefaults.standard.set("", forKey: "bookingcode")
        
        //Open MenuBarApp
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let mainMenu : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
        mainMenu.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(mainMenu, animated: true, completion: nil)
    }
    

    @IBAction func sendData()
    {
        nextView()
    }

}
