//
//  securityViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 19/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class securityViewController: UIViewController
{
    @IBOutlet weak var textDescription : UITextView?
    @IBOutlet weak var scrollView: UIScrollView!
    
    var currentPointOfInterest : PointOfInteresModel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        textDescription?.text = currentPointOfInterest?.step_security
        scrollView.contentSize = CGSize(width: (textDescription?.frame.size.width)!, height: (textDescription?.frame.size.height)!)
    }

}
