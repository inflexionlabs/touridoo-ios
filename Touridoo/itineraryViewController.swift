//
//  itineraryViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/08/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class itineraryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    var tourDescription : TourDescriptionModel?
    var TourStepsArray = NSMutableArray.init()
    
    @IBOutlet weak var tableSteps : UITableView?
    var selectedIndexPath : IndexPath = IndexPath.init(row: 0, section: 0)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tableSteps?.delegate = self
        self.tableSteps?.dataSource = self
        
    }
    func updateData()
    {
        if tourDescription?.tourSteps != nil
        {
            if tourDescription?.tourSteps?.count != 0
            {
                for integer in 0...((tourDescription?.tourSteps?.count)! - 1)
                {
                    let tourStep : NSDictionary = tourDescription?.tourSteps!.object(at: integer) as! NSDictionary
                    
                    let tour = TourStepModel.init()
                    
                    tour.step_type = tourStep.object(forKey: "step_type") as? Int
                    tour.duration_in_minutes = tourStep.object(forKey: "duration_in_minutes") as? Int
                    tour.id_user_guide = tourStep.object(forKey: "id_user_guide") as? Int
                    tour.id_tour = tourStep.object(forKey: "id_tour") as? Int
                    tour.id = tourStep.object(forKey: "id") as? Int
                    tour.step_index = tourStep.object(forKey: "step_index") as? Int
                    tour.is_current_step = tourStep.object(forKey: "is_current_step") as? Bool
                    tour.step_name = tourStep.object(forKey: "step_name") as? String
                    tour.step_description = tourStep.object(forKey: "step_description") as? String
                    tour.step_essentials = tourStep.object(forKey: "step_essentials") as? String
                    tour.step_suggestions = tourStep.object(forKey: "step_suggestions") as? String
                    tour.step_security = tourStep.object(forKey: "step_security") as? String
                    tour.step_food = tourStep.object(forKey: "step_food") as? String
                    tour.latitude = tourStep.object(forKey: "latitude") as? String
                    tour.longitude = tourStep.object(forKey: "longitude") as? String
                    tour.HasWiFi = tourStep.object(forKey: "HasWiFi") as? Bool
                    tour.HasBathrooms = tourStep.object(forKey: "HasBathrooms") as? Bool
                    tour.HasCoffeeShop = tourStep.object(forKey: "HasCoffeeShop") as? Bool
                    tour.HasFood = tourStep.object(forKey: "HasFood") as? Bool
                    tour.HasGiftShop = tourStep.object(forKey: "HasGiftShop") as? Bool
                    tour.HasWardrobe = tourStep.object(forKey: "HasWardrobe") as? Bool
                    tour.HasAccessibility = tourStep.object(forKey: "HasAccessibility") as? Bool
                    tour.HasCashMachine = tourStep.object(forKey: "HasCashMachine") as? Bool
                    tour.ImageURL = tourStep.object(forKey: "ImageURL") as? String
                    
                    let minutes : Int? = tourStep.object(forKey: "duration_in_minutes") as? Int
                    let minString : String? = String(minutes!)
                    
                    let dicData = PointOfInteresModel.init(title: tour.step_name!,
                                                           stay: minString! ,
                                                           inLocation: tour.is_current_step!,
                                                           step_description: tour.step_description,
                                                           step_essentials: tour.step_essentials,
                                                           step_suggestions: tour.step_suggestions,
                                                           step_security: tour.step_security,
                                                           step_food : tour.step_food,
                                                           step_type: tour.step_type,
                                                           step_ended: true,
                                                           realIndex: tour.step_index,
                                                           duration_in_minutes: tour.duration_in_minutes,
                                                           HasWiFi: tour.HasWiFi,
                                                           HasBathrooms:  tour.HasBathrooms,
                                                           HasCoffeeShop: tour.HasCoffeeShop,
                                                           HasFood: tour.HasFood,
                                                           HasGiftShop: tour.HasGiftShop,
                                                           HasWardrobe:  tour.HasWardrobe,
                                                           HasAccessibility: tour.HasAccessibility,
                                                           HasCashMachine: tour.HasCashMachine,
                                                           ImageURL:  tour.ImageURL)
                    
                    TourStepsArray.add(dicData)
                }
                
            }
        }
        
        tableSteps?.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    //TableView methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : PointsOfInterestTableViewCell = tableView.dequeueReusableCell(withIdentifier: "pointsOfInterest_cell") as! PointsOfInterestTableViewCell
        
        let dicData : PointOfInteresModel = TourStepsArray.object(at: (indexPath.row)) as! PointOfInteresModel
        let title : String = dicData.title
        //let stay : String = dicData.stay
        //let inLocation : Bool = dicData.inLocation
        
        cell.titleLabel?.text = title
        cell.titleLabel?.adjustsFontSizeToFitWidth = true
        
        let durationString : String = String(dicData.duration_in_minutes!)
        cell.duration?.text = NSLocalizedString("Staying time: ", comment: "Staying time: ") + durationString + " " + NSLocalizedString("minutes", comment: "minutes")
        cell.duration?.font = Tools.fontAppRegular(withSize: 11)
        
        let arrayServices : NSMutableArray = NSMutableArray.init()
        
        if dicData.HasWiFi! {arrayServices.add("wifi")}
        if dicData.HasBathrooms! {arrayServices.add("bathroom")}
        if dicData.HasCoffeeShop! {arrayServices.add("coffee")}
        if dicData.HasGiftShop! {arrayServices.add("gifts")}
        if dicData.HasFood! {arrayServices.add("food")}
        if dicData.HasWardrobe! {arrayServices.add("wardrobe")}
        if dicData.HasAccessibility! {arrayServices.add("Accessibility")}
        if dicData.HasCashMachine! {arrayServices.add("ATM")}
      
        cell.setServicesIcon(services: arrayServices)
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return TourStepsArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedIndexPath = IndexPath(row: (indexPath.row), section: 0)
        selectDescription()
    }
    
    func selectDescription()
    {
        let story = UIStoryboard.init(name: "OnTour", bundle: nil)
        let scrollcomponent : PointOfInteresScrollViewController  = story.instantiateViewController(withIdentifier: "PointOfInteresScrollViewController") as! PointOfInteresScrollViewController
        
        scrollcomponent.currentTour = self.tourDescription
        scrollcomponent.alternative = true
        
        scrollcomponent.currentPointOfInterest = TourStepsArray.object(at: selectedIndexPath.row) as? PointOfInteresModel
        
        scrollcomponent.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        if tourDescription?.image?.count != 0
        {
            scrollcomponent.simpleImage = UIImage.init(data: (tourDescription?.image)!)
        }
        
        let dicData : PointOfInteresModel = TourStepsArray.object(at: (selectedIndexPath.row)) as! PointOfInteresModel
        let arrayServices : NSMutableArray = NSMutableArray.init()
        
        if dicData.HasWiFi! {arrayServices.add("wifi")}
        if dicData.HasBathrooms! {arrayServices.add("bathroom")}
        if dicData.HasCoffeeShop! {arrayServices.add("coffee")}
        if dicData.HasGiftShop! {arrayServices.add("gifts")}
        if dicData.HasFood! {arrayServices.add("food")}
        if dicData.HasWardrobe! {arrayServices.add("wardrobe")}
        if dicData.HasAccessibility! {arrayServices.add("Accessibility")}
        if dicData.HasCashMachine! {arrayServices.add("ATM")}
        
        scrollcomponent.servicesArray = arrayServices
        
        self.navigationController?.pushViewController(scrollcomponent, animated: true)
    }
    

}
