//
//  ProfileViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate,ResponseServicesProtocol
{
    
    @IBOutlet weak var scrollData  : UIScrollView?
    
    @IBOutlet weak var nameTextField : UITextField?
    @IBOutlet weak var lastNameTextField : UITextField?
    @IBOutlet weak var mailTextField : UITextField?
    @IBOutlet weak var cellPhoneTextField : UITextField?
    @IBOutlet weak var prefixTextField : UITextField?
    @IBOutlet weak var radioButtonTerms : UIButton?
    @IBOutlet weak var radioButtonOfferts : UIButton?
    
    @IBOutlet weak var createAccountButton : UIButton?
    
    @IBOutlet weak var labelTerms : UILabel?
    @IBOutlet weak var labelOfferts : UILabel?
    @IBOutlet weak var labelComments : UILabel?
    
    @IBOutlet weak var signinlabel : UILabel?
    @IBOutlet weak var signinlabeldescription : UILabel?
    
    @IBOutlet weak var passwordTextField : UITextField?
    @IBOutlet weak var confirmPasswordTextField : UITextField?
    
    @IBOutlet weak var extraFields : UIView?
    
    let newAccountData = NewAccountModel.init()
    
    var setupScrollSize : Bool = false
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        scrollData?.backgroundColor = UIColor.clear
        
        nameTextField?.delegate = self;
        lastNameTextField?.delegate = self;
        mailTextField?.delegate = self;
        cellPhoneTextField?.delegate = self;
        passwordTextField?.delegate = self
        confirmPasswordTextField?.delegate = self
        prefixTextField?.delegate = self
        
        nameTextField?.placeholder = NSLocalizedString("Name", comment: "Name")
        lastNameTextField?.placeholder = NSLocalizedString("Last name", comment: "Last name")
        mailTextField?.placeholder = NSLocalizedString("Mail", comment: "Mail")
        cellPhoneTextField?.placeholder = NSLocalizedString("Cell phone number", comment: "Cell phone number")
        
        addDoneButtonOnKeyboard()
        
        labelTerms?.text = NSLocalizedString("Accept Privacy Policy", comment: "Accept Privacy Policy")
        labelOfferts?.text = NSLocalizedString("Get notifications and alerts", comment: "Get notifications and alerts")
        
        labelComments?.text = NSLocalizedString("Coments (optional) - 0/500", comment: "Coments (optional) - 0/500")
        labelTerms?.adjustsFontSizeToFitWidth = true
        
        radioButtonTerms?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButtonOfferts?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        
        let tapTerms = UITapGestureRecognizer.init(target: self, action: #selector(tapTermsAction))
        labelTerms?.addGestureRecognizer(tapTerms)
        let tapOfferts = UITapGestureRecognizer.init(target: self, action: #selector(tapOffertsAction))
        labelOfferts?.addGestureRecognizer(tapOfferts)
        
        extraFields?.backgroundColor = UIColor.clear
        if Tools.iPhone4(){
            Tools.AddHeight(View: scrollData!, Points: 80)
            var contentInset:UIEdgeInsets = self.scrollData!.contentInset
            //contentInset.bottom = (scrollData?.frame.size.height)!
            contentInset.bottom = 100
            self.scrollData?.contentInset = contentInset
        }
    }
    
    func tapOffertsAction()
    {
        radioAction(radioButton: radioButtonOfferts!)
    }
    
    func tapTermsAction()
    {
        if radioButtonTerms?.isSelected == false
        {
            radioAction(radioButton: radioButtonTerms!)
        }
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let tycosView : PrivacyPolicyViewController  = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        tycosView.typeModaController = true
        self.present(tycosView, animated: true, completion: nil)
        
    }
    
    @IBAction func radioAction(radioButton : UIButton)
    {
        Tools.feedback()
        
        if radioButton.isSelected
        {
            radioButton.isSelected = false
        }
        else
        {
            radioButton.isSelected = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        if token != nil
        {
            hasSession(animation: false)
        }
        
        Tools.hiddenBurgerButton(hidden: false)
    }
    
    func updateUI()
    {
        Tools.hiddenBurgerButton(hidden: false)
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        if token != nil
        {
            hasSession(animation: true)
        }
    }
    
    func hasSession(animation : Bool)
    {
        self.createAccountButton?.setTitle(NSLocalizedString("Save", comment: "Save"), for: UIControlState.normal)
        createAccountButton?.tag = 10
        
        self.radioButtonTerms?.isUserInteractionEnabled = false
        self.labelTerms?.isUserInteractionEnabled = false
        self.radioButtonTerms?.isSelected = true
        if Tools.iPhone4()
        {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            self.scrollData?.contentInset = contentInset
        }
        
        if animation == true
        {
            UIView.animate(withDuration: 0.30, animations:
                {
                    self.extraFields?.alpha = 0
                    
                    let upSize = CGFloat(90)
                    Tools.UpView(View: self.radioButtonTerms!, Points: upSize)
                    Tools.UpView(View: self.radioButtonOfferts!, Points: upSize)
                    Tools.UpView(View: self.labelTerms!, Points: upSize)
                    Tools.UpView(View: self.labelOfferts!, Points: upSize)
                    Tools.UpView(View: self.scrollData!, Points: 60)
                    self.signinlabel?.alpha = 0
                    self.signinlabeldescription?.alpha = 0
            })
        }
        else
        {
            self.extraFields?.alpha = 0
            let upSize = CGFloat(90)
            Tools.UpView(View: self.radioButtonTerms!, Points: upSize)
            Tools.UpView(View: self.radioButtonOfferts!, Points: upSize)
            Tools.UpView(View: self.labelTerms!, Points: upSize)
            Tools.UpView(View: self.labelOfferts!, Points: upSize)
            Tools.UpView(View: self.scrollData!, Points: 60)
            self.signinlabel?.alpha = 0
            self.signinlabeldescription?.alpha = 0
        }
        
        let session = sessionData.shared
        
        if session.User != nil
        {
            if session.User?.name != nil {
                nameTextField?.text = session.User?.name
                nameTextField?.isEnabled = false
            }
            
            if session.User?.lastname != nil {
                lastNameTextField?.text = session.User?.lastname
                lastNameTextField?.isEnabled = false
            }
            
            if session.User?.email != nil {
                mailTextField?.text = session.User?.email
                mailTextField?.isEnabled = false
            }
            
            if session.User?.phone_number != nil {
                cellPhoneTextField?.text = session.User?.phone_number
            }
            
            if session.User?.phone_prefix != nil
            {
                prefixTextField?.text = session.User?.phone_prefix
                var selectedPrefix = (session.User?.phone_prefix)!
                let separate = selectedPrefix.components(separatedBy: " ")
                if separate.count > 1
                {
                    selectedPrefix = separate[1]
                }
                
                selectedPrefix = selectedPrefix.replacingOccurrences(of: "(", with: "")
                selectedPrefix = selectedPrefix.replacingOccurrences(of: ")", with: "")
                
                prefixTextField?.text = selectedPrefix
                
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hiddenKeyboard))
        self.view.addGestureRecognizer(tap)
        scrollData?.contentSize = CGSize(width: (scrollData?.frame.size.width)!, height: (scrollData?.frame.size.height)! + CGFloat(1))
        scrollData?.keyboardDismissMode = UIScrollViewKeyboardDismissMode.onDrag
        
        //Add notifications for crazy keyboard fix!
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    @IBAction func openPrefixSelector()
    {
       Tools.openPrefixSelector(inView: self, withTextField: prefixTextField!)
    }
    
    func keyboardWillShow(notification:NSNotification)
    {
        //Keyboard is in da house!
        if !Tools.iPhone4()
        {
            var userInfo = notification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = self.view.convert(keyboardFrame, from: nil)
            
            var contentInset:UIEdgeInsets = self.scrollData!.contentInset
            contentInset.bottom = keyboardFrame.size.height
            self.scrollData?.contentInset = contentInset
        }
    }
    
    func keyboardWillHide(notification:NSNotification)
    {
        //Hidden Keyboard
        if !Tools.iPhone4()
        {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            self.scrollData?.contentInset = contentInset
        }
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        //Remove notifications
        NotificationCenter.default.removeObserver(self)
    }
    
    func hiddenKeyboard()
    {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    @IBAction func openLogin()
    {
        //Open tutorial
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let LoginModal : LoginModalViewController = storyboard.instantiateViewController(withIdentifier: "LoginModalViewController") as! LoginModalViewController
        LoginModal.view.frame = UIScreen.main.bounds
        LoginModal.view.alpha = 0
        LoginModal.parentController = self
        //tutorialView.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        
        self.view.addSubview(LoginModal.view)
        self.addChildViewController(LoginModal)
        
        UIView.animate(withDuration: 0.30, animations:
            {
                LoginModal.view.alpha = 1
                //tutorialView.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    @IBAction func createAccount()
    {
        UIApplication.shared.keyWindow?.endEditing(true)
        
        
        if createAccountButton?.tag == 10
        {
            if self.prefixTextField?.text == ""
            {
                Tools.showAlertView(withText: NSLocalizedString("Please select a phone prefix", comment: "Please select a phone prefix"))
                return
            }
            
            if cellPhoneTextField?.text == ""
            {
                Tools.showAlertView(withText: NSLocalizedString("Please enter your cell phone", comment: "Please enter your cell phone"))
                return
            }
            
            if cellPhoneTextField?.text?.characters.count != 10
            {
                Tools.showAlertView(withText: NSLocalizedString("Please enter a valid cell number (10 digits)", comment: "Please enter a valid cell number (10 digits)"))
                return
            }
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                Tools.showActivityView(inView: self)
                
                let services = TouridooServices.init(delegate: self)
                
                services.updateUserData(username: (self.mailTextField?.text)!,
                                        prefix: (self.prefixTextField?.text)!,
                                        phone: (self.cellPhoneTextField?.text)!)
            }
            
            return
        }
        
        if nameTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your name", comment: "Please enter your name"))
            return
        }
        
        if lastNameTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your last name", comment: "Please enter your last name"))
            return
        }
        
        if mailTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your mail", comment: "Please enter your mail"))
            return
        }
        
        if isValidEmail(testStr: (mailTextField?.text)!) == false
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter a valid email", comment: "Please enter a valid email"))
            return
        }
        
        if self.prefixTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please select a phone prefix", comment: "Please select a phone prefix"))
            return
        }
        
        if cellPhoneTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your cell phone", comment: "Please enter your cell phone"))
            return
        }
        
        if cellPhoneTextField?.text?.characters.count != 10
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter a valid cell number (10 digits)", comment: "Please enter a valid cell number (10 digits)"))
            return
        }
        
        if passwordTextField?.text == "" {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Please enter your password", comment: "Please enter your password"), ModalViewController: self)
            return
        }
        
        if (passwordTextField?.text?.characters.count)! < 8
        {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("The password must contain 8 characters or more", comment: "The password must contain 8 characters or more"), ModalViewController: self)
            return
        }
        
        if confirmPasswordTextField?.text == ""
        {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Please confirm your password", comment: "Please confirm your password"), ModalViewController: self)
            return
        }
        
        if (confirmPasswordTextField?.text?.characters.count)! < 8
        {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Confirm password must contain 8 characters or more", comment: "Confirm password must contain 8 characters or more"), ModalViewController: self)
            return
        }
        
        if passwordTextField?.text != confirmPasswordTextField?.text
        {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Passwords do not match", comment: "Passwords do not match"), ModalViewController: self)
            return
        }
        
        if radioButtonTerms?.isSelected == false
        {
            Tools.showAlertView(withText: NSLocalizedString("You need to accept the terms and conditions to continue", comment: "You need to accept the terms and conditions to continue"))
            return
        }
       
        newAccountData.name = nameTextField?.text
        newAccountData.last_name = lastNameTextField?.text
        newAccountData.email = mailTextField?.text
        newAccountData.phone_prefix = self.prefixTextField?.text
        newAccountData.phone_number = cellPhoneTextField?.text
        newAccountData.rendezvous_point_name = ""
        newAccountData.rendezvous_point_address = ""
        newAccountData.hotel_reservation_owner_name = ""
        newAccountData.comments = ""
        newAccountData.policy_accepted = true
        newAccountData.receive_info = radioButtonOfferts?.isSelected
        newAccountData.password = self.passwordTextField?.text
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showActivityView(inView: self)
            
            let services = TouridooServices.init(delegate: self)
            
            services.createNewUser(name: (self.newAccountData.name)!,
                                   lastName: (self.newAccountData.last_name)!,
                                   email: (self.newAccountData.email)!,
                                   phonePrefix: (self.newAccountData.phone_prefix)!,
                                   PhoneNumber: (self.newAccountData.phone_number)!,
                                   rendezvous_point: (self.newAccountData.rendezvous_point_name)!,
                                   rendezvous_address: (self.newAccountData.rendezvous_point_address)!,
                                   hotelReservationName: (self.newAccountData.hotel_reservation_owner_name)!,
                                   comments: (self.newAccountData.comments)!,
                                   policy_accepted: (self.newAccountData.policy_accepted)!,
                                   recived_info: (self.newAccountData.receive_info)!,
                                   password: self.newAccountData.password!)
        }
    
    }
    
    
    
    

    func onSucces(Result : String, name : ServiceName)
    {
        
        if name == ServiceName.UPDATE_USER_DATA
        {
            
            let session = sessionData.shared
            session.User?.phone_number = self.cellPhoneTextField?.text
            session.User?.phone_prefix = self.prefixTextField?.text
            
            let dispatchTime = DispatchTime.now() + 0.50;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                Tools.hiddenActivityView(inView: self)
                Tools.showDoneView(withText: NSLocalizedString("Saved correctly", comment: "Saved correctly"))

            }
            
            
            return
        }
        
        if name == ServiceName.CREATE_USER
        {
            let UserName : String = newAccountData.email!
            let Password : String  =  newAccountData.password!
            let services = TouridooServices.init(delegate: self)
            
            services.Login(username: UserName, password: Password)
            
            UserDefaults.standard.set(UserName, forKey: "email")
            UserDefaults.standard.set(Password, forKey: "password")
            
            return
        }
        
        if name == ServiceName.LOGIN
        {
            let services = TouridooServices.init(delegate: self)
            services.GetMyTours()
            return
        }
        
        
        if name == ServiceName.GET_MY_TOURS
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                    loadTourData.updateData(content: content)
                    //ReloadTourData.updateFlags()
                    
                    let dispatchTime = DispatchTime.now() + 0.50;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                    {
                        Tools.hiddenActivityView(inView: self)
                        
                        self.hasSession(animation: true)
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.MainMenuApp.updateDataAfterLogin()
                        appDelegate.MainMenuApp.updateButtonsUI()
                        
                        self.showDoneAlert()
                    }
                }
            }
            
        }
    }
    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showDoneAlert()
    {
        let dispatchTime = DispatchTime.now() + 0.35;
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let AlertView = UIAlertController(title: "Touridoo",
                                              message: NSLocalizedString("Registered successfully!", comment: "Registered successfully!"),
                                              preferredStyle: UIAlertControllerStyle.alert)
            
            AlertView.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "OK"),
                                              style: UIAlertActionStyle.cancel,
                                              handler:self.callAction))
            
            self.present(AlertView, animated: true, completion: nil)
        }
       
    }
    
    func callAction(alert: UIAlertAction!)
    {
        Tools.hiddenBurgerButton(hidden: false)
    }
    
    func donePrefixButtonAction()
    {
        cellPhoneTextField?.becomeFirstResponder()
    }
    

    
    func addDoneButtonOnPrefixKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Next", comment: "Next"), style: UIBarButtonItemStyle.done, target: self, action:#selector(donePrefixButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        prefixTextField?.inputAccessoryView = doneToolbar
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if textField == nameTextField {
            lastNameTextField?.becomeFirstResponder()
        }
        
        if textField == lastNameTextField {
            mailTextField?.becomeFirstResponder()
        }
        
        if textField == mailTextField {
           
            if prefixTextField?.text == ""
            {
                openPrefixSelector()
            }
            else
            {
                cellPhoneTextField?.becomeFirstResponder()
            }
        }
        
        
        if textField == passwordTextField {
            confirmPasswordTextField?.becomeFirstResponder()
        }
        
        if textField == confirmPasswordTextField {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Next", comment: "Next"), style: UIBarButtonItemStyle.done, target: self, action:#selector(doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        cellPhoneTextField?.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        passwordTextField?.becomeFirstResponder()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var maxlenght : Int = 50;
        
        if textField == cellPhoneTextField
        {
            maxlenght = 10
        }
        
        let textlenght = textField.text?.characters.count
        
        let newlenght = textlenght! + string.characters.count - range.length
        return newlenght <= maxlenght
        
    }
    
}
