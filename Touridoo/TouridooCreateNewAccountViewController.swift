//
//  TouridooCreateNewAccountViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 06/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TouridooCreateNewAccountViewController: UIViewController, UITextFieldDelegate, ResponseServicesProtocol
{

    @IBOutlet weak var createAccountButton : UIButton?
    @IBOutlet weak var skipButton : UIButton?
    
    @IBOutlet weak var mailTextField : UITextField?
    @IBOutlet weak var passwordTextField : UITextField?
    @IBOutlet weak var confirmPasswordTextField : UITextField?
    @IBOutlet weak var signInLabel : UILabel?
    @IBOutlet weak var signinDescription : UILabel?
    
    var newAccountData : NewAccountModel?
    var currentTour : TourDescriptionModel?
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        
        mailTextField?.delegate = self
        passwordTextField?.delegate = self
        confirmPasswordTextField?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
        createAccountButton?.setTitle(NSLocalizedString("Sign in", comment:"Sign in"), for: UIControlState.normal)
        skipButton?.setTitle(NSLocalizedString("Skip", comment:"Skip"), for: UIControlState.normal)
        signInLabel?.text = NSLocalizedString("Sign in", comment:"Sign in")
        signinDescription?.text = NSLocalizedString("We invite you to sign in on Touridoo, for an expedite tour booking process",
                                                    comment: "We invite you to sign in on Touridoo, for an expedite tour booking process")
        mailTextField?.placeholder = NSLocalizedString("Mail", comment: "Mail")
        passwordTextField?.placeholder = NSLocalizedString("Password (At lease 8 characters)", comment: "Password (At lease 8 characters)")
        confirmPasswordTextField?.placeholder = NSLocalizedString("Confirm password", comment: "Confirm password")
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hiddenKeyboard))
        self.view.addGestureRecognizer(tap)
        self.mailTextField?.text = newAccountData?.email
    }
    
    func hiddenKeyboard()
    {
       UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    func onSucces(Result: String, name: ServiceName)
    {
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let NewPassword : String = (self.passwordTextField?.text)!
            UserDefaults.standard.set(NewPassword, forKey: "password")
            self.NextAction()
        }
    }
    
    func onError(Error: String, name: ServiceName)
    {
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }
  
    @IBAction func NextAction()
    {
         UserDefaults.standard.set(true, forKey: "showReservationView");
        
        if UserDefaults.standard.bool(forKey: "noRateApp") == false
        {
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                let ReserveController : RateAppViewController  = self.storyboard!.instantiateViewController(withIdentifier: "RateAppViewController") as! RateAppViewController
                ReserveController.currentTour = self.currentTour
                ReserveController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(ReserveController, animated: true, completion: nil)
            }
        }
        else
        {
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                //Open MenuBarApp
                let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                let mainMenu : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
                mainMenu.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(mainMenu, animated: true, completion: nil)
            }
        }
    
    }
    
    
    @IBAction func createAccount()
    {
        print("Create account")
        
        if mailTextField?.text == "" {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Please enter your Mail", comment: "Please enter your Mail"), ModalViewController: self)
            return
        }
        
        if passwordTextField?.text == "" {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Please enter your password", comment: "Please enter your password"), ModalViewController: self)
            return
        }
        
        if confirmPasswordTextField?.text == ""
        {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Please confirm your password", comment: "Please confirm your password"), ModalViewController: self)
            return
        }
        
        if (passwordTextField?.text?.characters.count)! < 8
        {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("The password must contain 8 characters or more", comment: "The password must contain 8 characters or more"), ModalViewController: self)
            return
        }
        
        if (confirmPasswordTextField?.text?.characters.count)! < 8
        {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("The password must contain 8 characters or more", comment: "The password must contain 8 characters or more"), ModalViewController: self)
            return
        }
        
        if passwordTextField?.text != confirmPasswordTextField?.text
        {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Passwords do not match", comment: "Passwords do not match"), ModalViewController: self)
            return
        }
        
        mailTextField?.resignFirstResponder()
        passwordTextField?.resignFirstResponder()
        confirmPasswordTextField?.resignFirstResponder()
      
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showActivityView(inView: self)
            
            let Service = TouridooServices.init(delegate: self)
            
            let UserName : String = UserDefaults.standard.object(forKey: "email") as! String
            let Password : String = UserDefaults.standard.object(forKey: "provisional_password") as! String
            let NewPassword : String = (self.passwordTextField?.text)!
        
            Service.changePassword(username: UserName,
                                   old_password: Password,
                                   new_password: NewPassword)

        }
    
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if textField == mailTextField {
            passwordTextField?.becomeFirstResponder()
        }
        
        if textField == passwordTextField {
            confirmPasswordTextField?.becomeFirstResponder()
        }
        
        if textField == confirmPasswordTextField {
            textField.resignFirstResponder()
        }
        

        return true
    }

}
