//
//  GuideModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class GuideModel: NSObject
{
    var receive_info : Bool?
    var policy_accepted : Bool?
    var phone_number : String?
    var email : String?
    var last_name : String?
    var name : String?
    var username : String?
    var id : Int?
    var car_id : Int?
    var languages : NSArray?
    var PicProfile : String?
    var plates : String?
}
