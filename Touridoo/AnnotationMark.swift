//
//  AnnotationMark.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 27/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit
import MapKit

class AnnotationMark: NSObject, MKAnnotation
{
    var title : String?
    var subTit : String?
    var coordinate : CLLocationCoordinate2D
    var image : UIImage?
    
    init(title:String, coordinate : CLLocationCoordinate2D, subtitle:String, image : UIImage?)
    {
        self.title = title;
        self.coordinate = coordinate;
        self.subTit = subtitle;
        self.image = image;
    }

}
