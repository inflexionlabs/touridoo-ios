//
//  OnTour_BACKUPViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 03/10/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class OnTour_BACKUPViewController: UIViewController {

}
/*
 - - - - - - - - - - - - - - - -  -
 
 
 
 
 //
 //  OnTourViewController.swift
 //  Touridoo
 //
 //  Created by Eduardo Espinoza Kramsky on 06/06/17.
 //  Copyright © 2017 Inflexion Software. All rights reserved.
 //
 
 import UIKit
 import MapKit
 
 class OnTourViewController: UIViewController, MKMapViewDelegate, ResponseServicesProtocol, CLLocationManagerDelegate
 {
 var mapView : MKMapView! = MKMapView.init()
 
 @IBOutlet weak var mapViewReference : UIView?
 @IBOutlet weak var imageDriver : UIImageView?
 @IBOutlet weak var imageVehicle : UIImageView?
 @IBOutlet weak var viewTourDetail : UIView?
 @IBOutlet weak var myReservationButton : UIButton?
 @IBOutlet weak var detailsButton : UIButton?
 @IBOutlet weak var dropdownImage : UIImageView?
 @IBOutlet weak var minutes : UILabel?
 @IBOutlet weak var aproxTime : UILabel?
 @IBOutlet weak var tourName : UILabel?
 @IBOutlet weak var driverName : UILabel?
 @IBOutlet weak var vehiculeName : UILabel?
 @IBOutlet weak var bookingNumber : UILabel?
 @IBOutlet weak var passangersNumber : UILabel?
 @IBOutlet weak var day : UILabel?
 @IBOutlet weak var date : UILabel?
 @IBOutlet weak var hour : UILabel?
 @IBOutlet weak var duration : UILabel?
 
 @IBOutlet weak var carNumber : UILabel?
 
 @IBOutlet weak var reservationsNumLabel : UILabel?
 @IBOutlet weak var thankULabel : UITextView?
 
 var downloadImage : Bool = false
 
 var locationManager = CLLocationManager()
 
 var currentIndex : Int?
 var timer : Timer?
 var clockFirstTime : Bool = false
 
 var MapInitialConfiguration : Bool = false
 var OnlyOnZoom : Bool = false
 
 var latitudeDriver : Float = 0.0
 var longitudeDriver : Float = 0.0
 
 var latitudeMeeting : Float = 19.4131
 var longitudeMeeting : Float = -99.1646
 
 //var currentTour : Tour?
 var isViewTourHidde : Bool = true
 var OriginalFrameMap : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
 
 var UIConfiguration : Bool = false
 var requestMapNumber : Int =  0
 
 override func viewDidLoad()
 {
 super.viewDidLoad()
 
 imageDriver?.layer.cornerRadius = (imageDriver?.layer.frame.size.height)! / 2;
 imageDriver?.layer.masksToBounds = true
 
 myReservationButton?.setTitle(NSLocalizedString("My booking", comment: "My booking"), for: UIControlState.normal)
 detailsButton?.setTitle(NSLocalizedString("Details and recommendations", comment: "Details and recommendations"), for: UIControlState.normal)
 
 self.minutes?.text = ""
 
 thankULabel?.text = NSLocalizedString("Thank you for your booking, enjoy!", comment: "Thank you for your booking, enjoy!")
 
 }
 
 override func viewWillAppear(_ animated: Bool)
 {
 Tools.DownView(View: viewTourDetail!, Points: (viewTourDetail?.frame.size.height)!)
 OriginalFrameMap = (mapViewReference?.frame)!;
 
 reservationsNumLabel?.adjustsFontSizeToFitWidth = true
 
 let session = sessionData.shared
 
 if session.currentTour != nil
 {
 if session.driverImage != nil
 {
 imageDriver?.image = session.driverImage
 }
 }
 
 
 }
 
 func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
 {
 print("changue status")
 
 if status == CLAuthorizationStatus.authorizedWhenInUse
 {
 if mapView != nil
 {
 mapView.frame = CGRect(x: 0, y: 0, width: OriginalFrameMap.size.width, height: OriginalFrameMap.size.height)
 mapView?.delegate = self
 mapViewReference?.addSubview(mapView!)
 //self.mapView?.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
 
 self.mapView?.isUserInteractionEnabled = false
 
 self.mapView?.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
 
 if MapInitialConfiguration == false
 {
 MapInitialConfiguration = true
 
 let dispatchTime = DispatchTime.now() + 1.0;
 DispatchQueue.main.asyncAfter(deadline: dispatchTime)
 {
 self.calculateArrivingTime()
 }
 }
 }
 }
 
 }
 
 override func viewDidAppear(_ animated: Bool)
 {
 let tourState = UserDefaults.standard.integer(forKey: "tourState")
 if tourState == 2
 {
 self.aproxTime?.text = ""
 self.minutes?.text = NSLocalizedString("Is there", comment: "Is there")
 self.minutes?.textColor = UIColor.darkGray
 
 if isViewTourHidde == true
 {
 isViewTourHidde = false;
 
 UIView.animate(withDuration: 0.25, delay: 1.0, options: UIViewAnimationOptions.curveEaseOut, animations:
 {
 self.viewTourDetail?.frame = self.OriginalFrameMap
 self.mapView?.alpha = 0;
 Tools.DownView(View: self.mapViewReference!, Points: 10)
 
 self.detailsButton?.alpha = 0;
 self.myReservationButton?.alpha = 0;
 
 
 }, completion:
 { _ in
 self.view.isUserInteractionEnabled = true;
 })
 }
 
 }
 
 if timer == nil
 {
 timer = Timer.scheduledTimer(timeInterval: TimeInterval(3.0), target: self, selector: #selector(updateData), userInfo: nil, repeats: true)
 timer?.fire()
 }
 
 if UIConfiguration == true
 {
 return
 }
 
 UIConfiguration = true
 
 //Check for Location Services
 if (CLLocationManager.locationServicesEnabled())
 {
 locationManager = CLLocationManager()
 locationManager.delegate = self
 locationManager.desiredAccuracy = kCLLocationAccuracyBest
 locationManager.requestWhenInUseAuthorization()
 
 }
 
 
 Tools.hiddenBurgerButton(hidden: false)
 
 
 driverName?.text = ""
 vehiculeName?.text = ""
 tourName?.text = ""
 
 carNumber?.text = ""
 
 let session = sessionData.shared
 
 
 if session.currentTour != nil
 {
 if session.driverImage != nil
 {
 imageDriver?.image = session.driverImage
 }
 else
 {
 if session.currentTour?.tourRun?.guide?.PicProfile != nil
 {
 let urlPic : String = (session.currentTour?.tourRun?.guide?.PicProfile)!
 
 if downloadImage == false
 {
 downloadImage = true
 
 let urlToDownload : URL = URL.init(string: urlPic)!
 
 do
 {
 let RealImage = try  Data.init(contentsOf: urlToDownload)
 
 if RealImage.count != 0
 {
 imageDriver?.image = UIImage.init(data: RealImage)
 session.driverImage = UIImage.init(data: RealImage)
 }
 }
 catch
 {
 print(error)
 }
 }
 }
 }
 
 if session.currentTour?.tourRun?.guide?.plates != nil
 {
 carNumber?.text = session.currentTour?.tourRun?.guide?.plates
 }
 
 if session.currentTour?.tourRun?.current_latitude != nil
 {
 latitudeDriver = (session.currentTour?.tourRun?.current_latitude)!
 }
 
 if session.currentTour?.tourRun?.current_longitude != nil
 {
 longitudeDriver = (session.currentTour?.tourRun?.current_longitude)!
 }
 
 if session.currentTour?.name != nil
 {
 tourName?.text = session.currentTour?.name
 tourName?.adjustsFontSizeToFitWidth = true
 }
 
 if session.currentTour?.tourRun?.guide?.name != nil
 {
 driverName?.text = (session.currentTour?.tourRun?.guide?.name)! + " " + (session.currentTour?.tourRun?.guide?.last_name)!
 driverName?.adjustsFontSizeToFitWidth = true
 }
 
 if session.currentTour?.tourRun?.car?.brand != nil
 {
 vehiculeName?.text = (session.currentTour?.tourRun?.car?.brand)! + "-" + (session.currentTour?.tourRun?.car?.subbrand)!
 }
 
 if session.currentTour?.duration_in_hours != nil {
 
 let num : Int = (session.currentTour?.duration_in_hours)!
 let hours : String = String(num)
 duration?.text = NSLocalizedString("Duration:", comment: "Duration:") + " " + hours + " " + NSLocalizedString("hours", comment: "hours")
 }
 
 if session.currentTour?.booking?.booking_code != nil
 {
 let upperCase = session.currentTour?.booking?.booking_code?.uppercased()
 bookingNumber?.text = upperCase
 }
 
 if session.currentTour?.booking?.adult_passengers != nil
 {
 let num : Int = (session.currentTour?.booking?.adult_passengers)!
 passangersNumber?.text = String(num)
 }
 
 if session.currentTour?.booking?.tour_date != nil
 {
 let stingDate : String = (session.currentTour?.booking?.tour_date!)!
 
 let dateFormatter = DateFormatter()
 dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
 
 let dateObj = dateFormatter.date(from: stingDate)
 
 var hourString : String?
 dateFormatter.dateFormat = "HH:mm"
 hourString = dateFormatter.string(from: dateObj!)
 
 var dayString : String?
 dateFormatter.dateFormat = "dd"
 dayString = dateFormatter.string(from: dateObj!)
 
 var monthString : String?
 dateFormatter.dateFormat = "MMM, yyyy"
 monthString = dateFormatter.string(from: dateObj!)
 
 day?.text = dayString
 hour?.text = hourString
 date?.text = monthString
 
 }
 }
 
 currentIndex = UserDefaults.standard.integer(forKey: "tourState")
 
 
 }
 
 func updateData()
 {
 print("Update data")
 
 if clockFirstTime == true
 {
 let code : String? = UserDefaults.standard.object(forKey: "bookingcode") as? String
 
 if (UserDefaults.standard.bool(forKey: "bookingcode_mode") == true) && (code != nil)
 {
 let Service = TouridooServices.init(delegate: self)
 Service.bookingCode(code: code!, deviceID: "")
 }
 else
 {
 let Service = TouridooServices.init(delegate: self)
 Service.GetMyTours()
 }
 }
 
 if clockFirstTime == false
 {
 clockFirstTime = true
 }
 }
 
 override func viewDidDisappear(_ animated: Bool)
 {
 print("kill clock!")
 timer?.invalidate()
 timer = nil
 
 mapView = nil
 }
 
 func onSucces(Result : String, name : ServiceName)
 {
 if name == ServiceName.GET_MY_TOURS || name == ServiceName.BOOKING_CODE
 {
 let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
 let success : Bool = dataResult.object(forKey: "success") as! Bool
 
 if success
 {
 if (dataResult.object(forKey: "content") != nil)
 {
 let content : NSArray = dataResult.object(forKey: "content") as! NSArray
 loadTourData.updateData(content: content)
 loadTourData.updateFlags()
 
 let session = sessionData.shared
 
 if session.currentTour != nil
 {
 if session.currentTour?.tourRun?.current_latitude != nil
 {
 latitudeDriver = (session.currentTour?.tourRun?.current_latitude)!
 }
 
 if session.currentTour?.tourRun?.current_longitude != nil
 {
 longitudeDriver = (session.currentTour?.tourRun?.current_longitude)!
 }
 }
 
 requestMapNumber = requestMapNumber + 1
 print(String(requestMapNumber))
 
 let num = Int(requestMapNumber) % 5
 
 if num == 0
 {
 updateMap()
 }
 
 let newIndex = UserDefaults.standard.integer(forKey: "tourState")
 
 if newIndex != currentIndex
 {
 if newIndex != 2
 {
 let dispatchTime = DispatchTime.now() + 1.0;
 DispatchQueue.main.asyncAfter(deadline: dispatchTime)
 {
 loadTourData.updateUI()
 }
 }
 else
 {
 let dispatchTime = DispatchTime.now() + 1.0;
 DispatchQueue.main.asyncAfter(deadline: dispatchTime)
 {
 self.aproxTime?.text = ""
 self.minutes?.text = NSLocalizedString("Is there", comment: "Is there")
 self.minutes?.textColor = UIColor.darkGray
 
 if self.isViewTourHidde == true
 {
 self.isViewTourHidde = false;
 
 UIView.animate(withDuration: 0.25, delay: 1.0, options: UIViewAnimationOptions.curveEaseOut, animations:
 {
 self.viewTourDetail?.frame = self.OriginalFrameMap
 self.mapView?.alpha = 0;
 Tools.DownView(View: self.mapViewReference!, Points: 10)
 
 self.detailsButton?.alpha = 0;
 self.myReservationButton?.alpha = 0;
 
 
 }, completion:nil)
 }
 }
 
 }
 }
 
 }
 }
 
 }
 }
 
 func onError(Error : String, name : ServiceName)
 {}
 
 func mapViewDidStopLocatingUser(_ mapView: MKMapView) {
 print("")
 }
 
 func mapViewDidFinishLoadingMap(_ mapView: MKMapView)
 {
 print("Finish loading Map")
 }
 
 func updateMap()
 {
 if mapView != nil
 {
 mapView.removeOverlays(mapView.overlays)
 calculateArrivingTime()
 }
 }
 
 func calculateArrivingTime()
 {
 if mapView == nil
 {
 return
 }
 
 if latitudeDriver == 0.0
 {
 return
 }
 
 let request = MKDirectionsRequest()
 let source = MKMapItem(placemark:
 MKPlacemark(coordinate:CLLocationCoordinate2D(latitude: CLLocationDegrees(latitudeDriver),
 longitude: CLLocationDegrees(longitudeDriver)), addressDictionary: nil))
 source.name = "Meeting Point"
 request.source = source
 
 let destination = MKMapItem(placemark:
 MKPlacemark(coordinate:CLLocationCoordinate2D(latitude: CLLocationDegrees(latitudeMeeting),
 longitude:CLLocationDegrees(longitudeMeeting)), addressDictionary: nil))
 destination.name = "Destination"
 request.destination = destination
 
 request.transportType = MKDirectionsTransportType.automobile
 
 let directions = MKDirections(request: request)
 directions.calculateETA
 {
 (response, error) -> Void in
 if error == nil
 {
 if let estimate = response
 {
 print("Travel time \(estimate.expectedTravelTime / 60)")
 let estimateTime : Double = estimate.expectedTravelTime
 let travelTime : Int = Int(estimateTime / Double(60))
 
 let tourState = UserDefaults.standard.integer(forKey: "tourState")
 if tourState != 2
 {
 self.minutes?.text = String(travelTime) + " " + NSLocalizedString("minutes", comment: "minutes")
 }
 
 
 print("Departing at \(estimate.expectedDepartureDate)")
 print("Arriving at \(estimate.expectedArrivalDate)")
 
 self.drawRoute()
 }
 }
 else
 {
 //print(error?.localizedDescription)
 self.drawRoute()
 }
 
 }
 }
 
 func  setDriverLocation()
 {
 if mapView == nil
 {
 return
 }
 
 let allAnnotations = self.mapView.annotations
 self.mapView.removeAnnotations(allAnnotations)
 
 let locationDriver : CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(latitudeDriver), CLLocationDegrees(longitudeDriver))
 let driverIndicator = AnnotationMark(title: "", coordinate: locationDriver, subtitle: "",image: UIImage.init(named: "map_indicator_driver.png"))
 
 self.mapView?.addAnnotation(driverIndicator);
 
 let locationMeetingPoint : CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(latitudeMeeting), CLLocationDegrees(longitudeMeeting))
 let meetingIndicator = AnnotationMark(title: "", coordinate: locationMeetingPoint, subtitle: "",image: UIImage.init(named: "marker_green.png"))
 
 self.mapView?.addAnnotation(meetingIndicator);
 
 }
 
 func drawRoute()
 {
 if mapView == nil
 {
 return
 }
 
 // 2.
 let locationDriver : CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(latitudeDriver), CLLocationDegrees(longitudeDriver))
 
 let destinationLocation = CLLocationCoordinate2DMake(CLLocationDegrees(latitudeMeeting), CLLocationDegrees(longitudeMeeting))
 
 // 3.
 let sourcePlacemark = MKPlacemark(coordinate: locationDriver, addressDictionary: nil)
 let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
 
 // 4.
 let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
 let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
 
 // 5.
 let sourceAnnotation = MKPointAnnotation()
 sourceAnnotation.title = "Driver"
 
 if let location = sourcePlacemark.location {
 sourceAnnotation.coordinate = location.coordinate
 }
 
 let destinationAnnotation = MKPointAnnotation()
 destinationAnnotation.title = "User"
 
 if let location = destinationPlacemark.location {
 destinationAnnotation.coordinate = location.coordinate
 }
 
 let directionRequest = MKDirectionsRequest()
 
 directionRequest.source = sourceMapItem
 directionRequest.destination = destinationMapItem
 directionRequest.transportType = .automobile
 
 // Calculate the direction
 let directions = MKDirections(request: directionRequest)
 
 // 8.
 directions.calculate {
 (response, error) -> Void in
 
 guard let response = response else {
 if let error = error {
 print("Error: \(error)")
 }
 self.mapView?.isUserInteractionEnabled = true
 return
 }
 
 let route = response.routes[0]
 self.mapView?.add((route.polyline), level: MKOverlayLevel.aboveRoads)
 
 let rect = route.polyline.boundingMapRect
 let scale : Double = 3.0;
 let rectZoom : MKMapRect = MKMapRect(origin: MKMapPointMake((rect.origin.x * 1), (rect.origin.y * (1))),
 size: MKMapSizeMake((rect.size.width * scale), (rect.size.height * scale)))
 
 //self.mapView?.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
 
 if self.OnlyOnZoom == false
 {
 self.OnlyOnZoom = true
 self.mapView?.setVisibleMapRect(rectZoom, animated: true)
 }
 
 self.mapView?.isUserInteractionEnabled = true
 
 
 }
 
 self.setDriverLocation()
 }
 
 func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer
 {
 let renderer = MKPolylineRenderer(overlay: overlay)
 renderer.strokeColor = Tools.colorApp()
 renderer.lineWidth = 2.0
 
 return renderer
 }
 
 func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView])
 {
 print("")
 }
 
 func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
 
 if !(annotation is AnnotationMark) {
 return nil
 }
 
 let reuseId = "test"
 
 var annotationView : MKAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
 
 if annotationView == nil
 {
 annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
 annotationView?.canShowCallout = true
 }
 else {
 annotationView?.annotation = annotation
 }
 
 let currentAnno = annotation as! AnnotationMark
 annotationView?.image = currentAnno.image
 
 return annotationView
 }
 
 
 func mapViewWillStartLocatingUser(_ mapView: MKMapView)
 {
 print("star search user location")
 mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
 }
 
 @IBAction func tapOnTourDetail()
 {
 self.view.isUserInteractionEnabled = false;
 Tools.feedback()
 
 if isViewTourHidde == false
 {
 //HIDDEN TOUR DETAIL VIEW
 isViewTourHidde = true;
 
 UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations:
 {
 Tools.DownView(View: self.viewTourDetail!, Points: (self.viewTourDetail?.frame.size.height)!)
 self.mapView?.alpha = 1;
 self.mapViewReference?.frame = self.OriginalFrameMap
 
 self.detailsButton?.alpha = 1;
 self.myReservationButton?.alpha = 1;
 
 }, completion:
 { _ in
 self.view.isUserInteractionEnabled = true;
 })
 
 }
 else
 {
 isViewTourHidde = false;
 
 UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations:
 {
 self.viewTourDetail?.frame = self.OriginalFrameMap
 self.mapView?.alpha = 0;
 Tools.DownView(View: self.mapViewReference!, Points: 10)
 
 self.detailsButton?.alpha = 0;
 self.myReservationButton?.alpha = 0;
 
 
 }, completion:
 { _ in
 self.view.isUserInteractionEnabled = true;
 })
 }
 }
 
 @IBAction func openDetailsAndRecommendations()
 {
 let DetailsController : DetailsAndRecommendationsViewController  = self.storyboard!.instantiateViewController(withIdentifier: "DetailsAndRecommendationsViewController") as! DetailsAndRecommendationsViewController
 DetailsController.view.frame = (self.view.frame)
 //DetailsController.view.alpha = 0;
 Tools.PushViewCenter(View: DetailsController.view, Points: self.view.frame.size.width)
 
 self.view.addSubview(DetailsController.view)
 self.addChildViewController(DetailsController)
 
 UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
 {
 Tools.PullViewCenter(View: DetailsController.view, Points: self.view.frame.size.width)
 
 }, completion:
 { _ in
 })
 
 }
 
 @IBAction func contactGuide()
 {
 print("Help me please! I cant find my mexican sombrero!")
 
 let session = sessionData.shared
 var phoneNumber : String = (session.currentTour?.tourRun?.guide?.phone_number)!
 phoneNumber = phoneNumber.replacingOccurrences(of: "+52 ", with: "")
 let number = URL(string: "tel://" + phoneNumber)
 UIApplication.shared.open(number!)
 
 /*
 let AlertView = UIAlertController(title: NSLocalizedString("Call your host", comment: "Call your host"),
 message: NSLocalizedString("Do you want to make a phone call to your host?", comment: "Do you want to make a phone call to your host?"),
 preferredStyle: UIAlertControllerStyle.alert)
 
 AlertView.addAction(UIAlertAction(title: NSLocalizedString("Call", comment: "Call"),
 style: UIAlertActionStyle.default,
 handler:callAction))
 
 AlertView.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"),
 style: UIAlertActionStyle.cancel,
 handler: cancelAction))
 
 self.present(AlertView, animated: true, completion: nil)
 */
 }
 
 func callAction(alert: UIAlertAction!)
 {
 print("Help me please! I cant find my mexican sombrero!")
 
 let session = sessionData.shared
 var phoneNumber : String = (session.currentTour?.tourRun?.guide?.phone_number)!
 phoneNumber = phoneNumber.replacingOccurrences(of: "+52 ", with: "")
 let number = URL(string: "tel://" + phoneNumber)
 UIApplication.shared.open(number!)
 
 }
 
 func cancelAction(alert: UIAlertAction!) {
 print("Cancel")
 }
 
 
 }

 
 
 - - - - - - - - - -- - -  - - -
 */
