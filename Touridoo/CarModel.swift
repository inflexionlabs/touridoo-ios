//
//  CarModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class CarModel: NSObject
{
    var id : Int?
    var brand : String?
    var subbrand : String?
    var model : Int?
    var color_rgb : String?
    var services : String?
    var photo_url : String?
}

