//
//  yahooWeather.h
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 14/09/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YQL.h"
#import "weatherModel.h"

@interface yahooWeather : NSObject

-(weatherModel *)getFromWoeid:(NSString *)woeid;
-(NSString *)toCelsius:(NSString *)degrees;

@end
