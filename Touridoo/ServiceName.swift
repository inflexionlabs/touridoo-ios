//
//  ServiceName.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 21/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import Foundation

enum ServiceName
{
    case GET_REGIONS
    case GET_TOURS
    case LOGIN
    case GET_MY_TOURS
    case GET_TOUR_BY_ID
    case GET_TOUR_DESCRIPTION
    case GET_TOUR_DATES
    case CREATE_USER
    case BOOKING_TOUR
    case RATE_TOUR
    case RATE_CATALOG
    case REGISTER_FOR_NOTIFICATIONS
    case CHECK_TOUR_AVAILABILITY
    case CHANGE_PASSWORD
    case RATE_APP
    case UPDATE_TOUR
    case EMPTY_USER
    case UPDATE_USER_DATA
    case BOOKING_CODE
    case SEARCH 
}
