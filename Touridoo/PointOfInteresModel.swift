//
//  PointOfInteresModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 16/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PointOfInteresModel: NSObject
{
    var title : String
    var stay : String
    var inLocation : Bool
    var step_description : String?
    var step_essentials : String?
    var step_suggestions : String?
    var step_security : String?
    var step_food : String?
    var step_type : Int?
    var step_ended : Bool?
    var realIndex : Int?
    var duration_in_minutes : Int?
    var HasWiFi : Bool?
    var HasBathrooms : Bool?
    var HasCoffeeShop : Bool?
    var HasFood : Bool?
    var HasGiftShop : Bool?
    var HasWardrobe : Bool?
    var HasAccessibility : Bool?
    var HasCashMachine : Bool?
    var ImageURL : String?
    
    init(title : String,
         stay: String,
         inLocation : Bool,
         step_description : String?,
         step_essentials : String?,
         step_suggestions : String?,
         step_security : String?,
         step_food : String?,
         step_type : Int?,
         step_ended : Bool?,
         realIndex : Int?,
         duration_in_minutes : Int?,
         HasWiFi : Bool?,
         HasBathrooms : Bool?,
     HasCoffeeShop : Bool?,
     HasFood : Bool?,
     HasGiftShop : Bool?,
     HasWardrobe : Bool?,
     HasAccessibility : Bool?,
     HasCashMachine : Bool?,
     ImageURL : String?)
    {
        self.title = title
        self.stay = stay
        self.inLocation = inLocation
        self.step_description = step_description
        self.step_essentials = step_essentials
        self.step_suggestions = step_suggestions
        self.step_security = step_security
        self.step_food = step_food
        self.step_type = step_type
        self.step_ended = step_ended
        self.realIndex = realIndex
        self.duration_in_minutes = duration_in_minutes
        self.HasWiFi = HasWiFi
        self.HasBathrooms = HasBathrooms
        self.HasCoffeeShop = HasCoffeeShop
        self.HasFood = HasFood
        self.HasGiftShop = HasGiftShop
        self.HasWardrobe = HasWardrobe
        self.HasAccessibility = HasAccessibility
        self.HasCashMachine = HasCashMachine
        self.ImageURL = ImageURL
    }

}
