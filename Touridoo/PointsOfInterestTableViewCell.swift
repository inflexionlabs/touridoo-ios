//
//  PointsOfInterestTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 16/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PointsOfInterestTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var redBackground : UIView?
    @IBOutlet weak var iconService1 : UIImageView?
    @IBOutlet weak var iconService2 : UIImageView?
    @IBOutlet weak var iconService3 : UIImageView?
    @IBOutlet weak var iconService4 : UIImageView?
    @IBOutlet weak var iconService5 : UIImageView?
    @IBOutlet weak var iconService6 : UIImageView?
    @IBOutlet weak var iconService7 : UIImageView!
    @IBOutlet weak var iconService8 : UIImageView!
    
    var iconBath : UIImage = UIImage.init(named: "bathroom.png")!
    var iconCoffee : UIImage = UIImage.init(named: "coffee.png")!
    var iconFood : UIImage = UIImage.init(named: "food.png")!
    var iconGifts : UIImage = UIImage.init(named: "gifts.png")!
    var iconWard : UIImage = UIImage.init(named: "wardrobe.png")!
    var iconWifi : UIImage = UIImage.init(named: "wifi.png")!
    var iconAccessibility : UIImage = UIImage.init(named: "servicesAcces.png")!
    var iconATM : UIImage = UIImage.init(named: "servicesATM.png")!
    
    @IBOutlet weak var timerLabel : UILabel?
    @IBOutlet weak var duration : UILabel?
    
    var stepTime : Int = 0;
    
     var timer : Timer?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        redBackground?.layer.borderColor = Tools.colorAppSecondary().cgColor
        redBackground?.layer.borderWidth = 1.0
        redBackground?.layer.cornerRadius = 2.0
        
        timerLabel?.text =  "";
        
        let date : Date? = UserDefaults.standard.object(forKey: "last_notifcation") as? Date
        
        if date != nil
        {
            requestTimer()
            var minutes : Int = offsetFrom(date: date!)
            if minutes >= stepTime
            {
                minutes = stepTime
            }
            
            //timerLabel?.text = String(stepTime - minutes) + " min"
        }
    }
    
    func requestTimer()
    {
        if timer == nil
        {
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(10.0), target: self, selector: #selector(countDown), userInfo: nil, repeats: true)
            timer?.fire()
        }
    }
    
    func countDown()
    {
        if timer == nil
        {
            return
        }
        
        let date : Date? = UserDefaults.standard.object(forKey: "last_notifcation") as? Date
        
        if date != nil
        {
            var minutes : Int = offsetFrom(date: date!)
            if minutes >= stepTime
            {
                minutes = stepTime
            }
            
            if stepTime != 0
            {
                timerLabel?.text = String(stepTime - minutes) + " min"
            }
        }
        
    }
    
    func offsetFrom(date: Date) -> Int
    {
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: Date.init());
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        
        return Int(difference.minute!)
        
    }

    
    func setImageWithID(icon : UIImageView, id : String)
    {
        if id == "bathroom"
        {
            icon.image = iconBath
        }
        
        if id == "coffee"
        {
            icon.image = iconCoffee
        }
        
        if id == "food"
        {
            icon.image = iconFood
        }
        
        if id == "gifts"
        {
            icon.image = iconGifts
        }
        
        if id == "wardrobe"
        {
            icon.image = iconWard
        }
        
        if id == "wifi"
        {
            icon.image = iconWifi
        }
        
        if id == "Accessibility"
        {
            icon.image = iconAccessibility
        }
        
        if id == "ATM"
        {
            icon.image = iconATM
        }
        
    }
    
    func setServicesIcon(services : NSArray)
    {
        
        if services.count == 1
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
        }
        
        if services.count == 2
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
        }
        
        if services.count == 3
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
        }
        
        if services.count == 4
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
        }
        
        if services.count == 5
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5!, id: services.object(at: 4) as! String)
        }
        
        if services.count == 6
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5!, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6!, id: services.object(at: 5) as! String)
        }
        
        if services.count == 7
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5!, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6!, id: services.object(at: 5) as! String)
            setImageWithID(icon: iconService7!, id: services.object(at: 6) as! String)
        }
        
        if services.count == 8
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5!, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6!, id: services.object(at: 5) as! String)
            setImageWithID(icon: iconService7!, id: services.object(at: 6) as! String)
            setImageWithID(icon: iconService8!, id: services.object(at: 7) as! String)
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
