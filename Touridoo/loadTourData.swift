//
//  loadTourData.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 21/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class loadTourData: NSObject {

    class func updateData(content : NSArray)
    {
        if content.count == 0
        {
            print("User dont has a tour")
        }
        else
        {
            let tourObject = NSMutableArray.init()
            
            for integer in 0...(content.count - 1)
            {
                let dataServer : NSDictionary = content.object(at: integer) as! NSDictionary
                
                let name : String? = dataServer.object(forKey: "name") as? String
                let descriptionTour : String? = dataServer.object(forKey: "description") as? String
                let image_url : String? = dataServer.object(forKey: "image_url") as? String
                let duration_in_hours : Int? = dataServer.object(forKey: "duration_in_hours") as? Int
                let id : Int? = dataServer.object(forKey: "id") as? Int
                let stars_average : Int? = dataServer.object(forKey: "stars_average") as? Int
                let image_footer_label : String? = dataServer.object(forKey: "image_footer_label") as? String
                let details : String? = dataServer.object(forKey: "details") as? String
                let rendezvous_description : String? = dataServer.object(forKey: "rendezvous_description") as? String
                let cancelation_description : String? = dataServer.object(forKey: "cancelation_description") as? String
                let rates_count : Int? = dataServer.object(forKey: "rates_count") as? Int
                let base10_stars_average : Int? = dataServer.object(forKey: "base10_stars_average") as? Int
                let rates : String? = dataServer.object(forKey: "rates") as? String
                let max_occupants : Int? = dataServer.object(forKey: "max_occupants") as? Int
                let woeid : String? = dataServer.object(forKey: "Woeid") as? String
                
                var Prices: [PriceModel] = []
                let nsArrayPrices : NSArray? = dataServer.object(forKey: "Prices") as? NSArray
                
                if nsArrayPrices != nil
                {
                    for integer in 0...((nsArrayPrices?.count)! - 1)
                    {
                        let price : NSDictionary = nsArrayPrices!.object(at: integer) as! NSDictionary
                        
                        let id: Int = price.object(forKey: "Id") as! Int
                        let discount: Int = price.object(forKey: "Discount") as! Int
                        let value: Float = price.object(forKey: "Value") as! Float
                        let name: String = price.object(forKey: "Name") as! String
                        let description: String = price.object(forKey: "Description") as! String
                        let currency: String = price.object(forKey: "currency") as! String
                        
                        let PriceData: PriceModel = PriceModel.init(id: id, discount: discount, value: value, name: name, priceDescription: description, currency: currency)
                        
                        Prices.append(PriceData)
                    }
                }
                
                let TourRunData : NSDictionary = dataServer.object(forKey: "tour_run") as! NSDictionary
                
                let idTour : Int?  = TourRunData.object(forKey: "id") as? Int
                let id_tour : Int?  = TourRunData.object(forKey: "id_tour") as? Int
                let id_tour_step : Int? = TourRunData.object(forKey: "id_tour_step") as? Int
                let tour_date : String? = TourRunData.object(forKey: "tour_date") as? String
                let started_at : String? = TourRunData.object(forKey: "started_at") as? String
            
                let tour_steps_server : NSArray? = TourRunData.object(forKey: "tour_steps") as? NSArray
                
                let TourStepsArray = NSMutableArray.init()
                
                if tour_steps_server?.count != 0
                {
                    for integer in 0...((tour_steps_server?.count)! - 1)
                    {
                        let tourStep : NSDictionary = tour_steps_server!.object(at: integer) as! NSDictionary
                        
                        let tour = TourStepModel.init()
                        
                        tour.step_type = tourStep.object(forKey: "step_type") as? Int
                        tour.duration_in_minutes = tourStep.object(forKey: "duration_in_minutes") as? Int
                        tour.id_user_guide = tourStep.object(forKey: "id_user_guide") as? Int
                        tour.id_tour = tourStep.object(forKey: "id_tour") as? Int
                        tour.id = tourStep.object(forKey: "id") as? Int
                        tour.step_index = tourStep.object(forKey: "step_index") as? Int
                        tour.is_current_step = tourStep.object(forKey: "is_current_step") as? Bool
                        tour.step_name = tourStep.object(forKey: "step_name") as? String
                        tour.step_description = tourStep.object(forKey: "step_description") as? String
                        tour.step_essentials = tourStep.object(forKey: "step_essentials") as? String
                        tour.step_suggestions = tourStep.object(forKey: "step_suggestions") as? String
                        tour.step_security = tourStep.object(forKey: "step_security") as? String
                        tour.step_food = tourStep.object(forKey: "step_food") as? String
                        tour.latitude = tourStep.object(forKey: "latitude") as? String
                        tour.longitude = tourStep.object(forKey: "longitude") as? String
                        tour.HasWiFi = tourStep.object(forKey: "HasWiFi") as? Bool
                        tour.HasBathrooms = tourStep.object(forKey: "HasBathrooms") as? Bool
                        tour.HasCoffeeShop = tourStep.object(forKey: "HasCoffeeShop") as? Bool
                        tour.HasFood = tourStep.object(forKey: "HasFood") as? Bool
                        tour.HasGiftShop = tourStep.object(forKey: "HasGiftShop") as? Bool
                        tour.HasWardrobe = tourStep.object(forKey: "HasWardrobe") as? Bool
                        tour.HasAccessibility = tourStep.object(forKey: "HasAccessibility") as? Bool
                        tour.HasCashMachine = tourStep.object(forKey: "HasCashMachine") as? Bool
                        tour.ImageURL = tourStep.object(forKey: "ImageURL") as? String
                        
                        TourStepsArray.add(tour)
                    }
                    
                }
                
                let has_tour_ended : Bool? = TourRunData.object(forKey: "has_tour_ended") as? Bool
                let has_tour_started : Bool? = TourRunData.object(forKey: "has_tour_started") as? Bool
                //let has_tour_started = true
                
                let id_car : Int? = TourRunData.object(forKey: "id_car") as? Int
                let id_guide_user : Int? = TourRunData.object(forKey: "id_guide_user") as? Int
                
                let carData : NSDictionary? = TourRunData.object(forKey: "car") as? NSDictionary
                
                let car = CarModel.init()
                car.id = carData?.object(forKey: "id") as? Int
                car.brand = carData?.object(forKey: "brand") as? String
                car.subbrand = carData?.object(forKey: "subbrand") as? String
                car.model = carData?.object(forKey: "model") as? Int
                car.color_rgb = carData?.object(forKey: "color_rgb") as? String
                car.services = carData?.object(forKey: "services") as? String
                car.photo_url = carData?.object(forKey: "photo_url") as? String
                
                let current_longitude : Float? = TourRunData.object(forKey: "current_longitude") as? Float
                let current_latitude : Float? = TourRunData.object(forKey: "current_latitude") as? Float
                
                let guideData : NSDictionary? = TourRunData.object(forKey: "guide") as? NSDictionary
                
                let guide = GuideModel.init()
                guide.receive_info = guideData?.object(forKey: "receive_info") as? Bool
                guide.policy_accepted = guideData?.object(forKey: "policy_accepted") as? Bool
                guide.phone_number = guideData?.object(forKey: "phone_number") as? String
                guide.email = guideData?.object(forKey: "email") as? String
                guide.last_name = guideData?.object(forKey: "last_name") as? String
                guide.name = guideData?.object(forKey: "name") as? String
                guide.username = guideData?.object(forKey: "username") as? String
                guide.id = guideData?.object(forKey: "id") as? Int
                guide.car_id = guideData?.object(forKey: "car_id") as? Int
                guide.languages = guideData?.object(forKey: "languages") as? NSArray
                guide.PicProfile = guideData?.object(forKey: "PicProfile") as? String
                guide.plates = guideData?.object(forKey: "plates") as? String
                guide.plates = ""
                
                let TourData = TourRunModel.init(id: idTour, id_tour: id_tour, id_tour_step: id_tour_step, tour_date: tour_date, started_at: started_at, tour_steps: TourStepsArray,has_tour_ended: has_tour_ended,car: car,id_car: id_car,id_guide_user: id_guide_user,current_longitude: current_longitude,current_latitude: current_latitude,guide: guide,has_tour_started: has_tour_started)
                
                let BookingData : NSDictionary = dataServer.object(forKey: "booking") as! NSDictionary
                
                let idBook : Int?  = BookingData.object(forKey: "id") as? Int
                let booking_code : String?  = BookingData.object(forKey: "booking_code") as? String
                let tour_dateBook : String?  = BookingData.object(forKey: "tour_date") as? String
                let id_user : Int?  = BookingData.object(forKey: "id_user") as? Int
                let id_payment : Int?  = BookingData.object(forKey: "id_payment") as? Int
                let id_tourBoo : Int?  = BookingData.object(forKey: "id_tour") as? Int
                let HasRatedTour : Bool?  = BookingData.object(forKey: "HasRatedTour") as? Bool
                let TotalPrice : Float?  = BookingData.object(forKey: "TotalPrice") as? Float
                
                let meetingPoint : NSDictionary = BookingData.object(forKey: "meetingPoint") as! NSDictionary
                
                let LongitudeM : Float?  = meetingPoint.object(forKey: "Longitude") as? Float
                let LatitudeM : Float?  = meetingPoint.object(forKey: "Latitude") as? Float
                let meetingPointName : String?  = meetingPoint.object(forKey: "name") as? String
                let meetingPointDescrip : String?  = meetingPoint.object(forKey: "description") as? String
                
                var TicketsArray: [PriceModel] = []
                let nsTicketsPrices : NSArray? = BookingData.object(forKey: "Tickets") as? NSArray
                
                if nsTicketsPrices != nil
                {
                    for integer in 0...((nsTicketsPrices?.count)! - 1)
                    {
                        let price : NSDictionary = nsTicketsPrices!.object(at: integer) as! NSDictionary
                        
                        let id: Int = price.object(forKey: "Id") as! Int
                        let discount: Int = price.object(forKey: "Discount") as! Int
                        let value: Float = price.object(forKey: "Value") as! Float
                        let name: String = price.object(forKey: "Name") as! String
                        let description: String = price.object(forKey: "Description") as! String
                        let currency: String = price.object(forKey: "currency") as! String
                        
                        let PriceData: PriceModel = PriceModel.init(id: id, discount: discount, value: value, name: name, priceDescription: description, currency: currency)
                        
                        TicketsArray.append(PriceData)
                    }
                }
                
                let booking = BookingModel.init(id: idBook,
                                                booking_code: booking_code,
                                                tour_date: tour_dateBook,
                                                id_user: id_user,
                                                id_payment: id_payment,
                                                id_tour: id_tourBoo,
                                                HasRatedTour: HasRatedTour,
                                                TotalPrice: TotalPrice,
                                                tickets: TicketsArray,
                                                LongitudeMeetingPoint: LongitudeM,
                                                LatitudeMeetingPoint: LatitudeM,
                                                meetingPoint: meetingPointName,
                                                meetingPointDescription: meetingPointDescrip)
                
               
                let TourInfo = Tour.init(name: name,
                                         descriptionTour: descriptionTour,
                                         image_url: image_url,
                                         duration_in_hours: duration_in_hours,
                                         id: id,
                                         stars_average: stars_average,
                                         image_footer_label: image_footer_label,
                                         details: details,
                                         rendezvous_description: rendezvous_description,
                                         cancelation_description: cancelation_description,
                                         rates_count: rates_count,
                                         base10_stars_average: base10_stars_average,
                                         rates: rates,
                                         max_occupants: max_occupants,
                                         TourRun: TourData,
                                         booking: booking,
                                         woeid: woeid,
                                         singlePrice: -1000,
                                         prices: Prices)
                
                print(TourInfo.name! + TourInfo.descriptionTour!)
                
                tourObject.add(TourInfo)
            }
        
            let session = sessionData.shared
            session.ToursArray = NSArray.init(array: tourObject)
        }
        
        updateFlags()
    }
    
    class func updateUI()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        MainMenuApp.updateTourStatus()
    }
    
    class func updateFlags()
    {
        let session = sessionData.shared
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if session.ToursArray != nil
        {
            if session.ToursArray?.count != 0
            {
                for i in 0...((session.ToursArray?.count)! - 1)
                {
                    let currentTour : Tour = session.ToursArray?.object(at: i) as! Tour
                    
                    if currentTour.tourRun?.has_tour_started == true && currentTour.tourRun?.has_tour_ended == false
                    {
                        
                        let steps : NSArray = (currentTour.tourRun?.tour_steps)!
                        
                        var hastTour : Bool = false
                        
                        for index in 0...(steps.count - 1)
                        {
                            let tourStep : TourStepModel = steps.object(at: index) as! TourStepModel
                            
                            
                            if tourStep.is_current_step == true && tourStep.step_type != 0
                            {
                                //step_type == 0 -> No ha iniciado
                                
                                //step_type == 1 -> Traslado a un punto de encuentro
                                if (tourStep.step_type == 1)
                                {
                                    print("\nEstado Tour: 🚌\nConductor en camino\n")
                                    UserDefaults.standard.set(1, forKey: "tourState")
                                    hastTour = true
                                }
                                
                                //step_type == 2 -> Llego al punto de encuentro
                                if (tourStep.step_type == 2)
                                {
                                    print("\nEstado Tour: 🏨\nEl Conductor llego al punto de encuentro\n")
                                    UserDefaults.standard.set(2, forKey: "tourState")
                                    hastTour = true
                                }
                                
                                // step_type == 3 -> Camino a un punto de interés
                                if tourStep.step_type == 3
                                {
                                    print("\nEstado Tour: 🚍\nEn el tour, en traslado\n")
                                    UserDefaults.standard.set(3, forKey: "tourState")
                                    hastTour = true
                                }
                                
                                //step_type == 4 -> En un punto de interés
                                if tourStep.step_type == 4
                                {
                                    print("\nEstado Tour: 🏞\nEn el tour, punto de interes\n")
                                    UserDefaults.standard.set(4, forKey: "tourState")
                                    hastTour = true
                                }
                                
                                //step_type == 5 -> El tour ha terminado
                                if tourStep.step_type == 5 && currentTour.booking?.HasRatedTour == nil && (UserDefaults.standard.bool(forKey: "bookingcode_mode") == false)
                                {
                                    print("\nEstado Tour: 👍\nFinalizo, hay que calificar\n")
                                    hastTour = true
                                    UserDefaults.standard.set(5, forKey: "tourState")
                                }
                                
                                //step_type == 6 -> El tour cerrado
                                if tourStep.step_type! >= 6 && currentTour.booking?.HasRatedTour == nil && (UserDefaults.standard.bool(forKey: "bookingcode_mode") == false)
                                {
                                    print("\nEstado Tour: 👍\nFinalizo, hay que calificar\n")
                                    UserDefaults.standard.set(5, forKey: "tourState")
                                    hastTour = true
                                }
                                
                                if tourStep.step_type! >= 5 && currentTour.booking?.HasRatedTour == nil && (UserDefaults.standard.bool(forKey: "bookingcode_mode") == true)
                                {
                                    UserDefaults.standard.set(6, forKey: "tourState")
                                    hastTour = true
                                }
                            }
                            
                        }
                        
                        if hastTour == false{
                            print("\nTour no iniciado o ya paso\n")
                        }
                        
                        UserDefaults.standard.set(hastTour, forKey: "hasTour")
                        session.currentTour = currentTour;
                        
                    }
                    
                    let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
                    MainMenuApp.updateButtonsUI()
                    
                    if currentTour.tourRun?.has_tour_started == true && currentTour.tourRun?.has_tour_ended == true
                    {
                        let steps : NSArray = (currentTour.tourRun?.tour_steps)!
                        
                        for index in 0...(steps.count - 1)
                        {
                            let tourStep : TourStepModel = steps.object(at: index) as! TourStepModel
                            
                            if tourStep.is_current_step == true && tourStep.step_type != 0
                            {
                                if currentTour.tourRun?.has_tour_started == true && (currentTour.tourRun?.has_tour_ended)! {
                                    
                                    //Finalizo
                                    if tourStep.step_type == 3
                                    {
                                        UserDefaults.standard.set(true, forKey: "hasTour")
                                        UserDefaults.standard.set(2, forKey: "tourState")
                                    }
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                
                
            }
        }
        
    }

}
