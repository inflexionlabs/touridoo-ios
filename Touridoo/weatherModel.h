//
//  weatherModel.h
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 14/09/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface weatherModel : NSObject

@property NSString *initialTemperature;
@property UIImage *initialWeatherType;

@property NSString *finalTemperature;
@property UIImage *finalWeatherType;

@end
