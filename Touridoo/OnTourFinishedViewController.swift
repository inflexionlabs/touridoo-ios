//
//  OnTourFinishedViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 19/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class OnTourFinishedViewController: UIViewController,ResponseServicesProtocol
{

    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sendButton : UIButton?
    @IBOutlet weak var addCommentButton: UIButton!
    
    
    var offStar : UIImage = UIImage.init(named: "star_rate_off.png")!
    var onStar : UIImage = UIImage.init(named: "star_rate_on.png")!
    
    @IBOutlet weak var star1 : UIButton?
    @IBOutlet weak var star2 : UIButton?
    @IBOutlet weak var star3 : UIButton?
    @IBOutlet weak var star4 : UIButton?
    @IBOutlet weak var star5 : UIButton?
    
    var rate : Int = 0
    
    @IBOutlet weak var star1Guide : UIButton?
    @IBOutlet weak var star2Guide : UIButton?
    @IBOutlet weak var star3Guide : UIButton?
    @IBOutlet weak var star4Guide : UIButton?
    @IBOutlet weak var star5Guide : UIButton?
    
    var rateGuide : Int = 0
    var id_tour_rate_type : Int = 0
    
    
    @IBOutlet weak var tourIsOver: UILabel!
    @IBOutlet weak var duration : UILabel?
    @IBOutlet weak var titleLable : UILabel?
    @IBOutlet weak var descriptionlabel: UILabel?
    @IBOutlet weak var datelabel : UILabel?
    @IBOutlet weak var imageTour : UIImageView?
    
    @IBOutlet weak var textQuestions : UITextView?
    
    var comment = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sendButton?.setTitle(NSLocalizedString("Send", comment:"Send"), for: UIControlState.normal)
        addCommentButton.setTitle(NSLocalizedString("Add a comment", comment: "Add a comment"), for: .normal)
        if Tools.iPhone4(){
           // Tools.AddHeight(View: self.scrollView, Points: 80)
            scrollView.contentSize = CGSize(width: scrollView.frame.size.width, height: scrollView.frame.size.height + CGFloat(80))
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        textQuestions?.text = NSLocalizedString("Thank you for your booking, enjoy! Please feel free to reach us for any questions", comment: "Thank you for your booking, enjoy! Please feel free to reach us for any questions")
        tourIsOver.text = NSLocalizedString("The tour is over", comment: "The tour is over")
        Tools.hiddenBurgerButton(hidden: true)
        
       let session = sessionData.shared
        
        if session.currentTour != nil
        {
            if session.currentTour?.booking?.tour_date != nil
            {
                let stingDate : String = (session.currentTour?.booking?.tour_date!)!
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                
                let dateObj = dateFormatter.date(from: stingDate)
                
                var hourString : String?
                dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm"
                hourString = dateFormatter.string(from: dateObj!)
                
                datelabel?.text = hourString
            }
            
            if session.currentTour?.name != nil
            {
                titleLable?.text = session.currentTour?.name
                descriptionlabel?.text = session.currentTour?.name
                descriptionlabel?.adjustsFontSizeToFitWidth = true
            }
            
            if session.currentTour?.duration_in_hours != nil {
                
                let num : Int = (session.currentTour?.duration_in_hours)!
                let hours : String = String(num)
                duration?.text = hours + " " + NSLocalizedString("hours", comment: "hours")
            }
            
        }
        
        
        if session.currentTourImage != nil
        {
            if session.currentTourImage?.count != 0
            {
                imageTour?.image = UIImage.init(data: session.currentTourImage!)
            }
        }
        else
        {
            if session.currentTour != nil
            {
                let image_url : String? = session.currentTour?.image_url
                
                var RealImage : Data?
                
                if image_url != nil
                {
                    let urlToDownload : URL = URL.init(string: image_url!)!
                    do {
                        RealImage = try  Data.init(contentsOf: urlToDownload)
                    } catch {
                        print(error)
                    }
                    
                    if RealImage != nil
                    {
                        if RealImage?.count != 0
                        {
                            session.currentTourImage = RealImage
                            imageTour?.image = UIImage.init(data: RealImage!)
                        }
                    }
                }
            }
        }
        
        
    }
    
    @IBAction func addCommen()
    {
        let commentView : AddAComentViewController = self.storyboard!.instantiateViewController(withIdentifier: "AddAComentViewController") as! AddAComentViewController
        commentView.parentController = self
        commentView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(commentView, animated: true, completion: nil)
    }

    @IBAction func starAction(starView : UIButton)
    {
        
        Tools.feedback()
        
        if starView == star1 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star2?.setBackgroundImage(offStar, for: UIControlState.normal)
            star3?.setBackgroundImage(offStar, for: UIControlState.normal)
            star4?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            rate = 1
            
        }
        
        if starView == star2 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star3?.setBackgroundImage(offStar, for: UIControlState.normal)
            star4?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            doRebound(viewEffect: star2!)
            
            rate = 2
            
        }
        
        if starView == star3 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star4?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            doRebound(viewEffect: star2!)
            doRebound(viewEffect: star3!)
            
            rate = 3
            
        }
        
        if starView == star4 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3?.setBackgroundImage(onStar, for: UIControlState.normal)
            star4?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star5?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            doRebound(viewEffect: star2!)
            doRebound(viewEffect: star3!)
            doRebound(viewEffect: star4!)
            
            rate = 4
            
        }
        
        if starView == star5 {
            
            star1?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3?.setBackgroundImage(onStar, for: UIControlState.normal)
            star4?.setBackgroundImage(onStar, for: UIControlState.normal)
            star5?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1!)
            doRebound(viewEffect: star2!)
            doRebound(viewEffect: star3!)
            doRebound(viewEffect: star4!)
            doRebound(viewEffect: star5!)
            
            rate = 5
        }
    }
    
    @IBAction func starActionGuide(starView : UIButton)
    {
        
        Tools.feedback()
        
        if starView == star1Guide {
            
            star1Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star2Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            star3Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            star4Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1Guide!)
            rateGuide = 1
            
        }
        
        if starView == star2Guide {
            
            star1Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star3Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            star4Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1Guide!)
            doRebound(viewEffect: star2Guide!)
            
            rateGuide = 2
            
        }
        
        if starView == star3Guide {
            
            star1Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star4Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            star5Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1Guide!)
            doRebound(viewEffect: star2Guide!)
            doRebound(viewEffect: star3Guide!)
            
            rateGuide = 3
            
        }
        
        if starView == star4Guide {
            
            star1Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star4Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            star5Guide?.setBackgroundImage(offStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1Guide!)
            doRebound(viewEffect: star2Guide!)
            doRebound(viewEffect: star3Guide!)
            doRebound(viewEffect: star4Guide!)
            
            rateGuide = 4
            
        }
        
        if starView == star5Guide {
            
            star1Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star2Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star3Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star4Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            star5Guide?.setBackgroundImage(onStar, for: UIControlState.normal)
            
            doRebound(viewEffect: star1Guide!)
            doRebound(viewEffect: star2Guide!)
            doRebound(viewEffect: star3Guide!)
            doRebound(viewEffect: star4Guide!)
            doRebound(viewEffect: star5Guide!)
            
            rateGuide = 5
        }
    }
    
    func doRebound(viewEffect : UIView)
    {
        
        UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                viewEffect.transform = CGAffineTransform(scaleX: 1.10, y: 1.10)
                
        }, completion:
            { _ in
                UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                    {
                        viewEffect.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion:nil)
                
        })
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }
    
    func nextView()
    {
        let rateView : rateServiceViewController = self.storyboard!.instantiateViewController(withIdentifier: "rateServiceViewController") as! rateServiceViewController
        rateView.view.frame = UIScreen.main.bounds
        rateView.view.alpha = 0
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        MainMenuApp.view.addSubview(rateView.view)
        MainMenuApp.addChildViewController(rateView)
        
        UIView.animate(withDuration: 0.30, animations: {rateView.view.alpha = 1})
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        
        if name == ServiceName.GET_MY_TOURS
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                    loadTourData.updateData(content: content)
                    loadTourData.updateFlags()
                   
                    
                }
            }
            
            Tools.hiddenActivityView(inView: self)
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.nextView()
            }
            
            return
            
        }
        
        if name == ServiceName.RATE_TOUR
        {
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                let Service = TouridooServices.init(delegate: self)
                Service.GetMyTours()
            }
            
            return
        }
        
        if name == ServiceName.RATE_CATALOG
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            
            if success
            {
                let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                
                let array = NSMutableArray.init()
                
                for index in 0...(content.count - 1)
                {
                    let data : NSDictionary = content.object(at: index) as! NSDictionary
                    
                    let id : Int? = data.object(forKey: "id") as? Int
                    let language_code : String?  = data.object(forKey: "language_code") as? String
                    let name : String?  = data.object(forKey: "name") as? String
                    let descriptionText : String?  = data.object(forKey: "description") as? String
                    let icon_url : String?  = data.object(forKey: "icon_url") as? String
                    
                    let ratemodel = TourRateTypeModel.init()
                    ratemodel.id = id
                    ratemodel.language_code = language_code
                    ratemodel.name = name
                    ratemodel.descriptionText = descriptionText
                    ratemodel.icon_url = icon_url
                
                    id_tour_rate_type = id!
                    
                    array.add(ratemodel)
                }
                
            }
            
            let Service = TouridooServices.init(delegate: self)
            
            let session = sessionData.shared
        
            Service.rateTour(comment: self.comment,
                             stars: rate,
                             id_tour_runTour: (session.currentTour?.tourRun?.id)!,
                             id_tour_rate_type: id_tour_rate_type,
                             idTour: (session.currentTour?.id)!)
            
            return
            
        }
    }
    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
            self.nextView()
        }
        
    }
    
    @IBAction func sendData()
    {
        print("Send data")
        
        if rate == 0
        {
            Tools.showAlertView(withText: "Please select a rating for the tour")
            return
        }
        
        if rateGuide == 0
        {
            Tools.showAlertView(withText: "Please select a rating for your host")
            return
        }
        
        Tools.showActivityView(inView: self)
        self.view.isUserInteractionEnabled = false
        
        let Service = TouridooServices.init(delegate: self)
        Service.GetTourRateCatalog()
    
    }

}
