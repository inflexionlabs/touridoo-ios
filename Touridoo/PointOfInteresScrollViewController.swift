//
//  PointOfInteresScrollViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 19/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PointOfInteresScrollViewController: UIViewController, UIScrollViewDelegate
{
    @IBOutlet weak var titleView : UILabel?
    @IBOutlet weak var scrollLabels : UIScrollView?
    @IBOutlet weak var scrollViewControllers : UIScrollView?
    @IBOutlet weak var imageTour : UIImageView?
    var simpleImage : UIImage?
    
    var buttonRecommendations : UIButton?
    var buttonSuggestions: UIButton?
    var buttonSecurity : UIButton?
    var buttonServices : UIButton?
    
    var currentPointOfInterest : PointOfInteresModel?
    
    var recommendationsController : recommendationsViewController?
    var suggestionsController : suggestionsViewController?
    var securityController : securityViewController?
    var servicesController : servicesViewController?
  
    var freeScroll : Bool = true
    
    var iconBath : UIImage = UIImage.init(named: "toilet_white.png")!
    var iconCoffee : UIImage = UIImage.init(named: "coffe_white.png")!
    var iconFood : UIImage = UIImage.init(named: "food_white.png")!
    var iconGifts : UIImage = UIImage.init(named: "gift_white.png")!
    var iconWard : UIImage = UIImage.init(named: "wardrobe_white.png")!
    var iconWifi : UIImage = UIImage.init(named: "wifi_white.png")!
    var iconAccessibility : UIImage = UIImage.init(named: "servicesAcce_white.png")!
    var iconATM : UIImage = UIImage.init(named: "servicesATM_white.png")!
    
    @IBOutlet weak var pointName : UILabel?
    
    @IBOutlet weak var iconService1 : UIImageView!
    @IBOutlet weak var iconService2 : UIImageView!
    @IBOutlet weak var iconService3 : UIImageView!
    @IBOutlet weak var iconService4 : UIImageView!
    @IBOutlet weak var iconService5 : UIImageView!
    @IBOutlet weak var iconService6 : UIImageView!
    @IBOutlet weak var iconService7 : UIImageView!
    @IBOutlet weak var iconService8 : UIImageView!
    
    var servicesArray : NSArray?
    
    var alternative : Bool = false
    var currentTour : TourDescriptionModel?
    @IBOutlet weak var contactYourGuide : UIButton?
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        super.viewDidLoad()
        
        //Scroll component
        scrollLabels?.delegate = self
        scrollViewControllers?.delegate = self
        
        scrollLabelsSetUp()
        scrollViewControllerSetUp()
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        let dispatchTime = DispatchTime.now() + 0.10;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.setServicesIcon(services: self.servicesArray!)
        }
    }
    
    @IBAction func labelSelectAction(button : UIButton)
    {
        setSelectedSection(atIndex: button.tag)
    }
    
    func scrollViewControllerSetUp()
    {
        //Add ViewController in ScrollMain
        recommendationsController = self.storyboard?.instantiateViewController(withIdentifier: "recommendationsViewController") as? recommendationsViewController
        var FrameViewController : CGRect = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(0);
        FrameViewController.origin.y = CGFloat(0)
        recommendationsController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((recommendationsController?.view)!)
        recommendationsController?.currentPointOfInterest = self.currentPointOfInterest
        self.addChildViewController(recommendationsController!)
        
        suggestionsController = self.storyboard?.instantiateViewController(withIdentifier: "suggestionsViewController") as? suggestionsViewController
        FrameViewController = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(1);
        FrameViewController.origin.y = CGFloat(0)
        suggestionsController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((suggestionsController?.view)!)
        suggestionsController?.currentPointOfInterest = self.currentPointOfInterest
        self.addChildViewController(suggestionsController!)
        
        securityController = self.storyboard?.instantiateViewController(withIdentifier: "securityViewController") as? securityViewController
        FrameViewController  = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(2);
        FrameViewController.origin.y = CGFloat(0)
        securityController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((securityController?.view)!)
        securityController?.currentPointOfInterest = self.currentPointOfInterest
        self.addChildViewController(securityController!)
        
        servicesController = self.storyboard?.instantiateViewController(withIdentifier: "servicesViewController") as? servicesViewController
        FrameViewController = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(3);
        FrameViewController.origin.y = CGFloat(0)
        servicesController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((servicesController?.view)!)
        servicesController?.currentPointOfInterest = self.currentPointOfInterest
        self.addChildViewController(servicesController!)
        
        servicesController?.servicesArray = self.servicesArray
        
        let scrollHeight : CGFloat = (scrollViewControllers?.frame.size.height)!
        
        scrollViewControllers?.contentSize = CGSize(width: UIScreen.main.bounds.width * CGFloat(4), height: scrollHeight)
        scrollViewControllers?.isScrollEnabled = true
        scrollViewControllers?.isUserInteractionEnabled = true
        scrollViewControllers?.showsHorizontalScrollIndicator = false
        scrollViewControllers?.isPagingEnabled = true
    }
    
    func scrollLabelsSetUp()
    {
        //Set scrooll labels in UI
        
        var totalSize = CGFloat(0)
        
        //Button Recommendations
        var titleButton : String = NSLocalizedString("Must see", comment: "Must see")
        buttonRecommendations = createButton(title: titleButton, XCoordinate: CGFloat(0), width: CGFloat(180))
        totalSize = totalSize + (buttonRecommendations?.frame.size.width)!
        var indicatorView : UIView = createSelectIndicatorView(inButton: buttonRecommendations!, atIndex: 0)
        indicatorView.center = (buttonRecommendations?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview(buttonRecommendations!)
        
        //Button Suggestions
        titleButton = NSLocalizedString("Suggestions", comment: "Suggestions")
        buttonSuggestions = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(150))
        totalSize = totalSize + (buttonSuggestions?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: (buttonSuggestions)!, atIndex: 1)
        indicatorView.center = (buttonSuggestions?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview((buttonSuggestions)!)
        
        //Button Security
        titleButton = NSLocalizedString("Security", comment: "Security")
        buttonSecurity = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(100))
        totalSize = totalSize + (buttonSecurity?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: (buttonSecurity)!, atIndex: 2)
        indicatorView.center = (buttonSecurity?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview(buttonSecurity!)
        
        //Button Services
        titleButton = NSLocalizedString("Services", comment: "Services")
        buttonServices = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(100))
        totalSize = totalSize + (buttonServices?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: (buttonServices)!, atIndex: 3)
        indicatorView.center = (buttonServices?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview((buttonServices)!)
        
        let scrollHeight : CGFloat = (scrollLabels?.frame.size.height)!
        
        scrollLabels?.contentSize = CGSize(width: totalSize, height: scrollHeight)
        scrollLabels?.isScrollEnabled = true
        scrollLabels?.isUserInteractionEnabled = true
        scrollLabels?.showsHorizontalScrollIndicator = false
        
        setSelectedSection(atIndex: 0)
    }
    
    func setSelectedSection(atIndex : Int )
    {
        for subView : UIView in (scrollLabels?.subviews)!
        {
            if !(subView.isKind(of: UIButton.self))
            {
                let indicatorView : UIView  = subView
                var newAlpha = CGFloat(0);
                
                if indicatorView.tag == atIndex
                {
                    newAlpha = CGFloat(1)
                }
                UIView.animate(withDuration: 0.30, animations: {indicatorView.alpha = newAlpha})
            }
        }
        
        doScrollLabel(atIndex: atIndex)
        
        if atIndex == 3
        {
            //meetingController?.requestForMap()
        }
        
    }
    
    func doScrollLabel(atIndex : Int )
    {
        //recommendations
        if atIndex == 0
        {
            scrollLabels?.scrollRectToVisible((buttonRecommendations?.frame)!, animated: true)
            return
        }
        
        //suggestions
        if atIndex == 1
        {
            scrollLabels?.scrollRectToVisible((buttonRecommendations?.frame)!, animated: true)
            return
        }
        
        //security
        if atIndex == 2
        {
            scrollLabels?.scrollRectToVisible((buttonServices?.frame)!, animated: true)
            return
        }
        
        //services
        if atIndex == 3
        {
            scrollLabels?.scrollRectToVisible((buttonServices?.frame)!, animated: true)
            return
        }
        
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == scrollViewControllers && freeScroll
        {
            let index : Int = Int((scrollViewControllers?.contentOffset.x)!) / Int((scrollViewControllers?.frame.size.width)!)
            setSelectedSection(atIndex: index)
        }
    }
    
    @IBAction func scrollToViewController(button : UIButton)
    {
        
        freeScroll = false
        
        //Description
        if button.tag == 0 {
            scrollViewControllers?.scrollRectToVisible((recommendationsController?.view?.frame)!, animated: true)
        }
        
        //suggestionsControllersuggestionsController
        if button.tag  == 1 {
            scrollViewControllers?.scrollRectToVisible((suggestionsController?.view?.frame)!, animated: true)
        }
        
        //securityController
        if button.tag  == 2 {
            scrollViewControllers?.scrollRectToVisible((securityController?.view?.frame)!, animated: true)
        }
        
        //servicesController
        if button.tag  == 3 {
            scrollViewControllers?.scrollRectToVisible((servicesController?.view?.frame)!, animated: true)
            //servicesController.setData()
        }
        
       
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.freeScroll = true
        }
    }
    
    @IBAction func contactGuide()
    {
        if alternative == true
        {
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                
                let storyTours : UIStoryboard = UIStoryboard.init(name: "Tours", bundle: nil)
                let ReserveController : ReserveViewController  = storyTours.instantiateViewController(withIdentifier: "ReserveViewController") as! ReserveViewController
                ReserveController.currentTour = self.currentTour
                self.navigationController?.pushViewController(ReserveController, animated: true)
            }
            
            return
        }
        
        let AlertView = UIAlertController(title: NSLocalizedString("Call your host", comment: "Call your host"),
                                          message: NSLocalizedString("Do you want to make a phone call to your host?", comment: "Do you want to make a phone call to your host?"),
                                          preferredStyle: UIAlertControllerStyle.alert)
        
        AlertView.addAction(UIAlertAction(title: NSLocalizedString("Call", comment: "Call"),
                                          style: UIAlertActionStyle.default,
                                          handler:callAction))
        
        AlertView.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"),
                                          style: UIAlertActionStyle.cancel,
                                          handler: cancelAction))
        
        self.present(AlertView, animated: true, completion: nil)
    }
    
    func callAction(alert: UIAlertAction!)
    {
        print("Help me please! I cant find my mexican sombrero!")
        
        let session = sessionData.shared
        var phoneNumber : String = (session.currentTour?.tourRun?.guide?.phone_number)!
        phoneNumber = phoneNumber.replacingOccurrences(of: "+52 ", with: "")
        let number = URL(string: "tel://" + phoneNumber)
        UIApplication.shared.open(number!)
    }
    
    func cancelAction(alert: UIAlertAction!) {
        print("Cancel")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if alternative == true
        {
            contactYourGuide?.setTitle(NSLocalizedString("Book in", comment: "Book in"), for: UIControlState.normal)
        }
    
        
      let session = sessionData.shared
        
        if session.currentTour != nil
        {
            if session.currentTour?.name != nil
            {
                titleView?.text = session.currentTour?.name
            }
        }
        
        //OnTour
        if session.currentTourImage != nil
        {
            if session.currentTourImage?.count != 0
            {
                imageTour?.image = UIImage.init(data: session.currentTourImage!)
                
            }
        }
        
        //Reservations
        if simpleImage != nil
        {
            imageTour?.image = simpleImage
        }
        
        //Has Photo
        if currentPointOfInterest?.ImageURL != nil
        {
            if currentPointOfInterest?.ImageURL != ""
            {
                imageTour?.image = UIImage.init();
                
                var RealImage : Data?
                let image_url : String? = currentPointOfInterest?.ImageURL
                
                if image_url != nil {
                    if image_url != "" {
                        DispatchQueue.global().async {
                            if image_url != nil {
                                let urlToDownload : URL = URL.init(string: image_url!)!
                                do {
                                    RealImage = try  Data.init(contentsOf: urlToDownload)
                                } catch {  print(error) }
                            }
                            
                            DispatchQueue.main.async {
                                if RealImage != nil  {
                                    self.imageTour?.image = UIImage.init(data: RealImage!)
                                }
                            }
                        }
                    }
                }
            }
        }
        
        Tools.hiddenBurgerButton(hidden: true)
        
        pointName?.text = currentPointOfInterest?.title
    }
    
    func createButton(title : String, XCoordinate : CGFloat, width : CGFloat) -> UIButton
    {
        let scrollHeight : CGFloat = (scrollLabels?.frame.size.height)!
        
        let dynamicButton : UIButton = UIButton.init(frame: CGRect(x: XCoordinate,y: 0, width: width, height: scrollHeight))
        dynamicButton.setTitle(title, for: UIControlState.normal)
        dynamicButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        dynamicButton.titleLabel!.font = Tools.fontAppBold(withSize: 14)
        //dynamicButton.backgroundColor = Tools.getRandomColor()
        dynamicButton.addTarget(self, action: #selector(scrollToViewController(button:)), for: UIControlEvents.touchUpInside)
        
        return dynamicButton
    }
    
    func createSelectIndicatorView(inButton : UIButton, atIndex : Int) -> UIView
    {
        var SizeView : CGRect = inButton.frame
        SizeView.size.width = inButton.frame.size.width * CGFloat(0.85)
        SizeView.size.height = inButton.frame.size.height * CGFloat(0.7)
        
        let indicatorView : UIView  = UIView.init(frame: SizeView)
        indicatorView.tag = atIndex
        indicatorView.backgroundColor = Tools.colorApp()
        indicatorView.layer.cornerRadius = SizeView.size.height / 2;
        inButton.tag = atIndex
        indicatorView.alpha = 0;
        
        inButton.addTarget(self, action: #selector(labelSelectAction(button:)), for: UIControlEvents.touchUpInside)
        
        return indicatorView;
    }
    
    @IBAction func closeView()
    {
        if alternative == true {
            self.navigationController?.popViewController(animated: true)
        }
        else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }
    
    func setImageWithID(icon : UIImageView, id : String)
    {
        if id == "bathroom"
        {
            icon.image = iconBath
        }
        
        if id == "coffee"
        {
            icon.image = iconCoffee
        }
        
        if id == "food"
        {
            icon.image = iconFood
        }
        
        if id == "gifts"
        {
            icon.image = iconGifts
        }
        
        if id == "wardrobe"
        {
            icon.image = iconWard
        }
        
        if id == "wifi"
        {
            icon.image = iconWifi
        }
        
        if id == "Accessibility"
        {
            icon.image = iconAccessibility
        }
        
        if id == "ATM"
        {
            icon.image = iconATM
        }
        
    }
    
    func setServicesIcon(services : NSArray)
    {
        
        if services.count == 1
        {
            setImageWithID(icon: iconService1, id: services.object(at: 0) as! String)
        }
        
        if services.count == 2
        {
            setImageWithID(icon: iconService1, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2, id: services.object(at: 1) as! String)
        }
        
        if services.count == 3
        {
            setImageWithID(icon: iconService1, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3, id: services.object(at: 2) as! String)
        }
        
        if services.count == 4
        {
            setImageWithID(icon: iconService1, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4, id: services.object(at: 3) as! String)
        }
        
        if services.count == 5
        {
            setImageWithID(icon: iconService1, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5, id: services.object(at: 4) as! String)
        }
        
        if services.count == 6
        {
            setImageWithID(icon: iconService1, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6, id: services.object(at: 5) as! String)
        }
        
        if services.count == 7
        {
            setImageWithID(icon: iconService1, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6, id: services.object(at: 5) as! String)
            setImageWithID(icon: iconService7, id: services.object(at: 6) as! String)
        }
        
        if services.count == 8
        {
            setImageWithID(icon: iconService1, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6, id: services.object(at: 5) as! String)
            setImageWithID(icon: iconService7, id: services.object(at: 6) as! String)
            setImageWithID(icon: iconService8, id: services.object(at: 7) as! String)
        }
        
    }



}
