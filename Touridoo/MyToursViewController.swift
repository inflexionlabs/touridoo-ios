//
//  MyToursViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class MyToursViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet weak var tableReservation : UITableView!
    
    var arrayTours: NSMutableArray = NSMutableArray.init()
    var selectedIndexPath : IndexPath = IndexPath.init(row: 0, section: 0)
    var onRequest : Bool = false
    var showImages : Bool = false
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        super.viewDidLoad()
        
        tableReservation?.delegate = self
        tableReservation?.dataSource = self
        
        self.tableReservation.isUserInteractionEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Tools.hiddenBurgerButton(hidden: false)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : TourCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Tour_cell") as! TourCellTableViewCell
        
        let Tour : TourModel = arrayTours[indexPath.row] as! TourModel
        let nameString : String = Tour.name
        cell.name?.text = nameString
        
        cell.imageTour?.alpha = 0.5;
        cell.shadowImage?.alpha = 0;
        
        cell.setStars(stars: Tour.stars_average)
        
        if Tour.image?.count != 0
        {
            cell.imageTour?.image = UIImage.init(data: Tour.image!)
            
            UIView.animate(withDuration: 0.30, animations:
                {
                    cell.imageTour?.alpha = 1;
                    cell.shadowImage?.alpha = 1;
            })
        }
        else
        {
            var RealImage : Data?
            let image_url : String? = Tour.image_url
            
            if image_url != nil
            {
                if image_url != ""
                {
                    DispatchQueue.global().async
                        {
                            
                            if image_url != nil
                            {
                                let urlToDownload : URL = URL.init(string: image_url!)!
                                do
                                {
                                    RealImage = try  Data.init(contentsOf: urlToDownload)
                                }
                                catch
                                {
                                    print(error)
                                }
                            }
                            
                            DispatchQueue.main.async
                                {
                                    if RealImage != nil
                                    {
                                        cell.imageTour?.image = UIImage.init(data: RealImage!)
                                        Tour.image = RealImage
                                        
                                        UIView.animate(withDuration: 0.30, animations:
                                            {
                                                cell.imageTour?.alpha = 1;
                                                cell.shadowImage?.alpha = 1;
                                        })
                                    }
                            }
                    }
                }
            }
        }
        
        let hours : Int = Tour.duration_in_hours
        let hoursString : String = String(hours)
        cell.duration?.text = hoursString + " " + NSLocalizedString("hours", comment: "hours")
        
        let stingDate : String = Tour.date!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let dateObj = dateFormatter.date(from: stingDate)
        
        var hourString : String?
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm"
        hourString = dateFormatter.string(from: dateObj!)
        
        cell.dateTourlabel?.text = hourString
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let cell : TourCellTableViewCell = tableView.cellForRow(at: indexPath) as! TourCellTableViewCell
        cell.setSelected(false, animated: true)
        selectedIndexPath = indexPath
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 255
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTours.count
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        let session = sessionData.shared
        
        if session.ToursArray != nil
        {
            if session.ToursArray?.count != 0
            {
                arrayTours.removeAllObjects()
                for i in 0...((session.ToursArray?.count)! - 1)
                {
                    let currenTour : Tour = session.ToursArray?.object(at: i) as! Tour
                    
                    let image_url : String? = currenTour.image_url
                    
                    var RealImage : Data?
                    
                    if image_url != nil
                    {
                        let urlToDownload : URL = URL.init(string: image_url!)!
                        do {
                            RealImage = try  Data.init(contentsOf: urlToDownload)
                        } catch {
                            print(error)
                        }
                    }
                    
                    if RealImage == nil{
                        RealImage = Data.init()
                    }

                    var tourHasEnded : Bool = false
                    
                    let lastIndex = (currenTour.tourRun?.tour_steps?.count)! - 1
                    let finishTourIndex = (currenTour.tourRun?.tour_steps?.count)! - 2
                    let comingbackIndex = (currenTour.tourRun?.tour_steps?.count)! - 3
                    
                    let stepLast : TourStepModel = currenTour.tourRun?.tour_steps!.object(at: lastIndex) as! TourStepModel
                    let stepfinish : TourStepModel = currenTour.tourRun?.tour_steps!.object(at: finishTourIndex) as! TourStepModel
                    let stepcomingback: TourStepModel = currenTour.tourRun?.tour_steps!.object(at: comingbackIndex) as! TourStepModel
                    
                    if (stepLast.is_current_step == true) || (stepfinish.is_current_step == true) || (stepcomingback.is_current_step == true)
                    {
                        tourHasEnded = true
                    }
                    
                    if tourHasEnded == true
                    {
                        let Tourdata : TourModel = TourModel.init(id: currenTour.id!,
                                                                  image: RealImage,
                                                                  image_url: currenTour.image_url!,
                                                                  duration_in_hours: currenTour.duration_in_hours!,
                                                                  short_name: currenTour.name!,
                                                                  stars_average: currenTour.stars_average!,
                                                                  rates_count: currenTour.stars_average!,
                                                                  base10_stars_average: currenTour.base10_stars_average!,
                                                                  from_price: 0,date: currenTour.tourRun?.tour_date,
                                                                  type: "tour",
                                                                  TopTenNumber: 1,
                                                                  singlePrice: -1000,
                                                                  prices: currenTour.prices)
                        
                        arrayTours.add(Tourdata)
                    }
                }
            }
        }
        
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.showImages = true
            self.tableReservation.reloadData()
            self.tableReservation.isUserInteractionEnabled = true
            self.onRequest = true
        }
        
    }
    
}
