//
//  ReviewsTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 13/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class ReviewsTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var textReview : UITextView?
    @IBOutlet weak var nameLabel : UILabel?
    @IBOutlet weak var dateLabel : UILabel?
    @IBOutlet weak var countryLabel : UILabel?
    
    @IBOutlet weak var star1 : UIImageView?
    @IBOutlet weak var star2 : UIImageView?
    @IBOutlet weak var star3 : UIImageView?
    @IBOutlet weak var star4 : UIImageView?
    @IBOutlet weak var star5 : UIImageView?
    
    var starOff : UIImage = UIImage.init(named: "star_rate_off.png")!
    var starOn : UIImage = UIImage.init(named: "star_rate_on.png")!

    func setStars(stars : Int)
    {
        star1?.image = starOff
        star2?.image = starOff
        star3?.image = starOff
        star4?.image = starOff
        star5?.image = starOff
        
        if stars == 1
        {
            star1?.image = starOn
            star2?.image = starOff
            star3?.image = starOff
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 2
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOff
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 3
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 4
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOn
            star5?.image = starOff
        }
        
        if stars == 5
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOn
            star5?.image = starOn
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
