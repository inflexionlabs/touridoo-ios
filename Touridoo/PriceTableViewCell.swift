//
//  PriceTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 09/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PriceTableViewCell: UITableViewCell
{
    @IBOutlet weak var layerbordeView : UIView?
    @IBOutlet weak var centerView : UIView?
    @IBOutlet weak var priceLabel : UILabel?
    @IBOutlet weak var typeLabel : UILabel?
    @IBOutlet weak var offertLabel : UILabel?
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var currency: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    
//        layerbordeView?.layer.cornerRadius = ((layerbordeView?.frame.size.height)! / 2)
//        layerbordeView?.layer.borderColor = Tools.colorApp().cgColor
//        layerbordeView?.layer.borderWidth = 1;
//        layerbordeView?.backgroundColor = UIColor.clear
//
//        centerView?.backgroundColor = Tools.colorApp()
//        centerView?.layer.cornerRadius = ((centerView?.frame.size.height)! / 2)
//        centerView?.alpha = 0.45
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
