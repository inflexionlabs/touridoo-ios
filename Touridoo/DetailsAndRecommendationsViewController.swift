//
//  DetailsAndRecommendationsViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 16/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class DetailsAndRecommendationsViewController: UIViewController
{
    @IBOutlet weak var titleText : UILabel?
    @IBOutlet weak var scrollData  : UIScrollView?
    
    @IBOutlet weak var initWeatherView : UIView?
    @IBOutlet weak var finishWeatherView : UIView?
    
    @IBOutlet weak var textRecomendations : UITextView?
    @IBOutlet weak var textPick : UITextView?
    
    @IBOutlet weak var initialWeather : UILabel?
    @IBOutlet weak var initialImage : UIImageView?
    
    @IBOutlet weak var finalWeather : UILabel?
    @IBOutlet weak var finalImage : UIImageView?
    
    var latitude = ""
    var longitude = ""
    var fahren : Bool = true

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        initWeatherView?.layer.cornerRadius = 5
        initWeatherView?.layer.borderColor = Tools.colorAppSecondary().cgColor
        initWeatherView?.layer.borderWidth = 1
        
        finishWeatherView?.layer.cornerRadius = 5
        finishWeatherView?.layer.borderColor = Tools.colorAppSecondary().cgColor
        finishWeatherView?.layer.borderWidth = 1
        
        
        let tapInitila = UITapGestureRecognizer.init(target: self, action: #selector(self.changeDegrees(sender:)))
        initWeatherView?.addGestureRecognizer(tapInitila)
        
        let tapFinal = UITapGestureRecognizer.init(target: self, action: #selector(self.changeDegrees(sender:)))
        finishWeatherView?.addGestureRecognizer(tapFinal)
        if Tools.iPhone4(){
            scrollData?.contentSize = CGSize(width: (scrollData?.frame.size.width)!, height: (scrollData?.frame.size.height)! + CGFloat(10))
            Tools.AddHeight(View: textRecomendations!, Points: 20)
            Tools.AddHeight(View: textPick!, Points: 20)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        textRecomendations?.text = NSLocalizedString("For daytime activities, we recommend that participants wear sports clothing, comfortable walking shoes, sun screen lotion, a hat or cap, and bring a photo or video camera.", comment: "For daytime activities, we recommend that participants wear sports clothing, comfortable walking shoes, sun screen lotion, a hat or cap, and bring a photo or video camera.")
        
        textPick?.text = NSLocalizedString("We will pick you up at your hotel lobby. You must be 15 minutes before the indicated time", comment: "We will pick you up at your hotel lobby. You must be 15 minutes before the indicated time")
        
        Tools.hiddenBurgerButton(hidden: true)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
      
        let session = sessionData.shared
        
        if session.currentTour != nil
        {
            if session.currentTour?.tourRun != nil
            {
                if session.currentTour?.tourRun?.tour_steps != nil
                {
                    let steps : NSArray = (session.currentTour?.tourRun?.tour_steps)!
                    
                    for index in 0...(steps.count - 1)
                    {
                        if latitude == ""
                        {
                            let data : TourStepModel = steps[index] as! TourStepModel;
                            if data.latitude != nil
                            {
                                if data.latitude != ""
                                {
                                    latitude = data.latitude!
                                }
                            }
                        }
                        
                        if longitude == ""
                        {
                            let data : TourStepModel = steps[index] as! TourStepModel;
                            if data.longitude != nil
                            {
                                if data.longitude != ""
                                {
                                    longitude = data.longitude!
                                }
                            }
                        }
                    }
                }
            }
        }
        
        let yahooW = yahooWeather.init()
        let weather : weatherModel = yahooW.getFromWoeid(session.currentTour?.woeid)
      
        
        initialImage?.image = weather.initialWeatherType
        finalImage?.image = weather.finalWeatherType
        
        initialWeather?.text = weather.initialTemperature + "° F"
        finalWeather?.text = weather.finalTemperature + "° F"
        
        var languageID : String = Locale.current.languageCode!
        languageID = languageID.uppercased()
        if languageID == "ES"
        {
            let initial : String = yahooW.toCelsius(weather.initialTemperature)
            let final : String = yahooW.toCelsius(weather.finalTemperature)
            initialWeather?.text = initial + "° C"
            finalWeather?.text = final + "° C"
            
            fahren = false
        }
    }
    
    
    func changeDegrees(sender: UITapGestureRecognizer)
    {
        let session = sessionData.shared
        
        let yahooW = yahooWeather.init()
        let weather : weatherModel = yahooW.getFromWoeid(session.currentTour?.woeid)
        
        Tools.doRebound(viewEffect: sender.view!)
        
        if fahren == true
        {
            let initial : String = yahooW.toCelsius(weather.initialTemperature)
            let final : String = yahooW.toCelsius(weather.finalTemperature)
            initialWeather?.text = initial + "° C"
            finalWeather?.text = final + "° C"
        }
        else
        {
            initialImage?.image = weather.initialWeatherType
            finalImage?.image = weather.finalWeatherType
            
            initialWeather?.text = weather.initialTemperature + "° F"
            finalWeather?.text = weather.finalTemperature + "° F"
        }
        
        fahren = !fahren
    }
    
    @IBAction func closeView()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                Tools.PushViewCenter(View: self.view, Points: self.view.frame.size.width)
                
        }, completion:
            { _ in
                
                self.view.removeFromSuperview()
                self.parent?.removeFromParentViewController()
                
                Tools.hiddenBurgerButton(hidden: false)
        })
        
    }

    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }

}
