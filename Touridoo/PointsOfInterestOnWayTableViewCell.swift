//
//  PointsOfInterestOnWayTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 29/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PointsOfInterestOnWayTableViewCell: UITableViewCell
{
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var statyLabel : UILabel?
    
    @IBOutlet weak var iconOnWay : UIView?
    @IBOutlet weak var detailLabel : UILabel?
    @IBOutlet weak var timeLabel : UILabel?
    @IBOutlet weak var iconDetail : UIView?
    
    @IBOutlet weak var iconService1 : UIImageView?
    @IBOutlet weak var iconService2 : UIImageView?
    @IBOutlet weak var iconService3 : UIImageView?
    @IBOutlet weak var iconService4 : UIImageView?
    @IBOutlet weak var iconService5 : UIImageView?
    @IBOutlet weak var iconService6 : UIImageView?
    @IBOutlet weak var iconService7 : UIImageView!
    @IBOutlet weak var iconService8 : UIImageView!
    
    var iconBath : UIImage = UIImage.init(named: "bathroom.png")!
    var iconCoffee : UIImage = UIImage.init(named: "coffee.png")!
    var iconFood : UIImage = UIImage.init(named: "food.png")!
    var iconGifts : UIImage = UIImage.init(named: "gifts.png")!
    var iconWard : UIImage = UIImage.init(named: "wardrobe.png")!
    var iconWifi : UIImage = UIImage.init(named: "wifi.png")!
    var iconAccessibility : UIImage = UIImage.init(named: "servicesAcces.png")!
    var iconATM : UIImage = UIImage.init(named: "servicesATM.png")!
    
    @IBOutlet weak var arrivalLabel : UILabel?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        arrivalLabel?.adjustsFontSizeToFitWidth = true
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setImageWithID(icon : UIImageView, id : String)
    {
        if id == "bathroom"
        {
            icon.image = iconBath
        }
        
        if id == "coffee"
        {
            icon.image = iconCoffee
        }
        
        if id == "food"
        {
            icon.image = iconFood
        }
        
        if id == "gifts"
        {
            icon.image = iconGifts
        }
        
        if id == "wardrobe"
        {
            icon.image = iconWard
        }
        
        if id == "wifi"
        {
            icon.image = iconWifi
        }
        
        if id == "Accessibility"
        {
            icon.image = iconAccessibility
        }
        
        if id == "ATM"
        {
            icon.image = iconATM
        }
        
    }
    
    func setServicesIcon(services : NSArray)
    {
    
        if services.count == 1
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
        }
        
        if services.count == 2
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
        }
        
        if services.count == 3
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
        }
        
        if services.count == 4
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
        }
        
        if services.count == 5
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5!, id: services.object(at: 4) as! String)
        }
        
        if services.count == 6
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5!, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6!, id: services.object(at: 5) as! String)
        }
        
        if services.count == 7
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5!, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6!, id: services.object(at: 5) as! String)
            setImageWithID(icon: iconService7!, id: services.object(at: 6) as! String)
        }
        
        if services.count == 8
        {
            setImageWithID(icon: iconService1!, id: services.object(at: 0) as! String)
            setImageWithID(icon: iconService2!, id: services.object(at: 1) as! String)
            setImageWithID(icon: iconService3!, id: services.object(at: 2) as! String)
            setImageWithID(icon: iconService4!, id: services.object(at: 3) as! String)
            setImageWithID(icon: iconService5!, id: services.object(at: 4) as! String)
            setImageWithID(icon: iconService6!, id: services.object(at: 5) as! String)
            setImageWithID(icon: iconService7!, id: services.object(at: 6) as! String)
            setImageWithID(icon: iconService8!, id: services.object(at: 7) as! String)
        }
        
    }

}
