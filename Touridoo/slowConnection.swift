//
//  slowConnection.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 26/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class slowConnection: UIViewController {

    @IBOutlet weak var viewConnection : UIView?
    @IBOutlet weak var textDisplay : UILabel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        textDisplay?.text = NSLocalizedString("Your Internet connection is slow, please wait while we try to recover the connection", comment: "Your Internet connection is slow, please wait while we try to recover the connection")
        textDisplay?.adjustsFontSizeToFitWidth = true
        
        if viewConnection != nil
        {
            viewConnection?.alpha = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        Tools.DownView(View: self.viewConnection!, Points: 100)
    }
    
    func show()
    {
        viewConnection?.alpha = 1
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 1
                
                Tools.UpView(View: self.viewConnection!, Points: 100)
                
        }, completion:
            { _ in
        })
    }
    
    func hidden()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
                Tools.DownView(View: self.viewConnection!, Points: 100)
                
        }, completion:
            { _ in
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
                
                if MainMenuApp.slowConnectionView != nil
                {
                    MainMenuApp.slowConnectionView.view.removeFromSuperview()
                }
                
                if MainMenuApp.slowConnectionView != nil
                {
                    MainMenuApp.slowConnectionView = nil
                }
                
        })
    }

}
