//
//  MainMenuViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 30/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController, ResponseServicesProtocol
{
    var alertView : AlertWindow!
    var withoutConnectionView : withoutConnection!
    var slowConnectionView : slowConnection!
    var noSearchResultView : noSearchResults!
    var doneView : doneView!
    
    @IBOutlet weak var childViewOutlet : UIView?
    @IBOutlet weak var burgerButton : UIButton?
    
    @IBOutlet weak var OnTourButton : UIButton?
    @IBOutlet weak var OnTourButtonIcon : UIImageView?
    @IBOutlet weak var OnTourBlockView : UIView?
    
    @IBOutlet weak var myReservationsButton : UIButton?
    @IBOutlet weak var myReservationsButtonIcon : UIImageView?
    
    @IBOutlet weak var myToursButton : UIButton?
    @IBOutlet weak var myToursIcon : UIImageView?
    
    @IBOutlet weak var myProfileButton : UIButton?
    @IBOutlet weak var myProfileIcon : UIImageView?
    
    @IBOutlet weak var logoutButton : UIButton?
    @IBOutlet weak var logoutIcon : UIImageView?
    
    @IBOutlet weak var notificationsButton : UIButton?
    @IBOutlet weak var notificationsIcon : UIImageView?

    @IBOutlet weak var goStartButton: UIButton!
    @IBOutlet weak var goStartView: UIView!
    
    var currentViewController : UIViewController?

    var mainMenuIsOpen : Bool = false
    var originalCenter : CGPoint = CGPoint(x: 0, y: 0)
    var parcialCenter : CGPoint = CGPoint(x: 0, y: 0)
    var maximumDisplacement : CGFloat = 2.60;
    var panGestureTouchInit : CGPoint = CGPoint(x: 0, y: 0)
    var rightGesture : Bool = false
    var setUpInitialView = false
    var currentIndex : Int?
    var panGesture : UIPanGestureRecognizer?
    
    @IBOutlet weak var containerView : UIView?
    @IBOutlet weak var logoutView : UIView?
    var onTourMode = false
    var onTourUIConfig = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        
        OnTourBlockView?.backgroundColor = UIColor.clear
        
        if onTourUIConfig == false
        {
            onTourUIConfig = true
            
            Tools.UpView(View: containerView!, Points: 40)
            Tools.DownView(View: logoutView!, Points: 40)
            
            if Tools.iPhoneX()
            {
                Tools.DownView(View: burgerButton!, Points: 10)
                Tools.UpView(View: logoutView!, Points: 40)
                Tools.DownView(View: containerView!, Points: 30)
                Tools.AddHeight(View: (OnTourButton?.superview!)!, Points: 30)
            }
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.MainMenuApp = self
        
        childViewOutlet?.center = self.view.center
 
        if setUpInitialView == true
        {
            return
        }
        
        setUpInitialView = true
        
        panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(moveChildView(gesture:)))
        childViewOutlet?.addGestureRecognizer(panGesture!)
        
        //Shadow
        childViewOutlet?.layer.shadowColor = UIColor.black.cgColor
        childViewOutlet?.layer.shadowOpacity = 1
        childViewOutlet?.layer.shadowOffset = CGSize.zero
        childViewOutlet?.layer.shadowRadius = 5
        
        if UserDefaults.standard.bool(forKey: "showReservationView") == true
        {
            let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            
            let MenuViewController : MyReservationsViewController  = storyboard.instantiateViewController(withIdentifier: "MyReservationsViewController") as! MyReservationsViewController
            MenuViewController.view.frame = UIScreen.main.bounds
            currentViewController = MenuViewController
            
            //Create a Navigation Controller
            let navigationControllerList : UINavigationController = UINavigationController.init(rootViewController: MenuViewController)
            navigationController?.view.frame = UIScreen.main.bounds
            
            navigationController?.isNavigationBarHidden = true
            navigationController?.setNavigationBarHidden(true, animated: false)
            
            childViewOutlet?.addSubview(navigationControllerList.view)
            self.addChildViewController(navigationControllerList)
            
            childViewOutlet?.bringSubview(toFront: (burgerButton)!)
            return
        }
        
        //Set Main View (Tour list controller)
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Tours", bundle: nil)
        
        //Tours List Controller
        let MenuViewController : ToursListViewController  = storyboard.instantiateViewController(withIdentifier: "ToursListViewController") as! ToursListViewController
        MenuViewController.view.frame = UIScreen.main.bounds
        currentViewController = MenuViewController
        
        //Create a Navigation Controller
        let navigationControllerList : UINavigationController = UINavigationController.init(rootViewController: MenuViewController)
        navigationController?.view.frame = UIScreen.main.bounds
        
        navigationController?.isNavigationBarHidden = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        childViewOutlet?.addSubview(navigationControllerList.view)
        self.addChildViewController(navigationControllerList)
        
        childViewOutlet?.bringSubview(toFront: (burgerButton)!)
        
        if Tools.iPhone4()
        {
            Tools.AddHeight(View: goStartView, Points: 45)
        }
        
    }
    
    func updateButtonsUI()
    {
        self.myProfileButton?.isEnabled = true
        self.myProfileButton?.alpha = 1
        self.myProfileIcon?.alpha = 1
        
        self.notificationsButton?.isEnabled = true
        self.notificationsButton?.alpha = 1
        self.notificationsIcon?.alpha = 1
        
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        
        if UserDefaults.standard.bool(forKey: "hasTour") == true
        {
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.OnTourButton?.isEnabled = true
                self.OnTourButton?.alpha = 1
                self.OnTourButtonIcon?.alpha = 1
                self.OnTourBlockView?.alpha = 0;
                
                if self.onTourMode == false
                {
                    self.onTourMode = true
                    
                    Tools.DownView(View: self.containerView!, Points: 40)
                    Tools.UpView(View: self.logoutView!, Points: 40)
                }
            }
            
        }
        
    
        if token == nil
        {
            
            self.logoutButton?.isEnabled = false
            self.logoutButton?.alpha = 0.6
            self.logoutIcon?.alpha = 0.6
            
            let code : String? = UserDefaults.standard.object(forKey: "bookingcode") as? String
            
            if (UserDefaults.standard.bool(forKey: "bookingcode_mode") == true) && (code != nil)
            {
                self.logoutButton?.isEnabled = true
                self.logoutButton?.alpha = 1
                self.logoutIcon?.alpha = 1
            }
            
            
            self.myToursButton?.isEnabled = false
            self.myToursButton?.alpha = 0.6
            self.myToursIcon?.alpha = 0.6
            
            self.notificationsButton?.isEnabled = false
            self.notificationsButton?.alpha = 0.6
            self.notificationsIcon?.alpha = 0.6
        }
        else
        {
            self.logoutButton?.isEnabled = true
            self.logoutButton?.alpha = 1
            self.logoutIcon?.alpha = 1
    
            /*  Por el momento no necesita que se habilite 
            self.notificationsButton?.isEnabled = true
            self.notificationsButton?.alpha = 1
            self.notificationsIcon?.alpha = 1*/
            
            self.notificationsButton?.isEnabled = false
            self.notificationsButton?.alpha = 0.6
            self.notificationsIcon?.alpha = 0.6
        }
        
        let session = sessionData.shared
        
        if session.ToursArray != nil
        {
            if session.ToursArray?.count != 0
            {
                
                for i in 0...((session.ToursArray?.count)! - 1)
                {
                    let currenTour : Tour = session.ToursArray?.object(at: i) as! Tour
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    
                    var tourHasEnded : Bool = false
                    
                    if currenTour.tourRun?.tour_steps != nil
                    {
                        if currenTour.tourRun?.tour_steps?.count != 0
                        {
                            if (currenTour.tourRun?.tour_steps?.count)! > 3
                            {
                                let lastIndex = (currenTour.tourRun?.tour_steps?.count)! - 1
                                let finishTourIndex = (currenTour.tourRun?.tour_steps?.count)! - 2
                                let comingbackIndex = (currenTour.tourRun?.tour_steps?.count)! - 3
                        
                                let stepLast : TourStepModel = currenTour.tourRun?.tour_steps!.object(at: lastIndex) as! TourStepModel
                                let stepfinish : TourStepModel = currenTour.tourRun?.tour_steps!.object(at: finishTourIndex) as! TourStepModel
                                let stepcomingback: TourStepModel = currenTour.tourRun?.tour_steps!.object(at: comingbackIndex) as! TourStepModel
                                
                                if (stepLast.is_current_step == true) || (stepfinish.is_current_step == true) || (stepcomingback.is_current_step == true)
                                {
                                     tourHasEnded = true
                                }
                                
                            }
                           
                            
                        }
                        
                    }
                    
                    let dispatchTime = DispatchTime.now() + 0.30;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                    {
                        if tourHasEnded != false
                        {
                            self.myToursButton?.isEnabled = true
                            self.myToursButton?.alpha = 1
                            self.myToursIcon?.alpha = 1
                        }
                    
                    }
                    
                }
            }
        }
        
    }
    
    func updateTourStatus()
    {
        if UserDefaults.standard.bool(forKey: "hasTour") == false
        {
            return
        }
        
        cleanChildView()
        
        setOnTourControllerByState()
        
        childViewOutlet?.bringSubview(toFront: (burgerButton)!)
        OnTourButton?.isUserInteractionEnabled = true
    }
    
    func setOnTourControllerByState()
    {
        if UserDefaults.standard.bool(forKey: "hasTour") == true
        {
            OnTourButton?.isEnabled = true
            OnTourButton?.alpha = 1
            OnTourButtonIcon?.alpha = 1
            self.OnTourBlockView?.alpha = 0
            
            if !onTourMode
            {
                onTourMode = true
                
                Tools.DownView(View: containerView!, Points: 40)
                Tools.UpView(View: logoutView!, Points: 40)
            }
        }
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "OnTour", bundle: nil)
        
        let tourState = UserDefaults.standard.integer(forKey: "tourState")
        
        /*
         tourState == 0 -> No ha iniciado (como lo tenemos actualmente)
         tourState == 1 -> Traslado a un punto de encuentro (para recoger al usuario)
         tourState == 2 -> Llego al punto de encuentro (donde esta el usuario)
         tourState == 3 -> Camino a un punto de interés
         tourState == 4 -> En un punto de interés
         tourState == 5 -> El tour ha terminado
         tourState == 6 -> El tour cerrado
         */
        
        if tourState == 6
        {
            //The tour has finished
            let OnTourFinishedController = storyboard.instantiateViewController(withIdentifier: "OnTourFinishedBookingCodeViewController") as! OnTourFinishedBookingCodeViewController
            currentViewController = OnTourFinishedController
            
            childViewOutlet?.addSubview(OnTourFinishedController.view)
            self.addChildViewController(OnTourFinishedController)
            
        }else if tourState == 5
        {
            //The tour has finished
            let OnTourFinishedController = storyboard.instantiateViewController(withIdentifier: "OnTourFinishedViewController") as! OnTourFinishedViewController
            currentViewController = OnTourFinishedController
            
            childViewOutlet?.addSubview(OnTourFinishedController.view)
            self.addChildViewController(OnTourFinishedController)
        
        }
        else if (tourState == 3) || (tourState == 4)
        {
            //The tour has started
            let onTourStateController = storyboard.instantiateViewController(withIdentifier: "onTourStepsViewController") as! onTourStepsViewController
            
            currentViewController = onTourStateController
            
            childViewOutlet?.addSubview(onTourStateController.view)
            self.addChildViewController(onTourStateController)
        
            
        }else if (tourState == 1) || (tourState == 2) {
            
            let MenuViewController : OnTourViewController  = storyboard.instantiateViewController(withIdentifier: "OnTourViewController") as! OnTourViewController
            
            currentViewController = MenuViewController
            
            childViewOutlet?.addSubview(MenuViewController.view)
            self.addChildViewController(MenuViewController)
        }
    }
    
    func testViewControllerHasInitial()
    {
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Tours", bundle: nil)
        let MenuViewController : PaymentMethodViewController  = storyboard.instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
        currentViewController = MenuViewController
        
        childViewOutlet?.addSubview(MenuViewController.view)
        self.addChildViewController(MenuViewController)
        
        childViewOutlet?.bringSubview(toFront: (burgerButton)!)
    }

    //Selected menu option
    
    //Select Tours
    @IBAction func selectTours()
    {
    
        if currentViewController != nil{
            if (currentViewController?.isKind(of: ToursListViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Tours", bundle: nil)

        let MenuViewController : ToursListViewController  = storyboard.instantiateViewController(withIdentifier: "ToursListViewController") as! ToursListViewController
        MenuViewController.view.frame = UIScreen.main.bounds
        currentViewController = MenuViewController
        
        //Create a Navigation Controller
        let navigationControllerList : UINavigationController = UINavigationController.init(rootViewController: MenuViewController)
        navigationController?.view.frame = UIScreen.main.bounds
        
        navigationController?.isNavigationBarHidden = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        childViewOutlet?.addSubview(navigationControllerList.view)
        self.addChildViewController(navigationControllerList)
        
        childViewOutlet?.bringSubview(toFront: (burgerButton)!)
        
        closeMenu()

    }
    
    //Select ON TOUR
    @IBAction func selectONTOUR()
    {
        if UserDefaults.standard.bool(forKey: "hasTour") == false {
            return
        }
        
        if currentViewController != nil{
            if (currentViewController?.isKind(of: OnTourViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        setOnTourControllerByState()
        childViewOutlet?.bringSubview(toFront: (burgerButton)!)
        
        closeMenu()
    }
    
    //Select Profile
    @IBAction func selectProfile()
    {
        if currentViewController != nil{
            if (currentViewController?.isKind(of: ProfileViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let MenuViewController : ProfileViewController  = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
        putCurrentController(current: MenuViewController)
    }
    
    //Select my reservations
    @IBAction func selectMyReservations()
    {
        if currentViewController != nil{
            if (currentViewController?.isKind(of: MyReservationsViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let MenuViewController : MyReservationsViewController  = storyboard.instantiateViewController(withIdentifier: "MyReservationsViewController") as! MyReservationsViewController
        MenuViewController.view.frame = UIScreen.main.bounds
        currentViewController = MenuViewController
        
        //Create a Navigation Controller
        let navigationControllerList : UINavigationController = UINavigationController.init(rootViewController: MenuViewController)
        navigationController?.view.frame = UIScreen.main.bounds
        
        navigationController?.isNavigationBarHidden = true
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        childViewOutlet?.addSubview(navigationControllerList.view)
        self.addChildViewController(navigationControllerList)
        
        childViewOutlet?.bringSubview(toFront: (burgerButton)!)
        
        closeMenu()
    }
    
    //Select my tours
    @IBAction func selectMyTours()
    {
        if currentViewController != nil{
            if (currentViewController?.isKind(of: MyToursViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let MenuViewController : MyToursViewController  = storyboard.instantiateViewController(withIdentifier: "MyToursViewController") as! MyToursViewController
        
        putCurrentController(current: MenuViewController)
    }
    
    //Select FAQ
    @IBAction func selectFAQ()
    {
        if currentViewController != nil{
            if (currentViewController?.isKind(of: FAQViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let MenuViewController : FAQViewController  = storyboard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
        
        putCurrentController(current: MenuViewController)
        
    }
    
    //Select Contact
    @IBAction func selectContact()
    {
        
        if currentViewController != nil{
            if (currentViewController?.isKind(of: ContactViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let MenuViewController : ContactViewController  = storyboard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        
        putCurrentController(current: MenuViewController)
        
    }
    
    //Select Notifications
    @IBAction func selectNotifications()
    {
        
        if currentViewController != nil{
            if (currentViewController?.isKind(of: notificationsViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let MenuViewController : notificationsViewController  = storyboard.instantiateViewController(withIdentifier: "notificationsViewController") as! notificationsViewController
        
        putCurrentController(current: MenuViewController)
        
    }
    
    //Select tutorial
    @IBAction func selectTutorial()
    {
        closeMenu()
        
        //Open tutorial
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let tutorialView : IntroTutorialViewController = storyboard.instantiateViewController(withIdentifier: "IntroTutorialViewController") as! IntroTutorialViewController
        
        tutorialView.view.frame = UIScreen.main.bounds
        tutorialView.view.alpha = 0
        tutorialView.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        
        self.view.addSubview(tutorialView.view)
        self.addChildViewController(tutorialView)
        
        UIView.animate(withDuration: 0.30, animations:
            {
                tutorialView.view.alpha = 1
                tutorialView.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
        
    }
    
    //Select Privacy Policy
    @IBAction func selectPrivacyPolicy()
    {
        if currentViewController != nil{
            if (currentViewController?.isKind(of: PrivacyPolicyViewController.self))!{
                closeMenu()
                return
            }
        }
        
        cleanChildView()
        
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let MenuViewController : PrivacyPolicyViewController  = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        
        putCurrentController(current: MenuViewController)
        
    }
    
    //Select Logout
    @IBAction func selectLogOut()
    {
        let AlertView = UIAlertController(title: NSLocalizedString("Do you want to logout?", comment: "Do you want to logout?"),
                                          message: NSLocalizedString("", comment: ""),
                                          preferredStyle: UIAlertControllerStyle.alert)
        
        AlertView.addAction(UIAlertAction(title: NSLocalizedString("Logout", comment: "Logout"),
                                          style: UIAlertActionStyle.default,
                                          handler:closeAction))
        
        AlertView.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"),
                                          style: UIAlertActionStyle.cancel,
                                          handler: cancelAction))
        
        self.present(AlertView, animated: true, completion: nil)
    }
    
    func closeAction(alert: UIAlertAction!)
    {
        let session = sessionData.shared
        
        session.ToursArray = nil
        session.currentTour = nil
        session.currentTourImage = nil
        session.User = nil
    
        UserDefaults.standard.set(false, forKey: "hasTour")
        UserDefaults.standard.set(nil, forKey: "token")
        UserDefaults.standard.set(false, forKey: "hasData")
        UserDefaults.standard.set(-1, forKey: "tourState")
        UserDefaults.standard.set(false, forKey: "showReservationView");
        UserDefaults.standard.set(false, forKey: "device_registered")
        UserDefaults.standard.set(nil, forKey: "email")
        UserDefaults.standard.set(nil, forKey: "password")
        UserDefaults.standard.set(false, forKey: "bookingcode_mode")
        UserDefaults.standard.set("", forKey: "bookingcode")
        
        if self.onTourMode == true
        {
            self.onTourMode = false
            
            Tools.UpView(View: self.containerView!, Points: 40)
            Tools.DownView(View: self.logoutView!, Points: 40)
        }
        
        self.updateButtonsUI()
        selectTours()
    }
    
    func cancelAction(alert: UIAlertAction!) {
        print("Cancel")
    }
    

    func putCurrentController(current : UIViewController)
    {
        current.view.frame = UIScreen.main.bounds;
        currentViewController = current
        childViewOutlet?.addSubview(current.view)
        self.addChildViewController(current)
        
        childViewOutlet?.bringSubview(toFront: (burgerButton)!)
        burgerButton?.isHidden = false
        closeMenu()
    }
    
    
    func cleanChildView()
    {
        for subViews : UIView in (childViewOutlet?.subviews)!
        {
            if subViews != burgerButton
            {
                subViews.removeFromSuperview();
            }
        }
    }
    
    func requestForNotifications()
    {
        let Service = TouridooServices.init(delegate: self)
        let deviceID : String? = UserDefaults.standard.object(forKey: "device_token") as? String
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        
        if deviceID != nil && (UserDefaults.standard.bool(forKey: "device_registered") == false) && (token != nil)
        {
            Service.RegisterForPushNotifications(deciveID: deviceID!)
        }
    }
    

    override func viewDidAppear(_ animated: Bool)
    {
        updateButtonsUI()
        
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        
        if token != nil
        {
            requestForNotifications()
        }
        
        if originalCenter.x == 0 && originalCenter.y == 0 {
            originalCenter = self.view.center
        }
        
        if parcialCenter.x == 0 && parcialCenter.y == 0
        {
            var newCenter = self.childViewOutlet?.center
            newCenter?.x = self.view.center.x * self.maximumDisplacement
            parcialCenter = newCenter!
        }

    }
    
    func updateData()
    {
        currentIndex = UserDefaults.standard.integer(forKey: "tourState")
        print("Start update data")
        
        let code : String? = UserDefaults.standard.object(forKey: "bookingcode") as? String
        
        if (UserDefaults.standard.bool(forKey: "bookingcode_mode") == true) && (code != nil)
        {
            let Service = TouridooServices.init(delegate: self)
            Service.bookingCode(code: code!, deviceID: "")
        }
        else
        {
            let Service = TouridooServices.init(delegate: self)
            Service.GetMyTours()
        }
    }
    
    func updateDataAfterLogin()
    {
        let Service = TouridooServices.init(delegate: self)
        Service.GetMyTours()
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.GET_MY_TOURS || name == ServiceName.BOOKING_CODE
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                    loadTourData.updateData(content: content)
                    loadTourData.updateFlags()
                    self.updateButtonsUI()
                    
                    let newIndex = UserDefaults.standard.integer(forKey: "tourState")
                    
                    if newIndex != currentIndex
                    {
                        let dispatchTime = DispatchTime.now() + 0.31;
                        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                        {
                            loadTourData.updateUI()
                        }
                    }
                    
                }
            }
            
        }
    }
    
    func onError(Error : String, name : ServiceName)
    {
        
    }

    @IBAction func changeMenuState()
    {
        Tools.feedback()
        
        if isOpen() == false
        {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:
                {
                    var newCenter = self.childViewOutlet?.center
                    newCenter?.x = self.view.center.x * self.maximumDisplacement
                    self.childViewOutlet?.center = newCenter!
                    
                    //Tools.PullViewCenter(View: self.view, Points: 20)
                    
                    
            }, completion:nil)
            
            putGestureMenu()
        }
        else
        {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:
                {
                    self.childViewOutlet?.center = self.originalCenter
                    
                    //Tools.PushViewCenter(View: self.view, Points: 20)
                    
            }, completion:nil)
            
            removeGestureMenu()
        }
    }
    
    func tapGestureAction()
    {
        if mainMenuIsOpen
        {
            mainMenuIsOpen = false;
            UIView.animate(withDuration: 0.20, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:
                {
                    self.childViewOutlet?.center = self.originalCenter
                    
            }, completion:nil)
            
            removeGestureMenu()
        }
    }
    
    func openMenu()
    {
        mainMenuIsOpen = true;
        UIView.animate(withDuration: 0.20, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:
            {
                var newCenter = self.childViewOutlet?.center
                newCenter?.x = self.view.center.x * self.maximumDisplacement
                self.childViewOutlet?.center = newCenter!
                
        }, completion:nil)
        
        putGestureMenu()
        
    }
    
    func closeMenu()
    {
        mainMenuIsOpen = false;
        UIView.animate(withDuration: 0.20, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:
            {
                self.childViewOutlet?.center = self.originalCenter
                
        }, completion:nil)
        
        removeGestureMenu()
    }
    
    func putGestureMenu()
    {
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapGestureAction))
        tapGesture.numberOfTapsRequired = 1;
        tapGesture.view?.tag = 94;
        childViewOutlet?.addGestureRecognizer(tapGesture)
    }
    
    func removeGestureMenu()
    {
        for gesture : UIGestureRecognizer in (childViewOutlet?.gestureRecognizers)!
        {
            if gesture.isKind(of: UITapGestureRecognizer.classForCoder())
            {
                childViewOutlet?.removeGestureRecognizer(gesture)
            }
        }
    }
    
    func moveChildView(gesture: UIPanGestureRecognizer)
    {
    
        if gesture.state == UIGestureRecognizerState.began
        {
            UIApplication.shared.keyWindow?.endEditing(true)
            
            let velocity  = gesture.velocity(in: self.view)
            if  velocity.x > 0 {
                Tools.feedback()
            }
        }
    
        if gesture.state == UIGestureRecognizerState.changed
        {
            
            if panGestureTouchInit.x == 0 && panGestureTouchInit.y == 0 {
                panGestureTouchInit = gesture.translation(in: self.view)
            }
            
            let viewCenterX : CGFloat = self.view.center.x
            let maximumCenterX : CGFloat = viewCenterX * self.maximumDisplacement
            let childViewCenterX : CGFloat = (self.childViewOutlet?.center.x)!
            
            if (!(childViewCenterX > maximumCenterX) && !(childViewCenterX < viewCenterX))
            {
                let velocity  = gesture.velocity(in: self.view)
                
//                print("view: " + String(describing: viewCenterX))
//                print("child: " + String(describing: childViewCenterX))
//                print("velocity: " + String(describing: velocity.x))
                
                if ((childViewCenterX <= viewCenterX) && (velocity.x <= 0))
                {
                    return
                }

                let centerTouch = gesture.translation(in: self.view)
                let moveScale : CGFloat  = self.panGestureTouchInit.x + centerTouch.x;
                
                if mainMenuIsOpen {
                    self.childViewOutlet?.center = CGPoint(x: parcialCenter.x + moveScale, y: parcialCenter.y)
                }
                else {
                    self.childViewOutlet?.center = CGPoint(x: originalCenter.x + moveScale, y: originalCenter.y)
                }
                
                if velocity.x != 0 {
                    if  velocity.x > 0 {
                        rightGesture = true
                    }else{
                        rightGesture = false;
                    }
                }
            }
        }
        
        if gesture.state == UIGestureRecognizerState.ended
        {
            print("end touch")
            
            panGestureTouchInit = CGPoint(x: 0, y: 0)
            
            if rightGesture
            {
                openMenu()
            }
            else
            {
                closeMenu()
            }
        }
    }
    
    func isOpen() -> Bool {
        
        mainMenuIsOpen = !mainMenuIsOpen
        return !mainMenuIsOpen
    }
    
    func showTutorial()
    {
        
        if UserDefaults.standard.bool(forKey: "tutorial") == false
        {
            //Open tutorial
            let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let tutorialView : IntroTutorialViewController = storyboard.instantiateViewController(withIdentifier: "IntroTutorialViewController") as! IntroTutorialViewController
            
            tutorialView.view.frame = UIScreen.main.bounds
        
            self.view.addSubview(tutorialView.view)
            self.addChildViewController(tutorialView)
            
            UIView.animate(withDuration: 0.30, animations:
                {
                    tutorialView.view.alpha = 1
                    tutorialView.view.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent;
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
}
