//
//  noSearchResults.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 09/08/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class noSearchResults: UIViewController {

    @IBOutlet weak var viewConnection : UIView?
    @IBOutlet weak var textDisplay : UILabel?
    @IBOutlet weak var detailImage : UIImageView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        textDisplay?.text = NSLocalizedString("No search results", comment: "No search results")
        textDisplay?.adjustsFontSizeToFitWidth = true
        
        if viewConnection != nil
        {
            viewConnection?.alpha = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
       
        Tools.UpView(View: self.viewConnection!, Points: 50)
    }
    
    func show()
    {
        viewConnection?.alpha = 1
        
        UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 1
                
                 Tools.DownView(View: self.viewConnection!, Points: 50)
                
        }, completion:
            { _ in
        })
    }
    
    func hidden()
    {
        UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
                Tools.UpView(View: self.viewConnection!, Points: 50)
                
        }, completion:
            { _ in
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
                
                if MainMenuApp.noSearchResultView != nil
                {
                    MainMenuApp.noSearchResultView.view.removeFromSuperview()
                }
                
                if MainMenuApp.noSearchResultView != nil
                {
                    MainMenuApp.noSearchResultView = nil
                }
                
        })
    }

}
