//
//  yahooWeather.m
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 14/09/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

#import "yahooWeather.h"
#import <UIKit/UIKit.h>

@implementation yahooWeather

-(weatherModel *)getFromWoeid:(NSString *)woeid
{
    YQL *yql = [[YQL alloc] init];
    
    if ([woeid isKindOfClass:[NSNull class]])
    {
        woeid = @"116545";
    }
    
    if (woeid == nil)
    {
        woeid = @"116545";
    }
    
    if ([woeid isEqualToString:@""])
    {
        woeid = @"116545";
    }
    
    weatherModel *model = [[weatherModel alloc] init];
    
    NSString *queryString = [NSString stringWithFormat:@"select item from weather.forecast where woeid=%@",woeid];
    NSDictionary *results = [yql query:queryString];
    NSLog(@"%@",results[@"query"][@"count"]);
    NSLog(@"%@",results[@"query"][@"results"]);

    if (![results[@"query"][@"results"] isKindOfClass:[NSNull class]])
    {
        NSDictionary *channel = results[@"query"][@"results"][@"channel"];
        NSDictionary *item = channel[@"item"];
        
        NSDictionary *condition = item[@"condition"];
        NSString *initialTemperature = condition[@"temp"];
        NSString *initialWeatherType = condition[@"text"];
        
        NSArray *forecast = item[@"forecast"];
        NSDictionary * forecastDic = forecast[0];
        NSString *finalTemperature = forecastDic[@"low"];
        NSString *finalWeatherType = forecastDic[@"text"];
        
        model.initialTemperature = initialTemperature;
        model.initialWeatherType = [self imagebyWeatherType:initialWeatherType];
        model.finalTemperature = finalTemperature;
        model.finalWeatherType = [self imagebyWeatherType:finalWeatherType];
     
        return model;
    }
    
    return model;
    
}

-(NSString *)toCelsius:(NSString *)degrees
{
    float degressNumber = [degrees floatValue];
    degressNumber = degressNumber - 32;
    degressNumber = degressNumber / 1.8;
    int real = degressNumber;
    return [NSString stringWithFormat:@"%i",real];
}

-(UIImage *)imagebyWeatherType:(NSString *)type
{
    UIImage *image = [UIImage imageNamed:@"w_soleado.png"];
    
    if ([type isEqualToString:@"Partly Cloudy"] || [type isEqualToString:@"partly cloudy (day)"] || [type isEqualToString:@"mostly cloudy (night)"] || [type isEqualToString:@"partly cloudy"]) {
        image = [UIImage imageNamed:@"w_nublado.parcial.png"];
    }
    
    if ([type isEqualToString:@"Cloudy"] || [type isEqualToString:@"Mostly Cloudy"] || [type isEqualToString:@"mostly cloudy (day)"]) {
        image = [UIImage imageNamed:@"w_nublado.png"];
    }
    
    if ([type isEqualToString:@"severe thunderstorms"]) {
        image = [UIImage imageNamed:@"w_tomernta.con.lluvia.png"];
    }
    
    if ([type isEqualToString:@"thunderstorms"]) {
        image = [UIImage imageNamed:@"w_tormenta.electrica.png"];
    }
    
    if ([type isEqualToString:@"freezing rain"]) {
        image = [UIImage imageNamed:@"w_tormenta.nieve.png"];
    }
    
    if ([type isEqualToString:@"windy"]) {
        image = [UIImage imageNamed:@"w_viento.png"];
    }
    
    if ([type isEqualToString:@"Scattered Thunderstorms"]) {
        image = [UIImage imageNamed:@"w_chubascos.png"];
    }
    
    if ([type isEqualToString:@"mixed rain and hail"]) {
        image = [UIImage imageNamed:@"w_lluvia.ligera.png"];
    }
    
    if ([type isEqualToString:@"mixed rain and hail"]) {
        image = [UIImage imageNamed:@"w_lluvia.png"];
    }
    
    if ([type isEqualToString:@"snow"]) {
        image = [UIImage imageNamed:@"w_nieve.png"];
    }
   
    return image;
}

@end
