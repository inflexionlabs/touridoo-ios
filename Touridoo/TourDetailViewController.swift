//
//  TourDetailViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 02/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TourDetailViewController: UIViewController, UIScrollViewDelegate, ResponseServicesProtocol
{
    
    @IBOutlet weak var titleView : UILabel?
    @IBOutlet weak var scrollLabels : UIScrollView?
    @IBOutlet weak var scrollViewControllers : UIScrollView?
    @IBOutlet weak var NextButton : UIButton?
    
    var buttonDescription : UIButton?
    var buttonPrice: UIButton?
    var buttonDetails : UIButton?
    var buttonMettingPoint : UIButton?
    var buttonCancellations : UIButton?
    var buttonOptions : UIButton?
    var buttonOurValues : UIButton?
    var buttonItinerary : UIButton?
    
    var currentTour : TourModel?
    var tourDescription : TourDescriptionModel?
    
    var descriptionController : DescriptionTourViewController?
    var priceController : PriceTourViewController?
    var detailController : DetailTourViewController?
    var meetingController : MeetingPointTourViewController?
    var cancellationsController : CancellationsTourViewController?
    var optionsController : OptionsTourViewController?
    var ourValuesController : OurValuesTourViewController?
    var itineraryController : itineraryViewController?
    
    var freeScroll : Bool = true
    var onRequest : Bool = false
    var scrolledLeft: Bool = false
    var _lastContentOffset: CGPoint?
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        super.viewDidLoad()
        
        //Scroll component
        scrollLabels?.delegate = self
        scrollViewControllers?.delegate = self
        
        scrollLabelsSetUp()
        scrollViewControllerSetUp()
        
        self.view.isUserInteractionEnabled = false
        self.scrollViewControllers?.alpha = 0;
        self.NextButton?.isEnabled = false
        self.NextButton?.alpha = 0.4;
        
        NextButton?.setTitle(NSLocalizedString("Book in", comment: "Book in"), for: UIControlState.normal)
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if onRequest == true
        {
            return
        }
        
        Tools.showActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.10;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.loadTours()
        }
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
        let success : Bool = dataResult.object(forKey: "success") as! Bool
        
        if success
        {
            if (dataResult.object(forKey: "content") != nil)
            {
                let tour : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
                
                let id : Int = tour.object(forKey: "id") as! Int
                let name : String = tour.object(forKey: "name") as! String
                let descriptionTour : String = tour.object(forKey: "description") as! String
                let image_url : String = tour.object(forKey: "image_url") as! String
                let duration_in_hours : Int = tour.object(forKey: "duration_in_hours") as! Int
                let stars_average : Int = tour.object(forKey: "stars_average") as! Int
                let image_footer_label : String? = tour.object(forKey: "image_footer_label") as? String
                let details : String? = tour.object(forKey: "details") as? String
                let rendezvous_description : String? = tour.object(forKey: "rendezvous_description") as? String
                let cancelation_description : String? = tour.object(forKey: "cancelation_description") as? String
                let rates_count : Int = tour.object(forKey: "rates_count") as! Int
                let base10_stars_average : Int = tour.object(forKey: "base10_stars_average") as! Int
                let rates : NSArray = tour.object(forKey: "rates") as! NSArray
                let languages : String = ""
                let max_occupants : Int = 0
                let tourSteps : NSArray = tour.object(forKey: "tourSteps") as! NSArray
                let woeid : String? = tour.object(forKey: "Woeid") as? String
                
                var PricesArray: [PriceModel] = []
                let nsArrayPrices : NSArray? = tour.object(forKey: "Prices") as? NSArray
                
                if nsArrayPrices != nil
                {
                    for integer in 0...((nsArrayPrices?.count)! - 1)
                    {
                        let price : NSDictionary = nsArrayPrices!.object(at: integer) as! NSDictionary
                        
                        let id: Int = price.object(forKey: "Id") as! Int
                        let discount: Int = price.object(forKey: "Discount") as! Int
                        let value: Float = (price.object(forKey: "Value") as? NSNumber)?.floatValue ?? 0
                        let name: String = price.object(forKey: "Name") as! String
                        let description: String = price.object(forKey: "Description") as! String
                        let currency: String = price.object(forKey: "currency") as! String
                        
                        let PriceData: PriceModel = PriceModel.init(id: id, discount: discount, value: value, name: name, priceDescription: description, currency: currency)
                        
                        PricesArray.append(PriceData)
                    }
                }
                else
                {
                    //En caso de que no haya objetos de precio
                    let auxPriceModel: PriceModel = PriceModel.init(id: -1, discount: 0, value: 0, name: "generic", priceDescription: "generic", currency: "generic")
                    PricesArray.append(auxPriceModel)
                }
                
                var meetings: [MeetingPointsModel] = []
                let meetingsFromService : NSArray? = tour.object(forKey: "meetingPoints") as? NSArray
                if meetingsFromService != nil
                {
                    if meetingsFromService?.count != 0
                    {
                        for integer in 0...((meetingsFromService?.count)! - 1)
                        {
                            let meeting : NSDictionary = meetingsFromService!.object(at: integer) as! NSDictionary
                            
                            let id : Int? = meeting.object(forKey: "id") as? Int
                            let latitudeM : Float? = meeting.object(forKey: "Latitude") as? Float
                            let longitudeM : Float? = meeting.object(forKey: "Longitude") as? Float
                            let nameM : String? = meeting.object(forKey: "name") as? String
                            let DescripM : String? = meeting.object(forKey: "description") as? String
                            
                            let meetingModel : MeetingPointsModel = MeetingPointsModel.init(id: id!,
                                                                                            latitude: latitudeM,
                                                                                            longitude: longitudeM,
                                                                                            name: nameM,
                                                                                            descriptionText: DescripM,
                                                                                            isSelected: false)
                            
                            meetings.append(meetingModel)
                        }
                    }
                }
                
                self.tourDescription = TourDescriptionModel.init(id: id,
                                                                 name: name,
                                                                 descriptionTour: descriptionTour,
                                                                 image_url: image_url,
                                                                 duration_in_hours: duration_in_hours,
                                                                 stars_average: stars_average,
                                                                 image_footer_label: image_footer_label,
                                                                 details: details,
                                                                 rendezvous_description: rendezvous_description,
                                                                 cancelation_description: cancelation_description,
                                                                 rates_count: rates_count,
                                                                 base10_stars_average: base10_stars_average,
                                                                 rates: rates,
                                                                 languages: languages,
                                                                 max_occupants: max_occupants,
                                                                 image: currentTour?.image,
                                                                 tourSteps: tourSteps,
                                                                 woeid: woeid,
                                                                 prices: PricesArray,
                                                                 meetingPoints: meetings)
                
                
                let dispatchTime = DispatchTime.now() + 0.50;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    self.view.isUserInteractionEnabled = true
                    self.onRequest = true
                    Tools.hiddenActivityView(inView: self)
                    self.NextButton?.isEnabled = true
                    
                    self.descriptionController?.tourDescription = self.tourDescription
                    self.priceController?.tourDescription = self.tourDescription
                    self.detailController?.tourDescription = self.tourDescription
                    self.meetingController?.tourDescription = self.tourDescription
                    self.optionsController?.tourDescription = self.tourDescription
                    self.cancellationsController?.tourDescription = self.tourDescription
                    self.itineraryController?.tourDescription = self.tourDescription
                    
                    self.descriptionController?.updateData()
                    self.priceController?.updateData()
                    self.detailController?.updateData()
                    self.meetingController?.updateData()
                    self.optionsController?.updateData()
                    self.cancellationsController?.updateData()
                    self.itineraryController?.updateData()
                    
                    UIView.animate(withDuration: 0.50, animations: {
                        self.scrollViewControllers?.alpha = 1;
                        self.NextButton?.alpha = 1;
                    })
                }
            }
        }
        else
        {
            Tools.hiddenActivityView(inView: self)
            
            let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
            let error_message : String = content.object(forKey: "error_message") as! String
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                Tools.showAlertViewinModalViewController(withText: error_message, ModalViewController: self)
            }
            
        }
        
    }
    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }
    
    func loadTours()
    {
        let Service = TouridooServices.init(delegate: self)
        let id : Int = (currentTour?.id)!
        let idString = String(id)
        Service.GetTourById(id:idString)
    }
    
    @IBAction func reserveTour()
    {
        let ReserveController : ReserveViewController  = self.storyboard!.instantiateViewController(withIdentifier: "ReserveViewController") as! ReserveViewController
        ReserveController.currentTour = self.tourDescription
        self.navigationController?.pushViewController(ReserveController, animated: true)
    }
    
    @IBAction func labelSelectAction(button : UIButton)
    {
        setSelectedSection(atIndex: button.tag)
    }
    
    func scrollViewControllerSetUp()
    {
        
        //Add ViewController in ScrollMain
        descriptionController = self.storyboard?.instantiateViewController(withIdentifier: "DescriptionTourViewController") as? DescriptionTourViewController
        var FrameViewController : CGRect = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(0);
        FrameViewController.origin.y = CGFloat(0)
        descriptionController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((descriptionController?.view)!)
        self.addChildViewController(descriptionController!)
        
        itineraryController = self.storyboard?.instantiateViewController(withIdentifier: "itineraryViewController") as? itineraryViewController
        FrameViewController = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(1);
        FrameViewController.origin.y = CGFloat(0)
        itineraryController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((itineraryController?.view)!)
        self.addChildViewController(itineraryController!)
        
        priceController = self.storyboard?.instantiateViewController(withIdentifier: "PriceTourViewController2") as? PriceTourViewController
        FrameViewController = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(2);
        FrameViewController.origin.y = CGFloat(0)
        priceController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((priceController?.view)!)
        self.addChildViewController(priceController!)
        
        detailController = self.storyboard?.instantiateViewController(withIdentifier: "DetailTourViewController") as? DetailTourViewController
        FrameViewController  = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(3);
        FrameViewController.origin.y = CGFloat(0)
        detailController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((detailController?.view)!)
        self.addChildViewController(detailController!)
        
        meetingController = self.storyboard?.instantiateViewController(withIdentifier: "MeetingPointTourViewController") as? MeetingPointTourViewController
        FrameViewController = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(4);
        FrameViewController.origin.y = CGFloat(0)
        meetingController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((meetingController?.view)!)
        self.addChildViewController(meetingController!)
        
        cancellationsController = self.storyboard?.instantiateViewController(withIdentifier: "CancellationsTourViewController") as? CancellationsTourViewController
        FrameViewController = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(5);
        FrameViewController.origin.y = CGFloat(0)
        cancellationsController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((cancellationsController?.view)!)
        self.addChildViewController(cancellationsController!)
        
        optionsController = self.storyboard?.instantiateViewController(withIdentifier: "OptionsTourViewController") as? OptionsTourViewController
        FrameViewController  = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(6);
        FrameViewController.origin.y = CGFloat(0)
        optionsController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((optionsController?.view)!)
        self.addChildViewController(optionsController!)
        
        ourValuesController = self.storyboard?.instantiateViewController(withIdentifier: "OurValuesTourViewController") as? OurValuesTourViewController
        FrameViewController = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(7);
        FrameViewController.origin.y = CGFloat(0)
        ourValuesController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((ourValuesController?.view)!)
        self.addChildViewController(ourValuesController!)
        
        
        let scrollHeight : CGFloat = (scrollViewControllers?.frame.size.height)!
        
        scrollViewControllers?.contentSize = CGSize(width: UIScreen.main.bounds.width * CGFloat(8), height: scrollHeight)
        scrollViewControllers?.isScrollEnabled = true
        scrollViewControllers?.isUserInteractionEnabled = true
        scrollViewControllers?.showsHorizontalScrollIndicator = false
        scrollViewControllers?.isPagingEnabled = true
    }
    
    func scrollLabelsSetUp()
    {
        //Set scrooll labels in UI
        
        var totalSize = CGFloat(0)
        
        //Button Description
        var titleButton : String = NSLocalizedString("Description", comment: "Description")
        buttonDescription = createButton(title: titleButton, XCoordinate: CGFloat(0), width: CGFloat(130))
        totalSize = totalSize + (buttonDescription?.frame.size.width)!
        var indicatorView : UIView = createSelectIndicatorView(inButton: buttonDescription!, atIndex: 0)
        indicatorView.center = (buttonDescription?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview(buttonDescription!)
        
        //Button itinerary
        titleButton = NSLocalizedString("Itinerary", comment: "Itinerary")
        buttonItinerary = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(110))
        totalSize = totalSize + (buttonItinerary?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: (buttonItinerary)!, atIndex: 1)
        indicatorView.center = (buttonItinerary?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview((buttonItinerary)!)
        
        //Button Price
        titleButton = NSLocalizedString("Price", comment: "Price")
        buttonPrice = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(90))
        totalSize = totalSize + (buttonPrice?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: (buttonPrice)!, atIndex: 2)
        indicatorView.center = (buttonPrice?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview((buttonPrice)!)
        
        //Button Details
        titleButton = NSLocalizedString("Details", comment: "Details")
        buttonDetails = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(100))
        totalSize = totalSize + (buttonDetails?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: (buttonDetails)!, atIndex: 3)
        indicatorView.center = (buttonDetails?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview(buttonDetails!)
        
        //Button Meeting point
        titleButton = NSLocalizedString("Meeting point", comment: "Meeting point")
        buttonMettingPoint = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(200))
        totalSize = totalSize + (buttonMettingPoint?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: (buttonMettingPoint)!, atIndex: 4)
        indicatorView.center = (buttonMettingPoint?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview((buttonMettingPoint)!)
        
        //Button Cancellations
        titleButton = NSLocalizedString("Cancellations", comment: "Cancellations")
        buttonCancellations = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(160))
        totalSize = totalSize + (buttonCancellations?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: buttonCancellations!, atIndex: 5)
        indicatorView.center = (buttonCancellations?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview(buttonCancellations!)
        
        //Button Options
        titleButton = NSLocalizedString("Opinions", comment: "Opinions")
        buttonOptions = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(100))
        totalSize = totalSize + (buttonOptions?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: buttonOptions!, atIndex: 6)
        indicatorView.center = (buttonOptions?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview(buttonOptions!)
        
        //Button Our value
        titleButton = NSLocalizedString("Why Touridoo", comment: "Why Touridoo")
        buttonOurValues = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(180))
        totalSize = totalSize + (buttonOurValues?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: buttonOurValues!, atIndex: 7)
        indicatorView.center = (buttonOurValues?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview(buttonOurValues!)
        
        let scrollHeight : CGFloat = (scrollLabels?.frame.size.height)!
        
        scrollLabels?.contentSize = CGSize(width: totalSize, height: scrollHeight)
        scrollLabels?.isScrollEnabled = true
        scrollLabels?.isUserInteractionEnabled = true
        scrollLabels?.showsHorizontalScrollIndicator = false
        
        setSelectedSection(atIndex: 0)
    }
    
    func setSelectedSection(atIndex : Int )
    {
        for subView : UIView in (scrollLabels?.subviews)!
        {
            if !(subView.isKind(of: UIButton.self))
            {
                let indicatorView : UIView  = subView
                var newAlpha = CGFloat(0);
                
                if indicatorView.tag == atIndex
                {
                    newAlpha = CGFloat(1)
                }
                UIView.animate(withDuration: 0.30, animations: {indicatorView.alpha = newAlpha})
            }
        }
        
        doScrollLabel(atIndex: atIndex)
        
        if atIndex == 4
        {
            meetingController?.requestForMap()
        }
        
    }
    
    func doScrollLabel(atIndex : Int )
    {
        //Description
        if atIndex == 0
        {
            if scrolledLeft
            {
                scrollLabels?.scrollRectToVisible((buttonDescription?.frame)!, animated: true)
                return
            }
            scrollLabels?.scrollRectToVisible((buttonDescription?.frame)!, animated: true)
            return
        }
        
        //Itinerary
        if atIndex == 1
        {
            if scrolledLeft
            {
                scrollLabels?.scrollRectToVisible((buttonDescription?.frame)!, animated: true)
                return
            }
            scrollLabels?.scrollRectToVisible((buttonPrice?.frame)!, animated: true)
            return
        }
        
        //Price
        if atIndex == 2
        {
            if scrolledLeft
            {
                scrollLabels?.scrollRectToVisible((buttonItinerary?.frame)!, animated: true)
                return
            }
            scrollLabels?.scrollRectToVisible((buttonDetails?.frame)!, animated: true)
            return
        }
        
        //Details
        if atIndex == 3
        {
            if scrolledLeft
            {
                scrollLabels?.scrollRectToVisible((buttonPrice?.frame)!, animated: true)
                return
            }
            scrollLabels?.scrollRectToVisible((buttonMettingPoint?.frame)!, animated: true)
            return
        }
        
        //Meeting
        if atIndex == 4
        {
            if scrolledLeft
            {
                scrollLabels?.scrollRectToVisible((buttonDetails?.frame)!, animated: true)
                return
            }
            scrollLabels?.scrollRectToVisible((buttonCancellations?.frame)!, animated: true)
            return
        }
        
        //Cancelations
        if atIndex == 5
        {
            if scrolledLeft
            {
                scrollLabels?.scrollRectToVisible((buttonMettingPoint?.frame)!, animated: true)
                return
            }
            scrollLabels?.scrollRectToVisible((buttonOptions?.frame)!, animated: true)
            return
        }
        
        //Options
        if atIndex == 6
        {
            if scrolledLeft
            {
                scrollLabels?.scrollRectToVisible((buttonCancellations?.frame)!, animated: true)
                return
            }
            scrollLabels?.scrollRectToVisible((buttonOurValues?.frame)!, animated: true)
            return
        }
        
        //our values
        if atIndex == 7
        {
            scrollLabels?.scrollRectToVisible((buttonOurValues?.frame)!, animated: true)
            return
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        _lastContentOffset = scrollView.contentOffset
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        /*
        if ((_lastContentOffset?.x)! < scrollView.contentOffset.x) {
            print("Scrolled Right");
            scrolledLeft = false
        }
        else if ((_lastContentOffset?.x)! > scrollView.contentOffset.x) {
            print("Scrolled Left");
            scrolledLeft = true
        }*/
        
        if scrollView == scrollViewControllers && freeScroll
        {
            let index : Int = Int((scrollViewControllers?.contentOffset.x)!) / Int((scrollViewControllers?.frame.size.width)!)
            setSelectedSection(atIndex: index)
        }
    }
    
    @IBAction func scrollToViewController(button : UIButton)
    {
        
        freeScroll = false
        
        switch button.tag {
        case 0:
            //Description
            scrollLabels?.scrollRectToVisible((buttonDescription?.frame)!, animated: true)
            scrollViewControllers?.scrollRectToVisible((descriptionController?.view?.frame)!, animated: true)
            break
        case 1:
            //itinerary
            scrollRectToVisibleCenteredOn(visibleRect: (buttonItinerary?.frame)!, animated: true)
            scrollViewControllers?.scrollRectToVisible((itineraryController?.view?.frame)!, animated: true)
            break
        case 2:
            //Price
            scrollRectToVisibleCenteredOn(visibleRect: (buttonPrice?.frame)!, animated: true)
            scrollViewControllers?.scrollRectToVisible((priceController?.view?.frame)!, animated: true)
            break
        case 3:
            //Details
            scrollRectToVisibleCenteredOn(visibleRect: (buttonDetails?.frame)!, animated: true)
            scrollViewControllers?.scrollRectToVisible((detailController?.view?.frame)!, animated: true)
            break
        case 4:
            //Meeting
            scrollRectToVisibleCenteredOn(visibleRect: (buttonMettingPoint?.frame)!, animated: true)
            scrollViewControllers?.scrollRectToVisible((meetingController?.view?.frame)!, animated: true)
            break
        case 5:
            //Cancelations
            scrollRectToVisibleCenteredOn(visibleRect: (buttonCancellations?.frame)!, animated: true)
            scrollViewControllers?.scrollRectToVisible((cancellationsController?.view?.frame)!, animated: true)
            break
        case 6:
            //Options
            scrollRectToVisibleCenteredOn(visibleRect: (buttonOptions?.frame)!, animated: true)
            scrollViewControllers?.scrollRectToVisible((optionsController?.view?.frame)!, animated: true)
            break
        case 7:
            //our values
            scrollLabels?.scrollRectToVisible((buttonOurValues?.frame)!, animated: true)
            scrollViewControllers?.scrollRectToVisible((ourValuesController?.view?.frame)!, animated: true)
            break
        default:
            //default
            break
        }
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.freeScroll = true
        }
    }
    func scrollRectToVisibleCenteredOn(visibleRect: CGRect, animated: Bool)
    {
        let centeredRect = CGRect(x: visibleRect.origin.x + visibleRect.size.width/2 - (scrollLabels?.frame.size.width)!/2, y: visibleRect.origin.y + visibleRect.size.height/2 - (scrollLabels?.frame.size.height)!/2, width: (scrollLabels?.frame.size.width)!, height: (scrollLabels?.frame.size.height)!)
        
        scrollLabels?.scrollRectToVisible(centeredRect, animated: animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        titleView?.text = currentTour?.name
        titleView?.adjustsFontSizeToFitWidth = true
        Tools.hiddenBurgerButton(hidden: true)
    }
    
    func createButton(title : String, XCoordinate : CGFloat, width : CGFloat) -> UIButton
    {
        let scrollHeight : CGFloat = (scrollLabels?.frame.size.height)!
        
        let dynamicButton : UIButton = UIButton.init(frame: CGRect(x: XCoordinate,y: 0, width: width, height: scrollHeight))
        dynamicButton.setTitle(title, for: UIControlState.normal)
        dynamicButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        dynamicButton.titleLabel!.font = Tools.fontAppBold(withSize: 14)
        //dynamicButton.backgroundColor = Tools.getRandomColor()
        dynamicButton.addTarget(self, action: #selector(scrollToViewController(button:)), for: UIControlEvents.touchUpInside)
        
        return dynamicButton
    }
    
    func createSelectIndicatorView(inButton : UIButton, atIndex : Int) -> UIView
    {
        var SizeView : CGRect = inButton.frame
        SizeView.size.width = inButton.frame.size.width * CGFloat(0.85)
        SizeView.size.height = inButton.frame.size.height * CGFloat(0.7)
        
        let indicatorView : UIView  = UIView.init(frame: SizeView)
        indicatorView.tag = atIndex
        indicatorView.backgroundColor = Tools.colorApp()
        indicatorView.layer.cornerRadius = SizeView.size.height / 2;
        inButton.tag = atIndex
        indicatorView.alpha = 0;
        
        inButton.addTarget(self, action: #selector(labelSelectAction(button:)), for: UIControlEvents.touchUpInside)
        
        return indicatorView;
    }
    
    @IBAction func closeView()
    {
        self.navigationController?.popViewController(animated: true)
    }
}
