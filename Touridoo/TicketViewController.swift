//
//  TicketViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 04/10/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TicketViewController: UIViewController {

    @IBOutlet weak var barCode : UIImageView?
    @IBOutlet weak var header : UIView?
    @IBOutlet weak var ticketView : UIView?
    @IBOutlet weak var imageTour : UIImageView?
    @IBOutlet weak var tourName : UILabel?
    @IBOutlet weak var tourCity : UILabel?
    @IBOutlet weak var ticketsTitle : UILabel?
    @IBOutlet weak var Total : UILabel?
    @IBOutlet weak var day : UILabel?
    @IBOutlet weak var monthYear : UILabel?
    @IBOutlet weak var hour : UILabel?
    @IBOutlet weak var meetingPoint : UILabel?
    @IBOutlet weak var when : UILabel?
    @IBOutlet weak var duration : UILabel?
    @IBOutlet weak var language : UILabel?
    @IBOutlet weak var bookingCode : UILabel?
    @IBOutlet weak var bookingTitle : UILabel?
    @IBOutlet weak var summaryTickets : UITextView?
    
    var imageForTour : UIImage?
    var bookingCodeString : String?
    var tourTicket : Tour?
    
    var delegatePaymentView : PaymentMethodViewController?
    
    var animationView  = UIImageView.init()
    
    @IBOutlet weak var shareButton : UIButton?
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ticketView?.alpha = 0
    }
    
    func setTicketData(image : UIImage?, tourData : Tour)
    {
        if image != nil
        {
            imageForTour = image
        }
        
        bookingCodeString = tourData.booking?.booking_code
        tourTicket = tourData
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    @IBAction func shareTicket()
    {
        shareButton?.alpha = 0
        activityIndicator?.alpha = 1
        
        let ScreenShot : UIImage = ScreenShotInView(controller: self)
        let objetcs : Array = [ScreenShot]
        
        animationView = UIImageView.init(frame: self.view.frame)
        animationView.image = ScreenShot
        
        let activity = UIActivityViewController.init(activityItems: objetcs, applicationActivities: nil)
        activity.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
        activity.popoverPresentationController?.sourceView = self.view
        self.present(activity, animated: true, completion: nil)
        
        activity.completionWithItemsHandler = { activity, success, items, error in
            
            self.shareButton?.alpha = 1
            self.activityIndicator?.alpha = 0
            
            if success
            {
                self.view.addSubview(self.animationView)
                
                UIView.animate(withDuration: 0.30, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                    {
                        self.animationView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                }, completion:
                    { _ in
                        UIView.animate(withDuration: 0.30, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                            {
                                self.animationView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                                self.animationView.center = CGPoint(x: self.view.center.x / 2, y: self.view.center.y * 2)
                                self.animationView.alpha = 0
                                
                        }, completion:
                            { _ in
                                self.animationView.removeFromSuperview()
                        })
                })
            }
        }
    
    }
    
    func ScreenShotInView(controller : UIViewController) -> UIImage {
        
        let screen :  UIView = controller.view
        UIGraphicsBeginImageContextWithOptions(screen.bounds.size, false, UIScreen.main.scale)
        screen.layer.render(in: UIGraphicsGetCurrentContext()!)
        let screengrab : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return screengrab
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        
        if bookingCodeString != nil {
            let image = generateBarcode(from: bookingCodeString!)
            self.barCode?.image = image
        }
        
        if imageForTour != nil {
            let BWImage = convertImageToBW(image: imageForTour!)
            self.imageTour?.image = BWImage;
        }
        
        if tourTicket != nil {
            
            tourName?.text = tourTicket?.name?.uppercased()
            tourName?.adjustsFontSizeToFitWidth = true
            
            tourCity?.text = NSLocalizedString("MEXICO CITY", comment: "MEXICO CITY")
            ticketsTitle?.text = NSLocalizedString("TICKETS", comment: "TICKETS")
            
            var summary : String = ""
            
            for indexT in 0...((tourTicket?.booking?.tickets?.count)! - 1)
            {
                let ticketDetail : PriceModel = (tourTicket?.booking?.tickets![indexT])!
                let name : String = ticketDetail.name
                let value = NSString(format: "%.2f", (ticketDetail.value))
                let currency : String = ticketDetail.currency
                
                summary = summary + name.uppercased() + ": 1 ($"  + (value as String) + " " + currency.uppercased() + ")\n"
            }
            
            summaryTickets?.text = summary
            
            //summaryTickets?.adjustsFontSizeToFitWidth = true
            //summaryTickets?.backgroundColor = UIColor.red
    
            let totalPrice = NSString(format: "%.2f", (tourTicket?.booking?.TotalPrice)!)
            Total?.text = NSLocalizedString("TOTAL: ", comment: "TOTAL: ") + "($" + (totalPrice as String) + ")"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateTour = dateFormatter.date(from: (tourTicket?.booking?.tour_date)!)
            
            dateFormatter.dateFormat = "dd"
            let dayString : String = dateFormatter.string(from: dateTour!)
            day?.text = dayString
            
            dateFormatter.dateFormat = "MMMM, yyyy"
            let monthString : String = dateFormatter.string(from: dateTour!)
            monthYear?.text = monthString.uppercased()
            
            dateFormatter.dateFormat = "HH:mm a"
            let hourstring : String = dateFormatter.string(from: dateTour!)
            hour?.text = hourstring
            
            meetingPoint?.text = NSLocalizedString("MEETING POINT: ", comment: "MEETING POINT: ") + (self.tourTicket?.booking?.meetingPoint?.uppercased())!
            
            language?.text = NSLocalizedString("SPANISH", comment: "SPANISH")
            language?.text = NSLocalizedString("LANGUAGE: SPANISH", comment: "LANGUAGE: SPANISH")
            
            //dateFormatter.dateFormat = "MMMM dd, yyyy. HH:mm a"
            dateFormatter.dateFormat = "EEEE, MMM d, yyyy - HH:mm a"
            let whenString : String = dateFormatter.string(from: dateTour!)
            when?.text = NSLocalizedString("WHEN: ", comment: "WHEN: ") + whenString.uppercased()
            
            let hoursTour : Int = (tourTicket?.duration_in_hours)!
            duration?.text = NSLocalizedString("DURATION: ", comment: "DURATION: ") + String(hoursTour) + " " + NSLocalizedString("HOURS", comment: "HOURS")
            
            bookingCode?.text = tourTicket?.booking?.booking_code?.uppercased()
            
            bookingTitle?.text = NSLocalizedString("BOOKING CODE:", comment: "BOOKING CODE:")
        }
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.ticketView?.alpha = 1
        }, completion:nil)
        
    }
    
    @IBAction func close()
    {
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
    
        }, completion:
            { _ in
                
                self.view.removeFromSuperview()
                if self.delegatePaymentView != nil
                {
                    self.delegatePaymentView?.lastView()
                }
        })
    }
    
    func convertImageToBW(image:UIImage) -> UIImage {
        
        let filter = CIFilter(name: "CIPhotoEffectNoir")
        let ciInput = CIImage(image: image)
        filter?.setValue(ciInput, forKey: "inputImage")
        let ciOutput = filter?.outputImage
        let ciContext = CIContext()
        let cgImage = ciContext.createCGImage(ciOutput!, from: (ciOutput?.extent)!)
        
        return UIImage(cgImage: cgImage!)
    }

    func generateBarcode(from string: String) -> UIImage?
    {
        let data = string.data(using: String.Encoding.ascii)
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.applying(transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent;
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
