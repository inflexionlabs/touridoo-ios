//
//  BookingDataReserve.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class BookingDataReserve: NSObject
{
    var tourDate : String?
    var idTour : Int?
    var totalToPay : String?
    var meetingPointId : Int?
    var tickets : NSArray?
}
