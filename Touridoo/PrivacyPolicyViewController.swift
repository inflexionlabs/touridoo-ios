//
//  PrivacyPolicyViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController, UIScrollViewDelegate
{
    
    @IBOutlet weak var scrollLabels : UIScrollView?
    @IBOutlet weak var scrollViewControllers : UIScrollView?
    @IBOutlet weak var backButton : UIButton?
    
    var buttonTemrs : UIButton?
    var buttonPolicy: UIButton?

    var termsController : subviewTermsViewController?
    var PolicyController : subviewTermsViewController?
    
    var freeScroll : Bool = true
    var typeModaController : Bool?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Scroll component
        scrollLabels?.delegate = self
        scrollViewControllers?.delegate = self
        
        scrollLabelsSetUp()
        scrollViewControllerSetUp()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if typeModaController == true
        {
            backButton?.isHidden = false
            Tools.hiddenBurgerButton(hidden: true)
        }
        else
        {
            backButton?.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        //termsController?.textView?.text = getTerms()
        //PolicyController?.textView?.text = getPolicy()
    }
    
    func getTerms() -> String
    {
        return NSLocalizedString("", comment: "")
    }
    
    func getPolicy() -> String
    {
        return NSLocalizedString("", comment: "")
    }
    
    @IBAction func closeModal()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func labelSelectAction(button : UIButton)
    {
        setSelectedSection(atIndex: button.tag)
    }
    
    func scrollViewControllerSetUp()
    {
        //Add ViewController in ScrollMain
        termsController = self.storyboard?.instantiateViewController(withIdentifier: "termsController") as? subviewTermsViewController
        var FrameViewController : CGRect = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(0);
        FrameViewController.origin.y = CGFloat(0)
        termsController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((termsController?.view)!)
        self.addChildViewController(termsController!)
        
        PolicyController = self.storyboard?.instantiateViewController(withIdentifier: "policyController") as? subviewTermsViewController
        FrameViewController = (scrollViewControllers?.frame)!
        FrameViewController.origin.x = UIScreen.main.bounds.size.width * CGFloat(1);
        FrameViewController.origin.y = CGFloat(0)
        PolicyController?.view.frame = FrameViewController
        scrollViewControllers?.addSubview((PolicyController?.view)!)
        self.addChildViewController(PolicyController!)
        
        let scrollHeight : CGFloat = (scrollViewControllers?.frame.size.height)!
        
        scrollViewControllers?.contentSize = CGSize(width: UIScreen.main.bounds.width * CGFloat(2), height: scrollHeight)
        scrollViewControllers?.isScrollEnabled = true
        scrollViewControllers?.isUserInteractionEnabled = true
        scrollViewControllers?.showsHorizontalScrollIndicator = false
        scrollViewControllers?.isPagingEnabled = true
    }
    
    func scrollLabelsSetUp()
    {
        //Set scrooll labels in UI
        
        var totalSize = CGFloat(0)
        
        //Button Recommendations
        var titleButton : String = NSLocalizedString("Terms And Conditions", comment: "Terms And Conditions")
        buttonTemrs = createButton(title: titleButton, XCoordinate: CGFloat(0), width: CGFloat(230))
        totalSize = totalSize + (buttonTemrs?.frame.size.width)!
        var indicatorView : UIView = createSelectIndicatorView(inButton: buttonTemrs!, atIndex: 0)
        indicatorView.center = (buttonTemrs?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview(buttonTemrs!)
        
        //Button Suggestions
        titleButton = NSLocalizedString("Privacy Policy", comment: "Privacy Policy")
        buttonPolicy = createButton(title: titleButton, XCoordinate:totalSize, width: CGFloat(200))
        totalSize = totalSize + (buttonPolicy?.frame.size.width)!
        indicatorView = createSelectIndicatorView(inButton: (buttonPolicy)!, atIndex: 1)
        indicatorView.center = (buttonPolicy?.center)!
        scrollLabels?.addSubview(indicatorView)
        scrollLabels?.addSubview((buttonPolicy)!)
        
        let scrollHeight : CGFloat = (scrollLabels?.frame.size.height)!
        
        scrollLabels?.contentSize = CGSize(width: totalSize, height: scrollHeight)
        scrollLabels?.isScrollEnabled = true
        scrollLabels?.isUserInteractionEnabled = true
        scrollLabels?.showsHorizontalScrollIndicator = false
        
        setSelectedSection(atIndex: 0)
    }
    
    func setSelectedSection(atIndex : Int )
    {
        for subView : UIView in (scrollLabels?.subviews)!
        {
            if !(subView.isKind(of: UIButton.self))
            {
                let indicatorView : UIView  = subView
                var newAlpha = CGFloat(0);
                
                if indicatorView.tag == atIndex
                {
                    newAlpha = CGFloat(1)
                }
                UIView.animate(withDuration: 0.30, animations: {indicatorView.alpha = newAlpha})
            }
        }
        
        doScrollLabel(atIndex: atIndex)
        
        if atIndex == 3
        {
            //meetingController?.requestForMap()
        }
        
    }
    
    func doScrollLabel(atIndex : Int )
    {
        //recommendations
        if atIndex == 0
        {
            scrollLabels?.scrollRectToVisible((buttonTemrs?.frame)!, animated: true)
            return
        }
        
        //suggestions
        if atIndex == 1
        {
            scrollLabels?.scrollRectToVisible((buttonPolicy?.frame)!, animated: true)
            return
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == scrollViewControllers && freeScroll
        {
            let index : Int = Int((scrollViewControllers?.contentOffset.x)!) / Int((scrollViewControllers?.frame.size.width)!)
            setSelectedSection(atIndex: index)
        }
    }
    
    @IBAction func scrollToViewController(button : UIButton)
    {
        
        freeScroll = false
        
        //Description
        if button.tag == 0 {
            scrollViewControllers?.scrollRectToVisible((termsController?.view?.frame)!, animated: true)
        }
        
        //suggestionsControllersuggestionsController
        if button.tag  == 1 {
            scrollViewControllers?.scrollRectToVisible((PolicyController?.view?.frame)!, animated: true)
        }
        

        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.freeScroll = true
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }
   

    func createButton(title : String, XCoordinate : CGFloat, width : CGFloat) -> UIButton
    {
        let scrollHeight : CGFloat = (scrollLabels?.frame.size.height)!
        
        let dynamicButton : UIButton = UIButton.init(frame: CGRect(x: XCoordinate,y: 0, width: width, height: scrollHeight))
        dynamicButton.setTitle(title, for: UIControlState.normal)
        dynamicButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        dynamicButton.titleLabel!.font = Tools.fontAppBold(withSize: 14)
        //dynamicButton.backgroundColor = Tools.getRandomColor()
        dynamicButton.addTarget(self, action: #selector(scrollToViewController(button:)), for: UIControlEvents.touchUpInside)
        
        return dynamicButton
    }
    
    func createSelectIndicatorView(inButton : UIButton, atIndex : Int) -> UIView
    {
        var SizeView : CGRect = inButton.frame
        SizeView.size.width = inButton.frame.size.width * CGFloat(0.85)
        SizeView.size.height = inButton.frame.size.height * CGFloat(0.7)
        
        let indicatorView : UIView  = UIView.init(frame: SizeView)
        indicatorView.tag = atIndex
        indicatorView.backgroundColor = Tools.colorApp()
        indicatorView.layer.cornerRadius = SizeView.size.height / 2;
        inButton.tag = atIndex
        indicatorView.alpha = 0;
        
        inButton.addTarget(self, action: #selector(labelSelectAction(button:)), for: UIControlEvents.touchUpInside)
        
        return indicatorView;
    }
}


