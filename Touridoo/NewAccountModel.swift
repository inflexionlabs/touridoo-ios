//
//  NewAccountModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class NewAccountModel: NSObject
{
    var name : String?
    var last_name : String?
    var email : String?
    var phone_prefix : String?
    var phone_number : String?
    var rendezvous_point_name : String?
    var rendezvous_point_address : String?
    var hotel_reservation_owner_name : String?
    var comments : String?
    var policy_accepted : Bool?
    var receive_info : Bool?
    var password : String?
    var bookingData : BookingDataReserve?
}
