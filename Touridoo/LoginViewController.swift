//
//  LoginViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 26/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController
{
    @IBOutlet weak var mailTextField : UITextField?
    @IBOutlet weak var passwordTextField : UITextField?
    @IBOutlet weak var TouridooText : UILabel?
    @IBOutlet weak var backgroudShadow : UIImageView?
    @IBOutlet weak var loginButton : UIButton?
    @IBOutlet weak var createAccountButton : UIButton?
    @IBOutlet weak var skipButton : UIButton?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        mailTextField?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Username", comment: "Username"),
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        passwordTextField?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Password", comment: "Password"),
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.white])
       
        self.TouridooText?.alpha = 0
        self.backgroudShadow?.alpha = 0
        self.mailTextField?.superview?.alpha = 0;
        self.passwordTextField?.superview?.alpha = 0;
        self.loginButton?.superview?.alpha = 0;
        self.createAccountButton?.superview?.alpha = 0;
        self.skipButton?.superview?.alpha = 0;
        
        Tools.DownView(View: self.TouridooText!, Points: 10)
        Tools.DownView(View: (self.mailTextField?.superview)!, Points: 8)
        Tools.DownView(View: (self.passwordTextField?.superview)!, Points: 6)
        
        Tools.DownView(View: self.loginButton!, Points: 4)
        Tools.DownView(View: self.createAccountButton!, Points: 2)
        Tools.DownView(View: self.skipButton!, Points: 2)
 
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        //Open MenuBarApp
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let mainMenu : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
        mainMenu.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(mainMenu, animated: true, completion: nil)
        
        /*
        UIView.animate(withDuration: 1.0, delay: 1.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.TouridooText?.alpha = 1
                self.mailTextField?.superview?.alpha = 1;
                self.passwordTextField?.superview?.alpha = 1;
                self.backgroudShadow?.alpha = 1
                self.loginButton?.superview?.alpha = 1;
                self.createAccountButton?.superview?.alpha = 1;
                self.skipButton?.superview?.alpha = 1;
                
                Tools.UpView(View: self.TouridooText!, Points: 20)
                Tools.UpView(View: (self.mailTextField?.superview)!, Points: 15)
                Tools.UpView(View: (self.passwordTextField?.superview)!, Points: 10)
                Tools.UpView(View: (self.loginButton?.superview)!, Points: 5)
                Tools.UpView(View: (self.createAccountButton?.superview)!, Points: 1)
                Tools.UpView(View: (self.skipButton?.superview)!, Points: 1)
                
        }, completion:
            { _ in
                
        })
 */
    }
    
    @IBAction func skipAction()
    {
        //Open MenuBarApp
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let mainMenu : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
        mainMenu.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(mainMenu, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }

}
