//
//  ContactViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit
import MessageUI
import AVFoundation

class ContactViewController: UIViewController, MFMailComposeViewControllerDelegate
{
    @IBOutlet weak var textDesc : UITextView?
    @IBOutlet weak var callUsButton: UIButton!
    @IBOutlet weak var vBetaLabel : UILabel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //Ocultar por ahora el boton de Call us
        callUsButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        textDesc?.text = NSLocalizedString("If you have any question about your tour or booking method please feel free to reach us. Our goal is for you to have a premium experience of your tour", comment: "If you have any question about your tour or booking method please feel free to reach us. Our goal is for you to have a premium experience of your tour")
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            vBetaLabel?.text = "v" + version
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func callAction(alert: UIAlertAction!)
    {
        print("Hello! what can do touridoo for you!")
        let phoneNumber : String = "5511623579"
        let number = URL(string: "tel://" + phoneNumber)
        UIApplication.shared.open(number!)
    }
    
    @IBAction func mailAction(alert: UIAlertAction!)
    {
        let composeVC = MFMailComposeViewController()
        
        print("Dear Touridooo... today i wake up with...")
        
        if !MFMailComposeViewController.canSendMail()
        {
            print("Mail services are not available")
            //Tools.showAlertView(withText: "You dont have an email account set up on your iPhone")
            return
        }
        
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([sessionData.touridooContactMail])
        composeVC.setSubject("Hello Touridoo!")
        composeVC.setMessageBody("", isHTML: false)
        self.present(composeVC, animated: true, completion: nil)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
         controller.dismiss(animated: true, completion: nil)
    }
    
}
