//
//  TourRateTypeModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 23/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TourRateTypeModel: NSObject
{
    var id : Int?
    var language_code : String?
    var name : String?
    var descriptionText : String?
    var icon_url : String?
}
