//
//  InitialViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 26/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController,ResponseServicesProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        // GET Test
        // let Service = TouridooServices.init(delegate: self)
        // Service.GetInitialParameters()
        
        // POST Test
        // let Service = TouridooServices.init(delegate: self)
        // let ChoicesArray : NSArray = NSArray(object: "Swift")
        // Service.SetInitialParameters(question: "Favourite programming language?", choices: ChoicesArray)
        
        // LoaderView example
        // Tools.showActivityView(inView: self, withText: "Loading Tours...")
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        print(Result);
    }
    
    @IBAction func openMainMenu()
    {
        //Open MenuBarApp
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let mainMenu : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
        mainMenu.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(mainMenu, animated: true, completion: nil)
        
    }
    
    func onError(Error : String, name : ServiceName)
    {
        print(Error)
        Tools.hiddenActivityView(inView: self);
        Tools.printAlertForError(ErrorDescription: Error, currentViewController: self)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }


}
