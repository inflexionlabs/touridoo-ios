//
//  manageReservationViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 07/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class manageReservationViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, ResponseServicesProtocol, VRGCalendarViewDelegate, UITextFieldDelegate, UITextViewDelegate
{
    @IBOutlet weak var NextButton : UIButton?
    @IBOutlet weak var scrollData  : UIScrollView?
    @IBOutlet weak var quantityPaymentLabel: UILabel!
    @IBOutlet weak var duration : UILabel?
    @IBOutlet weak var descriptionlabel: UILabel?
    @IBOutlet weak var datelabel : UILabel?
    @IBOutlet weak var prefixTextField : UITextField?
    @IBOutlet weak var passangersNumber : UILabel?
    @IBOutlet weak var day : UILabel?
    @IBOutlet weak var date : UILabel?
    @IBOutlet weak var hour : UILabel?
    @IBOutlet weak var imageTour : UIImageView?
    var setupScrollSize : Bool = false
    var calendarSetupDays : Bool = false
    @IBOutlet weak var titleHeader : UILabel?
    var currentTour : Tour?
    
    var arraySchedules: NSMutableArray = NSMutableArray.init()
    var dynamicScheuldesArray = NSMutableArray.init()
    
    @IBOutlet weak var pickerViewSchedules : UIPickerView?
    @IBOutlet weak var selectAnHourLabel : UIButton?
    var pickerIsInView : Bool = false
    @IBOutlet weak var viewSchedule : UIView?
    
    var selectedHour : Date?
    
    var calendarView : VRGCalendarView?
    var onRequest : Bool = false
    
    var bookingData = BookingDataReserve.init()
    
    var image : Data?
    
    @IBOutlet weak var nameTextField : UITextField?
    @IBOutlet weak var lastNameTextField : UITextField?
    @IBOutlet weak var mailTextField : UITextField?
    @IBOutlet weak var cellPhoneTextField : UITextField?
    @IBOutlet weak var hotelOrMeetingTextField : UITextField?
    @IBOutlet weak var addreesTextField : UITextField?
    @IBOutlet weak var commentsTextField : UITextView?
    
    @IBOutlet weak var labelComments : UILabel?
    
    var selectedPrefix : String = ""
    var dateFromString : String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        calendarView = VRGCalendarView.init()
        calendarView?.delegate = self
        
        var XCalendarFrame : CGFloat = CGFloat(0)
        if Tools.iPhone7(){
            XCalendarFrame = CGFloat (25)
        }
        if Tools.iPhone7Plus(){
            XCalendarFrame = CGFloat (50)
        }
        
        calendarView?.frame = CGRect(x: XCalendarFrame, y: 255,
                                     width: (calendarView?.frame.size.width)!,
                                     height: (calendarView?.frame.size.height)!)
        calendarView?.backgroundColor = UIColor.clear
        
        scrollData?.addSubview(calendarView!)
        
        pickerViewSchedules?.delegate = self
        pickerViewSchedules?.dataSource = self
        
        selectAnHourLabel?.setTitle(NSLocalizedString("Select a schedule", comment: "Select a schedule"), for: UIControlState.normal)
        titleHeader?.text = NSLocalizedString("Modify", comment: "Modify")
        
        nameTextField?.delegate = self;
        lastNameTextField?.delegate = self;
        mailTextField?.delegate = self;
        cellPhoneTextField?.delegate = self;
        //hotelOrMeetingTextField?.delegate = self;
        //addreesTextField?.delegate = self;
        commentsTextField?.delegate = self;
        
        nameTextField?.placeholder = NSLocalizedString("Name", comment: "Name")
        lastNameTextField?.placeholder = NSLocalizedString("Last name", comment: "Last name")
        mailTextField?.placeholder = NSLocalizedString("Mail", comment: "Mail")
        cellPhoneTextField?.placeholder = NSLocalizedString("Cell phone number", comment: "Cell phone number")
        //hotelOrMeetingTextField?.placeholder = NSLocalizedString("Hotel or meeting point", comment: "Hotel or meeting point")
        //addreesTextField?.placeholder = NSLocalizedString("Address", comment: "Address")
        
        commentsTextField?.layer.borderWidth = 0.25;
        commentsTextField?.layer.borderColor = UIColor.lightGray.cgColor
        commentsTextField?.layer.cornerRadius = 3;
        
        addDoneButtonOnKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        Tools.hiddenBurgerButton(hidden: true)
        
        updateData()
    }
    
    func updateData()
    {
        let session = sessionData.shared
        
        if session.User != nil
        {
            if session.User?.name != nil {
                nameTextField?.text = session.User?.name
            }
            
            if session.User?.lastname != nil {
                lastNameTextField?.text = session.User?.lastname
            }
            
            if session.User?.email != nil {
                mailTextField?.text = session.User?.email
            }
            
            if session.User?.phone_number != nil {
                cellPhoneTextField?.text = session.User?.phone_number
            }
            
            if session.User?.phone_prefix != nil
            {
                prefixTextField?.text = session.User?.phone_prefix
                selectedPrefix = (session.User?.phone_prefix)!
                
                selectedPrefix = selectedPrefix.replacingOccurrences(of: "México (", with: "")
                selectedPrefix = selectedPrefix.replacingOccurrences(of: ")", with: "")
                
                selectedPrefix = selectedPrefix.replacingOccurrences(of: "United States (", with: "")
                selectedPrefix = selectedPrefix.replacingOccurrences(of: ")", with: "")
            }
        }
        
        if image != nil
        {
            if image?.count != 0
            {
                self.imageTour?.image = UIImage.init(data: image!)
            }
        }
        
        if currentTour?.booking?.tour_date != nil
        {
            let stingDate : String = (currentTour?.booking?.tour_date!)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            let dateObj = dateFormatter.date(from: stingDate)
            
            var totalDate : String?
            dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm"
            totalDate = dateFormatter.string(from: dateObj!)
            
            datelabel?.text = totalDate
            
            var hourString : String?
            dateFormatter.dateFormat = "HH:mm"
            hourString = dateFormatter.string(from: dateObj!)
            
            var dayString : String?
            dateFormatter.dateFormat = "dd"
            dayString = dateFormatter.string(from: dateObj!)
            
            var monthString : String?
            dateFormatter.dateFormat = "MMM, yyyy"
            monthString = dateFormatter.string(from: dateObj!)
            
            day?.text = dayString
            hour?.text = hourString
            date?.text = monthString
            
        }
        
        if currentTour?.name != nil
        {
            
            descriptionlabel?.text = currentTour?.name
        }
        
        if currentTour?.duration_in_hours != nil
        {
            let num : Int = (currentTour?.duration_in_hours)!
            let hours : String = String(num)
            duration?.text = hours + " " + NSLocalizedString("hours", comment: "hours")
        }
        
        if currentTour?.booking?.TotalPrice != nil
        {
            let pay : Float = (currentTour?.booking?.TotalPrice)!
            quantityPaymentLabel.text = Tools.currencyString(number: pay)
        }
        
        var numberOftickets : Int = 0
        
        numberOftickets = (currentTour?.booking?.tickets?.count)!
        passangersNumber?.text = String(numberOftickets)

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if pickerIsInView == true
        {
            hiddenPickerView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        pickerViewSchedules?.backgroundColor = viewSchedule?.backgroundColor
        
        self.scrollData?.bringSubview(toFront: self.pickerViewSchedules!)
        self.scrollData?.bringSubview(toFront: self.viewSchedule!)
        
        if setupScrollSize == false
        {
            setupScrollSize = true
            
            //let content : CGSize = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(1)))
            let content : CGSize = CGSize(width: (scrollData?.frame.size.width)!, height: CGFloat(1120))
            
            let newHeight : CGFloat = self.view.frame.size.height - CGFloat(60)
            
            let scrollFrameInStoryboar : CGRect = CGRect(x: (scrollData?.frame.origin.x)!,
                                                         y: (scrollData?.frame.origin.y)!,
                                                         width: (scrollData?.frame.size.width)!,
                                                         height: newHeight)
            
            
            scrollData?.frame = scrollFrameInStoryboar
            scrollData?.contentSize = content
        }
        
        if onRequest == true
        {
            return
        }
        
        Tools.showActivityView(inView: self)
        self.view.isUserInteractionEnabled = false
        self.loadSchedules()
        //Add notifications for crazy keyboard fix!
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification:NSNotification)
    {
        //Keyboard is in da house!
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollData!.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollData?.contentInset = contentInset
    }
    
    func keyboardWillHide(notification:NSNotification)
    {
        //Hidden Keyboard
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollData?.contentInset = contentInset
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        //Remove notifications
        NotificationCenter.default.removeObserver(self)
    }
    
    func calendarView(_ calendarView: VRGCalendarView!, switchedToMonth month: Int32, inYear year: Int32, targetHeight: Float, animated: Bool)
    {
        if arraySchedules.count != 0
        {
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.calendarView?.markDates(self.arraySchedules as Array)
            }
        }
    }
    
    func calendarView(_ calendarView: VRGCalendarView!, dateSelected date: Date!)
    {
        Tools.feedback()
        
        selectedHour = nil
        
        selectAnHourLabel?.setTitle(NSLocalizedString("Select a schedule", comment: "Select a schedule"), for: UIControlState.normal)
        
        if pickerIsInView == true
        {
            hiddenPickerView()
        }
        
        //Date validation
        let validateDate : Date = NSDate.init().addingTimeInterval((86400 * 3)) as Date
        var valid : Bool = false
        
        switch validateDate.compare(date)
        {
        case .orderedAscending     :
            valid = true
        case .orderedDescending    :
            valid = false
        case .orderedSame          :
            valid = false
        }
        
        if valid == false
        {
            dynamicScheuldesArray.removeAllObjects()
            self.pickerViewSchedules?.reloadAllComponents()
            
            Tools.showAlertView(withText: NSLocalizedString("This day is no longer available to modify the booking", comment: "This day is no longer available to modify the booking"))
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd"
        let selectedDayString = dateFormatter.string(from: date)
        
        dynamicScheuldesArray.removeAllObjects()
        dynamicScheuldesArray.add(NSLocalizedString("Select", comment: "Select"))
        
        for i in 0...(arraySchedules.count - 1)
        {
            let dateInArray : Date = arraySchedules.object(at: i) as! Date
            let arrayStringDate : String = dateFormatter.string(from: dateInArray)
            
            if arrayStringDate == selectedDayString {
                dynamicScheuldesArray.add(dateInArray)
            }
            
        }
        
        if dynamicScheuldesArray.count == 1
        {
            Tools.showAlertView(withText: NSLocalizedString("We dont have tours for that day, try another", comment: "We dont have tours for that day, try another"))
            return
        }
        else
        {
            //Selected day has available dates
            Tools.shakeAnimation(viewAnimation: self.viewSchedule!, moveScale: 1.0, minimumAlpha: 1.0, duration: 0.30)
        }
        
        self.pickerViewSchedules?.reloadAllComponents()
        
    }
    
    func calendarViewFinishedUpdate()
    {
        if calendarSetupDays == true
        {
            return
        }
        
        calendarSetupDays = true
        
        print("Finshed")
        
        self.view.isUserInteractionEnabled = true
        self.onRequest = true
        Tools.hiddenActivityView(inView: self)
        self.pickerViewSchedules?.reloadAllComponents()
        self.calendarSetupDays = true
        
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.GET_MY_TOURS
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                    loadTourData.updateData(content: content)
                }
            }
            
            UserDefaults.standard.set(true, forKey: "needs_update");
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.currentTour?.booking?.tour_date = self.dateFromString
                self.updateData()
                Tools.hiddenActivityView(inView: self)
                self.view.isUserInteractionEnabled = true
                Tools.showDoneView(withText: NSLocalizedString("Your tour has been modified", comment: "Your tour has been modified"))
                self.closeView()
                
            }
        
            return
        }
        
        if name == ServiceName.UPDATE_TOUR
        {
            let Service = TouridooServices.init(delegate: self)
            Service.GetMyTours()
            return
            
        }
        
        if name == ServiceName.CHECK_TOUR_AVAILABILITY
        {
            
        
            var tourAvailable : Bool = false
            
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = (dataResult.object(forKey: "success") != nil)
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
                    tourAvailable = (content.object(forKey: "tour_available") != nil)
                }
            }
            
            if tourAvailable == true
            {
                let dispatchTime = DispatchTime.now() + 0.30;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    let services = TouridooServices.init(delegate: self)
                    
                    services.updateTour(OldTourDate: (self.currentTour?.tourRun?.tour_date)!,
                                        OldIdTourRun: (self.currentTour?.tourRun?.id)!,
                                        NewTourDate: self.dateFromString,
                                        idTour: (self.currentTour?.id)!)
                }
               
            }
            else
            {
                Tools.hiddenActivityView(inView: self)
                
                let dispatchTime = DispatchTime.now() + 0.30;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    self.view.isUserInteractionEnabled = true
                    Tools.showAlertViewinModalViewController(withText: NSLocalizedString("We dont have tickets for that date, please select another", comment: "We dont have tickets for that date, please select another"), ModalViewController: self)
                }
            }
            
            return
            
        }
        
        if name == ServiceName.GET_TOUR_DATES
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = (dataResult.object(forKey: "success") != nil)
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    
                    let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
                    if content.object(forKey: "valid_dates_from_now") != nil
                    {
                        let dates : NSArray = content.object(forKey: "valid_dates_from_now") as! NSArray
                        
                        for i in 0...(dates.count - 1)
                        {
                            let stringDate : String = dates.object(at: i) as! String
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                            
                            let dateObj : Date = dateFormatter.date(from: stringDate)!
                            arraySchedules.add(dateObj)
                        }
                        
                    }
                    else
                    {
                        let singleDate : String = content.object(forKey: "specific_date") as! String
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        
                        let dateObj : Date = dateFormatter.date(from: singleDate)!
                        arraySchedules.add(dateObj)
                    }
                    
                    
                    let dispatchTime = DispatchTime.now() + 0.30;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                    {
                        self.calendarView?.markDates(self.arraySchedules as Array)
                    }
                    
                }
            }
        
            return
        }
    }
    
    func onError(Error : String, name : ServiceName)
    {
    
        Tools.hiddenActivityView(inView: self)
        
        if name == ServiceName.UPDATE_TOUR
        {
            print("No se hizo el update")
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.view.isUserInteractionEnabled = true
                Tools.showAlertForBackendError(ModalViewController: self, errorDescription: NSLocalizedString("An error occurred while modifying your booking, please try again later.", comment: "An error occurred while modifying your booking, please try again later."))
            }
            
            return
        }
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.view.isUserInteractionEnabled = true
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
        
        
    }
    
    
    func loadSchedules()
    {
        let Service = TouridooServices.init(delegate: self)
        let id : Int = (currentTour?.id)!
        let idString = String(id)
        Service.GetToursDatesBy(id: idString)
    }
    
    @IBAction func cancelBooking()
    {
        print("Cancel booking, give me my money bitch! 🤑")
    }

    @IBAction func modifyBooking()
    {
        if selectedHour == nil
        {
            Tools.showAlertView(withText: NSLocalizedString("Please select a schedule", comment: "Please select a schedule"))
            return
        }
        
        if pickerIsInView == true
        {
            hiddenPickerView()
        }
        
        if nameTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your name", comment: "Please enter your name"))
            return
        }
        
        if lastNameTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your last name", comment: "Please enter your last name"))
            return
        }
        
        if mailTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your mail", comment: "Please enter your mail"))
            return
        }
        
        
        if cellPhoneTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your cell phone", comment: "Please enter your cell phone"))
            return
        }
        
        if cellPhoneTextField?.text?.characters.count != 10
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter a valid cell number (10 digits)", comment: "Please enter a valid cell number (10 digits)"))
            return
        }
        
        /*
        if hotelOrMeetingTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your hotel or meeting point", comment: "Please enter your hotel or meeting point"))
            return
        }
        
        if addreesTextField?.text == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter your address", comment: "Please enter your address"))
            return
        }*/
        
        self.view.endEditing(true)
        
        let dispatchTime = DispatchTime.now() + 0.10;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let AlertView = UIAlertController(title: NSLocalizedString("Do you want to modify your booking?", comment: "Do you want to modify your booking?"),
                                              message: NSLocalizedString("", comment: ""),
                                              preferredStyle: UIAlertControllerStyle.alert)
            
            AlertView.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: "Yes"),
                                              style: UIAlertActionStyle.default,
                                              handler: self.closeAction))
            
            AlertView.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel"),
                                              style: UIAlertActionStyle.cancel,
                                              handler: self.cancelAction))
            
            self.present(AlertView, animated: true, completion: nil)
            
        }
        
        
    }
    
    func closeAction(alert: UIAlertAction!)
    {
        let userdata = NewAccountModel.init()
        userdata.name = nameTextField?.text
        userdata.last_name = lastNameTextField?.text
        userdata.email = mailTextField?.text
        userdata.phone_prefix = selectedPrefix
        userdata.phone_number = cellPhoneTextField?.text
        userdata.rendezvous_point_name = ""//hotelOrMeetingTextField?.text
        userdata.rendezvous_point_address = ""//addreesTextField?.text
        userdata.hotel_reservation_owner_name = ""
        userdata.comments = commentsTextField?.text
        userdata.policy_accepted = true
        userdata.receive_info = true
        userdata.password = ""
        userdata.bookingData = bookingData
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFromString = dateFormatter.string(from: selectedHour!)
        
        bookingData.tourDate = dateFromString
      
        bookingData.idTour = currentTour?.id
        //bookingData.totalToPay = TotalToPay
        
        let numberOfTickets : Int = 3
        
        Tools.showActivityView(inView: self)
        self.view.isUserInteractionEnabled = false
        
        let Service = TouridooServices.init(delegate: self)
        let id : Int = (currentTour?.id)!
        let idString = String(id)
        let ticket = String(numberOfTickets)
        Service.checkTourAvailability(idtour: idString, tourdate: dateFromString, ocupantsNumber: ticket)
    }
    
    func cancelAction(alert: UIAlertAction!) {
        print("Cancel")
    }

    //PickerView methods
    
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dynamicScheuldesArray.count
    }
    // Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if dynamicScheuldesArray.count == 0 {
            return ""
        }
        
        if row == 0
        {
            return NSLocalizedString("Select", comment: "Select")
        }
        
        let dateTour : Date = dynamicScheuldesArray.object(at: row) as! Date
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "HH:mm"
        let dateFromString = dateFormatter.string(from: dateTour)
        
        return dateFromString

        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if dynamicScheuldesArray.count == 0 {
            return
        }
        
        if row == 0 {
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let stingDate : Date = dynamicScheuldesArray.object(at: row) as! Date
        //let dateObj = dateFormatter.date(from: stingDate)
        
        dateFormatter.dateFormat = "HH:mm"
        let dateFromString = dateFormatter.string(from: stingDate)
        
        selectAnHourLabel?.setTitle(dateFromString, for: UIControlState.normal)
        selectAnHourLabel?.titleLabel?.font = Tools.fontAppBold(withSize: 15)
        
        selectedHour = dynamicScheuldesArray.object(at: row) as? Date
        print("selected hour: " + String(describing: selectedHour))
    }
    
    @IBAction func selectAnHourAction()
    {
        Tools.feedback()
        
        if pickerIsInView == false {
            openPickerView()
        }else{
            hiddenPickerView()
        }
    }
    
    func openPickerView()
    {
        if dynamicScheuldesArray.count == 0
        {
            Tools.showAlertView(withText: NSLocalizedString("Please select a day", comment: "Please select a day"))
            return
        }
        
        
        self.scrollData?.bringSubview(toFront: self.pickerViewSchedules!)
        self.scrollData?.bringSubview(toFront: self.viewSchedule!)
        
        UIApplication.shared.keyWindow?.endEditing(true)
        
        pickerIsInView = true
        self.scrollData?.scrollRectToVisible((self.viewSchedule?.frame)!, animated: true)
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.pickerViewSchedules?.frame = CGRect(x: (self.pickerViewSchedules?.frame.origin.x)!,
                                                         y: (self.pickerViewSchedules?.frame.origin.y)!,
                                                         width: (self.pickerViewSchedules?.frame.size.width)!,
                                                         height: CGFloat(300))
                
        }, completion:
            { _ in
        
            self.scrollData?.scrollRectToVisible((self.pickerViewSchedules?.frame)!, animated: true)
        })
        
    }
    
    func hiddenPickerView()
    {
        pickerIsInView = false
        
        UIView.animate(withDuration: 0.30, animations:
            {
                
                self.pickerViewSchedules?.frame = CGRect(x: (self.pickerViewSchedules?.frame.origin.x)!,
                                                         y: (self.pickerViewSchedules?.frame.origin.y)!,
                                                         width: (self.pickerViewSchedules?.frame.size.width)!,
                                                         height: CGFloat(0))
        })
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if textField == nameTextField {
            lastNameTextField?.becomeFirstResponder()
        }
        
        if textField == lastNameTextField {
            mailTextField?.becomeFirstResponder()
        }
        
        if textField == mailTextField {
            cellPhoneTextField?.becomeFirstResponder()
        }
        
        /*
        if textField == hotelOrMeetingTextField {
            addreesTextField?.becomeFirstResponder()
        }
        
        if textField == addreesTextField {
            textField.resignFirstResponder()
        }
 */
        
        return true
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Next", comment: "Next"), style: UIBarButtonItemStyle.done, target: self, action:#selector(doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        cellPhoneTextField?.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        self.view.endEditing(true)
        //hotelOrMeetingTextField?.becomeFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var maxlenght : Int = 50;
        
        if textField == cellPhoneTextField
        {
            maxlenght = 10
        }
        
        let textlenght = textField.text?.characters.count
        
        let newlenght = textlenght! + string.characters.count - range.length
        return newlenght <= maxlenght
        
    }
    

    
    @IBAction func closeView()
    {
        self.navigationController?.popViewController(animated: true)
    }


}
