//
//  MeetingPointsModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 20/10/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class MeetingPointsModel: NSObject
{
    var id : Int
    var latitude : Float?
    var longitude : Float?
    var name : String?
    var descriptionText : String?
    var isSelected : Bool
    
    init(id : Int,
         latitude : Float?,
         longitude : Float?,
         name : String?,
         descriptionText : String?,
         isSelected : Bool)
    {
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.name = name
        self.descriptionText = descriptionText
        self.isSelected = isSelected
    }
}
