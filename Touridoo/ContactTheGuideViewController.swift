//
//  ContactTheGuideViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 19/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class ContactTheGuideViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }

}
