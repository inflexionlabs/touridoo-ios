//
//  AppDelegate.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 26/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, ResponseServicesProtocol
{
    var MainMenuApp : MainMenuViewController!
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction:"AfqNhB06mbc0uhhKDFa3GYcroTYBXXowvx5S2mFAS2W0LxtMLkSJm4OqrAKSpETf6ZZrYA5aRlstXNVO",
                                                               PayPalEnvironmentSandbox:"AfR1fW1w_-xtZsBqhC9Hr3Bfeu_KiznjRSav4B6w-M02CqhBX__RnSFNokDW8yiBI9gM-1SXpcHaIae2"])
        
        UserDefaults.standard.set(false, forKey: "hasData")
        UserDefaults.standard.set(false, forKey: "hasTour")
        UserDefaults.standard.set(-1, forKey: "tourState")
        UserDefaults.standard.set(false, forKey: "showReservationView");
        UserDefaults.standard.set(false, forKey: "noRateApp");
        UserDefaults.standard.set(nil, forKey: "image_guide")
        //UserDefaults.standard.set(NSDate.init(), forKey: "last_notifcation")
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            
            if granted {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        
        Fabric.with([Crashlytics.self])
        return true
    }
    
    func requestForNotifications()
    {
        let Service = TouridooServices.init(delegate: self)
        let deviceID : String? = UserDefaults.standard.object(forKey: "device_token") as? String
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        
        if deviceID != nil && (UserDefaults.standard.bool(forKey: "device_registered") == false) && (token != nil)
        {
            Service.RegisterForPushNotifications(deciveID: deviceID!)
        }
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.REGISTER_FOR_NOTIFICATIONS {
            
            UserDefaults.standard.set("true", forKey: "device_registered")
            return
        }
    }
    
    func onError(Error : String, name : ServiceName)
    {
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // Convert token to string
        UIApplication.shared.applicationIconBadgeNumber = 0
        let deviceTokenString : String = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        if deviceTokenString != ""
        {
            UserDefaults.standard.set(deviceTokenString, forKey: "device_token")
        }
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Did Fail to register for notifications")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
         UserDefaults.standard.set(NSDate.init(), forKey: "last_notifcation")
        
        let userInfoData : NSDictionary = userInfo as NSDictionary
        let DicData : NSDictionary = userInfoData.object(forKey: "aps") as! NSDictionary
        let notificationText : String = DicData.object(forKey: "alert") as! String
        print(notificationText)
        
        if MainMenuApp != nil && (UserDefaults.standard.bool(forKey: "hasTour") == false)
        {
            MainMenuApp.updateData()
        }
    
        print("Did receive notification")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

