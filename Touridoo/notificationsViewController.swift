//
//  notificationsViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 11/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class notificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableCities : UITableView?
    var arrayNotifications : NSMutableArray = NSMutableArray.init()
    @IBOutlet weak var noNotificationsLabel : UILabel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableCities?.delegate = self
        tableCities?.dataSource = self
        
        noNotificationsLabel?.text = NSLocalizedString("You don't have notifications",comment: "You don't have notifications")
        data()
        
    }
    
    func data()
    {
        /*
        let dicFAQ = NSMutableDictionary.init()
        
        dicFAQ.setObject(NSLocalizedString("", comment: ""), forKey: "title" as NSCopying)
        dicFAQ.setObject(NSLocalizedString("You don't have notifications",comment: "You don't have notifications"), forKey: "subtitle" as NSCopying)
        arrayNotifications.add(dicFAQ)
 */
        
        if arrayNotifications.count == 0
        {
            noNotificationsLabel?.alpha = 1
            tableCities?.alpha = 0
        }
        else
        {
            noNotificationsLabel?.alpha = 0
            tableCities?.alpha = 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNotifications.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : FAQCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FAQCell")  as! FAQCellTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let dicData : NSDictionary = arrayNotifications.object(at: indexPath.row) as! NSDictionary
        let questionString : String = dicData.object(forKey: "title") as! String
        let answerString : String = dicData.object(forKey: "subtitle") as! String
        
        cell.question?.text = questionString
        cell.answer?.text = answerString
        
        cell.question?.adjustsFontSizeToFitWidth = true
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
}
