//
//  TourListByCityViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 30/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TourListByCityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ResponseServicesProtocol, UIGestureRecognizerDelegate, UITextFieldDelegate
{

    @IBOutlet weak var TableTours : UITableView!
    @IBOutlet weak var titleView : UILabel?
    @IBOutlet weak var searchTextField : UITextField?
    
    let session : sessionData = sessionData.shared
    
    var currentCity : CityModel?
    var selectedIndexPath : IndexPath = IndexPath.init(row: 0, section: 0)
    var onRequest : Bool = false
    
    var searchMode : Bool = false
    var arraySearchResult = NSMutableArray.init()
    var onSearchRequest = false

    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        super.viewDidLoad()
        
        if session.finishDownloadTours == false
        {
            //Hardcode Array
            let Tour : TourModel = TourModel.init(id: 0,
                                                  image: Data.init(),
                                                  image_url: "",
                                                  duration_in_hours: 0,
                                                  short_name: "",
                                                  stars_average: 0,
                                                  rates_count: 0,
                                                  base10_stars_average: 0,
                                                  from_price: 0,
                                                  date: "",
                                                  type: "",
                                                  TopTenNumber: 0,
                                                  singlePrice: -1000,
                                                  prices: nil)
            session.tours.add(Tour)
            session.tours.add(Tour)
            session.tours.add(Tour)
            session.tours.add(Tour)
            
        }
        
        TableTours?.delegate = self
        TableTours?.dataSource = self
        
        self.TableTours.isUserInteractionEnabled = false
        
        searchTextField?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Where do you want to go?", comment: "Where do you want to go?"),attributes: [NSForegroundColorAttributeName: UIColor.white])
         searchTextField?.delegate = self
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        titleView?.text = currentCity?.name
        Tools.hiddenBurgerButton(hidden: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : TourCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Tour_cell") as! TourCellTableViewCell
        cell.contentView.alpha = 0
        var Tour : TourModel = session.tours[indexPath.row] as! TourModel
        
        if searchMode == true && (arraySearchResult.count != 0)
        {
            Tour = self.arraySearchResult[indexPath.row] as! TourModel
        }
        
        let nameString : String = Tour.name
        cell.name?.text = nameString
        cell.name?.adjustsFontSizeToFitWidth = true
        cell.lenguage?.text = NSLocalizedString("Spanish", comment: "Spanish")
   
        if Tour.image != nil {
            if Tour.image?.count != 0 {
                 cell.imageTour?.image = UIImage.init(data: Tour.image!)
            }
        }
        
        let topten : Int =  Tour.TopTenNumber!
        cell.toptenNumber?.text = String(topten)
        
        if indexPath.row > 9 {
            cell.toptenView?.isHidden = true
        } else {
            cell.toptenView?.isHidden = false
        }
        
        cell.setStars(stars: Tour.stars_average)
    
        if Tour.from_price != nil {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale.current
            let price : Float = (Tour.from_price )!
            
            let priceNumber : NSNumber = NSNumber(value: price)
            var priceWithFormatter : String =  formatter.string(from: priceNumber)!
            priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "$", with: "")
            priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "MX", with: "")
            
            let realPrice = " $" + priceWithFormatter
            
            let offerFloat : Float = price * Float(1.20)
            let offerNumber : NSNumber = NSNumber(value: offerFloat)
            var offetWithFormatter : String =  formatter.string(from: offerNumber)!
            offetWithFormatter = offetWithFormatter.replacingOccurrences(of: "$", with: "")
            offetWithFormatter = offetWithFormatter.replacingOccurrences(of: "MX", with: "")
            
            let offerPrice = "$" + offetWithFormatter
            
            //let superText = NSMutableAttributedString()
        
            let offertText = NSMutableAttributedString(string: offerPrice)
            offertText.addAttribute(NSStrikethroughStyleAttributeName, value: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue), range: NSMakeRange(0, offertText.length))
            offertText.addAttribute(NSStrikethroughColorAttributeName, value: UIColor.lightGray, range: NSMakeRange(0, offertText.length))
            offertText.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: NSRange(location:0,length:offertText.length))
            
            cell.offertLabel?.attributedText = offertText
            cell.offertLabel?.adjustsFontSizeToFitWidth = true
            
            //superText.append(offertText)
        
            let priceText = NSMutableAttributedString(string: realPrice,
                                                       attributes: [NSFontAttributeName:Tools.fontAppBold(withSize: 13)])
            priceText.addAttribute(NSForegroundColorAttributeName,
                                    value: UIColor.white,
                                    range: NSRange(location:0,length:priceText.length))
            
            //superText.append(priceText)
            cell.priceLabel?.attributedText = priceText
            cell.priceLabel?.adjustsFontSizeToFitWidth = true
        }
        
        let hours : Int = Tour.duration_in_hours
        let hoursString : String = String(hours)
        cell.duration?.text = hoursString + " " + NSLocalizedString("hours", comment: "hours")
        
        cell.imageTour?.alpha = 1;
        cell.shadowImage?.alpha = 0;
        cell.detailsView.alpha = 0;
        cell.toptenView?.alpha = 0;
        
        if session.finishDownloadTours {
            cell.shadowImage?.alpha = 1;
            cell.detailsView.alpha = 1;
            cell.toptenView?.alpha = 1;
        }
        
        if Tour.image?.count != 0 {
            cell.imageTour?.image = UIImage.init(data: Tour.image!)
            
            UIView.animate(withDuration: 0.30, animations: {
                    cell.imageTour?.alpha = 1;
                    cell.shadowImage?.alpha = 1;
                    cell.detailsView.alpha = 1;
            })
        } else {
            var RealImage : Data?
            let image_url : String? = Tour.image_url
            
            if image_url != nil {
                if image_url != "" {
                    DispatchQueue.global().async {
                            if image_url != nil {
                                let urlToDownload : URL = URL.init(string: image_url!)!
                                do {
                                    RealImage = try  Data.init(contentsOf: urlToDownload)
                                } catch {  print(error) }
                            }
                            
                            DispatchQueue.main.async {
                                    if RealImage != nil  {
                                        cell.imageTour?.image = UIImage.init(data: RealImage!)
                                        if Tour.image?.count == 0 {
                                            Tour.image = RealImage
                                        }
                                        
                                        UIView.animate(withDuration: 0.30, animations: {
                                                cell.imageTour?.alpha = 1;
                                                cell.shadowImage?.alpha = 1;
                                                cell.detailsView.alpha = 1;
                                        })
                                    }
                            }
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        Tools.feedback()
        let cell : TourCellTableViewCell = tableView.cellForRow(at: indexPath) as! TourCellTableViewCell
        cell.setSelected(false, animated: true)
        selectedIndexPath = indexPath
        UIApplication.shared.keyWindow?.endEditing(true)
        selectedCity()
    }
    
    func selectedCity()
    {
        let dispatchTime = DispatchTime.now() + 0.1;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let byCityController : TourDetailViewController  = self.storyboard!.instantiateViewController(withIdentifier: "TourDetailViewController") as! TourDetailViewController
            var Tour : TourModel = self.session.tours[self.selectedIndexPath.row] as! TourModel
            if self.searchMode == true && (self.arraySearchResult.count != 0)
            {
                Tour  = self.arraySearchResult[self.selectedIndexPath.row] as! TourModel
            }
            byCityController.currentTour = Tour
            self.navigationController?.pushViewController(byCityController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 255
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.searchMode == true && (self.arraySearchResult.count != 0)
        {
            return self.arraySearchResult.count
        }
        
        return session.tours.count
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if onRequest == true
        {
            return
        }
        
        Tools.showActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.10;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.loadTours()
        }
        /*
        if session.finishDownloadTours == false
        {
            
        }
        else
        {
            self.TableTours.reloadData()
            self.TableTours.isUserInteractionEnabled = true
            self.onRequest = true
        }*/
        
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        
        let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
        let success : Bool = dataResult.object(forKey: "success") as! Bool
        
        if success
        {
            if (dataResult.object(forKey: "content") != nil)
            {
                session.tours.removeAllObjects()
                
                let content : NSArray = dataResult.object(forKey: "content") as! NSArray
          
                if content.count != 0
                {
                    for integer in 0...(content.count - 1)
                    {
                        let tour : NSDictionary = content.object(at: integer) as! NSDictionary
                        
                        let id : Int = tour.object(forKey: "id") as! Int
                        let duration_in_hours : Int = tour.object(forKey: "duration_in_hours") as! Int
                        let stars_average : Int = tour.object(forKey: "stars_average") as! Int
                        let rates_count : Int = tour.object(forKey: "rates_count") as! Int
                        let base10_stars_average : Int = tour.object(forKey: "base10_stars_average") as! Int
                        let from_price : Float? = tour.object(forKey: "from_price") as? Float
                        let image_url : String? = tour.object(forKey: "image_url") as? String
                        let short_name : String = tour.object(forKey: "short_name") as! String
                        let TopTenNumber : Int? = tour.object(forKey: "TopTenNumber") as? Int
                        //let country_name : String = tour.object(forKey: "country_name") as! String
                        
                        var Prices: [PriceModel] = []
                        let nsArrayPrices : NSArray? = tour.object(forKey: "Prices") as? NSArray
                        
                        if nsArrayPrices != nil
                        {
                            for integer in 0...((nsArrayPrices?.count)! - 1)
                            {
                                let price : NSDictionary = nsArrayPrices!.object(at: integer) as! NSDictionary
                                
                                let id: Int = price.object(forKey: "Id") as! Int
                                let discount: Int = price.object(forKey: "Discount") as! Int
                                let value: Float = price.object(forKey: "Value") as! Float
                                let name: String = price.object(forKey: "Name") as! String
                                let description: String = price.object(forKey: "Description") as! String
                                let currency: String = price.object(forKey: "currency") as! String
                                
                                let PriceData: PriceModel = PriceModel.init(id: id, discount: discount, value: value, name: name, priceDescription: description, currency: currency)
                                
                                Prices.append(PriceData)
                            }
                        }
                        
                        let Tourdata : TourModel = TourModel.init(id: id,
                                                                  image: Data.init(),
                                                                  image_url: image_url,
                                                                  duration_in_hours: duration_in_hours,
                                                                  short_name: short_name,
                                                                  stars_average: stars_average,
                                                                  rates_count: rates_count,
                                                                  base10_stars_average: base10_stars_average,
                                                                  from_price: from_price,
                                                                  date: "",
                                                                  type: "",
                                                                  TopTenNumber: TopTenNumber,
                                                                  singlePrice: -1000,
                                                                  prices: Prices)
                        
                        session.tours.add(Tourdata)
                    }
                }
                
                
                let dispatchTime = DispatchTime.now() + 0.30;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    self.session.finishDownloadTours = true
                    self.TableTours.reloadData()
                    self.TableTours.isUserInteractionEnabled = true
                    self.onRequest = true
                    Tools.hiddenActivityView(inView: self)
                }
            }
        }
        else
        {
            Tools.hiddenActivityView(inView: self)
            
            let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
            let error_message : String = content.object(forKey: "error_message") as! String
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                Tools.showAlertViewinModalViewController(withText: error_message, ModalViewController: self)
            }
        }
        
    }
    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }
    
    func loadTours()
    {
        let Service = TouridooServices.init(delegate: self)
        let id : Int = (currentCity?.id)!
        let StringID : String = String(id)
        Service.GetTours(regionID: StringID)
    }
    
    @IBAction func closeView()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //SEARCH BAR METHODS
    
    func search(text : String)
    {
        print("Search: " + text)
        
        TableTours.isUserInteractionEnabled = false
        
        let services = TouridooServices.init(delegate: self)
        //services.searchTours(searchText: text)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxlenght : Int = 30;
        
        let textlenght = textField.text?.characters.count
        
        let newlenght = textlenght! + string.characters.count - range.length
        
        print("Chars: " + String(newlenght))
        
        if (newlenght >= 1)
        {
            if self.onSearchRequest == false
            {
                DispatchQueue.main.async
                    {
                        print("ON SEARCH REQUEST")
                        
                        self.onSearchRequest = true
                        self.searchMode = true
                        let searhText : String = (textField.text)! + string
                        self.search(text: searhText)
                }
            }
        }
        else
        {
            if self.searchMode == true
            {
                self.searchMode = false
                self.TableTours.reloadData()
            }
        }
        
        return newlenght <= maxlenght
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.50, animations: {
            cell.contentView.alpha = 1.0
        })
        
        //Tools.feedback()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if self.searchMode == true
        {
            self.searchMode = false
            self.TableTours.reloadData()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }

}
