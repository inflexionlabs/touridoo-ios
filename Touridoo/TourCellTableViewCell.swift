//
//  TourCellTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TourCellTableViewCell: UITableViewCell {

    @IBOutlet weak var name : UILabel?
    @IBOutlet weak var imageTour : UIImageView?
    @IBOutlet weak var shadowImage : UIImageView?
    @IBOutlet weak var detailsView : UIView!
    @IBOutlet weak var priceLabel : UILabel?
    @IBOutlet weak var offertLabel : UILabel?
    @IBOutlet weak var duration : UILabel?
    @IBOutlet weak var dateTourlabel : UILabel?
    @IBOutlet weak var attractionsNumber : UILabel?
    @IBOutlet weak var lenguage : UILabel?
    
    @IBOutlet weak var star1 : UIImageView?
    @IBOutlet weak var star2 : UIImageView?
    @IBOutlet weak var star3 : UIImageView?
    @IBOutlet weak var star4 : UIImageView?
    @IBOutlet weak var star5 : UIImageView?
    
    @IBOutlet weak var toptenNumber : UILabel?
    @IBOutlet weak var toptenView : UIView?
    @IBOutlet weak var toptenDescriptionLabel : UILabel?
    
    var starOff : UIImage = UIImage.init(named: "star_off.png")!
    var starOn : UIImage = UIImage.init(named: "star_on.png")!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        star1?.image = starOff
        star2?.image = starOff
        star3?.image = starOff
        star4?.image = starOff
        star5?.image = starOff
        
        toptenDescriptionLabel?.text = NSLocalizedString("Most visited!", comment: "Most visited!")
        toptenDescriptionLabel?.adjustsFontSizeToFitWidth = true
    }

    func setStars(stars : Int)
    {
        star1?.image = starOff
        star2?.image = starOff
        star3?.image = starOff
        star4?.image = starOff
        star5?.image = starOff
        
        if stars == 1
        {
            star1?.image = starOn
            star2?.image = starOff
            star3?.image = starOff
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 2
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOff
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 3
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 4
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOn
            star5?.image = starOff
        }
        
        if stars == 5
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOn
            star5?.image = starOn
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
