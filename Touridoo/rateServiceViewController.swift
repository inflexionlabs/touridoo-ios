//
//  rateServiceViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 21/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class rateServiceViewController: UIViewController
{
    
    @IBOutlet weak var option1 : UILabel?
    @IBOutlet weak var option2 : UILabel?
    @IBOutlet weak var option3 : UILabel?
    @IBOutlet weak var option4 : UILabel?
    @IBOutlet weak var option5 : UILabel?
    @IBOutlet weak var option6 : UILabel?
    @IBOutlet weak var option7 : UILabel?
    @IBOutlet weak var option8 : UILabel?
    
    @IBOutlet weak var radioButton1 : UIButton?
    @IBOutlet weak var radioButton2 : UIButton?
    @IBOutlet weak var radioButton3 : UIButton?
    @IBOutlet weak var radioButton4 : UIButton?
    @IBOutlet weak var radioButton5 : UIButton?
    @IBOutlet weak var radioButton6 : UIButton?
    @IBOutlet weak var radioButton7 : UIButton?
    @IBOutlet weak var radioButton8 : UIButton?
    
    @IBOutlet weak var sendButton : UIButton?
    @IBOutlet weak var skipButton : UIButton?
    @IBOutlet weak var headerView : UIView?
    

    override func viewWillAppear(_ animated: Bool)
    {
        
        let pathTop = UIBezierPath(roundedRect:(headerView?.bounds)!,
                                byRoundingCorners:[.topRight, .topLeft],
                                cornerRadii: CGSize(width: 5.0, height:  5.0))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = pathTop.cgPath
        headerView?.layer.mask = maskLayer
        
        let pathBottom = UIBezierPath(roundedRect:(skipButton?.bounds)!,
                                   byRoundingCorners:[.bottomLeft, .bottomRight],
                                   cornerRadii: CGSize(width: 5.0, height:  5.0))
        
        let maskLayerB = CAShapeLayer()
        
        maskLayerB.path = pathBottom.cgPath
        skipButton?.layer.mask = maskLayerB
        
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        sendButton?.setTitle(NSLocalizedString("Send", comment:"Send"), for: UIControlState.normal)
        skipButton?.setTitle(NSLocalizedString("Skip", comment:"Skip"), for: UIControlState.normal)
        
        radioButton1?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButton2?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButton3?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButton4?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButton5?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButton6?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButton7?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        radioButton8?.addTarget(self, action: #selector(radioAction(radioButton:)), for: UIControlEvents.touchUpInside)
        
        let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption1))
        option1?.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption2))
        option2?.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption3))
        option3?.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption4))
        option4?.addGestureRecognizer(tap4)
        
        let tap5 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption5))
        option5?.addGestureRecognizer(tap5)
        
        let tap6 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption6))
        option6?.addGestureRecognizer(tap6)
        
        let tap7 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption7))
        option7?.addGestureRecognizer(tap7)
        
        let tap8 = UITapGestureRecognizer.init(target: self, action: #selector(tapOption8))
        option8?.addGestureRecognizer(tap8)
        
        option1?.text = NSLocalizedString("Destinations offer variety", comment: "Destinations offer variety")
        option2?.text = NSLocalizedString("Our team attention", comment: "Our team attention")
        option3?.text = NSLocalizedString("Tour prices", comment: "Tour prices")
        option4?.text = NSLocalizedString("Transportation commodities", comment: "Transportation commodities")
        option5?.text = NSLocalizedString("Service is convenient and fast", comment: "Service is convenient and fast")
        option6?.text = NSLocalizedString("Schedules & itineraries", comment: "Schedules & itineraries")
        option7?.text = NSLocalizedString("Payment methods", comment: "Payment methods")
        option8?.text = NSLocalizedString("Our service adapts to your needs", comment: "Our service adapts to your needs")
        option8?.adjustsFontSizeToFitWidth = true
        
        option1?.isUserInteractionEnabled = true
        option2?.isUserInteractionEnabled = true
        option3?.isUserInteractionEnabled = true
        option4?.isUserInteractionEnabled = true
        option5?.isUserInteractionEnabled = true
        option6?.isUserInteractionEnabled = true
        option7?.isUserInteractionEnabled = true
        option8?.isUserInteractionEnabled = true
        
    }
    
    @IBAction func skipAction()
    {
        print("Send data")
        
         UserDefaults.standard.set(false, forKey: "hasTour")
        
        //Open MenuBarApp
        let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let mainMenu : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
        mainMenu.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(mainMenu, animated: true, completion: nil)
    
    }
    
    @IBAction func sendData()
    {
        self.skipAction()
    }

    @IBAction func radioAction(radioButton : UIButton)
    {
        Tools.feedback()
        
        if radioButton.isSelected {
            radioButton.isSelected = false
        }
        else
        {
            radioButton.isSelected = true
        }
    }
    
    func tapOption1()
    {
        radioAction(radioButton: radioButton1!)
    }
    func tapOption2()
    {
        radioAction(radioButton: radioButton2!)
    }
    func tapOption3()
    {
        radioAction(radioButton: radioButton3!)
    }
    func tapOption4()
    {
        radioAction(radioButton: radioButton4!)
    }
    func tapOption5()
    {
        radioAction(radioButton: radioButton5!)
    }
    func tapOption6()
    {
        radioAction(radioButton: radioButton6!)
    }
    func tapOption7()
    {
        radioAction(radioButton: radioButton7!)
    }
    func tapOption8()
    {
        radioAction(radioButton: radioButton8!)
    }
    

}
