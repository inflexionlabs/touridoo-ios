//
//  errorCodeCatalog.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 12/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class errorCodeCatalog: NSObject
{
    
    class func messageByCode(code : Int) -> String
    {
        if code == 0 {
            return ""
        }
        
        if code == 1 {
            return NSLocalizedString("Name is required", comment: "Name is required")
        }
        
        if code == 2 {
            return NSLocalizedString("Last name is required", comment: "Last name is required")
        }
        
        if code == 3 {
            return NSLocalizedString("Mail format is not correct", comment: "Mail format is not correct")
        }
        
        if code == 4 {
            return NSLocalizedString("You must accept the terms and conditions", comment: "You must accept the terms and conditions")
        }
        
        if code == 5 {
            return NSLocalizedString("Email is required", comment: "Email is required")
        }
        
        if code == 6 {
            return NSLocalizedString("The mail is already registered", comment: "The mail is already registered")
        }
        
        if code == 7 {
            return NSLocalizedString("The password does not match", comment: "The password does not match")
        }
        
        return ""
    }

}
