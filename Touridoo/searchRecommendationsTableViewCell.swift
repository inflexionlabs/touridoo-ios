//
//  searchRecommendationsTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 11/08/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class searchRecommendationsTableViewCell: UITableViewCell {

    @IBOutlet weak var title : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
