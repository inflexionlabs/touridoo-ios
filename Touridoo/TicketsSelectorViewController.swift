//
//  TicketsSelectorViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 21/10/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TicketsSelectorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ResponseServicesProtocol
{
    @IBOutlet weak var titleText : UILabel?
    @IBOutlet weak var NextButton : UIButton?
    @IBOutlet weak var lastTickets : UILabel?
    
    @IBOutlet weak var textViewDescriptionTouridoo : UITextView?
    @IBOutlet weak var tableTickets : UITableView!
    var arrayPrice: NSMutableArray = NSMutableArray.init()
    var totalTicketsLabel : UILabel!
    var currentTour : TourDescriptionModel?
    var bookingData : BookingDataReserve?
    var TotalToPay : String = ""
    var onRequest : Bool = false
    var ticketsAva : Int = 0
    
    var ticketsArray = NSMutableArray.init();
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        
        tableTickets?.delegate = self
        tableTickets?.dataSource = self
        
        if currentTour?.prices != nil
        {
            if currentTour?.prices?.count != 0
            {
                for integer in 0...((currentTour?.prices?.count)! - 1)
                {
                    let priceData : PriceModel =  (currentTour?.prices![integer])!
                    
                    let price : Float = priceData.value
                    let priceString : String = String(price)
                    
                    let dicData = NSMutableDictionary()
                    dicData.setObject(priceString, forKey: "price" as NSCopying)
                    dicData.setObject(priceData.name, forKey: "type" as NSCopying)
                    dicData.setObject(priceData.priceDescription, forKey: "description" as NSCopying)
                    dicData.setObject(priceData.currency, forKey: "currency" as NSCopying)
                    dicData.setObject(priceData.id, forKey: "Id" as NSCopying)
                    arrayPrice.add(dicData)
                    
                }
            }
        }
        
        let superText = NSMutableAttributedString()
        
        let firstText = NSLocalizedString("If you don't find the service that you’re looking for, contact us in ", comment: "If you don't find the service that you’re looking for, contact us in ")
        var atributteText = NSMutableAttributedString(string: firstText, attributes: [NSFontAttributeName:Tools.fontAppRegular(withSize: 12)])
        atributteText.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location:0,length:atributteText.length))
        superText.append(atributteText)
        
        let secondText : String = sessionData.touridooContactMail
        atributteText = NSMutableAttributedString(string: secondText, attributes: [NSFontAttributeName:Tools.fontAppRegular(withSize: 12)])
        atributteText.addAttribute(NSForegroundColorAttributeName, value: Tools.colorAppSecondary(), range: NSRange(location:0,length:atributteText.length))
        superText.append(atributteText)
        
        let thirtdText : String = NSLocalizedString(" to provide you a better service and the best touristic experience.", comment: " to provide you a better service and the best touristic experience.")
        atributteText = NSMutableAttributedString(string: thirtdText, attributes: [NSFontAttributeName:Tools.fontAppRegular(withSize: 12)])
        atributteText.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location:0,length:atributteText.length))
        superText.append(atributteText)
        
        textViewDescriptionTouridoo?.attributedText = superText
        textViewDescriptionTouridoo?.textAlignment = NSTextAlignment.center
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if (MainMenuApp.alertView != nil)
        {
            MainMenuApp.alertView.kill()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
        titleText?.text = NSLocalizedString("Tickets", comment: "Tickets")
        titleText?.adjustsFontSizeToFitWidth = true
        NextButton?.setTitle(NSLocalizedString("Search for availability", comment: "Search for availability"), for: UIControlState.normal)
        
        lastTickets?.text = NSLocalizedString("Tickets available: ", comment: "Tickets available: " ) + String(ticketsAva)
        lastTickets?.adjustsFontSizeToFitWidth = true
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        
    }
    
    @IBAction func NextAction()
    {
        let toPay : String = totalTicketsLabel.text!
        var toPayString = toPay.replacingOccurrences(of: "$", with: "")
        toPayString = toPayString.replacingOccurrences(of: ",", with: "")
        toPayString = toPayString.replacingOccurrences(of: " MXN", with: "")
        toPayString = toPayString.replacingOccurrences(of: "MX", with: "")
        let total : Float = Float(toPayString)!
        
        if total == 0
        {
            Tools.showAlertView(withText: NSLocalizedString("Please select the number of tickets you need", comment: "Please select the number of tickets you need"))
            return
        }
        
        TotalToPay = toPayString
        
        bookingData?.totalToPay = TotalToPay
      
        bookingData?.tickets = ticketsArray
        
        
        Tools.showActivityView(inView: self)
        self.view.isUserInteractionEnabled = false
        
        let dispatchTime = DispatchTime.now() + 0.50;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let Service = TouridooServices.init(delegate: self)
            let id : Int = (self.currentTour?.id)!
            let idString = String(id)
            let ticket = String(self.ticketsArray.count)
            let tourDate :String = (self.bookingData?.tourDate)!
            Service.checkTourAvailability(idtour: idString, tourdate: tourDate, ocupantsNumber: ticket)
        }
    
    }
    
    func onSucces(Result: String, name: ServiceName) {
        
        if name == ServiceName.CHECK_TOUR_AVAILABILITY
        {
            
            var tourAvailable : Bool = false
            
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = (dataResult.object(forKey: "success") != nil)
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
                    tourAvailable = content.object(forKey: "tour_available") as! Bool
                }
            }
            
            if tourAvailable == true
            {
                //Next Step
                let dispatchTime = DispatchTime.now() + 0.50;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    Tools.hiddenActivityView(inView: self)
                    
                    let ReserveController : PersonalDataForReserveViewController  = self.storyboard!.instantiateViewController(withIdentifier: "PersonalDataForReserveViewController") as! PersonalDataForReserveViewController
                    ReserveController.currentTour = self.currentTour
                    ReserveController.bookingData = self.bookingData
                    self.navigationController?.pushViewController(ReserveController, animated: true)
                }
            }
            else
            {
                let dispatchTime = DispatchTime.now() + 0.30;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    Tools.hiddenActivityView(inView: self)
                    self.NextButton?.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    Tools.showAlertViewinModalViewController(withText: NSLocalizedString("We do not have enough tickets by that date, please select another", comment: "We do not have enough tickets by that date, please select another"), ModalViewController: self)
                }
            }
            return
        }
    }
    
    func onError(Error: String, name: ServiceName) {
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.hiddenActivityView(inView: self)
            self.NextButton?.alpha = 1
            self.view.isUserInteractionEnabled = true
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("We do not have enough tickets by that date, please select another", comment: "We do not have enough tickets by that date, please select another"), ModalViewController: self)
        }
    }

    @IBAction func closeView()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //update Total Price Label
    func updateTotal()
    {
        if totalTicketsLabel != nil
        {
            ticketsArray.removeAllObjects()
            
            var total : Float = 0.0
            
            for indexCell in 0...(arrayPrice.count - 1)
            {
                let cell : NumberOfTicketsTableViewCell = tableTickets.cellForRow(at: IndexPath(row: indexCell, section: 0)) as! NumberOfTicketsTableViewCell
                let totalByCell : String = (cell.totalPriceLabel?.text)!
                var totalString = totalByCell.replacingOccurrences(of: "$", with: "")
                totalString = totalString.replacingOccurrences(of: ",", with: "")
                totalString = totalString.replacingOccurrences(of: ",", with: "")
                totalString = totalString.replacingOccurrences(of: " MXN", with: "")
                
                total = total + Float(totalString)!
                
                if cell.numberOfTickets >= 1
                {
                    for y in 1...cell.numberOfTickets
                    {
                        let dicData = NSDictionary.init(object: Int(cell.idPrice!), forKey: "idPrice" as NSCopying)
                        ticketsArray.add(dicData)
                    }
                }
                
            }
            
            let priceNumber : NSNumber = NSNumber(value: total)
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale.current
            let priceWithFormatter : String =  formatter.string(from: priceNumber)!
            
            totalTicketsLabel.text = priceWithFormatter
            totalTicketsLabel.text = totalTicketsLabel.text?.replacingOccurrences(of: "MX", with: "")
            let session = sessionData.shared
            totalTicketsLabel.text = totalTicketsLabel.text! + " " + session.currency!
            totalTicketsLabel.adjustsFontSizeToFitWidth = true
            
            Tools.feedback()
            
            print("Number of Tickets: " + String(ticketsArray.count))
            print("")
            
            if ticketsArray.count <= ticketsAva
            {
                let resultTickets : Int = ticketsAva - ticketsArray.count
                lastTickets?.text = NSLocalizedString("Tickets available: ", comment: "Tickets available: " ) + String(resultTickets)
                lastTickets?.adjustsFontSizeToFitWidth = true
            }
            
        }
    }
    
    //TableView methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.row == arrayPrice.count
        {
            //Cell total prices from Tickets
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "ticketsTotalPrice_cell")!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            totalTicketsLabel = cell.contentView.viewWithTag(90) as! UILabel
            return cell
        }
        else
        {
            let cell : NumberOfTicketsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Tickets_cell") as! NumberOfTicketsTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            let dicData : NSMutableDictionary = arrayPrice.object(at: indexPath.row) as! NSMutableDictionary
            let priceString : String = dicData.object(forKey: "price") as! String
            let typeString : String = dicData.object(forKey: "type") as! String
            let descriptionString : String = dicData.object(forKey: "description") as! String
            let currencyString : String = dicData.object(forKey: "currency") as! String
            let idPrice : Int? = dicData.object(forKey: "Id") as! Int?
            
            cell.price = Float(priceString)!
            cell.TypeLabel?.text = typeString + " - " + descriptionString + ""
            cell.TypeLabel?.adjustsFontSizeToFitWidth = true
            cell.priceLabel?.text = Tools.currencyString(number: Float(priceString)!)
            cell.priceLabel?.text = cell.priceLabel?.text?.replacingOccurrences(of: "MX", with: "")
            cell.priceLabel?.text = (cell.priceLabel?.text)! + " " + currencyString.uppercased()
            cell.priceLabel?.adjustsFontSizeToFitWidth = true
            cell.delegateTableViewController = self
            cell.idPrice = idPrice
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPrice.count + 1
    }
    
}
