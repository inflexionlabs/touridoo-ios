//
//  TourRunModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TourRunModel: NSObject
{
    var id : Int?
    var id_tour : Int?
    var id_tour_step : Int?
    var tour_date : String?
    var started_at : String?
    var tour_steps : NSArray?
    var has_tour_ended : Bool?
    var has_tour_started : Bool?
    var car : CarModel?
    var id_car : Int?
    var id_guide_user : Int?
    var current_longitude : Float?
    var current_latitude : Float?
    var guide : GuideModel?

    init(id : Int?, id_tour : Int?, id_tour_step : Int?, tour_date : String?, started_at : String?, tour_steps : NSArray?, has_tour_ended : Bool?, car : CarModel?, id_car : Int?, id_guide_user : Int?,current_longitude : Float?, current_latitude : Float?, guide : GuideModel?,has_tour_started : Bool?)
    {
        self.id = id
        self.id_tour = id_tour
        self.id_tour_step = id_tour_step
        self.tour_date = tour_date
        self.started_at = started_at
        self.tour_steps = tour_steps
        self.has_tour_ended = has_tour_ended
        self.car = car
        self.id_car = id_car
        self.id_guide_user = id_guide_user
        self.current_latitude = current_latitude
        self.current_longitude = current_longitude
        self.guide = guide
        self.has_tour_started = has_tour_started
    }
    
}
