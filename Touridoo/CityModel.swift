//
//  CityModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 01/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class CityModel: NSObject
{
    var id : Int?
    var name : String?
    var image : Data?
    var descriptionRegion : String?
    var country_name : String?
    var latitude : String?
    var longitud : String?
    var imageURL : String?
    var attractions : Int?
    var type : String?
    
    init( id : Int?,
          name : String?,
          image: Data?,
          descriptionRegion : String?,
          country_name : String?,
          latitude : String?,
          longitud : String?,
          imageURL : String?,
          attractions : Int?,
          type : String?)
    {
        self.id = id
        self.name = name
        self.image = image
        self.descriptionRegion = descriptionRegion
        self.country_name = country_name
        self.latitude = latitude
        self.longitud = longitud
        self.imageURL = imageURL
        self.attractions = attractions
        self.type = type
    }
}
