//
//  Tools.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 29/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class Tools: NSObject
{
    //Create default alert for Services errors
    class func printAlertForError (ErrorDescription : String, currentViewController : UIViewController)
    {
        let dispatchTime = DispatchTime.now() + 0.15;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let AlertView = UIAlertController(title: "Oooops!", message: ErrorDescription, preferredStyle: UIAlertControllerStyle.alert)
            AlertView.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: UIAlertActionStyle.default, handler: nil))
            currentViewController.present(AlertView, animated: true, completion: nil)
        }
        
    }
    
    //return true if the screen size is equal to the iPhone 7 or 7 Plus
    class func iPhonesote() -> Bool
    {
        let screenWidth = Int(UIScreen.main.bounds.size.width);
        
        if (screenWidth == 414 || screenWidth == 375)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //return true if the screen size is equal to the iPhone 4, iPhone SE (5 & 5S) or iPad
    class func iPhonecito() -> Bool
    {
        let screenWidth = Int(UIScreen.main.bounds.size.height);
        
        if (screenWidth == 480 || screenWidth == 568)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    class func iPhone4() -> Bool
    {
        let screenWidth = Int(UIScreen.main.bounds.size.height);
        
        if (screenWidth == 480)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    class func iPhone5() -> Bool
    {
        let screenWidth = Int(UIScreen.main.bounds.size.height);
        
        if (screenWidth == 568)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    class func iPhone7() -> Bool
    {
        let screenWidth = Int(UIScreen.main.bounds.size.width);
        
        if (screenWidth == 375)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    class func iPhone7Plus() -> Bool
    {
        let screenWidth = Int(UIScreen.main.bounds.size.width);
        
        if (screenWidth == 414)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    class func iPhoneX() -> Bool
    {
        let screenWidth = Int(UIScreen.main.bounds.size.height);
        
        if (screenWidth == 812)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //Move UI Helper
    
    class func UpView(View : UIView, Points : CGFloat)
    {
        var NewFrame : CGRect = View.frame
        NewFrame.origin.y = View.frame.origin.y - Points
        View.frame = NewFrame
    }
    
    class func DownView(View : UIView, Points : CGFloat)
    {
        var NewFrame : CGRect = View.frame
        NewFrame.origin.y = View.frame.origin.y + Points
        View.frame = NewFrame
    }
    
    class func PushViewCenter(View : UIView, Points : CGFloat)
    {
        var NewFrame : CGPoint = View.center
        NewFrame.x = View.center.x + Points;
        View.center = NewFrame
    }
    
    class func PullViewCenter(View : UIView, Points : CGFloat)
    {
        var NewFrame : CGPoint = View.center
        NewFrame.x = View.center.x - Points;
        View.center = NewFrame
    }
    
    class func ShortenHeight(View : UIView, Points : CGFloat)
    {
        var NewFrame : CGRect = View.frame
        NewFrame.size.height = View.frame.size.height - Points
        View.frame = NewFrame
    }
    
    class func AddHeight(View : UIView, Points : CGFloat)
    {
        var NewFrame : CGRect = View.frame
        NewFrame.size.height = View.frame.size.height + Points
        View.frame = NewFrame
    }
    
    class func AddWidth(View : UIView, Points : CGFloat)
    {
        var NewFrame : CGRect = View.frame
        NewFrame.size.width = View.frame.size.width + Points;
        View.frame = NewFrame
    }
    
    class func ShortenWidth(View : UIView, Points : CGFloat)
    {
        var NewFrame : CGRect = View.frame
        NewFrame.size.width = View.frame.size.width - Points;
        View.frame = NewFrame
    }
    
    class func hiddenBurgerButton(hidden : Bool)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        MainMenuApp.burgerButton?.isHidden = hidden
        MainMenuApp.panGesture?.isEnabled = !hidden
    }
    
    //Convert NSDictionary To String
    class func DictionaryToJSONData(jsonObject: AnyObject) throws -> String?
    {
        let data: NSData? = try? JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
        
        var jsonStr: String?
        if data != nil {
            jsonStr = String(data: data! as Data, encoding: String.Encoding.utf8)
        }
        
        return jsonStr
    }
    
    //Show ActivityIndicatorView in ViewController
    class func showActivityView(inView : UIViewController)
    {
        if UserDefaults.standard.bool(forKey: "hasTour") == true
        {
            return
        }
        
        //Block ViewController for animation & request
        print("YOU SHALL... NO PASS! ✋🏼👳🏼🤚🏼")
        inView.view.isUserInteractionEnabled = false;
        
        //Create LoaderController var
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil);
        let activityViewController : LoaderController = mainStoryboard.instantiateViewController(withIdentifier: "LoaderController") as! LoaderController
        
        let screenFrame : CGRect = UIScreen.main.bounds
        
        activityViewController.view.frame = screenFrame
        activityViewController.view.alpha = 0;
        activityViewController.view.tag = 789;
        activityViewController.setText(text: "")
        activityViewController.textDescription?.adjustsFontSizeToFitWidth = true
        
        inView.view.addSubview(activityViewController.view);
        inView.view.bringSubview(toFront: activityViewController.view)
        inView.parent?.view.bringSubview(toFront: activityViewController.view)
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
        {
            activityViewController.view.alpha = 1;
            
        }, completion:nil)
 
    }
    
    //Hidden ActivityIndicatorView in ViewController
    class func hiddenActivityView(inView : UIViewController)
    {
        
        for subView : UIView in inView.view.subviews
        {
            //Search in viewController for View with tag 789 (LoaderController)
            if subView.tag == 789 {
                
                let dispatchTime = DispatchTime.now() + 0.10
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    
                    UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                        {
                            subView.alpha = 0;
                            
                    }, completion:{ _ in
                    
                         subView.removeFromSuperview();
                    })
                    
                }
            }
        }
        
        //Unblock ViewController
        print("Unblock viewcontroller")
        inView.view.isUserInteractionEnabled = true;

    }
    
    //Font App 
    class func fontAppRegular(withSize : CGFloat) -> UIFont
    {
        return UIFont(name: "Montserrat-Regular", size: withSize)!
    }
    
    class func fontAppBold(withSize : CGFloat) -> UIFont
    {
        return UIFont(name: "Montserrat-SemiBold", size: withSize)!
    }
    
    //Convert String (response) to Nsdictionary
    class func JSONDataToDiccionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    //return round image
    class func RoundImage(cornerRadius: Float, image: UIImage) -> UIImage {
        
        let imageView = UIImageView(image: image)
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, 1.0);
        UIBezierPath(roundedRect: imageView.bounds, cornerRadius:CGFloat(cornerRadius)).addClip()
        image.draw(in: imageView.bounds)
        imageView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return imageView.image!
        
    }
    
    class func colorApp() -> UIColor
    {
        return UIColor.init(colorLiteralRed: 227.0 / 255.0, green: 34.0 / 255.0, blue: 48.0 / 255.0, alpha: 1)
    }
    
    class func colorAppSecondary() -> UIColor
    {
        return UIColor.init(colorLiteralRed: 182.0 / 255.0, green: 26.0 / 255.0, blue: 42.0 / 255.0, alpha: 1)
    }
    
    //get random color for UI testing
    class func getRandomColor() -> UIColor
    {
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
    class func feedback()
    {
        let generator = UIImpactFeedbackGenerator(style: UIImpactFeedbackStyle.light)
        generator.impactOccurred()
    }
    
    
    class func showAlertWithGenericBackendError()
    {
        showAlertView(withText:NSLocalizedString("An error occurred in the system, please try again later", comment: "An error occurred in the system, please try again later"))
    }
    
    class func showAlertWithGenericBackendErrorInModalViewController(ModalViewController : UIViewController)
    {
        showAlertViewinModalViewController(withText:NSLocalizedString("An error occurred in the system, please try again later", comment: "An error occurred in the system, please try again later"),ModalViewController :ModalViewController)
    }
 
    
    class func showAlertForBackendError(errorDescription : String)
    {
        if errorDescription == "" {
            return
        }
        
        showAlertView(withText:errorDescription)
    }
    
    class func showAlertForBackendError(ModalViewController : UIViewController, errorDescription : String)
    {
        if errorDescription == "" {
            return
        }
        
        showAlertViewinModalViewController(withText: errorDescription,
            ModalViewController :ModalViewController)
    }
    
    //Show AlertView
    class func showAlertView(withText : String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if (MainMenuApp.alertView != nil)
        {
            return
        }
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
        
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil);
        let Alertview : AlertWindow = mainStoryboard.instantiateViewController(withIdentifier: "AlertWindow") as! AlertWindow
        
        let screenFrame : CGRect = CGRect(x: 0,
                                          y: 0,
                                          width: UIScreen.main.bounds.size.width,
                                          height: 60)
        
        Alertview.view.frame = screenFrame
        
        Alertview.setText(withText: withText)
       
        MainMenuApp.view.addSubview(Alertview.view);
        MainMenuApp.view.bringSubview(toFront: Alertview.view)
        MainMenuApp.alertView = Alertview
        
        Alertview.view.center = MainMenuApp.view.center
      
        Alertview.doYourMagic()
        
        var time : Float = 0
        time = (Float(withText.characters.count) / Float(13.0))
        if time < 3.0
        {
            time = 3.0
        }
        
        let dispatchTime = (DispatchTime.now() + 3.5)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Alertview.hidden()
        }
    }
    
    class func showNotConnectionAvailable ()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if (MainMenuApp.withoutConnectionView != nil)
        {
            return
        }
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
        
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil);
        let withoutConnectionView : withoutConnection = mainStoryboard.instantiateViewController(withIdentifier: "withoutConnection") as! withoutConnection
        
        let screenFrame : CGRect = CGRect(x: 0,
                                          y: UIScreen.main.bounds.size.height - 105,
                                          width: UIScreen.main.bounds.size.width,
                                          height: 100)
        
        withoutConnectionView.view.frame = screenFrame
        
        MainMenuApp.view.addSubview(withoutConnectionView.view);
        MainMenuApp.view.bringSubview(toFront: withoutConnectionView.view)
        MainMenuApp.withoutConnectionView = withoutConnectionView
    
        withoutConnectionView.show()
        
        let dispatchTime = (DispatchTime.now() + 10.0)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            withoutConnectionView.hidden()
        }
    }
    
    class func showNoSearchResult()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if (MainMenuApp.noSearchResultView != nil)
        {
            return
        }
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
        
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil);
        let withoutConnectionView : noSearchResults = mainStoryboard.instantiateViewController(withIdentifier: "noSearchResults") as! noSearchResults
        
        let screenFrame : CGRect = CGRect(x: 0,
                                          y: 0,
                                          width: UIScreen.main.bounds.size.width,
                                          height: 50)
        
        withoutConnectionView.view.frame = screenFrame
        
        MainMenuApp.view.addSubview(withoutConnectionView.view);
        MainMenuApp.view.bringSubview(toFront: withoutConnectionView.view)
        MainMenuApp.noSearchResultView = withoutConnectionView
        
        withoutConnectionView.show()
        
        let dispatchTime = (DispatchTime.now() + 3.0)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            withoutConnectionView.hidden()
        }
    }
    
    class func showDoneView(withText : String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if (MainMenuApp.doneView != nil)
        {
            return
        }
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
        
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil);
        let doneView : doneView = mainStoryboard.instantiateViewController(withIdentifier: "doneView") as! doneView
        
        let screenFrame : CGRect = CGRect(x: 0,
                                          y: 0,
                                          width: UIScreen.main.bounds.size.width,
                                          height: 100)
        
        doneView.view.frame = screenFrame
        
        MainMenuApp.view.addSubview(doneView.view);
        MainMenuApp.view.bringSubview(toFront: doneView.view)
        MainMenuApp.doneView = doneView
        
        doneView.textDisplay?.text = withText
        doneView.show()
        
        let dispatchTime = (DispatchTime.now() + 4.0)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            doneView.hidden()
        }
    }
    
    class func showSlowConnectionView ()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if (MainMenuApp.slowConnectionView != nil)
        {
            return
        }
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
        
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil);
        let slowConnectionView : slowConnection = mainStoryboard.instantiateViewController(withIdentifier: "slowConnection") as! slowConnection
        
        let screenFrame : CGRect = CGRect(x: 0,
                                          y: UIScreen.main.bounds.size.height - 105,
                                          width: UIScreen.main.bounds.size.width,
                                          height: 100)
        
        slowConnectionView.view.frame = screenFrame
        
        MainMenuApp.view.addSubview(slowConnectionView.view);
        MainMenuApp.view.bringSubview(toFront: slowConnectionView.view)
        MainMenuApp.slowConnectionView = slowConnectionView
        
        slowConnectionView.show()

    }
    
    class func hiddeSlowConnectionView ()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        if (MainMenuApp.slowConnectionView != nil)
        {
            MainMenuApp.slowConnectionView.hidden()
        }
    }
    
    
    //Show AlertView
    class func showAlertViewinModalViewController(withText : String, ModalViewController : UIViewController)
    {
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.warning)
        
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil);
        let Alertview : AlertWindow = mainStoryboard.instantiateViewController(withIdentifier: "AlertWindow") as! AlertWindow
        
        let screenFrame : CGRect = CGRect(x: 0,
                                          y: 0,
                                          width: UIScreen.main.bounds.size.width,
                                          height: 60)
        
        Alertview.view.frame = screenFrame
        
        Alertview.setText(withText: withText)
        
        
        ModalViewController.view.addSubview(Alertview.view);
        ModalViewController.view.bringSubview(toFront: Alertview.view)
        
        Alertview.view.center = ModalViewController.view.center
        
        Alertview.doYourMagic()
        
        var time : Float = 0
        time = (Float(withText.characters.count) / Float(13.0))
        if time < 3.0
        {
            time = 3.0
        }
        
        let dispatchTime = (DispatchTime.now() + 3.5)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Alertview.hiddenInModalViewController()
        }
       
    }
    
    class func doPushEffect(viewEffect : UIView, duration : Double, scale : CGFloat)
    {
        self.feedback()
        
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                viewEffect.transform = CGAffineTransform(scaleX: scale, y: scale)
                viewEffect.alpha = 0.75
                
        }, completion:
            { _ in
                UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                    {
                        viewEffect.transform = CGAffineTransform(scaleX: 1, y: 1)
                        viewEffect.alpha = 1
                        
                }, completion:nil)
                
        })
    }
    
    class func doRebound(viewEffect : UIView)
    {
        self.feedback()
        
        UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                viewEffect.transform = CGAffineTransform(scaleX: 1.02, y: 1.02)
                
        }, completion:
            { _ in
                UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                    {
                        viewEffect.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion:nil)
                
        })
    }
    
    class func shakeAnimation(viewAnimation : UIView, moveScale : CGFloat, minimumAlpha : CGFloat, duration : CGFloat)
    {
        let partialDuration : CGFloat = duration / CGFloat(3)
        
        UIView.animate(withDuration: TimeInterval(partialDuration), delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                Tools.PushViewCenter(View: viewAnimation, Points: moveScale)
                viewAnimation.alpha = minimumAlpha
                
        }, completion:
            { _ in
                UIView.animate(withDuration: TimeInterval(partialDuration), delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                    {
                        Tools.PullViewCenter(View: viewAnimation, Points: (moveScale * CGFloat(2)))
                        
                }, completion:
                    { _ in
                        UIView.animate(withDuration: TimeInterval(partialDuration), delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                            {
                                Tools.PushViewCenter(View: viewAnimation, Points: moveScale)
                                viewAnimation.alpha = 1.0
                                
                        }, completion:nil)
                })
        })
    }
    
    class func currencyString(number : Float) -> String
    {
        let price : Float = number
        let priceNumber : NSNumber = NSNumber(value: price)
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        
        var priceWithFormatter : String =  formatter.string(from: priceNumber)!
        priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "$", with: "")
        priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "MX", with: "")
        
        return priceWithFormatter
    }
    
    class func openPrefixSelector(inView : UIViewController, withTextField : UITextField)
    {
        inView.view.endEditing(true)
        
        UIApplication.shared.keyWindow?.endEditing(true)
        
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let prefixSelector : SelectPrefixView = story.instantiateViewController(withIdentifier: "SelectPrefixView") as! SelectPrefixView
        prefixSelector.view.alpha = 0;
        prefixSelector.viewOptions?.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        prefixSelector.textFieldDelegate = withTextField
        
        inView.view.addSubview(prefixSelector.view)
        inView.addChildViewController(prefixSelector)
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                prefixSelector.view.alpha = 1;
                prefixSelector.viewOptions?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
        }, completion:
            { _ in
        })
    }
    
    class func roundTopCorner(targetView : UIView, radius : CGFloat)
    {
        let maskPAth1 = UIBezierPath(roundedRect: targetView.bounds,
                                     byRoundingCorners: [.topLeft , .topRight],
                                     cornerRadii:CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = targetView.bounds
        maskLayer1.path = maskPAth1.cgPath
        targetView.layer.mask = maskLayer1
    }
    
    class func shortImageSize(image : UIImage) -> UIImage
    {
        UIGraphicsBeginImageContext(CGSize(width: image.size.width * (0.1), height: image.size.height * (0.1)))
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width * (0.1), height: image.size.height * (0.1)))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    class func showTicketForTour(image : UIImage?, tourData : Tour, destinationController : UIViewController, paymentView : PaymentMethodViewController?)
    {
        UIApplication.shared.keyWindow?.endEditing(true)
        
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let ticketView : TicketViewController = story.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
        
        if paymentView != nil
        {
            ticketView.delegatePaymentView = paymentView
        }
        
        ticketView.setTicketData(image: image, tourData: tourData)
        ticketView.view.alpha = 0;
        
        destinationController.view.addSubview(ticketView.view)
        destinationController.addChildViewController(ticketView)
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                ticketView.view.alpha = 1;
                
        }, completion:
            { _ in
        })
    }
    
    
}
