//
//  myreservationDetailViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 26/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class myreservationDetailViewController: UIViewController, UIGestureRecognizerDelegate
{
    // Labels estaticos
    @IBOutlet weak var bookingNumberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var meetingPointLabel: UILabel!
    @IBOutlet weak var peopleLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!

    @IBOutlet weak var duration : UILabel?
    @IBOutlet weak var titleLable : UILabel?
    @IBOutlet weak var descriptionlabel: UILabel?
    @IBOutlet weak var datelabel : UILabel?
    
    @IBOutlet weak var bookingNumber : UILabel?
    @IBOutlet weak var date : UILabel?
    @IBOutlet weak var hour : UILabel?
    @IBOutlet weak var meetingPoint : UILabel?
    @IBOutlet weak var payment : UILabel?
    @IBOutlet weak var people : UILabel?
    @IBOutlet weak var imageTour : UIImageView?
    @IBOutlet weak var modifyBooking : UIButton?
    @IBOutlet weak var setReminderButton: UIButton!
    @IBOutlet weak var detailsButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    var currentTour : Tour?
    var image : Data?
    //var imageFromTour: UIImage?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        if Tools.iPhone4(){
            Tools.AddHeight(View: self.scrollView, Points: 80)
            scrollView.contentSize = CGSize(width: scrollView.frame.size.width, height: scrollView.frame.size.height + CGFloat(70))
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        Tools.hiddenBurgerButton(hidden: true)
        
        if currentTour?.booking?.tour_date != nil
        {
            let stingDate : String = (currentTour?.booking?.tour_date!)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            let dateObj = dateFormatter.date(from: stingDate)
            
            var hourString : String?
            dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm"
            hourString = dateFormatter.string(from: dateObj!)
            
            datelabel?.text = hourString
            date?.text = hourString
            
            var singleHour : String?
            dateFormatter.dateFormat = "HH:mm"
            singleHour = dateFormatter.string(from: dateObj!)
            
            hour?.text = singleHour
            
            //Date validation
            let validateDate : Date = NSDate.init().addingTimeInterval((86400 * 3)) as Date
            var valid : Bool = false
            
            switch validateDate.compare(dateObj!)
            {
            case .orderedAscending     :
                valid = true
            case .orderedDescending    :
                valid = false
            case .orderedSame          :
                valid = false
            }
            
            if valid == false
            {
                modifyBooking?.isEnabled = false
                modifyBooking?.alpha = 0.6
            }
        }
        
        if currentTour?.name != nil
        {
            descriptionlabel?.text = currentTour?.name
            descriptionlabel?.adjustsFontSizeToFitWidth = true
        }
        
        if currentTour?.booking?.TotalPrice != nil
        {
            let pay : Float = (currentTour?.booking?.TotalPrice)!
            payment?.text = Tools.currencyString(number: pay)
            let ticket : PriceModel = (currentTour?.booking?.tickets![0])!
            payment?.text = (payment?.text)! + " " + ticket.currency
        }
        
        titleLable?.text = NSLocalizedString("My booking", comment: "My booking")
        bookingNumberLabel.text = NSLocalizedString("Booking number", comment: "Booking number")
        dateLabel.text = NSLocalizedString("Date", comment: "Date")
        hourLabel.text = NSLocalizedString("Hour", comment: "Hour")
        meetingPointLabel.text = NSLocalizedString("Meeting point", comment: "Meeting point")
        peopleLabel.text = NSLocalizedString("Tickets", comment: "Tickets")
        paymentLabel.text = NSLocalizedString("Payment", comment: "Payment")
        setReminderButton.setTitle(NSLocalizedString("Set reminder", comment: "Set reminder"), for: .normal)
        detailsButton.setTitle(NSLocalizedString("Details and recommendations", comment: "Details and recommendations"), for: .normal)
        modifyBooking?.setTitle(NSLocalizedString("Modify my booking", comment: "Modify my booking"), for: .normal)
        
        if currentTour?.duration_in_hours != nil
        {
            let num : Int = (currentTour?.duration_in_hours)!
            let hours : String = String(num)
            duration?.text = hours + " " + NSLocalizedString("hours", comment: "hours")
        }
         
        if currentTour?.booking?.booking_code != nil
        {
            let upperCase = currentTour?.booking?.booking_code?.uppercased()
            bookingNumber?.text = upperCase
        }
        
        var numberOftickets : Int = 0
        
        numberOftickets = (currentTour?.booking?.tickets?.count)!
        people?.text = String(numberOftickets)
        
        self.meetingPoint?.text = currentTour?.booking?.meetingPoint
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if image?.count != 0 && image != nil
        {
            self.imageTour?.image = UIImage.init(data: image!)
        }
        else
        {
            var RealImage : Data?
            let image_url : String? = self.currentTour?.image_url
            
            if image_url != nil
            {
                if image_url != ""
                {
                    DispatchQueue.global().async
                        {
                            
                            if image_url != nil
                            {
                                let urlToDownload : URL = URL.init(string: image_url!)!
                                do
                                {
                                    RealImage = try  Data.init(contentsOf: urlToDownload)
                                }
                                catch
                                {
                                    print(error)
                                }
                            }
                            
                            DispatchQueue.main.async
                                {
                                    if RealImage != nil
                                    {
                                        self.imageTour?.image = UIImage.init(data: RealImage!)
                                        self.image = RealImage
                                    }
                            }
                    }
                }
            }
        }
        
        if UserDefaults.standard.bool(forKey: "showReservationView") == true
        {
            let welcomeT = NSLocalizedString("Thank you for your booking!", comment: "Thank you for your booking!")
            
            Tools.showDoneView(withText: welcomeT)
        }
        
        UserDefaults.standard.set(false, forKey: "showReservationView");
    }
    
    @IBAction func closeView()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func detailsAndrecomendations()
    {
        //let story = UIStoryboard.init(name: "OnTour", bundle: nil)
        let DetailsController : DetailsAndRecommendationsBookingViewController = self.storyboard!.instantiateViewController(withIdentifier: "DetailsAndRecommendationsBookingViewController") as! DetailsAndRecommendationsBookingViewController
        DetailsController.currentTour = self.currentTour
        self.present(DetailsController, animated: true, completion: nil)
    }
    
    @IBAction func showTicket()
    {
        Tools.showTicketForTour(image: self.imageTour?.image,
                                tourData: self.currentTour!,
                                destinationController: self,
                                paymentView: nil)
    }
    
    @IBAction func setUpReminder()
    {
        print("set up reminder")
        
        let reminderView : setupReminderViewController = self.storyboard!.instantiateViewController(withIdentifier: "setupReminderViewController") as! setupReminderViewController
        reminderView.currentTour = self.currentTour
        reminderView.view.frame = UIScreen.main.bounds
        reminderView.view.alpha = 0
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        MainMenuApp.view.addSubview(reminderView.view)
        MainMenuApp.addChildViewController(reminderView)
        
        UIView.animate(withDuration: 0.30, animations: {reminderView.view.alpha = 1})

    }
    
    @IBAction func manageReservation()
    {
        print("manage reservation")
        let manageReservationView : manageReservationViewController  = self.storyboard!.instantiateViewController(withIdentifier: "manageReservationViewController") as! manageReservationViewController
        manageReservationView.currentTour = self.currentTour
        manageReservationView.image = self.image
        self.navigationController?.pushViewController(manageReservationView, animated: true)
    }

}
