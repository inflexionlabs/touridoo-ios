//
//  FAQCellTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 11/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class FAQCellTableViewCell: UITableViewCell
{
    @IBOutlet weak var question : UILabel?
    @IBOutlet weak var answer : UITextView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
