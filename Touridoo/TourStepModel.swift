//
//  TourStepModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 22/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class TourStepModel: NSObject
{
    var step_type : Int?
    var duration_in_minutes : Int?
    var id_user_guide : Int?
    var id_tour : Int?
    var id : Int?
    var step_index : Int?
    var is_current_step : Bool?
    var step_name : String?
    var step_description : String?
    var step_essentials : String?
    var step_suggestions : String?
    var step_security : String?
    var step_food : String?
    var HasWiFi : Bool?
    var HasBathrooms : Bool?
    var HasCoffeeShop : Bool?
    var HasFood : Bool?
    var HasGiftShop : Bool?
    var HasWardrobe : Bool?
    var HasAccessibility : Bool?
    var HasCashMachine : Bool?
    var latitude : String?
    var longitude : String?
    var ImageURL : String?

}

