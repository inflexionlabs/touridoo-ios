//
//  CancellationsTourViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 02/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class CancellationsTourViewController: UIViewController
{
    @IBOutlet weak var textviewDescription : UITextView?
    var tourDescription : TourDescriptionModel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    func updateData()
    {
        textviewDescription?.text = tourDescription?.cancelation_description
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        textviewDescription?.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }


}
