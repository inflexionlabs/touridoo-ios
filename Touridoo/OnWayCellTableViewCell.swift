//
//  OnWayCellTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 13/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class OnWayCellTableViewCell: UITableViewCell
{
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var visited : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
