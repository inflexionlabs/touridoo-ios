//
//  OurValuesTourViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 02/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class OurValuesTourViewController: UIViewController
{
    
    @IBOutlet weak var titlelabel : UILabel?
    @IBOutlet weak var descriptiontext : UITextView?
    @IBOutlet weak var scrollData  : UIScrollView?
    @IBOutlet weak var titleText : UITextView?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        titlelabel?.text = NSLocalizedString("Experience and seniority", comment: "Experience and seniority")
        descriptiontext?.text = NSLocalizedString("Our goal is to transform a tour in an everlasting memory of your holliday, through an extense collection of destinations for you to visit guided by our team, hand picked for their experience to guide you through the unknown", comment: "Our goal is to transform a tour in an everlasting memory of your holliday, through an extense collection of destinations for you to visit guided by our team, hand picked for their experience to guide you through the unknown")
        
        titleText?.text = NSLocalizedString( "Because we are a platform designed and created to assist touristics experiences with high quality vans operated by hosts of Turidoo who were previously selected through various tests to be certified, in addition:\n\n● New vans\n● Travel Insurance \n● GPS\n● Internet \n● Surveillance camera \n● Responsible drivers\n\nFor this and more, Touridoo will make you live the best touristic experience",
                                           comment:  "Because we are a platform designed and created to assist touristics experiences with high quality vans operated by hosts of Turidoo who were previously selected through various tests to be certified, in addition:\n\n● New vans\n● Travel Insurance \n● GPS\n● Internet \n● Surveillance camera \n● Responsible drivers\n\nFor this and more, Touridoo will make you live the best touristic experience")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let content: CGSize
        if Tools.iPhone4()
        {
             content = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(80)))
        }
        else
        {
             content = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(1)))
        }
        
        
        let newHeight : CGFloat = self.view.frame.size.height - CGFloat(0)
        
        let scrollFrameInStoryboar : CGRect = CGRect(x: (scrollData?.frame.origin.x)!,
                                                     y: (scrollData?.frame.origin.y)!,
                                                     width: (scrollData?.frame.size.width)!,
                                                     height: newHeight)
        
        
        scrollData?.frame = scrollFrameInStoryboar
        scrollData?.contentSize = content
    }


}
