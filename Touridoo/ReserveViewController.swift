//
//  ReserveViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 06/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class ReserveViewController: UIViewController, ResponseServicesProtocol, VRGCalendarViewDelegate, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var titleText : UILabel?
    @IBOutlet weak var NextButton : UIButton?
    @IBOutlet weak var scrollData  : UIScrollView?
    @IBOutlet weak var selectAHourInformationLabel : UILabel?
    
    var currentTour : TourDescriptionModel?
    var calendarView : VRGCalendarView?
    var bookingData = BookingDataReserve.init()
    
    var arraySchedules: NSMutableArray = NSMutableArray.init()
    var dynamicScheuldesArray = NSMutableArray.init()
    
    var selectedHour : Date?
    
    var onRequest : Bool = false
    var setupScrollSize : Bool = false
    var calendarSetupDays : Bool = false
    
    @IBOutlet weak var tableScheuldes : UITableView?
    @IBOutlet weak var containerView : UIView?
    @IBOutlet weak var schedulesPinkView : UIView?
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        
        calendarView = VRGCalendarView.init()
        calendarView?.delegate = self
        
        var XCalendarFrame : CGFloat = CGFloat(0)
        if Tools.iPhone7(){
            XCalendarFrame = CGFloat (25)
        }
        if Tools.iPhone7Plus(){
            XCalendarFrame = CGFloat (50)
        }
        
        calendarView?.frame = CGRect(x: XCalendarFrame, y: 0,
                                     width: (calendarView?.frame.size.width)!,
                                     height: (calendarView?.frame.size.height)!)
        calendarView?.backgroundColor = UIColor.clear
        
        scrollData?.addSubview(calendarView!)
     
        tableScheuldes?.delegate = self
        tableScheuldes?.dataSource = self
        
        //selectAnHourLabel?.setTitle(, for: UIControlState.normal)
        
        selectAHourInformationLabel?.text = NSLocalizedString("Select a day", comment: "Select a day")
        
    }
    
    func calendarView(_ calendarView: VRGCalendarView!, switchedToMonth month: Int32, inYear year: Int32, targetHeight: Float, animated: Bool)
    {
        if arraySchedules.count != 0
        {
            let dispatchTime = DispatchTime.now() + 0.10;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.calendarView?.markDates(self.arraySchedules as Array)
            }
            
        }
    }
    
    func calendarView(_ calendarView: VRGCalendarView!, dateSelected date: Date!)
    {
        
        if arraySchedules.count == 0
        {
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.warning)
            
            return
        }
        
        Tools.feedback()
        
        selectedHour = nil
        
        //Date validation
        let validateDate : Date = NSDate.init().addingTimeInterval(86400) as Date
        var valid : Bool = false
        
        switch validateDate.compare(date)
        {
            case .orderedAscending     :
                valid = true
            case .orderedDescending    :
                valid = false
            case .orderedSame          :
                valid = false
        }
        
        /*
        if valid == false
        {
            dynamicScheuldesArray.removeAllObjects()
            tableScheuldes?.reloadData()
            
            Tools.showAlertView(withText: NSLocalizedString("This day is no longer available", comment: "This day is no longer available"))
            return
        }*/
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd"
        let selectedDayString = dateFormatter.string(from: date)
        
        dynamicScheuldesArray.removeAllObjects()
        tableScheuldes?.reloadData()
        //dynamicScheuldesArray.add(NSLocalizedString("Select", comment: "Select"))
        
        for i in 0...(arraySchedules.count - 1)
        {
            let dateInArray : Date = arraySchedules.object(at: i) as! Date
            let arrayStringDate : String = dateFormatter.string(from: dateInArray)
            
            if arrayStringDate == selectedDayString {
                dynamicScheuldesArray.add(dateInArray)
            }
            
        }
        
        if dynamicScheuldesArray.count == 0
        {
            Tools.showAlertView(withText: NSLocalizedString("We dont have tours for that day, try another", comment: "We dont have tours for that day, try another"))
            selectAHourInformationLabel?.text = NSLocalizedString("Select a day", comment: "Select a day")
        }
        else
        {
            selectAHourInformationLabel?.text = NSLocalizedString("Select a hour", comment: "Select a hour")
           
        }
        
        tableScheuldes?.reloadData()
        
    }
    
    func calendarViewFinishedUpdate()
    {
        if calendarSetupDays == true
        {
            return
        }
        
        calendarSetupDays = true
            
        print("Finshed")
        
        self.view.isUserInteractionEnabled = true
        self.onRequest = true
        Tools.hiddenActivityView(inView: self)
        tableScheuldes?.reloadData()
        self.calendarSetupDays = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        
        if (MainMenuApp.alertView != nil)
        {
            MainMenuApp.alertView.kill()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
        titleText?.text = currentTour?.name
        titleText?.adjustsFontSizeToFitWidth = true
        //NextButton?.setTitle(NSLocalizedString("Search for availability", comment: "Search for availability"), for: UIControlState.normal)
        NextButton?.setTitle(NSLocalizedString("Tickets", comment: "Tickets"), for: UIControlState.normal)
        
        containerView?.backgroundColor = UIColor.clear
        schedulesPinkView?.layer.shadowColor = UIColor.gray.cgColor
        schedulesPinkView?.layer.shadowOffset = CGSize(width: 0, height: -2)
        schedulesPinkView?.layer.shadowOpacity = 0.10
        schedulesPinkView?.layer.shadowRadius = 1
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if setupScrollSize == false
        {
            setupScrollSize = true
            

            let content: CGSize
            
            if Tools.iPhone4()
            {
                 content  = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(90)))
            }
            else
            {
                 content  = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(50)))
            }
            
            let newHeight : CGFloat = self.view.frame.size.height - CGFloat(60)
            
            let scrollFrameInStoryboar : CGRect = CGRect(x: (scrollData?.frame.origin.x)!,
                                                         y: (scrollData?.frame.origin.y)!,
                                                         width: (scrollData?.frame.size.width)!,
                                                         height: newHeight)
            
            
            scrollData?.frame = scrollFrameInStoryboar
            scrollData?.contentSize = content

        }
        
        if onRequest == true
        {
            return
        }
        
        Tools.showActivityView(inView: self)
        self.view.isUserInteractionEnabled = false
        self.loadSchedules()
        
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.CHECK_TOUR_AVAILABILITY
        {
        
            var tourAvailable : Bool = false
            
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = (dataResult.object(forKey: "success") != nil)
            
            var TicketsA : Int = 0
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
                    tourAvailable = content.object(forKey: "tour_available") as! Bool
                    TicketsA = content.object(forKey: "passenger_spots_available") as! Int
                }
            }
            
            if tourAvailable == true
            {
                let dispatchTime = DispatchTime.now() + 0.50;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    Tools.hiddenActivityView(inView: self)
                    
                    self.view.isUserInteractionEnabled = true
                    self.NextButton?.alpha = 1
                    
                    let ReserveController : TicketsSelectorViewController  = self.storyboard!.instantiateViewController(withIdentifier: "TicketsSelectorViewController") as! TicketsSelectorViewController
                    ReserveController.currentTour = self.currentTour
                    ReserveController.bookingData = self.bookingData
                    ReserveController.ticketsAva = TicketsA
                    self.navigationController?.pushViewController(ReserveController, animated: true)
                }
            }
            else
            {
                let dispatchTime = DispatchTime.now() + 0.30;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    Tools.hiddenActivityView(inView: self)
                    self.NextButton?.alpha = 1
                    self.view.isUserInteractionEnabled = true
                    Tools.showAlertViewinModalViewController(withText: NSLocalizedString("We dont have tickets for that date, please select another", comment: "We dont have tickets for that date, please select another"), ModalViewController: self)
                }
            }
            return
        }
        
        if name == ServiceName.GET_TOUR_DATES
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = (dataResult.object(forKey: "success") != nil)
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    
                    let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
                    
                    if content.object(forKey: "valid_dates_from_now") != nil
                    {
                        let dates : NSArray = content.object(forKey: "valid_dates_from_now") as! NSArray
                        
                        for i in 0...(dates.count - 1)
                        {
                            let stringDate : String = dates.object(at: i) as! String
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                            
                            let dateObj : Date = dateFormatter.date(from: stringDate)!
                            arraySchedules.add(dateObj)
                        }
                        
                    }
                    else
                    {
                        if content.object(forKey: "specific_date") != nil
                        {
                            let singleDate : String = content.object(forKey: "specific_date") as! String
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                            
                            let dateObj : Date = dateFormatter.date(from: singleDate)!
                            arraySchedules.add(dateObj)
                        }
                        
                    }
                    
                    
                    let dispatchTime = DispatchTime.now() + 0.30;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                    {
                        self.calendarView?.markDates(self.arraySchedules as Array)
                    }
                    
                }
            }
            
            return
        }
    }
    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.NextButton?.alpha = 1
            self.view.isUserInteractionEnabled = true
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }
    
    
    func loadSchedules()
    {
        let Service = TouridooServices.init(delegate: self)
        let id : Int = (currentTour?.id)!
        let idString = String(id)
        Service.GetToursDatesBy(id: idString)
    }
    
    @IBAction func NextAction()
    {
   
        if selectedHour == nil
        {
            Tools.showAlertView(withText: NSLocalizedString("Please select a schedule", comment: "Please select a schedule"))
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dateFromString : String = dateFormatter.string(from: selectedHour!)
        
        bookingData.tourDate = dateFromString
        bookingData.idTour = currentTour?.id
        
        let numberOfTickets : Int = 1
        
        Tools.showActivityView(inView: self)
        self.view.isUserInteractionEnabled = false
        
        let dispatchTime = DispatchTime.now() + 0.50;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            let Service = TouridooServices.init(delegate: self)
            let id : Int = (self.currentTour?.id)!
            let idString = String(id)
            let ticket = String(numberOfTickets)
            Service.checkTourAvailability(idtour: idString, tourdate: dateFromString, ocupantsNumber: ticket)
        }
     
    }
    
    @IBAction func closeView() {
        self.navigationController?.popViewController(animated: true)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : meetingPointCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "meeting_point_cell") as! meetingPointCellTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
      
        let dateTour : Date = dynamicScheuldesArray.object(at: indexPath.row) as! Date
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "HH:mm a"
        let dateFromString = dateFormatter.string(from: dateTour)
        
        cell.title?.text =  dateFromString
        cell.subtitle?.text = ""
        cell.imageM?.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dynamicScheuldesArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 70))
        footer.backgroundColor = UIColor.clear
        return footer
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let cell : meetingPointCellTableViewCell = tableView.cellForRow(at: indexPath) as! meetingPointCellTableViewCell
        Tools.doPushEffect(viewEffect: cell.title!, duration: 0.30, scale: 0.98)
        Tools.doPushEffect(viewEffect: cell.imageDetail!, duration: 0.30, scale: 0.95)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let stingDate : Date = dynamicScheuldesArray.object(at: indexPath.row) as! Date
    
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy - HH:mm a"
        let dateFromString = dateFormatter.string(from: stingDate)
        
        selectAHourInformationLabel?.text = dateFromString
        
        selectedHour = dynamicScheuldesArray.object(at: indexPath.row) as? Date
        print("selected hour: " + String(describing: selectedHour))
    }
 

}
