//
//  FAQViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableCities : UITableView?
    //var arrayQuestions : NSMutableArray = NSMutableArray.init()
    
    struct Section
    {
        var sectionName : String!
        var arrayQuestions : NSMutableArray!
        init(sectionName: String, arrayQuestions: NSMutableArray!)
        {
            self.sectionName = sectionName
            self.arrayQuestions = arrayQuestions
        }
    }
    var arrayOfQAs = [Section]()
    let languageID = Locale.current.languageCode
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableCities?.delegate = self
        tableCities?.dataSource = self
        
        tableCities?.separatorStyle = UITableViewCellSeparatorStyle.none
        
        data()
        
    }
    
    func data()
    {
        var dicFAQ = NSMutableDictionary.init()
        var arrayQuestions : NSMutableArray = NSMutableArray.init()
        
        var question : String = NSLocalizedString("Why choose Touridoo?",
                                                  comment: "Why choose Touridoo?")
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        
        var answer : String = NSLocalizedString("Our team care to give you the best quality in tourist services, this through highly qualified and talented guides, vehicles in optimum conditions and also an app where you could check all the details of your tour and vehicle, be in contact with your guide and enjoy safely and securely.",
                                                comment: "Our team care to give you the best quality in tourist services, this through highly qualified and talented guides, vehicles in optimum conditions and also an app where you could check all the details of your tour and vehicle, be in contact with your guide and enjoy safely and securely.")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        
        arrayOfQAs.append(Section(sectionName: "Touridoo", arrayQuestions: arrayQuestions))
        
        dicFAQ = NSMutableDictionary.init()
        arrayQuestions = NSMutableArray.init()
        // Inicio de segunda sección
        question = NSLocalizedString("How do I book a tour?",
                                     comment: "How do I book a tour?")
        
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        answer = NSLocalizedString("It’s easy! Go to our website www.touridoo.com\nStep 1. Choose a tour\nStep 2. Click or touch Book In and follow the instructions. You will need your credit card to complete the purchase.",
                                   comment: "It’s easy! Go to our website www.touridoo.com\nStep 1. Choose a tour\nStep 2. Click or touch Book In and follow the instructions. You will need your credit card to complete the purchase.")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        dicFAQ = NSMutableDictionary.init()
        
        question = NSLocalizedString("My booking confirmation needs to be corrected. What should I do?",
                                     comment: "My booking confirmation needs to be corrected. What should I do?")
        
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        answer = NSLocalizedString("We are happy to help! Please send us an email or give us a call.\nEmail: contacto@touridoo.com\nPhone:",
                                   comment: "We are happy to help! Please send us an email or give us a call.\nEmail: contacto@touridoo.com\nPhone:")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        dicFAQ = NSMutableDictionary.init()
        
        question = NSLocalizedString("What’s is included in the tour price?",
                                     comment: "What’s is included in the tour price?")
        
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        answer = NSLocalizedString("Each tour has specific inclusions that that are part of the price, you could find it in the details of the tour.",
                                   comment: "Each tour has specific inclusions that that are part of the price, you could find it in the details of the tour.")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        dicFAQ = NSMutableDictionary.init()
        
        question = NSLocalizedString("Can I book more than one tour for the same day in different places?",
                                     comment: "Can I book more than one tour for the same day in different places?")
        
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        answer = NSLocalizedString("Yes, you can book many tours on the same day. We will ask you for the specific names of those who will go on each tour to validate each reservation.",
                                   comment: "Yes, you can book many tours on the same day. We will ask you for the specific names of those who will go on each tour to validate each reservation.")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        
        arrayOfQAs.append(Section(sectionName: NSLocalizedString("Booking tours", comment: "Booking tours"), arrayQuestions: arrayQuestions))
        
        dicFAQ = NSMutableDictionary.init()
        arrayQuestions = NSMutableArray.init()
        // Inicio de tercera sección
        question = NSLocalizedString("Can my companions have access to my itinerary?",
                                     comment: "Can my companions have access to my itinerary?")
        
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        answer = NSLocalizedString("Of course, all the companions who share the experience with you, will be able to use your reservation code to enjoy all functionalities of the app during the tour and follow the itinerary.",
                                   comment: "Of course, all the companions who share the experience with you, will be able to use your reservation code to enjoy all functionalities of the app during the tour and follow the itinerary.")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        dicFAQ = NSMutableDictionary.init()
        
        question = NSLocalizedString("What happens if I have a service complaint?",
                                     comment: "What happens if I have a service complaint?")
        
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        answer = NSLocalizedString("At the end of the tour you can rate it in the app, as well as the staff and infrastructure used. However you can also send us your opinions or suggestion to the mail: contacto@touridoo.com we will answer you as soon as possible. At Touridoo we care about offering you the best experience and we thank your opinions to give you an excellent service.",
                                   comment: "At the end of the tour you can rate it in the app, as well as the staff and infrastructure used. However you can also send us your opinions or suggestion to the mail: contacto@touridoo.com we will answer you as soon as possible. At Touridoo we care about offering you the best experience and we thank your opinions to give you an excellent service.")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        
        arrayOfQAs.append(Section(sectionName: NSLocalizedString("On tour", comment: "On tour"), arrayQuestions: arrayQuestions))
        
        dicFAQ = NSMutableDictionary.init()
        arrayQuestions = NSMutableArray.init()
        // Inicio de cuarta sección
        question = NSLocalizedString("How do I pay for my tour?",
                                     comment: "How do I pay for my tour?")
        
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        answer = NSLocalizedString("Pay Pal it is a widely known and reliable global payment platform so you can pay with Visa, Mastercard and American Express credit cards.",
                                   comment: "Pay Pal it is a widely known and reliable global payment platform so you can pay with Visa, Mastercard and American Express credit cards.")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        dicFAQ = NSMutableDictionary.init()
        
        question = NSLocalizedString("What is your cancellation policy?",
                                     comment: "What is your cancellation policy?")
        
        dicFAQ.setObject(question, forKey: "question" as NSCopying)
        answer = NSLocalizedString("The cancellations policy are different for each tour, please check it before booking in the cancellations section. If you need to cancel please contact customer support.\nEmail: contacto@touridoo.com",
                                   comment: "The cancellations policy are different for each tour, please check it before booking in the cancellations section. If you need to cancel please contact customer support.\nEmail: contacto@touridoo.com")
        dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
        arrayQuestions.add(dicFAQ)
        
        arrayOfQAs.append(Section(sectionName: NSLocalizedString("Payment and refunds", comment: "Payment and refunds"), arrayQuestions: arrayQuestions))
        
        dicFAQ = NSMutableDictionary.init()
        arrayQuestions = NSMutableArray.init()
        
        /*
         dicFAQ = NSMutableDictionary.init()
         arrayQuestions = NSMutableArray.init()
         
         question = NSLocalizedString("", comment: "")
         dicFAQ.setObject(question, forKey: "question" as NSCopying)
         
         var answer : String = NSLocalizedString("", comment: "")
         dicFAQ.setObject(answer, forKey: "answer" as NSCopying)
         arrayQuestions.add(dicFAQ)
         arrayOfQAs.append(Section(sectionName: "", arrayQuestions: arrayQuestions))
         
         */
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.tableCities?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfQAs[section].arrayQuestions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section
        {
            case 0:
                if indexPath.row == 0
                {
                    if languageID! == "es"
                    {
                        if Tools.iPhone4()
                        {
                            return 155
                        }
                        if Tools.iPhone5()
                        {
                            return 155
                        }
                        else
                        {
                            return 145
                        }
                    }
                    else
                    {
                        if Tools.iPhone4()
                        {
                            return 132
                        }
                        if Tools.iPhone5()
                        {
                            return 126
                        }
                        else
                        {
                            return 116
                        }
                        
                    }
                    
                }
            break
            case 1:
                if indexPath.row == 0
                {
                    if languageID! == "es"
                    {
                        return 132
                    }
                    else
                    {
                        if Tools.iPhone4()
                        {
                            return 118
                        }
                        if Tools.iPhone5()
                        {
                            return 110
                        }
                        else
                        {
                            return 100
                        }
                        
                    }
                }
                if indexPath.row == 1
                {
                    if languageID! == "es"
                    {
                        return 96
                    }
                    else
                    {
                        return 92
                    }
                }
                if indexPath.row == 2
                {
                    if languageID! == "es"
                    {
                        if Tools.iPhone4()
                        {
                            return 93
                        }
                        if Tools.iPhone5()
                        {
                            return 83
                        }
                        else
                        {
                            return 73
                        }
                    }
                    else
                    {
                        if Tools.iPhone4()
                        {
                            return 93
                        }
                        if Tools.iPhone5()
                        {
                            return 83
                        }
                        else
                        {
                            return 73
                        }
                        
                    }
                }
                if indexPath.row == 3
                {
                    if languageID! == "es"
                    {
                        if Tools.iPhone4()
                        {
                            return 122
                        }
                        if Tools.iPhone5()
                        {
                            return 112
                        }
                        else
                        {
                            return 102
                        }
                        
                    }
                    else
                    {
                        if Tools.iPhone4()
                        {
                            return 105
                        }
                        if Tools.iPhone5()
                        {
                            return 100
                        }
                        else
                        {
                            return 90
                        }
                        
                    }
                }
            break
            case 2:
                if indexPath.row == 0
                {
                    if languageID! == "es"
                    {
                        return 110
                    }
                    else
                    {
                         return 97
                    }
                }
                if indexPath.row == 1
                {
                    if languageID! == "es"
                    {
                        if Tools.iPhone4()
                        {
                            return 180
                        }
                        if Tools.iPhone5()
                        {
                            return 170
                        }
                        else
                        {
                            return 160
                        }
                        
                    }
                    else
                    {
                        if Tools.iPhone4()
                        {
                            return 162
                        }
                        if Tools.iPhone5()
                        {
                            return 155
                        }
                        else
                        {
                            return 145
                        }
                        
                    }
                }
            break
            case 3:
                if indexPath.row == 0
                {
                    if languageID! == "es"
                    {
                        return 95
                    }
                    else
                    {
                         return 82
                    }
                }
                if indexPath.row == 1
                {
                    if languageID! == "es"
                    {
                        return 130
                    }
                    else
                    {
                         return 125
                    }
                }
            break
            default:
                print("Default")
        }
        
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : FAQCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FAQCell")  as! FAQCellTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        //cell.question?.backgroundColor = UIColor.red
        //cell.answer?.backgroundColor = UIColor.blue
        

        let dicData : NSDictionary = arrayOfQAs[indexPath.section].arrayQuestions.object(at: indexPath.row) as! NSDictionary
        let questionString : String = dicData.object(forKey: "question") as! String
        let answerString : String = dicData.object(forKey: "answer") as! String
        
        cell.question?.text = questionString
        cell.answer?.text = answerString
        
        cell.question?.adjustsFontSizeToFitWidth = true
        
        if indexPath.section == 2
        {
            if indexPath.row == 1
            {
                Tools.AddHeight(View: (cell.answer)!, Points: 15)
            }
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrayOfQAs.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrayOfQAs[section].sectionName
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        view.backgroundColor = UIColor.white
        let label = UILabel()
        label.text = arrayOfQAs[section].sectionName
        label.frame = CGRect(x: 10, y: 5, width: 400, height: 20)
        label.font = UIFont(name: "Montserrat-SemiBold", size: 18)
        label.textColor = UIColor(red: 185/255, green: 29/255, blue: 47/255, alpha: 1)
        view.addSubview(label)
        return view
    
    }
}

