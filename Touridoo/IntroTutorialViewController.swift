//
//  IntroTutorialViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 26/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class IntroTutorialViewController: UIViewController, UIScrollViewDelegate
{
    @IBOutlet weak var scrollPager : UIScrollView?
    @IBOutlet weak var skipbutton : UIButton?
    
    @IBOutlet weak var indicator1 : UIImageView?
    @IBOutlet weak var indicator2 : UIImageView?
    @IBOutlet weak var indicator3 : UIImageView?
    @IBOutlet weak var indicator4 : UIImageView?
    @IBOutlet weak var indicator5 : UIImageView?
    
    var offcarrusel : UIImage = UIImage.init(named: "carrusel_off.png")!
    var oncarrusel : UIImage = UIImage.init(named: "carrusel_on.png")!

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func closeIntro()
    {
        UserDefaults.standard.set(true, forKey: "tutorial")
        
        UIView.animate(withDuration: 0.30, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                self.view.alpha = 0
                self.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            
        }, completion:
            { _ in
                
                self.view.removeFromSuperview()
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.requestForNotifications()
        })
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        scrollPager?.delegate = self
        scrollPager?.isPagingEnabled = true
        scrollPager?.showsHorizontalScrollIndicator = false
        
        let page1  : tutorialSubview = self.storyboard?.instantiateViewController(withIdentifier: "page1") as! tutorialSubview
        page1.text = NSLocalizedString("It’s easy! Just pick a tour",
                                       comment: "It’s easy! Just pick a tour")
        
        let page2  : tutorialSubview  = self.storyboard?.instantiateViewController(withIdentifier: "page2") as! tutorialSubview
        page2.text = NSLocalizedString("Choose time and date to book",
                                       comment: "Choose time and date to book")
        
        let page3  : tutorialSubview  = self.storyboard?.instantiateViewController(withIdentifier: "page3") as! tutorialSubview
        page3.text = NSLocalizedString("Select how many people are going",
                                       comment: "Select how many people are going")
        
        let page4  : tutorialSubview  = self.storyboard?.instantiateViewController(withIdentifier: "page4") as! tutorialSubview
        page4.text = NSLocalizedString("Choose a meeting point",
                                       comment: "Choose a meeting point")
        
        let page5  : tutorialSubview  = self.storyboard?.instantiateViewController(withIdentifier: "page5") as! tutorialSubview
        page5.text = NSLocalizedString("Complete your payment and you’re ready to go",
                                       comment: "Complete your payment and you’re ready to go")
        
        page1.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width,
                                                 height: UIScreen.main.bounds.size.height)
        page2.view.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0,
                                                 width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        page3.view.frame = CGRect(x: (UIScreen.main.bounds.size.width * CGFloat(2)), y: 0,
                                                     width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        page4.view.frame = CGRect(x: (UIScreen.main.bounds.size.width * CGFloat(3)), y: 0,
                                                 width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        page5.view.frame = CGRect(x: (UIScreen.main.bounds.size.width * CGFloat(4)), y: 0,
                                                 width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        
        page1.view.backgroundColor = UIColor.clear
        page2.view.backgroundColor = UIColor.clear
        page3.view.backgroundColor = UIColor.clear
        page4.view.backgroundColor = UIColor.clear
        page5.view.backgroundColor = UIColor.clear
        
        scrollPager?.addSubview((page1.view)!)
        scrollPager?.addSubview((page2.view)!)
        scrollPager?.addSubview((page3.view)!)
        scrollPager?.addSubview((page4.view)!)
        scrollPager?.addSubview((page5.view)!)
        
        scrollPager?.contentSize = CGSize(width: (UIScreen.main.bounds.size.width * CGFloat(5)) , height: UIScreen.main.bounds.size.height)
        
    }
    
    @IBAction func setindicator(index : Int)
    {
        print("index: " + String(index))
        
        Tools.feedback()
        
        if index == 0
        {
            alwaysOff()
            indicator1?.image = oncarrusel
            doRebound(viewEffect: indicator1!)
        }
        
        if index == 1
        {
            alwaysOff()
            indicator2?.image = oncarrusel
            doRebound(viewEffect: indicator2!)
        }
        
        if index == 2
        {
            alwaysOff()
            indicator3?.image = oncarrusel
            doRebound(viewEffect: indicator3!)
        }
        
        if index == 3
        {
            alwaysOff()
            indicator4?.image = oncarrusel
            doRebound(viewEffect: indicator4!)
        }
        
        if index == 4
        {
            alwaysOff()
            indicator5?.image = oncarrusel
            doRebound(viewEffect: indicator5!)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let index : Int = Int((scrollPager?.contentOffset.x)!) / Int((scrollPager?.frame.size.width)!)
        setindicator(index: index)
    }
    
    func alwaysOff()
    {
        indicator1?.image = offcarrusel
        indicator2?.image = offcarrusel
        indicator3?.image = offcarrusel
        indicator4?.image = offcarrusel
        indicator5?.image = offcarrusel
    }
    
    func doRebound(viewEffect : UIView)
    {
        
        UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
            {
                viewEffect.transform = CGAffineTransform(scaleX: 1.250, y: 1.250)
                
        }, completion:
            { _ in
                UIView.animate(withDuration: 0.15, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:
                    {
                        viewEffect.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion:nil)
                
        })
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }

}
