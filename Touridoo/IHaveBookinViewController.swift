//
//  IHaveBookinViewController.swift
//  Touridoo
//
//  Created by Mario Alejandro Pérez Colmenares on 8/29/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class IHaveBookinViewController: UIViewController, UIGestureRecognizerDelegate, ResponseServicesProtocol
{

    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var myBookings: UILabel!
    @IBOutlet weak var bookinCodeLabel: UILabel!
    
    var code : String = "";
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        sendButton.setTitle(NSLocalizedString("Send", comment: "Send"), for: UIControlState.normal)
        myBookings.text = NSLocalizedString("My bookings", comment: "My bookings")
        codeTextField?.placeholder = NSLocalizedString("Code", comment: "Code")
        bookinCodeLabel.text = NSLocalizedString("Type your booking code", comment: "Type your booking code")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Tools.hiddenBurgerButton(hidden: true)
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hiddenKeyboard))
        self.view.addGestureRecognizer(tap)
        
        codeTextField.becomeFirstResponder()
    }
    
    
    
    func hiddenKeyboard()
    {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    @IBAction func backAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendCodeAction(_ sender: Any)
    {
        print("Send Code")
        
        code = codeTextField.text!
        
        if code == ""
        {
            Tools.showAlertView(withText: NSLocalizedString("Please enter the booking code", comment: "Please enter the booking code"))
            return
        }
        
        Tools.showActivityView(inView: self)
        self.view.isUserInteractionEnabled = false
        
        var deviceID : String? = UserDefaults.standard.object(forKey: "device_token") as? String
        if deviceID == nil
        {
            deviceID = ""
        }
        let service = TouridooServices.init(delegate: self)
        service.bookingCode(code: code, deviceID: deviceID!)
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.BOOKING_CODE
        {
            
            let dispatchTime = DispatchTime.now() + 0.50;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                Tools.hiddenActivityView(inView: self)
                self.view.isUserInteractionEnabled = true
                
                let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
                let success : Bool = dataResult.object(forKey: "success") as! Bool
                
                if success
                {
                    if (dataResult.object(forKey: "content") != nil)
                    {
                        let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                        
                        if content.count == 0
                        {
                            Tools.showAlertView(withText: NSLocalizedString("Booking code is incorrect", comment: "Booking code is incorrect"))
                        }
                        else
                        {
                            UserDefaults.standard.set(true, forKey: "bookingcode_mode")
                            UserDefaults.standard.set(self.code, forKey: "bookingcode")
                            
                            loadTourData.updateData(content: content)
                            loadTourData.updateFlags()
                            
                            let session = sessionData.shared
                            //let toursArray = session.ToursArray?.object(at: 0)
                            let currentTour : Tour = session.ToursArray?.object(at: 0) as! Tour
                            let tourName : String = currentTour.name!
                            
                            Tools.showDoneView(withText: "Tour: " + tourName )
                            UserDefaults.standard.set(true, forKey: "needs_update");
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                }
                
                
            }
            
        }
    }
    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.view.isUserInteractionEnabled = true
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }

}
