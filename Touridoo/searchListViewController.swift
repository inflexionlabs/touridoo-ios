//
//  searchListViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 11/08/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class searchListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    @IBOutlet weak var table1 : UITableView!
    @IBOutlet weak var table2 : UITableView!
    
    var array1 = NSMutableArray.init()
    var array2 = NSMutableArray.init()
    
    var delegate : ToursListViewController?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        array1.add(["region" : "Argentina"])
        array1.add(["tour" : "Buenos Aires"])
        array1.add(["tour" : "Valle de la Luna"])
        array1.add(["tour" : "Parque Nacional de Iguazú"])
        array1.add(["tour" : "Glaciar Perito Moreno"])
        array1.add(["tour" : "Bariloche"])
        array1.add(["tour" : "Mendoza"])
        
        array1.add(["region" : "Brasil"])
        array1.add(["tour" : "Salvador de Bahía"])
        array1.add(["tour" : "Recife"])
        array1.add(["tour" : "Río de Janeiro"])
        array1.add(["tour" : "Parque Nacional de Iguazú"])
        array1.add(["tour" : "Brasilia"])
        
        array2.add(["region" : "Bolivia"])
        array2.add(["tour" : "El Salar de Uyuni"])
        array2.add(["tour" : "Parque Nacional Torotoro"])
        array2.add(["tour" : "Lago Titicaca"])
        
        array2.add(["region" : "Chile"])
        array2.add(["tour" : "Santiago"])
        array2.add(["tour" : "Parque Nacional Lauca"])
        array2.add(["tour" : "Iquique"])
        array2.add(["tour" : "Isla de Pascua"])
        
        array2.add(["region" : "Colombia"])
        array2.add(["tour" : "Bogotá"])
        
        table1.dataSource = self
        table1.delegate = self;
        
        table2.dataSource = self
        table2.delegate = self;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let cell : searchRecommendationsTableViewCell = tableView.cellForRow(at: indexPath) as! searchRecommendationsTableViewCell
        cell.setSelected(false, animated: true)
        
        let textToDisplay : String = (cell.title?.text)!
        if textToDisplay != ""
        {
            if delegate != nil
            {
             delegate?.searchTextField?.text = ""
             delegate?.searchTextField?.text = textToDisplay
             delegate?.onSearchRequest = true
             delegate?.searchMode = true
                
                let dispatchTime = DispatchTime.now() + 0.10;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    self.delegate?.search(text: textToDisplay)
                }
            }
        }
    }
    
    func selectedCity()
    {
       
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : searchRecommendationsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "search_cell") as! searchRecommendationsTableViewCell
    
        cell.title?.text = ""
        
        let dicData : NSDictionary?
        
        if tableView == table1
        {
            dicData = array1.object(at: indexPath.row) as? NSDictionary
        }
        else
        {
            dicData = array2.object(at: indexPath.row) as? NSDictionary
        }
        
        if dicData != nil
        {
            let textRegion : String? = dicData?.object(forKey: "region") as? String
            if textRegion != nil
            {
                cell.title?.text = textRegion
                cell.title?.font = Tools.fontAppBold(withSize: 13)
                cell.title?.textColor = Tools.colorApp()
                cell.title?.adjustsFontSizeToFitWidth = true
            }
            
            let textTour : String? = dicData?.object(forKey: "tour") as? String
            if textTour != nil
            {
                cell.title?.text = textTour
                cell.title?.font = Tools.fontAppBold(withSize: 11)
                cell.title?.textColor = UIColor.darkGray
                cell.title?.adjustsFontSizeToFitWidth = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 20
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == table1
        {
            return array1.count
        }
        
        if tableView == table2
        {
            return array2.count
        }
        
        return 10
    }

}
