//
//  OptionsTourViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 02/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class OptionsTourViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    @IBOutlet weak var tableReviews : UITableView!
    var arrayReviews : NSArray?
    var tourDescription : TourDescriptionModel?
    
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var subtitleLabel : UITextView?
    @IBOutlet weak var noOpinionsLabel : UILabel?
    
    @IBOutlet weak var rateNumberTenBase : UILabel?
    @IBOutlet weak var numberOfOpinions : UILabel?
    
    @IBOutlet weak var star1 : UIImageView?
    @IBOutlet weak var star2 : UIImageView?
    @IBOutlet weak var star3 : UIImageView?
    @IBOutlet weak var star4 : UIImageView?
    @IBOutlet weak var star5 : UIImageView?
    
    var starOff : UIImage = UIImage.init(named: "star_off.png")!
    var starOn : UIImage = UIImage.init(named: "star_on.png")!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableReviews?.delegate = self
        tableReviews?.dataSource = self
        
        tableReviews.backgroundView?.backgroundColor = UIColor.clear
        tableReviews.backgroundColor = UIColor.clear
    
        if arrayReviews != nil
        {
            if arrayReviews?.count == 0
            {
                self.tableReviews.isHidden = true
            }
        }
        
    }
    
    func updateData()
    {
        arrayReviews = tourDescription?.rates
        
        if arrayReviews != nil
        {
            if arrayReviews?.count == 0
            {
                self.tableReviews.isHidden = true
            }
            else
            {
                
                //Clean clear comments
                let allcomments = arrayReviews as! Array<Any>
                let onlyWithComments : NSMutableArray = NSMutableArray.init()
                
                for index in 0...(allcomments.count - 1)
                {
                    let dicData : NSDictionary = arrayReviews!.object(at: (index)) as! NSDictionary
                    let comment : String? = dicData.object(forKey: "comment") as? String
                    if comment != nil
                    {
                        if comment != ""
                        {
                            onlyWithComments.add(dicData)
                        }
                    }
                }
                
                arrayReviews = onlyWithComments
                
                
                self.tableReviews.isHidden = false
                noOpinionsLabel?.isHidden = true
                
                tableReviews.reloadData()
                
                let number : Int = (tourDescription?.rates_count)!
                self.numberOfOpinions?.text = String(number) + NSLocalizedString(" opinions", comment: " opinions")
                let rate : Int = (tourDescription?.base10_stars_average)!
                self.rateNumberTenBase?.text = String(rate) + ""
                
            }
        }
        
        setStars(stars: (tourDescription?.stars_average)!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.titleLabel?.text = NSLocalizedString("Customer reviews", comment: "Customer reviews")
        self.subtitleLabel?.text = NSLocalizedString("All reviews were written by real travelers who have booked with us.",
                                                     comment: "All reviews were written by real travelers who have booked with us.")
        noOpinionsLabel?.text = NSLocalizedString("We still no have reviews about this tour",
                                                  comment: "We still no have reviews about this tour")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.row == 0
        {
            //Cell for simple effect
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "clear_cell")!
            
            cell.backgroundView?.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            
            return cell
        }
        else
        {
            let cell : ReviewsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "review_cell") as! ReviewsTableViewCell
            
            let dicData : NSDictionary = arrayReviews!.object(at: (indexPath.row - 1)) as! NSDictionary
            
            let user : String? = dicData.object(forKey: "user_name") as? String
            let comment : String? = dicData.object(forKey: "comment") as? String
            let country : String = "México"
            let date : String? = convertDate(date: (dicData.object(forKey: "created_at") as? String)!)
            let stars : Int? = dicData.object(forKey: "stars") as? Int
            
           
            
            cell.setStars(stars: stars!)
            
            cell.nameLabel?.text = user
            cell.dateLabel?.text = date
            cell.countryLabel?.text = country
            cell.textReview?.text = comment
            
            cell.backgroundView?.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            
            return cell
        }
        
    }
    func convertDate(date: String)-> String
    {
        let components = date.components(separatedBy: "T")
        let numbers = components[0].components(separatedBy: "-")
        let day = numbers[2]
        let month = numbers[1]
        let year = numbers[0]
        let realDate: String = String(day) + "-" + String(month) + "-" + String(year) + " T " + components[1]
        return realDate
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0 //|| indexPath.row == arrayReviews.count
        {
             return 152
        }
        else
        {
             return 140
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayReviews != nil
        {
            return arrayReviews!.count + 1
        }
        
        return 1;
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tableReviews.frame.size.width, height: 50))
        footer.backgroundColor = UIColor.clear
        return footer
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func setStars(stars : Int)
    {
        star1?.image = starOff
        star2?.image = starOff
        star3?.image = starOff
        star4?.image = starOff
        star5?.image = starOff
        
        if stars == 1
        {
            star1?.image = starOn
            star2?.image = starOff
            star3?.image = starOff
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 2
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOff
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 3
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOff
            star5?.image = starOff
        }
        
        if stars == 4
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOn
            star5?.image = starOff
        }
        
        if stars == 5
        {
            star1?.image = starOn
            star2?.image = starOn
            star3?.image = starOn
            star4?.image = starOn
            star5?.image = starOn
        }
    }

    
}
