//
//  ForgotMyPasswordViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class ForgotMyPasswordViewController: UIViewController, UITextFieldDelegate, ResponseServicesProtocol
{
    @IBOutlet weak var mailTextField : UITextField?
    @IBOutlet weak var forgotPassword : UIButton?
    var mailBackup : String = ""

    override func viewDidLoad()
    {
        super.viewDidLoad()
        mailTextField?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
         mailTextField?.placeholder = NSLocalizedString("Mail", comment: "Mail")
        mailTextField?.becomeFirstResponder()
        forgotPassword?.setTitle(NSLocalizedString("Search", comment: "Search"), for: UIControlState.normal)
       
        if mailBackup != ""
        {
            mailTextField?.text = mailBackup
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hiddenKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func hiddenKeyboard()
    {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    

    func onSucces(Result : String, name : ServiceName)
    {
        //DONE
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.closeView()
        }
        
    }
    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }
    
    @IBAction func loginAction()
    {
        print("Login")
        
        if mailTextField?.text == "" {
            Tools.showAlertViewinModalViewController(withText: NSLocalizedString("Please enter your mail", comment: "Please enter your mail"), ModalViewController: self)
            return
        }
        
        mailTextField?.resignFirstResponder()
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            //Tools.showActivityView(inView: self)
            //let Service = TouridooServices.init(delegate: self)
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }
    
    @IBAction func backAction()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func closeView()
    {
        backAction()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == mailTextField {
            textField.resignFirstResponder()
        }
        
        return true
    }

}
