//
//  DescriptionTourViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 02/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class DescriptionTourViewController: UIViewController
{
    @IBOutlet weak var textViewInitialDescriptionTour : UITextView?
    @IBOutlet weak var textViewtourDetails: UITextView?
    @IBOutlet weak var imageTour : UIImageView?
    @IBOutlet weak var scrollData  : UIScrollView?
    
    @IBOutlet weak var viewForTable : UIView?

    var tourDescription : TourDescriptionModel?
    
    var scrollExtraContent : Int = Int(0);
    var textContent = 0;
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewDidLoad()
    }
    
    func updateData()
    {
        if tourDescription?.image?.count != 0
        {
            self.imageTour?.image = UIImage.init(data: (tourDescription?.image)!)
            UserDefaults.standard.set((tourDescription?.image)!, forKey: "last_tour_image")
        }
        
        
        //textViewInitialDescriptionTour?.text = tourDescription?.descriptionTour
        //Set tour data
        
        //First Module
        //var title = NSMutableAttributedString(string: NSLocalizedString("Description", comment: "Description"), attributes: [NSFontAttributeName:Tools.fontAppBold(withSize: 14)])
        //title.addAttribute(NSForegroundColorAttributeName, value: Tools.colorApp(), range: NSRange(location:0,length:title.length))
        
        var subTitleText = "\n" + (tourDescription?.descriptionTour)! + "\n\n"
        
        var descriptionText = NSMutableAttributedString(string: subTitleText, attributes: [NSFontAttributeName:Tools.fontAppRegular(withSize: 15)])
        descriptionText.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location:0,length:descriptionText.length))
        
        let superText = NSMutableAttributedString()
        //superText.append(title)
        superText.append(descriptionText)
        
        
        if tourDescription?.details != nil
        {
            //third Module
            let title = NSMutableAttributedString(string: NSLocalizedString("To consider", comment: "To consider"), attributes: [NSFontAttributeName:Tools.fontAppBold(withSize: 14)])
            title.addAttribute(NSForegroundColorAttributeName, value: Tools.colorApp(), range: NSRange(location:0,length:title.length))
            
            subTitleText =  "\n" + (tourDescription?.details)! + "\n\n"
            
            descriptionText = NSMutableAttributedString(string: subTitleText, attributes: [NSFontAttributeName:Tools.fontAppRegular(withSize: 12)])
            descriptionText.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location:0,length:descriptionText.length))
            
            superText.append(title)
            superText.append(descriptionText)
        }
        
        
        if tourDescription?.rendezvous_description != nil
        {
            //second Module
            /*
            var title = NSMutableAttributedString(string: NSLocalizedString("Itinerary", comment: "Itinerary"), attributes: [NSFontAttributeName:Tools.fontAppBold(withSize: 14)])
            title.addAttribute(NSForegroundColorAttributeName, value: Tools.colorApp(), range: NSRange(location:0,length:title.length))
            
            subTitleText = "\n" + (tourDescription?.rendezvous_description)! + "\n\n"
 */
            /*
            let title = NSMutableAttributedString(string: NSLocalizedString("Meeting point", comment: "Meeting point"), attributes: [NSFontAttributeName:Tools.fontAppBold(withSize: 14)])
            title.addAttribute(NSForegroundColorAttributeName, value: Tools.colorApp(), range: NSRange(location:0,length:title.length))
            
            subTitleText = "\n\n" + NSLocalizedString("We will pick you up at your hotel lobby. You must be 15 minutes before the indicated time", comment: "We will pick you up at your hotel lobby. You must be 15 minutes before the indicated time") + "\n\n"
            
            descriptionText = NSMutableAttributedString(string: subTitleText, attributes: [NSFontAttributeName:Tools.fontAppRegular(withSize: 12)])
            descriptionText.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location:0,length:descriptionText.length))
            
            superText.append(title)
            superText.append(descriptionText)*/
    }
 
        
        textViewtourDetails?.attributedText = superText
        
        let textHeight : Int = Int((textViewtourDetails?.frame.size.height)!);
        
        textViewtourDetails?.sizeToFit()
        
        let newHeight : Int = Int((textViewtourDetails?.frame.size.height)!);
        
        if newHeight > textHeight
        {
            scrollExtraContent = newHeight - textHeight
        }
        
        scrollData?.contentSize = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(scrollExtraContent)))
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        scrollData?.contentSize = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(scrollExtraContent)))
    }


}
