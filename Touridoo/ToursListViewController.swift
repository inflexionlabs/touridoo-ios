 //
//  ToursListViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 30/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class ToursListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ResponseServicesProtocol, UITextFieldDelegate
{

    @IBOutlet weak var TableCities : UITableView!
    @IBOutlet var searchTextField : UITextField?
    @IBOutlet weak var buttoncloseRecommendations : UIButton?
    @IBOutlet weak var searchView : UIView?
    
    var selectedIndexPath : IndexPath = IndexPath.init(row: 0, section: 0)
    var onRequest : Bool = false
    var refreshControl : UIRefreshControl?
    var firstTimeData : Bool = false
    
    var timer : Timer?
    var seconds : Int = -1
    var downloadImageNumber : Int = 0
    
    let session : sessionData = sessionData.shared
    
    var searchMode : Bool = false
    var arraySearchResult = NSMutableArray.init()
    var onSearchRequest = false
    var selectOption = false
    
    var UIUpdate : Bool = false
    var RequestSerch : Bool = false
    var cleanText : Bool = false
    
    var displayEffect : Int = 0
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        super.viewDidLoad()
     
        TableCities?.delegate = self
        TableCities?.dataSource = self
        TableCities.decelerationRate = UIScrollViewDecelerationRateFast
        if session.finishDownloadRegions == false
        {
            //Default
            let Tour : CityModel = CityModel.init(id: 0, name: "Touridoo",image: Data.init(),descriptionRegion: "", country_name: "",  latitude: "",longitud: "",imageURL: "",attractions: 0,type: "region")
            session.regions.add(Tour)
            session.regions.add(Tour)
            session.regions.add(Tour)
            session.regions.add(Tour)
            session.regions.add(Tour)
        }
        
        searchTextField?.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Where do you want to go?", comment: "Where do you want to go?"),attributes: [NSForegroundColorAttributeName: UIColor.white])
        searchTextField?.delegate = self
        
        self.TableCities.isUserInteractionEnabled = false
        
        refreshControl = UIRefreshControl.init()
        refreshControl?.backgroundColor = Tools.colorApp()
        refreshControl?.tintColor = UIColor.white
        refreshControl?.addTarget(self, action: #selector(refreshTable), for: UIControlEvents.valueChanged)
        refreshControl?.attributedTitle =  NSAttributedString(string: NSLocalizedString("Refresh", comment: "Refresh"),attributes: [NSForegroundColorAttributeName: UIColor.white])
        TableCities.addSubview(refreshControl!)
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : TourCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Region_cell") as! TourCellTableViewCell
        
        if displayEffect <= 3
        {
            cell.contentView.alpha = 0
        }
        
        var region : CityModel = CityModel.init(id: 0, name: "",image: Data.init(),descriptionRegion: "", country_name: "",  latitude: "",longitud: "",imageURL: "",attractions: 0,type: "")
        
        if session.regions.count != 0 {
            
            if searchMode == true
            {
                let object = self.arraySearchResult[indexPath.row]
                
                if (object as AnyObject).isKind(of: CityModel.self) {
                    region = self.arraySearchResult[indexPath.row] as! CityModel
                } else {
                    
                     let cell : TourCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Tour_cell") as! TourCellTableViewCell
                    
                    let Tour = self.arraySearchResult[indexPath.row] as! TourModel
                    let nameString : String? = Tour.name
                    cell.name?.text = nameString
                    cell.imageTour?.image = UIImage.init(named: "")
                    return tourCell(cell: cell, Tour: Tour)
                }
               
            }
            else
            {
                region = session.regions[indexPath.row] as! CityModel
            }
            
        }
        
        return regionCell(cell: cell, region: region)
    }
    
    func tourCell(cell : TourCellTableViewCell, Tour : TourModel) -> UITableViewCell
    {
        let nameString : String = Tour.name
        cell.name?.text = nameString
        cell.lenguage?.text = NSLocalizedString("Spanish", comment: "Spanish")
        cell.name?.adjustsFontSizeToFitWidth = true
        if Tour.image != nil {
            if Tour.image?.count != 0 {
                cell.imageTour?.image = UIImage.init(data: Tour.image!)
            }
        }
        
        if Tour.from_price != nil {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale.current
            let price : Float = (Tour.from_price )!
            
            let priceNumber : NSNumber = NSNumber(value: price)
            var priceWithFormatter : String =  formatter.string(from: priceNumber)!
            priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "$", with: "")
            priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "MX", with: "")
            
            let realPrice = "  $" + priceWithFormatter
            
            let offerFloat : Float = price * Float(1.20)
            let offerNumber : NSNumber = NSNumber(value: offerFloat)
            var offetWithFormatter : String =  formatter.string(from: offerNumber)!
            offetWithFormatter = offetWithFormatter.replacingOccurrences(of: "$", with: "")
            offetWithFormatter = offetWithFormatter.replacingOccurrences(of: "MX", with: "")
            
            let offerPrice = "$" + offetWithFormatter
            
            let superText = NSMutableAttributedString()
            
            let offertText = NSMutableAttributedString(string: offerPrice)
            offertText.addAttribute(NSStrikethroughStyleAttributeName, value: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue), range: NSMakeRange(0, offertText.length))
            offertText.addAttribute(NSStrikethroughColorAttributeName, value: UIColor.lightGray, range: NSMakeRange(0, offertText.length))
            offertText.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: NSRange(location:0,length:offertText.length))
            
            superText.append(offertText)
            
            let priceText = NSMutableAttributedString(string: realPrice,
                                                      attributes: [NSFontAttributeName:Tools.fontAppBold(withSize: 13)])
            priceText.addAttribute(NSForegroundColorAttributeName,
                                   value: UIColor.white,
                                   range: NSRange(location:0,length:priceText.length))
            
            superText.append(priceText)
            cell.priceLabel?.attributedText = superText
            cell.priceLabel?.adjustsFontSizeToFitWidth = true
        }
        
        let hours : Int = Tour.duration_in_hours
        let hoursString : String = String(hours)
        cell.duration?.text = hoursString + " " + NSLocalizedString("hours", comment: "hours")
    
        cell.imageTour?.alpha = 1;
        cell.shadowImage?.alpha = 1;
        cell.detailsView.alpha = 1;
        
        if session.finishDownloadTours {
            cell.shadowImage?.alpha = 1;
            cell.detailsView.alpha = 1;
        }
        
        if Tour.image?.count != 0 {
            cell.imageTour?.image = UIImage.init(data: Tour.image!)
            
            UIView.animate(withDuration: 0.30, animations: {
                cell.imageTour?.alpha = 1;
                cell.shadowImage?.alpha = 1;
                cell.detailsView.alpha = 1;
            })
        } else {
            
            var RealImage : Data?
            let image_url : String? = Tour.image_url
            
            if image_url != nil {
                if image_url != "" {
                    DispatchQueue.global().async {
                        if image_url != nil {
                            let urlToDownload : URL = URL.init(string: image_url!)!
                            do {
                                RealImage = try  Data.init(contentsOf: urlToDownload)
                            } catch {  print(error) }
                        }
                        
                        DispatchQueue.main.async {
                            if RealImage != nil  {
                                cell.imageTour?.image = UIImage.init(data: RealImage!)
                                if Tour.image?.count == 0 {
                                    Tour.image = RealImage
                                }
                                
                                UIView.animate(withDuration: 0.30, animations: {
                                    cell.imageTour?.alpha = 1;
                                    cell.shadowImage?.alpha = 1;
                                    cell.detailsView.alpha = 1;
                                })
                            }
                        }
                    }
                }
            }
        }
        
         return cell
    }
    
    func regionCell(cell : TourCellTableViewCell, region : CityModel) -> UITableViewCell
    {
        cell.imageTour?.image = UIImage.init(named: "")
        let nameString : String? = region.name
        cell.name?.text = nameString
        cell.name?.adjustsFontSizeToFitWidth = true
        cell.imageTour?.alpha = 1
        cell.shadowImage?.alpha = 1
        
        var attra : Int  = 0
        if region.attractions != nil{
            attra = region.attractions!;
        }
        
        cell.attractionsNumber?.text = String(attra) + " " + NSLocalizedString("attractions", comment: "attractions")
        
        if region.image?.count != 0{
            cell.imageTour?.image = UIImage.init(data: region.image!)
            UIView.animate(withDuration: 0.30, animations:
                {
                    cell.imageTour?.alpha = 1
                    cell.shadowImage?.alpha = 1
            })
        }
        else
        {
            cell.imageTour?.image = UIImage.init(named: "")
            var RealImage : Data?
            let image_url : String? = region.imageURL
            
            if image_url != nil {
                if image_url != "" {
                    DispatchQueue.global().async {
                        if image_url != nil {
                            let urlToDownload : URL = URL.init(string: image_url!)!
                            do {
                                RealImage = try  Data.init(contentsOf: urlToDownload)
                            } catch { print(error) }
                        }
                        
                        DispatchQueue.main.async {
                            if RealImage != nil {
                                self.downloadImageNumber = self.downloadImageNumber + 1
                                if region.image?.count == 0 {
                                    cell.imageTour?.image = UIImage.init(data: RealImage!)
                                    region.image = RealImage
                                    RealImage = nil
                                }
                                
                                UIView.animate(withDuration: 0.30, animations: {
                                    cell.imageTour?.alpha = 1;
                                    cell.shadowImage?.alpha = 1;
                                })
                            }
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.selectOption = true
        
        Tools.feedback()
        let cell : TourCellTableViewCell = tableView.cellForRow(at: indexPath) as! TourCellTableViewCell
        cell.setSelected(false, animated: true)
        selectedIndexPath = indexPath
        UIApplication.shared.keyWindow?.endEditing(true)
        selectedCity()
    }
    
    func selectedCity()
    {
        let dispatchTime = DispatchTime.now() + 0.10;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            
            if self.searchMode == true && (self.arraySearchResult.count != 0)
            {
                let object = self.arraySearchResult[self.selectedIndexPath.row]
                
                if (object as AnyObject).isKind(of: CityModel.self)
                {
                    let byCityController : TourListByCityViewController  = self.storyboard!.instantiateViewController(withIdentifier: "TourListByCityViewController") as! TourListByCityViewController
                    let City = self.arraySearchResult[self.selectedIndexPath.row] as! CityModel
                    byCityController.currentCity = City
                    self.navigationController?.pushViewController(byCityController, animated: true)
                }
                else
                {
                    let Tour = self.arraySearchResult[self.selectedIndexPath.row] as! TourModel
                    
                    let byCityController : TourDetailViewController  = self.storyboard!.instantiateViewController(withIdentifier: "TourDetailViewController") as! TourDetailViewController
                    byCityController.currentTour = Tour
                    self.navigationController?.pushViewController(byCityController, animated: true)
                }
            }
            else
            {
                let byCityController : TourListByCityViewController  = self.storyboard!.instantiateViewController(withIdentifier: "TourListByCityViewController") as! TourListByCityViewController
                let City : CityModel = self.session.regions[self.selectedIndexPath.row] as! CityModel
                byCityController.currentCity = City
                self.navigationController?.pushViewController(byCityController, animated: true)

            }
            
            if self.searchMode == true
            {
                self.searchTextField?.text = ""
                self.searchMode = false
                self.TableCities.reloadData()
                
                self.selectOption = false
            }
           
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 255
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchMode == true
        {
            return arraySearchResult.count
        }
        
        return session.regions.count
    }

    override func viewWillAppear(_ animated: Bool)
    {
        //displayEffect = 0;
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Tools.hiddenBurgerButton(hidden: false)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let MainMenuApp : MainMenuViewController = appDelegate.MainMenuApp
        MainMenuApp.showTutorial()

    }
    
    override func viewDidAppear(_ animated: Bool)
    {
       
        
        selectOption = false
        RequestSerch = false
        
        //Deshabilita tabla
        session.tours.removeAllObjects()
        
        if onRequest == true
        {
            return
        }
    
        if session.finishDownloadRegions == false
        {
            Tools.showActivityView(inView: self)
            self.loadTours()
        }
        else
        {
            self.TableCities.reloadData()
            self.TableCities.isUserInteractionEnabled = true
            self.onRequest = true
        }
        
    }
    
    func refreshTable()
    {
        self.refreshControl?.endRefreshing()
    }
    
    func onSucces(Result : String, name : ServiceName)
    {
        
        if name == ServiceName.SEARCH
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            
            if success
            {
                arraySearchResult.removeAllObjects()
                
                let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
                
                let regionArray : NSArray = content.object(forKey: "regions") as! NSArray
                let ToursArray : NSArray = content.object(forKey: "tours") as! NSArray
                
                if regionArray.count != 0
                {
                    for integer in 0...(regionArray.count - 1)
                    {
                        let data : NSDictionary = regionArray.object(at: integer) as! NSDictionary
                        
                        let type : String? = data.object(forKey: "type") as? String
                        let id : Int? = data.object(forKey: "id") as? Int
                        let name : String? = data.object(forKey: "name") as? String
                        let description : String? = data.object(forKey: "description") as? String
                        let country_name : String? = data.object(forKey: "country_name") as? String
                        let latitude : String? = data.object(forKey: "latitude") as? String
                        let longitud : String?  = data.object(forKey: "longitud") as? String
                        let image_url : String? = data.object(forKey: "image_url") as? String
                        let attractions : Int? = data.object(forKey: "attractions") as? Int
                        
                        let Tour : CityModel = CityModel.init(id: id,
                                                              name: name,
                                                              image: Data.init(),
                                                              descriptionRegion: description,
                                                              country_name: country_name,
                                                              latitude: latitude,
                                                              longitud: longitud,
                                                              imageURL: image_url,
                                                              attractions: attractions,
                                                              type: type)
                        
                        arraySearchResult.add(Tour)
                        
                    }
                }
                
                if ToursArray.count != 0
                {
                    for integer in 0...(ToursArray.count - 1)
                    {
                        let data : NSDictionary = ToursArray.object(at: integer) as! NSDictionary
                        
                        let id : Int = data.object(forKey: "id") as! Int
                        let duration_in_hours : Int = data.object(forKey: "duration_in_hours") as! Int
                        let stars_average : Int = data.object(forKey: "stars_average") as! Int
                        let rates_count : Int = data.object(forKey: "rates_count") as! Int
                        let base10_stars_average : Int = data.object(forKey: "base10_stars_average") as! Int
                        let from_price : Float? = data.object(forKey: "from_price") as? Float
                        let image_url : String? = data.object(forKey: "image_url") as? String
                        let short_name : String = data.object(forKey: "short_name") as! String
                        let TopTenNumber : Int? = data.object(forKey: "TopTenNumber") as? Int
                        
                        var Prices: [PriceModel] = []
                        let nsArrayPrices : NSArray? = data.object(forKey: "Prices") as? NSArray
                        
                        if nsArrayPrices != nil
                        {
                            for integer in 0...((nsArrayPrices?.count)! - 1)
                            {
                                let price : NSDictionary = nsArrayPrices!.object(at: integer) as! NSDictionary
                                
                                let id: Int = price.object(forKey: "Id") as! Int
                                let discount: Int = price.object(forKey: "Discount") as! Int
                                let value: Float = price.object(forKey: "Value") as! Float
                                let name: String = price.object(forKey: "Name") as! String
                                let description: String = price.object(forKey: "Description") as! String
                                let currency: String = price.object(forKey: "currency") as! String
                                
                                let PriceData: PriceModel = PriceModel.init(id: id, discount: discount, value: value, name: name, priceDescription: description, currency: currency)
                                
                                Prices.append(PriceData)
                            }
                        }
                        
                        let Tourdata : TourModel = TourModel.init(id: id,
                                                                  image: Data.init(),
                                                                  image_url: image_url,
                                                                  duration_in_hours: duration_in_hours,
                                                                  short_name: short_name,
                                                                  stars_average: stars_average,
                                                                  rates_count: rates_count,
                                                                  base10_stars_average: base10_stars_average,
                                                                  from_price: from_price,
                                                                  date: "",type: "",
                                                                  TopTenNumber: TopTenNumber,
                                                                  singlePrice: -1000,
                                                                  prices: Prices)
                        
                        arraySearchResult.add(Tourdata)
                        
                    }
                }
                
                
                    let dispatchTime = DispatchTime.now() + 0.30;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                        self.TableCities.reloadData()
                        self.TableCities.isUserInteractionEnabled = true
                        UIView.animate(withDuration: 0.10, animations: {self.TableCities.alpha = 1})
                        self.onSearchRequest = false
                        self.RequestSerch = false
                    
                        
                        if regionArray.count == 0 && ToursArray.count == 0 {
                            Tools.showNoSearchResult()
                        }
                    
                    }
                }
                else
                {
                    
                    let dispatchTime = DispatchTime.now() + 0.30;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                        self.TableCities.isUserInteractionEnabled = true
                        UIView.animate(withDuration: 0.10, animations: {self.TableCities.alpha = 1})
                        Tools.showNoSearchResult()
                        self.onSearchRequest = false
                        self.RequestSerch = false
                    }
                }
            
            return
        }
        
        if name == ServiceName.LOGIN
        {
            let Service = TouridooServices.init(delegate: self)
            Service.GetMyTours()
            return
        }
        
        let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
        let success : Bool = dataResult.object(forKey: "success") as! Bool
        
        if success
        {
            if name == ServiceName.GET_REGIONS
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    session.regions.removeAllObjects()
                    
                    let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                    
                    for integer in 0...(content.count - 1)
                    {
                        let region : NSDictionary = content.object(at: integer) as! NSDictionary
                        
                        let id : Int? = region.object(forKey: "id") as? Int
                        let name : String? = region.object(forKey: "name") as? String
                        let description : String? = region.object(forKey: "description") as? String
                        let country_name : String? = region.object(forKey: "country_name") as? String
                        let latitude : String? = region.object(forKey: "latitude") as? String
                        let longitud : String?  = region.object(forKey: "longitud") as? String
                        let image_url : String? = region.object(forKey: "image_url") as? String
                        let attractions : Int? = region.object(forKey: "attractions") as? Int
                        let type : String? = region.object(forKey: "type") as? String
        
                        let Tour : CityModel = CityModel.init(id: id,
                                                              name: name,
                                                              image: Data.init(),
                                                              descriptionRegion: description,
                                                              country_name: country_name,
                                                              latitude: latitude,
                                                              longitud: longitud,
                                                              imageURL: image_url,
                                                              attractions: attractions,
                                                              type: type)
                        session.regions.add(Tour)
                    }
                    let dispatchTime = DispatchTime.now() + 0.30;
                    DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                    {
                        self.session.finishDownloadRegions = true
                        self.TableCities.reloadData()
                        self.TableCities.isUserInteractionEnabled = true
                        self.onRequest = true
                        
                        let code : String? = UserDefaults.standard.object(forKey: "bookingcode") as? String
                        
                        if (UserDefaults.standard.bool(forKey: "bookingcode_mode") == true) && (code != nil)
                        {
                            let Service = TouridooServices.init(delegate: self)
                            Service.bookingCode(code: code!, deviceID: "")
                        }
                    }
                
                }
            }
            else if name == ServiceName.GET_MY_TOURS
            {
                
                let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
                let success : Bool = dataResult.object(forKey: "success") as! Bool
                
                if success
                {
                    if (dataResult.object(forKey: "content") != nil)
                    {
                        let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                        loadTourData.updateData(content: content)
                        loadTourData.updateFlags()
                        
                    }
                }
                
                let Service = TouridooServices.init(delegate: self)
                requestTimer()
                Service.GetRegions()
            }
            else if name == ServiceName.BOOKING_CODE
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                    loadTourData.updateData(content: content)
                }
                
            }
        }
        else
        {
            Tools.hiddenActivityView(inView: self)
            
            let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
            let error_message : String = content.object(forKey: "error_message") as! String
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                Tools.showAlertViewinModalViewController(withText: error_message, ModalViewController: self)
            }
        }
    
    }
    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        if name == ServiceName.LOGIN
        {
            UserDefaults.standard.set(nil, forKey: "token")
            UserDefaults.standard.set(nil, forKey: "email")
            UserDefaults.standard.set(nil, forKey: "password")
            
            
            let Service = TouridooServices.init(delegate: self)
            Service.GetMyTours()
            return
        }
        else
        {
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        displayEffect = displayEffect + 1
        UIView.animate(withDuration: 0.80, animations: {
            cell.contentView.alpha = 1.0
        })
       
    }
    
    func loadTours()
    {
        //login
        let Service = TouridooServices.init(delegate: self)
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        requestTimer()
        
        if token == nil
        {
            Service.GetRegions()
        }
        else
        {
            if UserDefaults.standard.object(forKey: "email") == nil {
                Service.GetRegions()
                return
            }
            
            Service.Login(username: UserDefaults.standard.object(forKey: "email") as! String, password: UserDefaults.standard.object(forKey: "password") as! String)
        }
        
    }
    
    func requestTimer()
    {
        if timer == nil
        {
            timer = Timer.scheduledTimer(timeInterval: TimeInterval(1.0), target: self, selector: #selector(countDown), userInfo: nil, repeats: true)
            timer?.fire()
        }
    }
    
    func countDown()
    {
        if timer == nil
        {
            return
        }
        
        seconds = seconds + 1
        
        print("Request time: " + String(describing: seconds) + " Seconds")
        
        if (seconds == 8) && (downloadImageNumber <= 1)
        {
            Tools.showSlowConnectionView()
        }
        
        if (downloadImageNumber >= 1)
        {
            Tools.hiddenActivityView(inView: self)
            Tools.hiddeSlowConnectionView()
            print("FINISH LOADING WITH IMAGES")
            self.timer?.invalidate()
            self.timer = nil
        }
        
        if seconds == 60
        {
            Tools.hiddenActivityView(inView: self)
            Tools.showNotConnectionAvailable()
            Tools.hiddeSlowConnectionView()
            print("Time out request")
            self.timer?.invalidate()
            self.timer = nil
        }
        
    }
    
    //SEARCH BAR METHODS
    
    func search(text : String)
    {
        if selectOption == true
        {
            return
        }
        
        print("Search: " + text)
    
        TableCities.isUserInteractionEnabled = false
        
        if RequestSerch == false
        {
            RequestSerch = true
            
            let dispatchTime = DispatchTime.now() + 0.50;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                if self.searchTextField?.text != ""
                {
                    let services = TouridooServices.init(delegate: self)
                    services.search(searchText: (self.searchTextField?.text)!)
                }
            }
            
        }
    
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxlenght : Int = 30;
        
        let textlenght = textField.text?.characters.count
        
        let newlenght = textlenght! + string.characters.count - range.length
        
        print("Chars: " + String(newlenght))
        
        if (newlenght >= 3)
        {
            if self.RequestSerch == false
            {
                DispatchQueue.main.async
                    {
                        print("ON SEARCH REQUEST")
                        
                        self.onSearchRequest = true
                        
                        if self.cleanText == false
                        {
                            self.searchMode = true
                            let searhText : String = (textField.text)! + string
                            self.search(text: searhText)
                            
                        }
                        else
                        {
                            self.cleanText = false
                        }
                       
                        
                }
            }
            
        }
        else
        {
            if self.searchMode == true
            {
                self.searchMode = false
                self.TableCities.reloadData()
            }
        }
        
        return newlenght <= maxlenght
        
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if self.searchMode == true
        {
            self.searchMode = false
            self.cleanText = true
            
            let dispatchTime = DispatchTime.now() + 0.30;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                self.TableCities.reloadData()
            }
            
        }
        
        return true
    }
    
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
}
