//
//  sessionData.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 21/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class sessionData: NSObject
{
    static let touridooContactMail : String = "contacto@touridoo.com"
    
    // Can't init is singleton
    private override init() { }
    
    // Shared Instance
    static let shared = sessionData()
    
    // User data
    var ToursArray  : NSArray?
    var currentTour : Tour?
    var currentTourImage : Data?
    var User : UserData?
    var driverImage : UIImage?
    var currency : String?
    
    //App Data
    var regions: NSMutableArray = NSMutableArray.init()
    var finishDownloadRegions : Bool = false
    
    var tours: NSMutableArray = NSMutableArray.init()
    var finishDownloadTours : Bool = false
    
    func tourWithBookingCode(bookingCode : String) -> Tour {
        
        var resultTour : Tour?
        
        for index in 0...((ToursArray?.count)! - 1)
        {
            let tourModel : Tour = ToursArray![index] as! Tour
            
            if tourModel.booking?.booking_code == bookingCode
            {
                resultTour = tourModel
            }
        }
        
        return resultTour!
        
    }
    
}
