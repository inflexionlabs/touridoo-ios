//
//  NumberOfTicketsTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 14/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class NumberOfTicketsTableViewCell: UITableViewCell
{

    @IBOutlet weak var priceLabel : UILabel?
    @IBOutlet weak var totalPriceLabel : UILabel?
    @IBOutlet weak var numberLabel : UILabel?
    @IBOutlet weak var TypeLabel : UILabel?
    
    var idPrice : Int?
    
    var numberOfTickets : Int  = 0
    var price : Float = 0.0
    var delegateTableViewController : TicketsSelectorViewController?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func plusAction()
    {
        numberOfTickets += 1
        doYourMagic()
    }
    
    @IBAction func minusAction()
    {
        if numberOfTickets != 0 {
             numberOfTickets -= 1
        }
        
        doYourMagic()
    }

    
    func doYourMagic()
    {
        numberLabel?.text = String(numberOfTickets)
        let totalPrice : Float = price * Float(numberOfTickets)
        totalPriceLabel?.text = Tools.currencyString(number: totalPrice)
        totalPriceLabel?.text = totalPriceLabel?.text?.replacingOccurrences(of: "MX", with: "")
        let session = sessionData.shared
        totalPriceLabel?.text = (totalPriceLabel?.text)! + " " + session.currency!
        totalPriceLabel?.adjustsFontSizeToFitWidth = true
        delegateTableViewController?.updateTotal()
    }
}
