//
//  servicesViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 19/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class servicesViewController: UIViewController {

    @IBOutlet weak var textDescription : UITextView?
    @IBOutlet weak var scrollView: UIScrollView!
    var currentPointOfInterest : PointOfInteresModel?
    
    @IBOutlet weak var wifiLabel : UILabel?
    @IBOutlet weak var bathLabel : UILabel?
    @IBOutlet weak var WatLabel : UILabel?
    @IBOutlet weak var CoffLabel : UILabel?
    @IBOutlet weak var FoodLabel : UILabel?
    @IBOutlet weak var GiftsLabel : UILabel?
    @IBOutlet weak var AccessLabel : UILabel?
    @IBOutlet weak var ATMLabel : UILabel?
    
    @IBOutlet weak var wifiIcon : UIImageView?
    @IBOutlet weak var bathIcon : UIImageView?
    @IBOutlet weak var WatIcon : UIImageView?
    @IBOutlet weak var CoffIcon : UIImageView?
    @IBOutlet weak var FoodIcon : UIImageView?
    @IBOutlet weak var GiftsIcon : UIImageView?
    @IBOutlet weak var AccessIcon : UIImageView?
    @IBOutlet weak var ATMIcon : UIImageView?
    
    var servicesArray : NSArray?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        bathLabel?.text = NSLocalizedString("Bathrooms", comment: "Bathrooms")
        WatLabel?.text = NSLocalizedString("Wardrobe", comment: "Wardrobe")
        CoffLabel?.text = NSLocalizedString("Coffee shop", comment: "Coffee shop")
        FoodLabel?.text = NSLocalizedString("Food", comment: "Food")
        GiftsLabel?.text = NSLocalizedString("Souvenirs Store", comment: "Souvenirs Store")
        AccessLabel?.text = NSLocalizedString("Accessibility", comment: "Accessibility")
        ATMLabel?.text = NSLocalizedString("ATM", comment: "ATM")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (servicesArray != nil)
        {
            if (servicesArray?.count != 0)
            {
                setData()
            }
        }
        
        scrollView.contentSize = CGSize(width: (textDescription?.frame.size.width)!, height: (textDescription?.frame.size.height)! + CGFloat(60))
        textDescription?.text = ""
    }
    
    func setData()
    {
        for index in 0...((servicesArray?.count)! - 1)
        {
            let id : String = servicesArray?.object(at: index) as! String
            setEnable(id: id)
        }
    }
    
    func setEnable(id : String)
    {
        if id == "bathroom"
        {
            bathLabel?.alpha = 1
            bathIcon?.alpha = 1
        }
        
        if id == "coffee"
        {
            CoffLabel?.alpha = 1
            CoffIcon?.alpha = 1
        }
        
        if id == "food"
        {
            FoodLabel?.alpha = 1
            FoodIcon?.alpha = 1
        }
        
        if id == "gifts"
        {
            GiftsLabel?.alpha = 1
            GiftsIcon?.alpha = 1
        }
        
        if id == "wardrobe"
        {
            WatLabel?.alpha = 1
            WatIcon?.alpha = 1
        }
        
        if id == "wifi"
        {
            wifiLabel?.alpha = 1
            wifiIcon?.alpha = 1
        }
        
        if id == "Accessibility"
        {
            AccessLabel?.alpha = 1
            AccessIcon?.alpha = 1
        }
        
        if id == "ATM"
        {
            ATMLabel?.alpha = 1
            ATMIcon?.alpha = 1
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    


}
