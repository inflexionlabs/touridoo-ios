//
//  PriceModel.swift
//  Touridoo
//
//  Created by Mario Alejandro Pérez Colmenares on 10/16/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PriceModel: NSObject
{
    var id : Int
    var discount : Int
    var value : Float
    var name : String
    var priceDescription : String
    var currency : String
    
    init(id : Int,
         discount: Int,
         value : Float,
         name : String,
         priceDescription : String,
         currency : String)
    {
        self.id = id
        self.discount = discount
        self.value = value
        self.name = name
        self.priceDescription = priceDescription
        self.currency = currency

    }
    
}
