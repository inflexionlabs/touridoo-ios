//
//  ReviewModel.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 13/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class ReviewModel: NSObject {

    var userName : String
    var rate : Int
    var date : String
    var opinion : String
    var country : String
    
    init (user : String, rate : Int, date : String, opinion : String ,country : String )
    {
        self.userName = user
        self.rate = rate
        self.date = date
        self.opinion = opinion
        self.country = country
    }
    
}
