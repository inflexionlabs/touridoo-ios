//
//  MyReservationsViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/05/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class MyReservationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var tableReservation : UITableView!
    @IBOutlet weak var bookinButton: UIButton!
    @IBOutlet weak var myBookings: UILabel!
   // @IBOutlet weak var bookinButtonSubView: UIView!

    var arrayTours: NSMutableArray = NSMutableArray.init()
    var arrayToursBooking: NSMutableArray = NSMutableArray.init()
    var selectedIndexPath : IndexPath = IndexPath.init(row: 0, section: 0)
    var onRequest : Bool = false
    var tourImage : Data?
    var hiddeButton : Bool = false
    var tourData : TourModel?
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        super.viewDidLoad()
      
        tableReservation?.delegate = self
        tableReservation?.dataSource = self
        
        self.tableReservation.isUserInteractionEnabled = false
        
        bookinButton.setTitle(NSLocalizedString("I have a booking", comment: "I have a booking"), for: UIControlState.normal)
        myBookings.text = NSLocalizedString("My bookings", comment: "My bookings")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Tools.hiddenBurgerButton(hidden: false)
        
        if hiddeButton == true
        {
            return
        }
        
        let token : String? = UserDefaults.standard.object(forKey: "token") as? String
        let code : String? = UserDefaults.standard.object(forKey: "bookingcode") as? String
        
        if ((UserDefaults.standard.bool(forKey: "bookingcode_mode") == true) && (code != nil)) || (token != nil)
        {
            if hiddeButton == false
            {
                hiddeButton = true
                bookinButton.isHidden = true
                bookinButton.superview?.isHidden = true
                if Tools.iPhoneX() || Tools.iPhone7Plus()
                {
                    Tools.UpView(View: self.tableReservation!, Points: 89)
                    Tools.AddHeight(View: self.tableReservation!, Points: 89)
                }
                else
                {
                    Tools.UpView(View: self.tableReservation!, Points: 75)
                    Tools.AddHeight(View: self.tableReservation!, Points: 75)
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : TourCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Tour_cell") as! TourCellTableViewCell
        
        let Tour : TourModel = arrayTours[indexPath.row] as! TourModel
        let nameString : String = Tour.name
        cell.name?.text = nameString
        
        cell.imageTour?.alpha = 0.5;
        cell.shadowImage?.alpha = 0;
        
        cell.setStars(stars: Tour.stars_average)
        
        if Tour.image?.count != 0
        {
            cell.imageTour?.image = UIImage.init(data: Tour.image!)
            tourImage = Tour.image
            
            UIView.animate(withDuration: 0.30, animations:
                {
                    cell.imageTour?.alpha = 1;
                    cell.shadowImage?.alpha = 1;
            })
        }
        else
        {
            var RealImage : Data?
            let image_url : String? = Tour.image_url
            
            if image_url != nil
            {
                if image_url != ""
                {
                    DispatchQueue.global().async
                        {
                            
                            if image_url != nil
                            {
                                let urlToDownload : URL = URL.init(string: image_url!)!
                                do
                                {
                                    RealImage = try  Data.init(contentsOf: urlToDownload)
                                }
                                catch
                                {
                                    print(error)
                                }
                            }
                            
                            DispatchQueue.main.async
                                {
                                    if RealImage != nil
                                    {
                                        cell.imageTour?.image = UIImage.init(data: RealImage!)
                                        Tour.image = RealImage
                                        self.tourImage = RealImage
                                        
                                        UIView.animate(withDuration: 0.30, animations:
                                            {
                                                cell.imageTour?.alpha = 1;
                                                cell.shadowImage?.alpha = 1;
                                        })
                                    }
                            }
                    }
                }
            }
        }
        
        let hours : Int = Tour.duration_in_hours
        let hoursString : String = String(hours)
        cell.duration?.text = hoursString + " " + NSLocalizedString("hours", comment: "hours")
        
        let stingDate : String = Tour.date!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let dateObj = dateFormatter.date(from: stingDate)
        
        var hourString : String?
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm"
        hourString = dateFormatter.string(from: dateObj!)
        
        cell.dateTourlabel?.text = hourString
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let cell : TourCellTableViewCell = tableView.cellForRow(at: indexPath) as! TourCellTableViewCell
        cell.setSelected(false, animated: true)
        selectedIndexPath = indexPath
        
        selectedCity()
    }
    
    func selectedCity()
    {
        let byCityController : myreservationDetailViewController  = self.storyboard!.instantiateViewController(withIdentifier: "myreservationDetailViewController") as! myreservationDetailViewController
        
        let session = sessionData.shared
      
        if session.ToursArray != nil
        {
            if session.ToursArray?.count != 0 {
                
                let dispatchTime = DispatchTime.now() + 0.1;
                DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                {
                    self.tourData = self.arrayTours[self.selectedIndexPath.row] as? TourModel
                    var Tour : Tour = self.arrayToursBooking.object(at: self.selectedIndexPath.row) as! Tour
                    self.tourImage = self.tourData?.image
                    
                    if UserDefaults.standard.bool(forKey: "showReservationView") == true
                    {
                        let lastBooking : String = UserDefaults.standard.object(forKey: "lastBookingCodeReservation") as! String
                        if lastBooking != "" {
                            Tour = session.tourWithBookingCode(bookingCode: lastBooking)
                            UserDefaults.standard.set("", forKey: "lastBookingCodeReservation")
                        }
                    }
                    
                    byCityController.currentTour = Tour
                    byCityController.image = self.tourImage
                    self.navigationController?.pushViewController(byCityController, animated: true)
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 255
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTours.count
    }
    
    @IBAction func haveBookinAction(_ sender: Any) {
        let pass : IHaveBookinViewController = self.storyboard?.instantiateViewController(withIdentifier: "IHaveBookinViewController") as! IHaveBookinViewController
        self.navigationController?.pushViewController(pass, animated: true)
    }
    override func viewDidAppear(_ animated: Bool)
    {
        if UserDefaults.standard.bool(forKey: "needs_update") == true
        {
            UserDefaults.standard.set(false, forKey: "needs_update");
            onRequest = false
            
        }
        
        if onRequest == true
        {
            return
        }
        
        let session = sessionData.shared
        
        if session.ToursArray != nil
        {
            if session.ToursArray?.count != 0
            {
                arrayTours.removeAllObjects()
                
                for i in 0...((session.ToursArray?.count)! - 1)
                {
                    let currenTour : Tour = session.ToursArray?.object(at: i) as! Tour
                    
                    var tourHasEnded : Bool = false
                    
                    let lastIndex = (currenTour.tourRun?.tour_steps?.count)! - 1
                    let finishTourIndex = (currenTour.tourRun?.tour_steps?.count)! - 2
                    let comingbackIndex = (currenTour.tourRun?.tour_steps?.count)! - 3
                    
                    let stepLast : TourStepModel = currenTour.tourRun?.tour_steps!.object(at: lastIndex) as! TourStepModel
                    let stepfinish : TourStepModel = currenTour.tourRun?.tour_steps!.object(at: finishTourIndex) as! TourStepModel
                    let stepcomingback: TourStepModel = currenTour.tourRun?.tour_steps!.object(at: comingbackIndex) as! TourStepModel
                    
                    if (stepLast.is_current_step == true) || (stepfinish.is_current_step == true) || (stepcomingback.is_current_step == true)
                    {
                        tourHasEnded = true
                    }
                    
                    if tourHasEnded == false
                    {
                        arrayToursBooking.add(currenTour)
                        
                        let Tourdata : TourModel = TourModel.init(id: currenTour.id!,
                                                                  image: Data.init(),
                                                                  image_url: currenTour.image_url!,
                                                                  duration_in_hours: currenTour.duration_in_hours!,
                                                                  short_name: currenTour.name!,
                                                                  stars_average: currenTour.stars_average!,
                                                                  rates_count: currenTour.stars_average!,
                                                                  base10_stars_average: currenTour.base10_stars_average!,
                                                                  from_price: 0,
                                                                  date: currenTour.tourRun?.tour_date,
                                                                  type: "tour",
                                                                  TopTenNumber: 1,
                                                                  singlePrice: -1000,
                                                                  prices: currenTour.prices)
                        
                        arrayTours.add(Tourdata)
                    }
                }
            }
        }
        
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            self.tableReservation.reloadData()
            self.tableReservation.isUserInteractionEnabled = true
            self.onRequest = true
            
            if UserDefaults.standard.bool(forKey: "showReservationView") == true
            {
                self.selectedCity()
            }
        }
    
    }

}
