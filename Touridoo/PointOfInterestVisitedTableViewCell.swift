//
//  PointOfInterestVisitedTableViewCell.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 29/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PointOfInterestVisitedTableViewCell: UITableViewCell
{

    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var visited : UILabel?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        visited?.layer.cornerRadius = (visited?.layer.frame.size.height)! / CGFloat(2)
        visited?.layer.borderColor = Tools.colorAppSecondary().cgColor
        visited?.layer.borderWidth = 1.0
        visited?.textColor = Tools.colorAppSecondary()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
