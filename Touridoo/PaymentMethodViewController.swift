//
//  PaymentMethodViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 06/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController, ResponseServicesProtocol, PayPalPaymentDelegate
{

    @IBOutlet weak var titleText : UILabel?
    @IBOutlet weak var NextButton : UIButton?
    @IBOutlet weak var scrollData  : UIScrollView?
    @IBOutlet weak var currencyMXNView  : UIView?
    @IBOutlet weak var currencyDLLSView  : UIView?
    
    @IBOutlet weak var creditCardView  : UIView?
    @IBOutlet weak var paypalView  : UIView?
    
    @IBOutlet weak var radioCreditCard  : UIButton?
    @IBOutlet weak var radioPayPal  : UIButton?
    
    @IBOutlet weak var tourName : UILabel?
    @IBOutlet weak var tourDate : UILabel?
    
    @IBOutlet weak var totaltoPay : UILabel?
    @IBOutlet weak var hourLabel : UILabel?
    
    @IBOutlet weak var ticketDescriptionAdult : UILabel?
    
    @IBOutlet weak var activity : UILabel?
    @IBOutlet weak var LanguageTile : UILabel?
    @IBOutlet weak var Language : UILabel?
    @IBOutlet weak var When : UILabel?
    @IBOutlet weak var Duration : UILabel?
    @IBOutlet weak var People : UILabel?
    @IBOutlet weak var TotalPrice : UILabel?
    @IBOutlet weak var ChooseCurrency : UILabel?
    @IBOutlet weak var ChoosePayment: UILabel?
    @IBOutlet weak var CreditCard : UILabel?
    @IBOutlet weak var theTypes : UILabel?
    @IBOutlet weak var weSendEmail : UILabel?
    
    @IBOutlet weak var ModeTile : UILabel?
    @IBOutlet weak var Mode : UILabel?

    var currentTour : TourDescriptionModel?
    var currency : String = ""
    var paymentMethod : String = ""
    var AmountToPay : Float = 0.0
    var DLLSCurrentPrice : Float = 19.04
    var setupScrollSize : Bool = false
    var newAccountData : NewAccountModel?
    var IdPayment : Int = 0
    var hasPreviousSession : Bool = false
    var currencySetup : Bool = false
    var paymentCode : String = ""
    var PayPalConfig = PayPalConfiguration();
    
    
    //SANDBOX
    /*var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }*/
    
    //PROD
    var environment:String = PayPalEnvironmentProduction {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    var acceptCreditCards: Bool = true {
        didSet {
            PayPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    
    
    override func viewDidLoad()
    {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
        scrollData?.backgroundColor = UIColor.clear
        
        PayPalConfig.acceptCreditCards = acceptCreditCards;
        PayPalConfig.merchantName = "Touridoo"
        PayPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.touridoo.com") as URL?
        PayPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.touridoo.com") as URL?
        PayPalConfig.languageOrLocale = NSLocale.preferredLanguages[0] as! String
        PayPalConfig.payPalShippingAddressOption = .payPal;
        
        PayPalMobile.preconnect(withEnvironment: environment)
        
        activity?.text = NSLocalizedString("Activity", comment: "Activity")
        LanguageTile?.text = NSLocalizedString("Language", comment: "Language")
        Language?.text = NSLocalizedString("Spanish / English", comment: "Spanish / English")
        When?.text = NSLocalizedString("When", comment: "When")
        Duration?.text = NSLocalizedString("Duration", comment: "Duration")
        People?.text = NSLocalizedString("Number of tickets: ", comment: "Number of tickets: ")
        TotalPrice?.text = NSLocalizedString("Total Price", comment: "Total Price")
        ChooseCurrency?.text = NSLocalizedString("Choose a currency", comment: "Choose a currency")
        ChoosePayment?.text = NSLocalizedString("Choose a payment method", comment: "Choose a payment method")
        CreditCard?.text = NSLocalizedString("Credit or debit card", comment: "Credit or debit card")
        theTypes?.text = NSLocalizedString("The types of currencies have been activated on Jul 18, 2017", comment: "The types of currencies have been activated on Jul 18, 2017")
        weSendEmail?.text = NSLocalizedString("We will send you an email with everything you need to enjoy the activity", comment: "We will send you an email with everything you need to enjoy the activity")
        
        ModeTile?.text = NSLocalizedString("Mode", comment: "Mode")
        Mode?.text = NSLocalizedString("Bilingual tour", comment: "Bilingual tour")
        
        CreditCard?.adjustsFontSizeToFitWidth = true
        
        tourName?.text = currentTour?.name
        let dateString : String = (newAccountData?.bookingData?.tourDate!)!
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let dateObj = dateFormatter.date(from: dateString)
        
        var hourString : String?
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy HH:mm"
        hourString = dateFormatter.string(from: dateObj!)
        
        tourDate?.text = hourString
        
        if newAccountData?.bookingData?.totalToPay != nil
        {
            
            let total : String = (newAccountData?.bookingData?.totalToPay)!
            let price : Float = Float(total)!
            AmountToPay = price
         
            let NewAmount : Float = AmountToPay / DLLSCurrentPrice
            let priceNumber : NSNumber = NSNumber(value: NewAmount)
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale.current
            var priceWithFormatter : String =  formatter.string(from: priceNumber)!
            priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "MX", with: "")
            
            self.totaltoPay?.text = priceWithFormatter + " USD"
            
        }
        
        if currentTour?.duration_in_hours != nil
        {
            let hours : Int = (currentTour?.duration_in_hours)!
            let hoursString : String = String(hours)
            self.hourLabel?.text = hoursString + " " + NSLocalizedString("hours", comment: "hours")
        }
        
        if currentTour?.prices != nil
        {
            if currentTour?.prices?.count != 0
            {
                for integer in 0...((currentTour?.prices?.count)! - 1)
                {
                    let priceData : PriceModel =  (currentTour?.prices![integer])!
                    let price: Float = priceData.value
                    //let priceString : String = String(price)
                    
                    let priceNumber : NSNumber = NSNumber(value: price)
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .currency
                    formatter.locale = Locale.current
                    let priceWithFormatter : String =  formatter.string(from: priceNumber)!
                    let numberOfTickets: Int = (newAccountData?.bookingData?.tickets?.count)!
                   
                    let number : String = String(numberOfTickets)
                    ticketDescriptionAdult?.text = number
                    /*
                    let result : Float = Float(numberOfTickets) * price
                    let resultNumber : NSNumber = NSNumber(value: result)
                    let resultString : String =  formatter.string(from: resultNumber)!
                    
                    if priceData.name == "Adultos"
                    {
                        let totalString : String =  number + " " + priceData.name + " x " + priceWithFormatter + " = " + resultString
                        
                    }*/
                   
                }
            }
        }
       

    }
    
    func MXNConvertion()
    {
        let priceNumber : NSNumber = NSNumber(value: AmountToPay)
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        var priceWithFormatter : String =  formatter.string(from: priceNumber)!
        priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "MX", with: "")
        self.totaltoPay?.text = priceWithFormatter + " MXN"
    }
    
    func DLLSConvertion()
    {
        let NewAmount : Float = AmountToPay / DLLSCurrentPrice
        let priceNumber : NSNumber = NSNumber(value: NewAmount)
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        var priceWithFormatter : String =  formatter.string(from: priceNumber)!
        priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "MX", with: "")
        
        self.totaltoPay?.text = priceWithFormatter + " USD"
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
        titleText?.text = NSLocalizedString("Payment method", comment: "Payment method")
        NextButton?.setTitle(NSLocalizedString("Confirm your booking", comment:"Confirm your booking"), for: UIControlState.normal)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        
        if setupScrollSize == false
        {
            setupScrollSize = true
            var content : CGSize
            if Tools.iPhone4()
            {
                 content = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(70)))
            }
            else
            {
                 content = CGSize(width: (scrollData?.frame.size.width)!, height: ((scrollData?.frame.size.height)! + CGFloat(1)))
            }
            
            
            let newHeight : CGFloat = self.view.frame.size.height - CGFloat(66)
            
            let scrollFrameInStoryboar : CGRect = CGRect(x: (scrollData?.frame.origin.x)!,
                                                         y: (scrollData?.frame.origin.y)!,
                                                         width: (scrollData?.frame.size.width)!,
                                                         height: newHeight)
            
            if content.height <= scrollFrameInStoryboar.size.height {
                content.height = scrollFrameInStoryboar.size.height + 1
            }
            
            scrollData?.frame = scrollFrameInStoryboar
            scrollData?.contentSize = content
            
        }
        
       // let tapCreditCard = UITapGestureRecognizer.init(target: self, action: #selector(selectCreditCard))
       // creditCardView?.addGestureRecognizer(tapCreditCard)
        
        let tapPayPal = UITapGestureRecognizer.init(target: self, action: #selector(selectPayPal))
        paypalView?.addGestureRecognizer(tapPayPal)
        
        let tapMXN = UITapGestureRecognizer.init(target: self, action: #selector(selectMXN))
        currencyMXNView?.addGestureRecognizer(tapMXN)
        
        let tapDLLS = UITapGestureRecognizer.init(target: self, action: #selector(selectDLLS))
        currencyDLLSView?.addGestureRecognizer(tapDLLS)
        
        if currencySetup == false
        {
            currencySetup = true
            
            selectMXN()
            selectPayPal()
        }
    }
    
    @IBAction func selectMXN()
    {
        currencyMXNView?.layer.cornerRadius = 5
        currencyMXNView?.layer.borderColor = Tools.colorApp().cgColor
        currencyMXNView?.layer.borderWidth = 1;
        
        currencyDLLSView?.layer.cornerRadius = 5
        currencyDLLSView?.layer.borderColor = UIColor.clear.cgColor
        currencyDLLSView?.layer.borderWidth = 1;
        
        Tools.doRebound(viewEffect: currencyMXNView!)
        
        currency = "MXN"
        
        MXNConvertion()
        
    }
    
    @IBAction func selectDLLS()
    {
        currencyMXNView?.layer.cornerRadius = 5
        currencyMXNView?.layer.borderColor = UIColor.clear.cgColor
        currencyMXNView?.layer.borderWidth = 1;
        
        currencyDLLSView?.layer.cornerRadius = 5
        currencyDLLSView?.layer.borderColor = Tools.colorApp().cgColor
        currencyDLLSView?.layer.borderWidth = 1;
        
        Tools.doRebound(viewEffect: currencyDLLSView!)
        
        currency = "USD"
        
        DLLSConvertion()
    }
    
    @IBAction func selectCreditCard()
    {
        paypalView?.layer.cornerRadius = 5
        paypalView?.layer.borderColor = UIColor.clear.cgColor
        paypalView?.layer.borderWidth = 1;
        
        creditCardView?.layer.cornerRadius = 5
        creditCardView?.layer.borderColor = Tools.colorApp().cgColor
        creditCardView?.layer.borderWidth = 1;
        
        radioCreditCard?.isSelected = true
        radioPayPal?.isSelected = false
        
        Tools.doRebound(viewEffect: creditCardView!)
        
        paymentMethod = "creditCard"
    }
    
    @IBAction func selectPayPal()
    {
        paypalView?.layer.cornerRadius = 5
        paypalView?.layer.borderColor = Tools.colorApp().cgColor
        paypalView?.layer.borderWidth = 1;
        
        creditCardView?.layer.cornerRadius = 5
        creditCardView?.layer.borderColor = UIColor.clear.cgColor
        creditCardView?.layer.borderWidth = 1;
        
        radioCreditCard?.isSelected = false
        radioPayPal?.isSelected = true
        
        Tools.doRebound(viewEffect: paypalView!)
        
        paymentMethod = "payPal"
    }
    

    @IBAction func NextAction()
    {
        if currency == "" {
            Tools.showAlertView(withText: NSLocalizedString("Please choose a currency", comment: "Please choose a currency"))
            return
        }
        
        if paymentMethod == "" {
            Tools.showAlertView(withText: NSLocalizedString("Please select a payment method", comment: "Please select a payment method"))
            return
        }
        
        payPressed()
    
    }
    
    func PaymentDone()
    {
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showActivityView(inView: self)
            
            let services = TouridooServices.init(delegate: self)
            
            let token : String? = UserDefaults.standard.object(forKey: "token") as? String
            if token != nil
            {
                self.hasPreviousSession = true
                
                let UserName : String = UserDefaults.standard.object(forKey: "email") as! String
                let Password : String = UserDefaults.standard.object(forKey: "password") as! String
                
                let services = TouridooServices.init(delegate: self)
                services.Login(username: UserName, password: Password)
                
                //UserDefaults.standard.set(UserName, forKey: "email")
                //UserDefaults.standard.set(Password, forKey: "password")
            }
            else
            {
                self.hasPreviousSession = false
                
                UserDefaults.standard.set(self.paymentCode, forKey: "provisional_password")
                
                services.createNewUser(name: (self.newAccountData?.name)!,
                                       lastName: (self.newAccountData?.last_name)!,
                                       email: (self.newAccountData?.email)!,
                                       phonePrefix: (self.newAccountData?.phone_prefix)!,
                                       PhoneNumber: (self.newAccountData?.phone_number)!,
                                       rendezvous_point: (self.newAccountData?.rendezvous_point_name)!,
                                       rendezvous_address: (self.newAccountData?.rendezvous_point_address)!,
                                       hotelReservationName: (self.newAccountData?.hotel_reservation_owner_name)!,
                                       comments: (self.newAccountData?.comments)!,
                                       policy_accepted: (self.newAccountData?.policy_accepted)!,
                                       recived_info: (self.newAccountData?.receive_info)!,
                                       password: self.paymentCode)
            }
            
            
        }
    }
    
    @IBAction func closeView()
    {
        self.navigationController?.popViewController(animated: true)
    }

    //CREATE USER && BOOKING
    
    func onSucces(Result : String, name : ServiceName)
    {
        if name == ServiceName.CREATE_USER
        {
            let UserName : String = (newAccountData?.email!)!
            let Password : String  = self.paymentCode
            let services = TouridooServices.init(delegate: self)
            services.Login(username: UserName, password: Password)
            
            UserDefaults.standard.set(UserName, forKey: "email")
            UserDefaults.standard.set(Password, forKey: "password")
            
            return
        }
        
        if name == ServiceName.LOGIN
        {
            var totalToPayPal : String = (newAccountData?.bookingData?.totalToPay)!
            
            if currency == "USD"
            {
                let price : Float = Float(totalToPayPal)!
                let NewAmount : Float = price
                totalToPayPal = String(format: "%.2f",NewAmount)
            }
            
            let pay : Float = Float(totalToPayPal)!
            
            let services = TouridooServices.init(delegate: self)
            
            services.bookingTour(tourDate: (newAccountData?.bookingData?.tourDate)!,
                                 idTour: (newAccountData?.bookingData?.idTour)!,
                                 idPayment: IdPayment,
                                 payment: self.paymentCode,
                                 total: pay,
                                 playerID: "",
                                 paymentToken: "",
                                 idMeetingPoint: (newAccountData?.bookingData?.meetingPointId)!,
                                 tickets: (newAccountData?.bookingData?.tickets)!)
            return
        }
        
        if name == ServiceName.BOOKING_TOUR
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            if success
            {
                let content : NSDictionary = dataResult.object(forKey: "content") as! NSDictionary
                let booking_code : String = content.object(forKey: "booking_code") as! String
                UserDefaults.standard.set(booking_code, forKey: "lastBookingCodeReservation")
                let Service = TouridooServices.init(delegate: self)
                Service.GetMyTours()
            }
            
        }
        
        if name == ServiceName.GET_MY_TOURS
        {
            let dataResult : NSDictionary = Tools.JSONDataToDiccionary(text: Result)! as NSDictionary
            let success : Bool = dataResult.object(forKey: "success") as! Bool
            
            if success
            {
                if (dataResult.object(forKey: "content") != nil)
                {
                    let content : NSArray = dataResult.object(forKey: "content") as! NSArray
                    loadTourData.updateData(content: content)
                    
                    if content.count > 2
                    {
                        UserDefaults.standard.set(true, forKey: "noRateApp");
                    }
                
                    let session = sessionData.shared
                    
                    let lastBooking : String = UserDefaults.standard.object(forKey: "lastBookingCodeReservation") as! String
                    if lastBooking != ""
                    {
                        let lastTour : Tour? = session.tourWithBookingCode(bookingCode: lastBooking)
                        let dispatchTime = DispatchTime.now() + 0.10;
                        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
                        {
                            Tools.hiddenActivityView(inView: self)
                            
                            let data : Data? = UserDefaults.standard.data(forKey: "last_tour_image")
                            let imageTour = UIImage.init(data: data!)
                            Tools.showTicketForTour(image: imageTour!, tourData: lastTour!, destinationController: self,paymentView: self)
                        }
                    }
                    
                }
            }
            
        }
    }
    
    func lastView()
    {
        let dispatchTime = DispatchTime.now() + 0.10;
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            if self.hasPreviousSession == false
            {
                let createAccountView : TouridooCreateNewAccountViewController  = self.storyboard!.instantiateViewController(withIdentifier: "TouridooCreateNewAccountViewController") as! TouridooCreateNewAccountViewController
                createAccountView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                
                createAccountView.currentTour = self.currentTour
                createAccountView.newAccountData = self.newAccountData
                
                self.present(createAccountView, animated: true, completion: nil)
            }
            else
            {
                UserDefaults.standard.set(true, forKey: "showReservationView");
                
                if UserDefaults.standard.bool(forKey: "noRateApp") == false
                {
                    let rateAppView : RateAppViewController  = self.storyboard!.instantiateViewController(withIdentifier: "RateAppViewController") as! RateAppViewController
                    rateAppView.currentTour = self.currentTour
                    rateAppView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    self.present(rateAppView, animated: true, completion: nil)
                }
                else
                {
                    //Open MenuBarApp
                    let storyboard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                    let mainMenu : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
                    mainMenu.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    self.present(mainMenu, animated: true, completion: nil)
                }
                
            }
            
            
        }
    }
    
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController)
    {
        print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            let dicData : NSDictionary = completedPayment.confirmation as NSDictionary
            let response : NSDictionary = dicData.object(forKey: "response") as! NSDictionary
            self.paymentCode = response.object(forKey: "id") as! String;
            self.PaymentDone()
        })
    }
    
    @IBAction func payPressed()
    {
        var totalToPayPal : String = (newAccountData?.bookingData?.totalToPay)!
        
        if currency == "USD"
        {
            let price : Float = Float(totalToPayPal)!
            //let NewAmount : Float = price / DLLSCurrentPrice
            totalToPayPal = String(format: "%.2f",price)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dateObj : Date = dateFormatter.date(from: (self.newAccountData?.bookingData?.tourDate)!)!
        dateFormatter.dateFormat = "dd-MM-yy-HH:mm"
        let dateFromString = dateFormatter.string(from: dateObj)
        
        let IDTour : Int = (self.currentTour?.id)!
        let TicketsN : Int = (self.newAccountData?.bookingData?.tickets?.count)!
        
        let SKUID = "SKU ID" + String(IDTour) + " T" + String(TicketsN) + " D" + dateFromString
        
        let item1 = PayPalItem(name: (self.currentTour?.name)!,
                               withQuantity: 1,
                               withPrice: NSDecimalNumber(string: totalToPayPal),
                               withCurrency: "MXN",
                               withSku: SKUID)
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "MXN", shortDescription: (self.currentTour?.name)!, intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        print(payment.description)
        
        if (payment.processable) {
            
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: PayPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            
            print("Payment not processalbe: \(payment)")
        }
        
    }

    
    func onError(Error : String, name : ServiceName)
    {
        Tools.hiddenActivityView(inView: self)
        
        if name == ServiceName.GET_MY_TOURS
        {
            let dispatchTime = DispatchTime.now() + 0.50;
            DispatchQueue.main.asyncAfter(deadline: dispatchTime)
            {
                let ReserveController : TouridooCreateNewAccountViewController  = self.storyboard!.instantiateViewController(withIdentifier: "TouridooCreateNewAccountViewController") as! TouridooCreateNewAccountViewController
                ReserveController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                
                ReserveController.currentTour = self.currentTour
                ReserveController.newAccountData = self.newAccountData
                
                self.present(ReserveController, animated: true, completion: nil)
            }
            
            return
        }
        
        let dispatchTime = DispatchTime.now() + 0.30;
        DispatchQueue.main.asyncAfter(deadline: dispatchTime)
        {
            Tools.showAlertForBackendError(ModalViewController: self, errorDescription: Error)
        }
    }
    


}
