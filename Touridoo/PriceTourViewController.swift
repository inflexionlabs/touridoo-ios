//
//  PriceTourViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 02/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class PriceTourViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var TableCities : UITableView!
    var arrayPrice: NSMutableArray = NSMutableArray.init()
    var tourDescription : TourDescriptionModel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        TableCities?.delegate = self
        TableCities?.dataSource = self
        
    }
    
    func updateData() // Este va a cambiar
    {
        var auxCurrency = ""
        
        if tourDescription?.prices != nil
        {
            if tourDescription?.prices?.count != 0
            {
                for integer in 0...((tourDescription?.prices?.count)! - 1)
                {
                    let priceData : PriceModel =  (tourDescription?.prices![integer])!
                    let price: Float = priceData.value
                    let priceNumber : NSNumber = NSNumber(value: price)
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .currency
                    formatter.locale = Locale.current
                    var priceWithFormatter : String =  formatter.string(from: priceNumber)!
                    priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "$", with: "")
                    priceWithFormatter = priceWithFormatter.replacingOccurrences(of: "MX", with: "")
                    
                    let dicData = NSMutableDictionary()
                    dicData.setObject(priceWithFormatter, forKey: "price" as NSCopying)
                    dicData.setObject(priceData.discount, forKey: "discount" as NSCopying)
                    dicData.setObject(priceData.name, forKey: "type" as NSCopying)
                    dicData.setObject(priceData.priceDescription, forKey: "description" as NSCopying)
                    dicData.setObject(priceData.currency, forKey: "currency" as NSCopying)
                    auxCurrency = priceData.currency
                    arrayPrice.add(dicData)
                }
            }
        }
        
        let session = sessionData.shared
        session.currency = auxCurrency.uppercased()
        
        TableCities.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell : PriceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "price_cell") as! PriceTableViewCell
        
        let dicData : NSMutableDictionary = arrayPrice.object(at: indexPath.row) as! NSMutableDictionary
        var priceString : String = dicData.object(forKey: "price") as! String
        let typeString : String = dicData.object(forKey: "type") as! String
        let descriptionString : String = dicData.object(forKey: "description") as! String
        let currencyString : String = dicData.object(forKey: "currency") as! String
        let discountInt: Int = dicData.object(forKey: "discount") as! Int
        
        cell.priceLabel?.text = "$ " + priceString
        cell.priceLabel?.adjustsFontSizeToFitWidth = true
        cell.typeLabel?.text = typeString
        cell.typeLabel?.adjustsFontSizeToFitWidth = true
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale.current
        
        priceString = priceString.replacingOccurrences(of: ",", with: "")
        let price : Float = Float(priceString)!
        let discount : Float = Float(1) + (Float(discountInt) / Float(100))
        let offerFloat : Float = price * discount
        let offerNumber : NSNumber = NSNumber(value: offerFloat)
        var offetWithFormatter : String =  formatter.string(from: offerNumber)!
        offetWithFormatter = offetWithFormatter.replacingOccurrences(of: "$", with: "")
        offetWithFormatter = offetWithFormatter.replacingOccurrences(of: "MX", with: "")
        
        let offerPrice = "$ " + offetWithFormatter
        
        let offertText = NSMutableAttributedString(string: offerPrice)
        offertText.addAttribute(NSStrikethroughStyleAttributeName, value: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue), range: NSMakeRange(0, offertText.length))
        offertText.addAttribute(NSStrikethroughColorAttributeName, value: UIColor.red, range: NSMakeRange(0, offertText.length))
        offertText.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: NSRange(location:0,length:offertText.length))
        
        cell.offertLabel?.attributedText = offertText
        cell.offertLabel?.adjustsFontSizeToFitWidth = true
        
        cell.descriptionLabel.text = descriptionString
        cell.descriptionLabel.adjustsFontSizeToFitWidth = true
        
        cell.currency.text = currencyString.uppercased()
        cell.descriptionLabel.adjustsFontSizeToFitWidth = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPrice.count
    }
}
