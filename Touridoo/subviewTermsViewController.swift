//
//  subviewTermsViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 31/07/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class subviewTermsViewController: UIViewController
{
    @IBOutlet weak var textView : UITextView?
    var textTerms : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        textView?.setContentOffset(.zero, animated: false)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if textView != nil
        {
            if textTerms != ""
            {
                textView?.text = textTerms
            }
        }
            
    }


}
