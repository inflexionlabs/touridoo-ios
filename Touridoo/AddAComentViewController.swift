//
//  AddAComentViewController.swift
//  Touridoo
//
//  Created by Eduardo Espinoza Kramsky on 19/06/17.
//  Copyright © 2017 Inflexion Software. All rights reserved.
//

import UIKit

class AddAComentViewController: UIViewController, UITextViewDelegate
{
    @IBOutlet weak var commentsTextField : UITextView?
    @IBOutlet weak var sendButton : UIButton?
    @IBOutlet weak var titleLable : UILabel?
    
     var parentController : OnTourFinishedViewController?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        sendButton?.setTitle(NSLocalizedString("Send", comment:"Send"), for: UIControlState.normal)
        
        commentsTextField?.delegate = self;
        
        commentsTextField?.layer.borderWidth = 0.25;
        commentsTextField?.layer.borderColor = UIColor.lightGray.cgColor
        commentsTextField?.layer.cornerRadius = 3;
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
        commentsTextField?.becomeFirstResponder()
        
        let session = sessionData.shared
        
        if session.currentTour?.name != nil
        {
            titleLable?.text = session.currentTour?.name
            titleLable?.adjustsFontSizeToFitWidth = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func closeView()
    {
        commentsTextField?.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent;
    }
    
    @IBAction func sendData()
    {
        if parentController != nil
        {
            parentController?.comment = (commentsTextField?.text)!
        }
        
        closeView()
    }

}
